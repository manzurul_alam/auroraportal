<?php 
session_start();

if(!empty($_SESSION['user_id'])){
//$_SESSION['tracking_number'] = "";
include ('function.php');
dbConnect();
//$message = 0;

if(!is_dir('../lecture/class_lecture')){
	mkdir('../lecture/class_lecture', 0777, true);
	mkdir('../lecture/class_lecture/toddler', 0777, true);
	mkdir('../lecture/class_lecture/preschool', 0777, true);
	mkdir('../lecture/class_lecture/kinder', 0777, true);
	mkdir('../lecture/class_lecture/elschool', 0777, true);
	chmod('../lecture/class_lecture/', 0777);
}

if(isset($_REQUEST["removeDoc"])){
	$query = mysqli_query(dbConnect(),"DELETE FROM ARRA_lecture WHERE id = '".$_REQUEST["lid"]."'");
	if($query){
		unlink("../lecture/class_lecture/".$_REQUEST["dir"]."/".$_REQUEST["removeDoc"]);
		$message = "Successfully Removed !!!";
	}
}
elseif(isset($_POST['save'])){
	if($_FILES["uploadFile"]["size"] < 1048576){
		if(isset($_POST["applicable_for"]) AND isset($_POST["subject"]) AND isset($_POST["lecture"])){
		
			$path ="../lecture/class_lecture/".$_POST["applicable_for"]."/" ; // Upload directory
			
			$uploaded_lecture =$_FILES["uploadFile"]["tmp_name"];
			$uploaded_lecture_path = $_FILES["uploadFile"]["name"];
			$uploaded_lecture_ext = pathinfo($uploaded_lecture_path, PATHINFO_EXTENSION);
			$uploaded_lecture_new =($_POST["subject"]."_".time().'.'.$uploaded_lecture_ext);
			
			//exec('ffmpeg -i '.$uploaded_lecture_path.' -f flv -s 320x240 '.'output-'.$uploaded_lecture_path.'');
			
			if(is_uploaded_file($uploaded_lecture)){
				if(move_uploaded_file($uploaded_lecture,$path.$uploaded_lecture_new)){
					
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_lecture VALUES ( NULL ,  '".$_POST["applicable_for"]."',  '".$_POST["subject"]."',  '".$_POST["lecture"]."', '".$uploaded_lecture_new."', 'class')") or die(mysql_error());
					if($query)
						$message = "Successfully Uploaded...";
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Mandatory field missing !!!";
		}
    }
	else{
		$message = "Keep the file size under 1MB!!!";
	}
 }
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            
            <div class="row">
				<div class="col-lg-12">
                   <h1 class="page-header">Class Lecture</h1>
                </div>
                <div class="col-lg-12">
					<?php if($_SESSION['access'] != "User") { ?>
						<?php 
						if(isset($message)){
							echo '<div class="row voffset2" id="messageDiv">
									<div class="col-md-6 col-md-offset-4">
										<div class="alert alert-success alert-dismissable">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>'.$message.'
										</div>
									</div>
								</div>';
							 //echo $message;
							unset($message);
						}
						?>
					<div class="panel panel-success">
						<div class="panel-heading">
							Upload Lecture
						</div>
						<div class="panel-body">
							<form role="form1" method="POST" action="#" enctype='multipart/form-data'>
								<fieldset>
									<div class="col-lg-12 form-group">
										<div class="col-md-4">
											<label>Applicable for</label>
										</div>
										<div class="col-md-6">
											<select name="applicable_for" class="form-control" required>
												<option value="">Select...</option>
												<option value="toddler">Toddler ( Age 2+ )</option>
												<option value="preschool">Pre School ( Age 3+ )</option>
												<option value="elschool">Pre Kindergarten ( Age 4+ )</option>
												<option value="kinder">Kindergarten ( Age 5+ )</option>
											</select>
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<div class="col-md-4">
											<label>Subject Name</label>
										</div>
										<div class="col-md-6">
											<input type="text" name="subject" class="form-control" required />
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<div class="col-md-4">
											<label>Lecture Name</label>
										</div>
										<div class="col-md-6">
											<input type="text" name="lecture" class="form-control" required />
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<div class="col-md-4">
											<label>Select File</label>
										</div>
										<div class="col-md-6">
											<input type="file" name="uploadFile" class="form-control" required />
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<div class="col-md-4">
											&nbsp;
										</div>
										<div class="col-md-6">
											<button type="submit" name="save" id="save" class="btn btn-outline btn-success"><i class="fa fa-upload fa-1x"></i> UPLOAD NOW</button>
										</div>
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
					<?php } ?>
					<div class="panel panel-success">
						<div class="panel-heading">
							List of Lecture
						</div>
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataTables-payment">
									<thead>
										<tr class="text-center">
											<th width=70px>SL</th>
											<th>Lecture Name</th>
											<th>Subject Name</th>
											<th>Applicable</th>
											<th>Thumbnail</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php $i=1;
										$query = mysqli_query(dbConnect(),"SELECT * FROM ARRA_lecture WHERE type = 'class' ORDER BY id DESC") or die(mysql_error());
										while($rows = mysqli_fetch_array($query)){ ?>
										<tr class="odd gradeX">
											<td><?php echo $i++; ?></td>
											<td><?php echo $rows["lecture"]; ?></td>
											<td><?php echo $rows["subject"]; ?></td>
											<td><?php echo $rows["applicable"]; ?></td>
											<td><img src="<?php echo "../lecture/class_lecture/".$rows["applicable"]."/".$rows["file_name"]; ?>" width="100px" height="75px" /></td>
											<td class="text-center">
												<a href="#" onclick="downloadFile('<?php echo urlencode($rows["file_name"]); ?>')"  title='Download'><button type='button' class='btn btn-success'><i class="fa fa-download fa-1x"></i> </button></a>
												<?php if($_SESSION['access'] != "User") { ?>
												<a href="?removeDoc=<?php echo $rows["file_name"]; ?>&lid=<?php echo $rows["id"]; ?>&dir=<?php echo $rows["applicable"]; ?>" title='Delete'><button type='button' class='btn btn-danger'><i class="fa fa-trash-o fa-1x"></i> </button></a>
												<?php } ?>
											</td>
										</tr>	
											<?php
										}
										?>
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datetimepicker1').datetimepicker({
            yearOffset:0,
            lang:'en',
            timepicker:false,
            format:'Y-m-d',
            formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>\
    
    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/applyforchk.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
            $('#dataTables-payment').DataTable({
                responsive: true
            });
            
             document.getElementById("myButton").onclick = function () {
                location.href = "apg.php";
            };
                              
          });
		 
		function downloadFile(filename) {
			// Construct the download URL
			var downloadUrl = '<?php echo "../lecture/class_lecture/" . $rows["applicable"] . "/"; ?>' + filename;

			// Create a temporary anchor element to trigger the download
			var anchor = document.createElement('a');
			anchor.href = downloadUrl;
			anchor.download = filename; // Set the filename for the downloaded file
			anchor.style.display = 'none';
			document.body.appendChild(anchor);

			// Simulate a click event to trigger the download
			anchor.click();

			// Clean up
			document.body.removeChild(anchor);
		}

    function redirect()
    {
    var url = "http://www.(url).com";
    window.location(url);
    //setTimeout('window.location.href="apg.php"', 0);
    }
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>