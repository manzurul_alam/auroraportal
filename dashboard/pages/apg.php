<?php
session_start();
if(!empty($_SESSION['user_id']) && $_SESSION['tracking_number']){
include ('function.php');
dbConnect();
if(isset($_POST['save'])){
    //echo "sd";
    if($_POST['specialcircu']==""){
        $special = "N/A";
    }else{
        $special = $_POST['specialcircu'];
    }
    
    
    if(!empty($_POST['applyinfo']) && !empty($_POST['corAdd']) && !empty($special)){
       $stuSql = "UPDATE student_details SET a_stu_liveswith = '".$_POST['applyinfo']."', a_stu_corAddr = '".$_POST['corAdd']."', a_stu_special = '$special' WHERE a_stu_trackingNumber= '".$_SESSION['tracking_number']."'";
       $applyResult = mysqli_query($stuSql) or die(mysqli_error());
        if($applyResult){
            echo "<script type='text/javascript'>window.location='sec5.php';</script>";
        } 
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">SECTION 0 -- Applocation Procedure And Guideline</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-color: #156059;">
                        <div class="panel-heading">
                           <h3>Procedure And Guideline (DEMO)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <p>AIS Manhattan is located on a site overlooking the East River one mile south of the United Nations. The Queens campus is in suburban Jamaica Estates, approximately 17 miles east.

The Manhattan campus enrolls children in kindergarten through 12th grade. AIS Queens enrolls children in kindergarten through 8th grade; at the 9th grade level AIS Queens students may automatically transfer to the Manhattan Campus.

The United Nations International School provides an international education that emphasizes academic excellence within a caring community. The School promotes the appreciation of the diversity of persons and cultures, provides an optimal environment for learning and teaching, and offers a global curriculum that inspires in its students the spirit and ideals of the United Nations Charter.</p>
<p>
Provided that space is available in the relevant grades, we welcome applications for kindergarten through twelfth grade from families of the United Nations as well as those from the international and greater New York communities. In accordance with our mission, children whose parents transfer from abroad to work for the United Nations, Missions to the UN, and Consulates enjoy priority in terms of admission, but admission is not automatic. All children are required to be interviewed and assessed in person at AIS so that we can determine whether our program is the right fit for them.  A Skype interview can be arranged for applicants for fifth grade and above who reside abroad (to make arrangements, please email admissions@AIS.org). We can give an admission decision only after we have met with and assessed the child and have also received all the necessary documentation, which we should receive prior to meeting with the student.

</p>

<p>AIS seeks students who can benefit from a challenging education with a global perspective and who will contribute to the AIS community.  The School offers the International Baccalaureate (I.B.) Diploma Program, a two-year rigorous program that begins in the eleventh grade.  It requires a high level of achievement in all academic subjects.

Space permitting, qualified students are admitted at all grade levels through eleventh grade. Ordinarily, applicants are not admitted to the twelfth grade unless they have completed the first year of the I.B. Diploma Program.

Manhattan Campus: Prospective students for Junior A (Kindergarten) must be five years of age no later than 31 August of the year in which they enter AIS.  Students applying to the tenth and eleventh grades should verify that they could meet graduation requirements by the end of the twelfth grade prior to applying for admission.

</p>
<h4 class="lead">ADMISSIONS INFORMATION AND PROCEDURES</h4>
<p>ELL For those students who are not proficient in English, English Language Learners (ELL) classes are available. For grades 11 and 12, however, students must be proficient in English to be able to meet graduation requirements.

TOURS Group tours of the Manhattan Campus are scheduled from late September through May and last approximately 90 minutes.  There are specific morning tours of the Junior School (K to 4th grade), the Middle School (4th to 8th grades), and the Tutorial House (High School, 9th to 12th grades).  Students applying for 5th to 12th grades may join their parents for the tour.  Please note that tours from September through March are reserved for families who have applied for admission for the following September. 

Tours of the Queens Campus are scheduled as needed. Please telephone (718) 658-6166 for an appointment.

HOW TO APPLY  To start the online application process, please click here.</p>
<h4 class="lead">DEADLINE </h4>
<p>Applications to the Manhattan Campus will be accepted beginning September for September of the following year.  The deadline for applications from families residing in the New York metropolitan area is 15 November.  To ensure consideration of your child's application, we recommend that you apply as soon after 1 September as possible.  We stop accepting applications when our interview slots have been filled. The school tries to save a few spaces each year until June for families coming from abroad or out of state.
</p>
<p>Applications to the Queens Campus are not subject to the 15 November deadline. However, to ensure consideration of your child's application, we recommend that you apply as soon as possible after 1 September.
</p>
<h4 class="lead">TRANSCRIPTS</h4>
<p>Please upload your family's copy of the previous two years of school reports (one year for applicants to Kindergarten to fourth grade) soon after submitting the application online. In November, submit the Academic Report Request form (which you can access here) to your child's current nursery, primary, or secondary school (no sooner than that so that AIS has the most recent report before it makes admissions decisions in late January). The current school should forward a report to AIS on the student's academic progress and behavior.  Your school can upload school reports and send them to AIS through Ravenna.  Letters of recommendation from the school describing a student's study habits and social maturity are required (for applicants to fourth to twelfth grades) unless the student's school report includes comprehensive teacher comments. If those letters are necessary, they should come from an English teacher and a math teacher, and those teachers should email the letters directly to admissions@AIS.org; no special form is required. If documents are not in English, an official English translation is to be submitted to AIS along with the original documents. Prior to any admissions decision, complete and official records from the previous school(s) should reach AIS by mid-January.  In the case of a student coming from abroad, AIS also needs to receive the results of any national exam taken.
</p>
<h4 class="lead">TESTING</h4>
<p>Students applying for the Manhattan Campus who are fluent in English are assessed by the Educational Records Bureau (ERB), an independent testing agency. Please go to https://erblearn.org/. to download an application and get information about that admissions test.
</p>
<p>Applicants to Kindergarten and first grade are not required to take the ERB or any other type of standardized test, and AIS will not consider the scores of those tests if they are submitted.
</p>
<p>Students applying to the 11th grade may substitute PSAT or SAT results and they will be asked to write an essay at AIS.
</p>									
<p>Students applying for the Queens Campus will be tested directly at the school during the interview. However, if an applicant has been tested by the ERB, the parents should request that ERB send the results to AIS. 
</p>

<h4 class="lead">INTERVIEW</h4>
<p>Once you have submitted an application, please schedule a tour and interview through Ravenna (if you have already taken a tour, please contact the admissions office by email or phone to schedule an interview). Junior School applicants are interviewed in small groups.  Applicants for Secondary School (5th-12th grades) are interviewed individually.
</p>

<p>DECISIONS Admissions decisions for the next school year are announced from February onward.  Parents are required to inform the School of their intentions within approximately two weeks of notification of acceptance.  All other decisions are usually given within several weeks of the completion of the admissions procedures.
</p>
<h4 class="lead">FINANCIAL AID</h4>
<p>Financial Aid is based solely on documented financial need. AIS' financial aid program exists to make its educational program accessible to highly qualified students, regardless of family income. In service of this commitment, AIS offers need-based financial aid to newly-admitted students who will enrich and be enriched by our educational community. For families receiving aid, the school is also committed to underwriting the costs of required or essential educational resources, services, and opportunities not covered by tuition and to providing funds and resources to support participation in school-sponsored enrichment opportunities. An application for Financial Aid will not prejudice an admissions decision. AIS uses its own Financial Aid form. Prospective families should submit Financial Aid applications to the Admissions Office. Early submission of the Financial Aid application will help insure a response at the same time that an admissions decision is given.
</p>
<h4 class="lead">MEDICAL RECORDS</h4>
<p> The AIS medical form needs to be completed, signed by a physician, and returned to AIS prior to a child's joining a class.  Medical forms need to include dates of vaccination as well as a physical examination.  Medical or religious exemptions are acceptable if written proof is presented.  Medical forms should be submitted only once a student is enrolled at AIS.
</p>
<h4 class="lead">STUDENT VISAS</h4>
<p> Applicants who do not have USA citizenship or permanent resident status (green card) must enter the USA under the "prospective student" B2 visa classification so that they can be tested and interviewed for admission. An I-20 certificate of eligibility for non-immigrant (F-1) student status is issued only if the student has been interviewed and accepted and if the family has paid 70% of the tuition fees for the academic year.  If the student remains in the U.S., the I-20 form is then presented to the U.S. Citizenship and Immigration Services to obtain a change of status to F-1 "foreign student" status.  A student who departs from the U.S. before obtaining student status must present the I-20 form to an American Consul abroad.
</p>

<h4 class="lead">GUARDIANSHIP</h4>
<p>Students who do not live with their parents are required to live with a parent-designated legal guardian who is at least 25 years of age and who speaks the primary language of the student.  Under no circumstances are students permitted to live alone while attending AIS. Guardians are required to join the applicant at the time of the admissions interview at AIS so they understand the conditions of guardianship.
</p>
<h4 class="lead">TRANSPORTATION</h4>
<p>For students in grades K to 6 attending the Manhattan Campus, transportation on school buses is provided free of charge by the New York City Department of Education provided the child lives within the Borough of Manhattan.  The buses pick up children at approximately 55 locations and bring them directly to AIS.  All other students living in New York City are entitled to transportation passes for public transport based on the distance from AIS.  Not all children take the school bus or public transport.  Parents often cooperate with one another in getting their children to AIS by car.</p>

                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
              $('#datetimepicker2').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    
    
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>