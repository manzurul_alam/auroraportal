<?php
include('function.php');

// Establish database connection
$connection = dbConnect();

// Check if the connection is successful
if (!$connection) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the 'id' parameter is set in the URL
if (isset($_GET['id'])) {
    // Sanitize the ID parameter
    $id = mysqli_real_escape_string($connection, $_GET['id']);

    // Prepare the DELETE query
    $deleteQuery = "DELETE FROM users WHERE aurora_sign_id = ?";

    // Prepare the statement
    if ($stmt = mysqli_prepare($connection, $deleteQuery)) {
        // Bind the ID parameter to the statement
        mysqli_stmt_bind_param($stmt, 'i', $id);

        // Execute the statement
        if (mysqli_stmt_execute($stmt)) {
            // Check if any rows were affected
            if (mysqli_stmt_affected_rows($stmt) > 0) {
                // Redirect to alluser.php if the deletion was successful
                header("Location: alluser.php");
                exit();
            } else {
                echo "No rows were deleted.";
            }
        } else {
            echo "Error executing query: " . mysqli_error($connection);
        }

        // Close the statement
        mysqli_stmt_close($stmt);
    } else {
        echo "Error preparing statement: " . mysqli_error($connection);
    }
} else {
    echo "ID parameter is missing.";
}

// Close the database connection
mysqli_close($connection);
?>
