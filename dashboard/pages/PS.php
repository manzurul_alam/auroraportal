<?php 
session_start();

if(!empty($_SESSION['user_id'])){
//$_SESSION['tracking_number'] = "";
include ('function.php');
dbConnect();
//$message = 0;
$count = 0;
 if(isset($_POST['save'])){
    $message = "Line 12";
    echo $_POST["id"];
    if(isset($_POST["statusName"]) AND 
        isset($_POST["deadDate"]) AND 
        $_POST["statusName"] != "" AND 
        $_POST["deadDate"] != "" ){
            if($_POST["id"] != ""){
                mysqli_query(dbConnect(),"UPDATE ARRA_dateline_status SET ARRA_status = '".$_POST["statusName"]."',ARRA_date_of_dateline='".$_POST["deadDate"]."' WHERE ARRA_dateline_id =  '".$_REQUEST["statusid"]."'");    
                $message = "Data Successfully Updated...";
            }else{
               $insertStatus = "INSERT INTO ARRA_dateline_status (ARRA_status,ARRA_date_of_dateline) VALUES ('".$_POST["statusName"]."','".$_POST["deadDate"]."')";
               $insertResult = mysqli_query(dbConnect(),$insertStatus);
                if($insertResult){
                    $message = "Successfullly Inserted";
                } 
            }
        }    
            
 }elseif(isset($_REQUEST["statusid"])){
    $query = mysqli_query(dbConnect(),"SELECT * FROM ARRA_dateline_status WHERE ARRA_dateline_id =  '".$_REQUEST["statusid"]."'");
    if(mysqli_num_rows($query) > 0){
        $editPaymentData = mysqli_fetch_array($query);
    }
    else{
        $message = "No Data Found";
    }
}
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
	<script type="text/javascript">
	window.onload = function () {
		var chart2 = new CanvasJS.Chart("chartContainer2",
		{
		  title:{
			text: "Payment Status"
		  },
		  legend : {
			fontSize: 13,
			fontColor: "black",
		  },
		   data: [
		  {
			 type: "pie",
			 indexLabelFontColor: "black",
		   showInLegend: true,
		   dataPoints: [
		   {  y: 95, legendText:"Paid" , indexLabel: "Paid"},
		   {  y: 5, legendText:"Due" , indexLabel: "Due"}
		   ]
		 }
		 ]
	   });

		chart2.render();
		
	  }
	</script>
	<script type="text/javascript" src="jsbar/canvasjs.min.js"></script>
	
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            
            <div class="row">
                <div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="chartContainer2" style="width: 100%; height: 400px;"></div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
				
				<div class="col-lg-12">
				<div class="panel panel-danger">
					<div class="panel-heading">
					   View All Payments
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										Following Tracking Number(s) has been generated
									</div>
									<!-- /.panel-heading -->
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-payment">
												<thead>
													<tr class="text-center">
														<th width=70px>SL</th>
														<th>Pmt.ID</th>
														<th>Tracking No</th>
														<th>Pay For</th>
														<th>Payment Date</th>
														<th>Amount</th>
														<th>TRX No</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1;
													$query = mysqli_query(dbConnect(),"SELECT * FROM ARRA_payment_info ORDER BY ARRA_payment_id DESC") or die(mysql_error());
													while($rows = mysqli_fetch_array($query)){ ?>
													<tr class="odd gradeX">
														<td><?php echo $i++; ?></td>
														<td><?php echo $rows["ARRA_payment_id"]; ?></td>
														<td><?php echo $rows["ARRA_tracking_number"]; ?></td>
														<td><?php echo $rows["ARRA_payment_type"]; ?></td>
														<td><?php echo $rows["ARRA_payment_date"]; ?></td>
														<td><?php echo $rows["ARRA_payment_amount"]; ?></td>
														<td><?php echo $rows["ARRA_payment_trx_no"]; ?></td>
														<td class="text-center">
    <?php
    // Initialize the $status variable
    $status = ''; // Provide a default value to avoid undefined variable warnings

    // Assuming you have logic to set $status based on certain conditions
    // For example, setting it based on a database query or session variable
    if (isset($rows['status'])) {
        $status = $rows['status'];
    } else {
        // Handle cases where $status is not set in $rows
        $status = 'default_value'; // or some appropriate default value
    }

    // Check user access rights
    if ($_SESSION['access'] == "SuperAdmin" || (getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "PSE") == "PSE")) {
        ?>
        <a href="?paymentid=<?php echo $rows['ARRA_payment_id']; ?>" title='Edit'>
            <button type='button' class='btn btn-success'><i class='glyphicon glyphicon-pencil'></i></button>
        </a>&nbsp;
        <?php
    }
    ?>
    <a href="payment_con_modal_print.php?paymentid=<?php echo $rows['ARRA_payment_id']; ?>" title='Apply' <?php if ($status == 'Initiation') echo "style='pointer-events: none;'"; ?>>
        <button type='button' class='btn btn-success'><i class='glyphicon glyphicon-print'></i></button>
    </a>
</td>

													</tr>	
														<?php
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
									<!-- /.panel-body -->
								</div>
								<!-- /.panel -->
							</div>
							<!-- /.col-lg-12 -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.panel-body -->
				</div>
                    <!-- /.panel (nested) -->
				</div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datetimepicker1').datetimepicker({
            yearOffset:0,
            lang:'en',
            timepicker:false,
            format:'Y-m-d',
            formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>\
    
    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/applyforchk.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
            $('#dataTables-payment').DataTable({
                responsive: true
            });
            
             document.getElementById("myButton").onclick = function () {
                location.href = "apg.php";
            };
                              
          });
    </script>
    
    <script type="text/javascript">
    function redirect()
    {
    var url = "http://www.(url).com";
    window.location(url);
    //setTimeout('window.location.href="apg.php"', 0);
    }
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>