<?php
session_start();
if(!empty($_SESSION['user_id'])){
    include('function.php'); // Ensure the function.php file is included properly
    dbConnect(); // Ensure the dbConnect function is defined properly

    $html = "";
    $catid = "";
    if(isset($_POST['catid'])) {
        $q = mysqli_query(dbConnect(), "SELECT cat_id FROM category WHERE cat_name = '" . $_POST['catid'] . "'");
        if ($q) {
            $result = mysqli_fetch_assoc($q);
            $catid = $result['cat_id'];
        } else {
            die(mysqli_error(dbConnect()));
        }
    }

    if(isset($_POST['subcat'])){
        $insertQuery = "INSERT INTO subcategory VALUES ('','$catid','" . $_POST['subcat'] . "',NOW(),'" . $_SESSION['alias_id'] . "')";
        $insertResult = mysqli_query(dbConnect(), $insertQuery);
        if(!$insertResult){
            die(mysqli_error(dbConnect()));
        }
    }

    $subcatQueryResult = mysqli_query(dbConnect(), "SELECT subcat_name FROM subcategory WHERE catid = '$catid'");
    if($subcatQueryResult){
        while($rowCat = mysqli_fetch_assoc($subcatQueryResult)){
            if(isset($_POST['subcat']) && $_POST['subcat'] == $rowCat['subcat_name']) {
                $html .= '<option value="' . $rowCat['subcat_name'] . '" selected >' . $rowCat['subcat_name'] . '</option>';
            } else {
                $html .= '<option value="' . $rowCat['subcat_name'] . '">' . $rowCat['subcat_name'] . '</option>';
            }
        }
        echo $html;
    } else {
        die(mysqli_error(dbConnect()));
    }
} else {
    require_once 'login.php';
}
?>
