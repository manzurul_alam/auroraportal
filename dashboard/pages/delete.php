<?php
// Check if the tracking ID for deletion is provided
if (isset($_POST['trackingidDelete'])) {
    // Get the tracking ID to delete
    $trackingNumberToDelete = $_POST['trackingidDelete'];

    // Perform the deletion action
    // Your deletion logic here, for example:
    // $deleted = deleteRecord($trackingNumberToDelete);
    // if ($deleted) {
    //     echo "Record deleted successfully.";
    // } else {
    //     echo "Failed to delete record.";
    // }

    // For demonstration purposes, let's assume deletion is successful
    echo "Record with tracking number $trackingNumberToDelete deleted successfully.";
} else {
    // If the tracking ID is not provided, display an error message
    echo "Error: Tracking ID for deletion not provided.";
}
?>
