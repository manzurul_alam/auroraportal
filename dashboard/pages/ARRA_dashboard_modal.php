						<?php function draw_modal_data($level, $type) { ?>
						<div class="modal fade" id="<?php echo $type; ?>Modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $type; ?>ModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="<?php echo $type; ?>ModalLabel">Toddler Summery</h4>
									</div>
									<div class="modal-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-<?php echo $type; ?>">
												<thead>
													<tr>
														<th>Tracking Number</th>
														<th>User Name</th>
														<th>User Email Address</th>
														<th>Date</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$query = arra_modal_summery("$type");
													if(mysql_num_rows($query)>0){
														$i = 0;
														while($row = mysql_fetch_array($query)){ ?>
													<tr>
														<td><?php echo $row["Tracking Number"]; ?></td>
														<td><?php echo $row["User Name"]; ?></td>
														<td><?php echo $row["User Email"]; ?></td>
														<td><?php echo $row["Date"]; ?></td>
													</tr> <?php
														}
													} else { ?>
													<tr>
														<td colspan=4>No Data Found...</td>
													</tr> <?php
													}
													
													?>
												</tbody>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<!--<button type="button" class="btn btn-primary">Save changes</button>-->
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div> <?php 
						} ?>