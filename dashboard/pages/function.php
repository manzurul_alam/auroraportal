<?php
// require 'PHPMailerAutoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// $smtpHost="smtp.gmail.com";
// $smtpPort=587;
// $smtPassword="rlxwczmkojawlbba";
// function dbConnect($close=true){

//     $host='localhost';
//     $user='root';
//     $pass='YeezY@002';
//     $database='arraNew';

//     $sqlconnect= mysql_connect($host,$user,$pass) or die(mysql_error());
//     //echo 'result='.$sqlconnect;
//     if (!mysql_select_db($database, $sqlconnect)){
//       echo '<script>alert("Database changing failed with error\n'.mysql_error().'");</script>';
//       return false;
//     }
//     return true;
//     //return $sqlconnect;
// }
function dbConnect(){
    $host = 'localhost';
    $user = 'root';
    $password = ''; // Remember to update with your actual password
    $database = 'arraNew';

    // Use mysqli for secure and recommended database connection
    $connection = mysqli_connect($host, $user, $password, $database);

    // Check connection
    if (!$connection) {
        // If connection fails, terminate script and display error message
        die("Connection failed: " . mysqli_connect_error());
    }

    // Return the connection object for further use
    return $connection;
}


function accessUser($username) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in the previous response

  $sqlQuery = "SELECT notification FROM user_alias WHERE email = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery); // Prepare the statement with placeholder

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $username);

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    return trim($row['notification']);
  } else {
    return "False";
  }

  // Close resources (recommended, but handled to some extent by dbConnect)
  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect
}
function getInArray($value, $user_id) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined

  $sqlQuery = "SELECT getIn(?) AS access"; // Use prepared statement with placeholder

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "i", $user_id); // Bind integer parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    $accessString = $row['access']; // Assuming the function 'getIn' returns a string

    $accessArray = explode(",", $accessString);

    if (in_array($value, $accessArray)) {
      return true;
    } else {
      return false;
    }
  } else {
    // Handle case where no access data is found for the user
    return false; // Or you can throw an exception or log a warning
  }

  // Close resources (recommended, but handled to some extent by dbConnect)
  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect
}
function arra_summery($type, $fdate, $tdate){
    $connection = dbConnect();
    
    $query = mysqli_query($connection, "SELECT COUNT(ap.`ARRA_applying_id`) AS numOfStd 
                                         FROM `ARRA_tracking` at 
                                         INNER JOIN `ARRA_applying` ap ON ap.`ARRA_applying_tracking` = at.`ARRA_tracking_number` 
                                         WHERE ap.`ARRA_applying_for` = '$type' 
                                         AND at.ARRA_tracking_date BETWEEN '$fdate' AND '$tdate'");
    
    $row = mysqli_fetch_array($query);
    
    mysqli_close($connection);
    
    return $row["numOfStd"];
}

function xss_cleaner($input_str) {
    $return_str = str_replace( array("'",'"'), array('&apos;','&#x22;'), $input_str );
    //$return_str = str_ireplace( '%3Cscript', '', $return_str );
    return $return_str;
}

function xss_undo($input_str) {
    $return_str = str_replace( array('&apos;','&#x22;'),array("'",'"'),  $input_str );
    //$return_str = str_ireplace( '%3Cscript', '', $return_str );
    return $return_str;
}

function getTracUser($userID){
    $data = array('Tracking' => array(), 'Status' => array());

    $connection = dbConnect();

    // Check connection
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        return false;
    }

    $query = "SELECT ARRA_tracking_number, ARRA_tracking_status FROM ARRA_tracking WHERE ARRA_tracking_user = '$userID'";
    $result = mysqli_query($connection, $query);

    if (!$result) {
        echo "Error: " . mysqli_error($connection);
        mysqli_close($connection);
        return false;
    }

    while ($row = mysqli_fetch_assoc($result)) {
        $data['Tracking'][] = $row['ARRA_tracking_number'];
        $data['Status'][] = $row['ARRA_tracking_status'];
    }

    mysqli_free_result($result);
    mysqli_close($connection);

    return $data;
}

function getActiveUser($userID) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT COUNT(*) AS active_count
               FROM ARRA_tracking AS AT
               INNER JOIN ARRA_applying AS AA ON AT.ARRA_tracking_user = AA.ARRA_applying_user
               WHERE AT.ARRA_tracking_user = ?
               AND AT.ARRA_tracking_status = 'COMPLETE'
               AND AT.tracking_sub_status = 'ACTIVE'";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "i", $userID); // Bind integer parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row['active_count'] > 0) {
    return "ACTIVE";
  } else {
    return "INACTIVE";
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}

function getAccArray($value12, $user_id) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  // No need to call a separate function (`getIn`), modify the query to directly check access
  $sqlQuery = "SELECT FIND_IN_SET(?, SUBSTRING_INDEX(access, '-', 1)) AS has_access
               FROM user_alias
               WHERE email = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "is", $value12, $user_id); // Bind string and integer parameters

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row['has_access'] > 0) {
    return true;
  } else {
    return false;
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}

function safe($value) {
    $connection = dbConnect(); // Assuming you have the dbConnect function defined

    if (!$connection) {
        // Handle connection error
        return $value; // Or throw an exception
    }

    $escaped_value = mysqli_real_escape_string($connection, trim($value));
    mysqli_close($connection); // Close connection if not handled by dbConnect
    return $escaped_value;
}
function getID($username) {
  // Validate and sanitize username (email)
  if (empty($username) || !filter_var($username, FILTER_VALIDATE_EMAIL)) {
    return false; // Indicate invalid or missing email
  }

  $connection = dbConnect(); // Assuming you have the dbConnect function defined elsewhere

  // Prepare the query to retrieve user ID securely
  $sqlQuery = "SELECT user_id FROM user_alias WHERE email = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $username); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  // Check if a user record is found
  if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    $userId = $row['user_id']; // Extract user ID securely
  } else {
    $userId = false; // Indicate user not found
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect()

  return $userId;
}
function hasPublish($noticeID, $publishFor) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT COUNT(*) AS published
               FROM publish
               WHERE publish_for = ? AND type_id = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "ss", $publishFor, $noticeID); // Bind strings

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row['published'] > 0) {
    return true;
  } else {
    return false;
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}

function getUserID($username, $password) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT user_id FROM user_alias WHERE email = ? AND password = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "ss", $username, password_hash($password, PASSWORD_DEFAULT)); // Hash password

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row) {
    return $row['user_id']; // Return user ID if found
  } else {
    return false; // Return false if credentials are invalid
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect
}

function getTrackingApp($userId) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT 1
               FROM ARRA_tracking
               INNER JOIN ARRA_applying ON ARRA_tracking.ARRA_tracking_number = ARRA_applying.ARRA_applying_tracking
               WHERE ARRA_tracking.ARRA_tracking_user = ?
               AND ARRA_tracking.ARRA_tracking_status = 'COMPLETE'
               AND ARRA_tracking.tracking_sub_status = 'ACTIVE'";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "i", $userId); // Bind integer parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  // We only care if there's at least one row (meaning a tracking app exists)
  $hasTrackingApp = (mysqli_num_rows($result) > 0);

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return $hasTrackingApp;
}
function getPass($username) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  // Use prepared statements to prevent SQL injection
  $sqlQuery = "SELECT user_id FROM user_alias WHERE email = ?";
  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement
  mysqli_stmt_bind_param($stmt, "s", $username); // Bind string parameter

  // Execute the prepared statement for user ID retrieval
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute user ID statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);

  mysqli_stmt_close($stmt); // Close user ID statement

  if ($row) { // Check if user exists in user_alias table
    $userId = $row['user_id'];

    // Use another prepared statement to retrieve password (don't return password in plain text)
    $sqlQuery = "SELECT aurora_signup_passwd FROM users WHERE aurora_sign_id = ?";
    $stmt = mysqli_prepare($connection, $sqlQuery);

    if (!$stmt) {
      die("Failed to prepare password statement: " . mysqli_error($connection));
    }

    // Bind parameters using prepared statement
    mysqli_stmt_bind_param($stmt, "i", $userId); // Bind integer parameter

    // Execute the prepared statement for password retrieval
    if (!mysqli_stmt_execute($stmt)) {
      die("Failed to execute password statement: " . mysqli_error($connection));
    }

    $result = mysqli_stmt_get_result($stmt);

    // It's generally not recommended to return passwords directly. Consider using password hashing for secure storage.
    // Placeholder for password retrieval logic (assuming you have a way to handle hashed passwords)

    mysqli_stmt_close($stmt); // Close password statement
  } else {
    // Handle case where user doesn't exist in user_alias table (e.g., return an error or empty value)
  }

  mysqli_close($connection); // Close connection if not handled by dbConnect

  // Placeholder for returning a meaningful value (based on your password handling logic)
  return null; // Replace with appropriate logic for password retrieval or error handling
}

function signUpMgs($user,$pass,$vid){
	$t=time();
    $tDate = date("Y-m-d g:i a T",$t);
    $i = base64_encode($user);
    $infoex = explode(",",$info);
    
    $sub = "AIS Sign Up E-mail";
    // $msg='Please use the following link to verify your email address for the AIS Application System:<br/>'
    //     .'<a href="http://27.147.195.222:2244/AURORA/dashboard/pages/varify.php?vid='.$vid.'&i='.$i.'">https://27.147.195.222:2244/AURORA/dashboard/pages/varify</a>'
    //     .'After verifying, you can proceed to your application by clicking here:<br />'
    //     .'<a href="http://27.147.195.222:2244/ARRA/dashboard/">https://27.147.195.222:2244/AURORA/dashboard/</a>';
	    $msg='Please use the following link to verify your email address for the AIS Application System:<br/>'
        .'<a href="http://ais.ascentbd.com:2244/arrald/dashboard/pages/varify.php?vid='.$vid.'&i='.$i.'">https://27.147.195.222:2244/AURORA/dashboard/pages/varify</a>'
        .'After verifying, you can proceed to your application by clicking here:<br />'
        .'<a href="http://ais.ascentbd.com:2244/arrald/dashboard/">http://ais.ascentbd.com:2244/arrald/dashboard/</a>';
	
    
    
	
    //$mail_body='http://'.$serverip.':'.$serverport.'/post.php?post_id='.$post_id.'&cat_id='.$cat_id;// Mail body
	//$subject = "New request:".$post_title;
	//$headers = "From: postmaster@localhost";
	//$headers .= "Content-type: text/html \r\n";
	$mail = new PHPMailer(true);
	
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 2;
	
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	$mail->IsHTML(true);

    //Set the hostname of the mail server
    //$mail->Host = '172.16.0.1';
    $mail->Host = "smtp.gmail.com";

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    //$mail->Port = 587;
    $mail->Port = 587;

    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    //$mail->Username = "webmaster@imaginebd.com";
    // $mail->Username = "webmailer@scholasticabd.com";
    $mail->Username = "no-reply@aurora-intl.org";

    //Password to use for SMTP authentication
    //$mail->Password = "Dhaka@1212#";
    $mail->Password = "rlxwczmkojawlbba";
    //$mail->Password = "WM123";
    //Set who the message is to be sent from
    //$mail->setFrom('webmaster@imaginebd.com',"Aurora International School");
    //$mail->setFrom('webmailer@scholasticabd.com',"Aurora International School");
    //$mail->setFrom('webmailer@aurora-intl.org', "Aurora International School");
    $mail->setFrom('no-reply@aurora-intl.org', "Aurora International School");	
	//Set an alternative reply-to address
	//$mail->addReplyTo('hypereemede007@gmail.com');
	//Set the subject line
	$mail->Subject = $sub;
	
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
	
	//Replace the plain text body with one created manually
	$value = $user;
    
    if(trim($value) != ''){ 
          $mail->Body = '<head>'.
                        '<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">'.
                        '<title>Task Manager Notification</title>'.
                        '</head>'.
                        '<body>'.
                        '<big>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Hi '.$infoex[0].' '.$infoex[1].',<br/>'.
                        '</span>'.
                        '<table style="text-align: left; font-style: italic; width: 100%;" border="0" cellpadding="10" cellspacing="0">'.
                        '<tbody>'.
                        '<tr>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;"> Your registration has been completed sucessfully. Following credentials use for login</span><br/>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Username : '.$user.',</span><br/>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Password : '.$pass.',</span><br/>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Mobile : '.$infoex[2].',</span><br/>'.
                        '<td>'.
                        '</td>'.
                        '</tr>'.
                        '<tr>'.
                        '<td ><big><big><span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">'.$msg.'</span></big></big></td>'.
                        '</tr>'.
                        '</tbody>'.
                        '</table>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;"><br>'.
                        '<br><br/>Thank You,<br/><br/>AIS Application System.<br/>'.
                        '</span>'.
                        '</big>'.
                        '</body>'.
                    '</html>';// Mail body
          
          $mail->AddAddress($value);
          //$mail1->ClearAddresses();
          //send the message, check for errors
          if (!$mail->send()) {
              echo "<script>alert('Mailer Error:".$mail->ErrorInfo ."')</script>";
              
          } else {
            $mail->ClearAddresses();
            
              
          }
          //echo '<script type="text/javascript">alert("E-mail notification has been sent successfully!");</script>';
        
        }
      
      return true;
}
 function passResetMgs($user,$pass){
    $t=time();
    $tDate = date("Y-m-d g:i a T",$t);
    $i = base64_encode($user);
    $infoex = explode(",",$info);
    
    $sub = "AIS Password Reset";
    $msg='Please use the following link to Login AIS Application System:<br/>'
        .'<a href="http://www.aurora-intl.org/e-portal">http://www.aurora-intl.org/e-portal</a>'
        .'<p style="font-size:10px;">We encourage to change your current password</p>';
    
    
    
    //$mail_body='http://'.$serverip.':'.$serverport.'/post.php?post_id='.$post_id.'&cat_id='.$cat_id;// Mail body
    //$subject = "New request:".$post_title;
    //$headers = "From: postmaster@localhost";
    //$headers .= "Content-type: text/html \r\n";
    $mail = new PHPMailer();
    
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;
    
    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';
    $mail->IsHTML(true);

    //Set the hostname of the mail server
    //$mail->Host = '27.147.195.222';
    $mail->Host = "smtp.gmail.com";

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    //$mail->Port = 587;
    $mail->Port = 587 ;

    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    //$mail->Username = "webmaster@imaginebd.com";
    // $mail->Username = "webmailer@scholasticabd.com";
    $mail->Username = "no-reply@aurora-intl.org";

    //Password to use for SMTP authentication
    //$mail->Password = "Dhaka@1212#";
    $mail->Password = "rlxwczmkojawlbba";
    //$mail->Password = "WM123";
    //Set who the message is to be sent from
    //$mail->setFrom('webmaster@imaginebd.com',"Aurora International School");
    //$mail->setFrom('webmailer@scholasticabd.com',"Aurora International School");
    $mail->setFrom('no-reply@aurora-intl.org', "Aurora International School");
	
    //Set an alternative reply-to address
    //$mail->addReplyTo('hypereemede007@gmail.com');
    //Set the subject line
    $mail->Subject = $sub;
    
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
    
    //Replace the plain text body with one created manually
    $value = $user;
    
    if(trim($value) != ''){ 
          $mail->Body = '<head>'.
                        '<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">'.
                        '<title>Task Manager Notification</title>'.
                        '</head>'.
                        '<body>'.
                        '<big>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Hi,<br/>'.
                        '</span>'.
                        '<table style="text-align: left; font-style: italic; width: 100%;" border="0" cellpadding="10" cellspacing="0">'.
                        '<tbody>'.
                        '<tr>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;"> You have been sent this message because you recently contacted Aurora International School about your password. If you did not make this request, please contact the AIS office immediately.</span><br/>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Please find your login details here:</span><br/><br/>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Username : '.$user.'</span><br/>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Password : '.$pass.'</span><br/><br/>'.
						'<p style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">Please use the following link to log in to the AIS system:<br/> <a href="http://www.aurora-intl.org/e-portal">http://www.aurora-intl.org/e-portal</a></p>'.
                        '<td>'.						
                        '</td>'.
                        '</tr>'.
                        '<tr>'.
                        '<td ><span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">We encourage you to change your password after you have logged into the system, by clicking on  "My Profile" in the top right hand corner, and then clicking on "Password Reset".</span></td>'.
                        '</tr>'.
                        '</tbody>'.
                        '</table>'.
                        '<span style="font-family: Segoe UI,Helvetica,Arial,sans-serif;">'.
                        '<br/>Thank You,<br/><br/>AIS Management.<br/>'.
                        '</span>'.
                        '</big>'.
                        '</body>'.
                    '</html>';// Mail body
          
          $mail->AddAddress($value);
          //$mail1->ClearAddresses();
          //send the message, check for errors
          if (!$mail->send()) {
              echo "<script>alert('Mailer Error:".$mail->ErrorInfo ."')</script>";
              
          } else {
            $mail->ClearAddresses();
            
              
          }
          //echo '<script type="text/javascript">alert("E-mail notification has been sent successfully!");</script>';
        
        }
      
      return true;
}

function eMailCheck($username) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT aurora_signup_username FROM users WHERE aurora_signup_username = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $username); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  if (mysqli_num_rows($result) === 1) {
    return false; // Email already exists
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return true; // Email doesn't exist
}

function hasNotice($noticeID) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT COUNT(*) AS has_notice FROM email WHERE notice_id = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "i", $noticeID); // Bind integer parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row['has_notice'] === 0) {
    return true; // Notice doesn't exist
  } else {
    return false; // Notice exists
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}

function accessCheck($username) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT aurora_user_access FROM users WHERE aurora_signup_username = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameter using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $username); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    return trim($row['aurora_user_access']);
  } else {
    // Handle case where username is not found
    return false; // Or you can throw an exception or log a warning
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}
function schoolInsert($tracking, $schools, $academicYears, $classes, $languages, $locations, $count) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $successfulInserts = 0;
  for ($i = 0; $i < $count; $i++) {  // Use $i < $count for proper iteration

    $sqlQuery = "INSERT INTO ARRA_stu_pre_school (
                  ARRA_stu_pre_school_tracking,
                  ARRA_stu_pre_school_has_school,
                  ARRA_stu_pre_school_name,
                  ARRA_stu_pre_school_aca_year,
                  ARRA_stu_pre_school_class_dtl,
                  ARRA_stu_pre_schoolcol_lan,
                  ARRA_stu_pre_school_loc
                ) VALUES (?, ?, ?, ?, ?, ?, ?)
                ";

    $stmt = mysqli_prepare($connection, $sqlQuery);

    if (!$stmt) {
      die("Failed to prepare statement: " . mysqli_error($connection));
    }

    // Bind parameters using prepared statement for security
    mysqli_stmt_bind_param($stmt, "sssssss", $tracking, "YES", $schools[$i], $academicYears[$i], $classes[$i], $languages[$i], $locations[$i]);

    // Execute the prepared statement
    if (!mysqli_stmt_execute($stmt)) {
      echo "Error inserting record " . ($i + 1) . ": " . mysqli_error($connection); // Log or handle errors here
    } else {
      $successfulInserts++;
    }

    mysqli_stmt_close($stmt);
  }

  if ($successfulInserts > 0) {
    return true;
  } else {
    return false;
  }

  mysqli_close($connection); // Close connection if not handled by dbConnect
}

function getPercent($total){
    $current = rand(1,$total);
    $percent = round(($current/$total)*100,1);
    
    return $percent;
}

function getAccessDetail($value){
   switch($value){
    case "HLP" :
                $detail = "Help";
                return $detail;
                break;
	case "HLP-R" :
                $detail = "Help - Read Only";
                return $detail;
                break;
    case "HLP-E" :
                $detail = "Help - Edit Mode";
                return $detail;
                break;
    case "HLP-D" :
                $detail = "Help - Delete Mode";
                return $detail;
                break;
    case "AST" :
                $detail = "Asset";
                return $detail;
                break;
	case "AST-R" :
                $detail = "Asset - Read Only";
                return $detail;
                break;
    case "AST-E" :
                $detail = "Asset - Edit Mode";
                return $detail;
                break;
    case "AST-D" :
                $detail = "Asset - Delete Mode";
                return $detail;
                break;
    case "HR" :
                $detail = "HR";
                return $detail;
                break;
	case "HR-R" :
                $detail = "HR - Read Only";
                return $detail;
                break;
    case "HR-E" :
                $detail = "HR - Edit Mode";
                return $detail;
                break;
    case "HR-D" :
                $detail = "HR - Delete Mode";
                return $detail;
                break;
    case "PB" :
                $detail = "Phone Book";
                return $detail;
                break;
	case "PB-R" :
                $detail = "Phone Book - Read Only";
                return $detail;
                break;
    case "PB-E" :
                $detail = "Phone Book - Edit Mode";
                return $detail;
                break;
    case "PB-D" :
                $detail = "Phone Book - Delete Mode";
                return $detail;
                break;
    case "POOL" :
                $detail = "POOL";
                return $detail;
                break;
	case "POOL-R" :
                $detail = "POOL - Read Only";
                return $detail;
                break;
    case "POOL-E" :
                $detail = "POOL - Edit Mode";
                return $detail;
                break;
    case "POOL-D" :
                $detail = "POOL - Delete Mode";
                return $detail;
                break;
    case "DF" :
                $detail = "Discussion Forum";
                return $detail;
                break;
	case "DF-R" :
                $detail = "Discussion Forum - Read Only";
                return $detail;
                break;
    case "DF-E" :
                $detail = "Discussion Forum - Edit Mode";
                return $detail;
                break;
    case "DF-D" :
                $detail = "Discussion Forum - Delete Mode";
                return $detail;
                break;
    
    case "PA" :
                $detail = "Parents Alerts";
                return $detail;
                break;
	case "PA-R" :
                $detail = "Parents Alerts - Read Only";
                return $detail;
                break;
    case "PA-E" :
                $detail = "Parents Alerts - Edit Mode";
                return $detail;
                break;
    case "PA-D" :
                $detail = "Parents Alerts - Delete Mode";
                return $detail;
                break;
    case "PL" :
                $detail = "Parents Login";
                return $detail;
                break;
	case "PL-R" :
                $detail = "Parents Login - Read Only";
                return $detail;
                break;
    case "PL-E" :
                $detail = "Parents Login - Edit Mode";
                return $detail;
                break;
    case "PL-D" :
                $detail = "Parents Login - Delete Mode";
                return $detail;
                break;
    case "NTC" :
                $detail = "Notice";
                return $detail;
                break;
	case "NTC-R" :
                $detail = "Notice - Read Only";
                return $detail;
                break;
    case "NTC-E" :
                $detail = "Notice - Edit Mode";
                return $detail;
                break;
    case "NTC-D" :
                $detail = "Notice - Delete Mode";
                return $detail;
                break;
    case "FU" :
                $detail = "File Upload";
                return $detail;
                break;
    case "FU-R" :
                $detail = "File Upload - Read Only";
                return $detail;
                break;
    case "FU-E" :
                $detail = "File Upload - Edit Mode";
                return $detail;
                break;
    case "FU-D" :
                $detail = "File Upload - Delete Mode";
                return $detail;
                break;
    case "ERPT" :
                $detail = "e-Report";
                return $detail;
                break;
	case "ERPT-R" :
                $detail = "e-Report - Read Only";
                return $detail;
                break;
    case "ERPT-E" :
                $detail = "e-Report - Edit Mode";
                return $detail;
                break;
    case "ERPT-D" :
                $detail = "e-Report - Delete Mode";
                return $detail;
                break;
    case "ELIB" :
                $detail = "e-Library";
                return $detail;
                break;
	case "ELIB-R" :
                $detail = "e-Library - Read Only";
                return $detail;
                break;
    case "ELIB-E" :
                $detail = "e-Library - Edit Mode";
                return $detail;
                break;
    case "ELIB-D" :
                $detail = "e-Library - Delete Mode";
                return $detail;
                break;
    case "CL" :
                $detail = "Class Lecture";
                return $detail;
                break;
	case "CL-R" :
                $detail = "Class Lecture - Read Only";
                return $detail;
                break;
    case "CL-E" :
                $detail = "Class Lecture - Edit Mode";
                return $detail;
                break;
    case "CL-D" :
                $detail = "Class Lecture - Delete Mode";
                return $detail;
                break;
    case "VL" :
                $detail = "Video Lecture";
                return $detail;
                break;
	case "VL-R" :
                $detail = "Video Lecture - Read Only";
                return $detail;
                break;
    case "VL-E" :
                $detail = "Video Lecture - Edit Mode";
                return $detail;
                break;
    case "VL-D" :
                $detail = "Video Lecture - Delete Mode";
                return $detail;
                break;
    case "PS" :
                $detail = "Payment Status";
                return $detail;
                break;
	case "PS-R" :
                $detail = "Payment Status - Read Only";
                return $detail;
                break;
    case "PS-E" :
                $detail = "Payment Status - Edit Mode";
                return $detail;
                break;
    case "PS-D" :
                $detail = "Payment Status - Delete Mode";
                return $detail;
                break;
    case "TS" :
                $detail = "Teachers Status";
                return $detail;
                break;
	case "TS-R" :
                $detail = "Teachers Status - Read Only";
                return $detail;
                break;
    case "TS-E" :
                $detail = "Teachers Status - Edit Mode";
                return $detail;
                break;
    case "TS-D" :
                $detail = "Teachers Status - Delete Mode";
                return $detail;
                break;
    case "SS" :
                $detail = "Students Status";
                return $detail;
                break;
	case "SS-R" :
                $detail = "Students Status - Read Only";
                return $detail;
                break;
    case "SS-E" :
                $detail = "Students Status - Edit Mode";
                return $detail;
                break;
    case "SS-D" :
                $detail = "Students Status - Delete Mode";
                return $detail;
                break;
    case "AIP" :
                $detail = "Admission Inquiry Pool";
                return $detail;
                break;
	case "AIP-R" :
                $detail = "Admission Inquiry Pool - Read Only";
                return $detail;
                break;
    case "AIP-E" :
                $detail = "Admission Inquiry Pool - Edit Mode";
                return $detail;
                break;
    case "AIP-D" :
                $detail = "Admission Inquiry Pool - Delete Mode";
                return $detail;
                break;
    case "UM-R" :
                $detail = "User Management - Read Only";
                return $detail;
                break;
    case "UM" :
                $detail = "User Management";
                return $detail;
                break;
	case "UM-E" :
                $detail = "User Management - Edit Mode";
                return $detail;
                break;
    case "UM-D" :
                $detail = "User Management - Delete Mode";
                return $detail;
                break;
    case "album" :
                $detail = "Album";
                return $detail;
                break;
    case "album-R" :
                $detail = "Create Album - Read Only";
                return $detail;
                break;
    case "album-E" :
                $detail = "Create Album - Edit Mode";
                return $detail;
                break;
    case "album-D" :
                $detail = "Create Album - Delete Mode";
                return $detail;
                break;
    case "gallery" :
                $detail = "View gallery";
                return $detail;
                break;
    case "gallery-R" :
                $detail = "View Album - Read Only";
                return $detail;
                break;
    case "gallery-E" :
                $detail = "View Album - Edit Mode";
                return $detail;
                break;
	case "gallery-D" :
                $detail = "View Album - Delete Mode";
                return $detail;
                break;

    
    
   } 
}
function getStatusDetail($value){
   switch($value){
        case "PAFF" :
                    $detail = "Pay Applying Form Fees (PAFF)";
                    return $detail;
                    break;
        case "FFDS" :
                    $detail = "Form Filling and Document Submission (FFDS)";
                    return $detail;
                    break;
        case "SL" :
                    $detail = "Short Listed (SL)";
                    return $detail;
                    break;
        case "FR" :
                    $detail = "Form Rejected (FR)";
                    return $detail;
                    break;
        case "PAF" :
                    $detail = "Pay Admission Fees (PAF)";
                    return $detail;
                    break;
        case "WAF" :
                    $detail = "Welcome to Aurora Family (WAF)";
                    return $detail;
                    break;
		case "WFF" :
                    $detail = "Waiting for Feedback (WFF)";
                    return $detail;
                    break;
		case "Temp":
					return "Temp";
					break;
   } 
   
}
//902770 Dated: 21-05-2015

function arra_summary($type, $fdate, $tdate) {

  // Replace with actual database connection logic
  $connection = dbConnect(); // Assuming you have the dbConnect function defined

  $sqlQuery = "SELECT COUNT(*) AS numOfStd
               FROM ARRA_tracking AS at
               INNER JOIN ARRA_applying AS ap ON at.ARRA_tracking_number = ap.ARRA_applying_tracking
               WHERE ap.ARRA_applying_for = ?
               AND at.ARRA_tracking_date BETWEEN ? AND ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "sss", $type, $fdate, $tdate); // Bind strings

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);

  $numOfStd = $row['numOfStd'];

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return $numOfStd;
}
//902770 Dated: 24-05-2015
function arra_modal_summery($type) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT 
                  at.ARRA_tracking_number AS 'Tracking Number', 
                  at.ARRA_tracking_name AS 'User Name', 
                  at.ARRA_tracking_user AS 'User Email Address', 
                  at.ARRA_tracking_date AS 'Date' 
               FROM ARRA_tracking AS at
               INNER JOIN ARRA_applying AS ap ON ap.ARRA_applying_tracking = at.ARRA_tracking_number
               WHERE ap.ARRA_applying_for = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $type); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);
  $data = []; // Create an empty array to store the results

  while ($row = mysqli_fetch_assoc($result)) {
    $data[] = $row; // Add each row of data to the array
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return $data; // Return the array containing the query results
}

function getAcess($access, $userId){
    switch($access){
      case "SuperAdmin" :
            $str = "WHERE (aurora_user_access = 'SuperAdmin' OR aurora_user_access = 'SubAdmin' OR aurora_user_access = 'Admin' OR aurora_user_access = 'User') AND (aurora_signup_username != '".$userId."')";  
            return $str;
            break;
      case "SubAdmin" :
            $str = "WHERE (aurora_user_access = 'User') AND (aurora_signup_username != '".$userId."') AND (aurora_signup_creator = '".$userId."') AND (aurora_signup_creator = '".$userId."')";  
            return $str;
            break;
      case "Admin" :
            $str = "WHERE (aurora_user_access = 'SubAdmin' OR aurora_user_access = 'Admin' OR aurora_user_access = 'User') AND (aurora_signup_username != '".$userId."')"; 
            //$str = "WHERE (aurora_user_access = 'SubAdmin' OR aurora_user_access = 'User') AND (aurora_signup_username != '".$userId."') AND (aurora_signup_creator = '".$userId."')";  
            return $str;
            break;
      case "User" :
            $str = "WHERE (aurora_user_access = 'User') AND (aurora_signup_username != '".$userId."') AND (aurora_signup_creator = '".$userId."')";  
            return $str;
            break;
    }
}

function getAccessArr($userId) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT aurora_user_access_area FROM users WHERE aurora_signup_username = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $userId); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    $accessArea = $row['aurora_user_access_area'];
  } else {
    $accessArea = ""; // Or handle case where user not found (e.g., return null)
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return $accessArea;
}

function getAccessArea($accessArra, $thisValue) {

  if (empty($accessArra)) {
    return false; // Handle empty access array case
  }

  $accessArrays = explode(",", $accessArra);
  return in_array($thisValue, $accessArrays, true); // Strict comparison
}


function getClassType($keyValue){
	switch($keyValue){
		case "toddler": case "Toddler":
			return "Toddler ( Age 2+ )";
			break;
		case "preschool": case "Preschool":
			return "Pre School ( Age 3+ )";
			break;
		case "elschool": case "Prekindergarten":
			return "Pre Kindergarten ( Age 4+ )";
			break;
		case "kinder": case "Kinder":
            return "Kindergarten ( Age 5+ )";
            break;
        case "Class-1":
			return "Class One ( age 6+ )";
			break;
		case "all": case "All":
			return "All";
			break;
		default: return $keyValue; break;
	}
}

function getImgFileValid($ext){
	switch($ext){
		case "jpeg": case "jpg": case "gif": case "png":
			return true;
			break;
		case "JPG": case "JPEG": case "GIF": case "PNG":
			return true;
			break;
		default:
			return false;
	}
}

function getDocFileValid($ext){
	switch($ext){
		case "jpeg": case "jpg": case "gif": case "png": case "doc": case "docx": case "pdf":
			return true;
			break;
		case "JPEG": case "JPG": case "GIF": case "PNG": case "DOC": case "DOCX": case "PDF":
			return true;
			break;
		default:
			return false;
	}
}

function initials($str) {
    $ret = '';
    foreach (explode(' ', $str) as $word)
        $ret .= strtoupper($word[0]);
    return $ret;
}

function getSpecialServices($val,$id){
      $connection = dbConnect(); // Assuming you have the dbConnect function defined

  if (!$connection) {
    // Handle connection error
    return "Error: Database connection failed.";
  }

  $sqlQuery = "SELECT ARRA_learning_profile_spesvr_other FROM ARRA_learning_profile WHERE ARRA_learning_profile_tracking = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameter using prepared statement
  mysqli_stmt_bind_param($stmt, "i", $id); // Bind integer parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $otherService = null;
  if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $otherService = $row['ARRA_learning_profile_spesvr_other'];
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection);
	switch($val){
		case "LSP":
			return "Learning Support Program";
			break;
		case "ORP":
			return "Other Remedial Program";
			break;
		case "SLT":
			return "Speech/Language Therapy";
			break;
		case "COUN":
			return "Counseling";
			break;
		case "GTHP":
			return "Gifted/Talented/Honors Program";
			break;
		case "POT":
			return "Physical/Occupational Therapy";
			break;
		case "LVHP":
			return "Limited Vision and/or Hearing Program";
			break;
		case "ESL":
			return "ESL (English as Additional Language)";
			break;
		case "other":
			return $otherService;
			break;
	}
}

function getDocTypeName($val){
	switch($val){
		case "SP":
			return "Student Photo";
			break;
		case "SBC":
			return "Student Birth Certificate";
			break;
		case "SPP":
			return "Student Individual Passport";
			break;
		case "FP":
			return "Father Passport";
			break;
		case "MP":
			return "Mother Passport";
			break;
		case "WP":
			return "Work Permit";
			break;
		case "SRC":
			return "Student Report Card";
			break;
		case "IVR":
			return "Immunization & Vaccination Records";
			break;
		case "PD":
			return "Physical Disability";
			break;
	}
}

function accessJoining($readValue,$editValue,$deleteValue){
       if($readValue==""){
           return $joinValue =  $editValue.",".$deleteValue;
       }elseif($editValue==""){
            return $joinValue =  $readValue.",".$deleteValue;
       }elseif($deleteValue==""){
            return $joinValue =  $readValue.",".$deleteValue;
       }elseif($readValue=="" AND $editValue==""){
            return $joinValue =  $deleteValue;
       }elseif($readValue=="" AND $deleteValue==""){
            return $joinValue =  $editValue;
       }elseif($editValue=="" AND $deleteValue==""){
            return $joinValue =  $readValue;
       }elseif($readValue=="" AND $editValue=="" AND $deleteValue==""){
            return false;
       }else{
           return $readValue.",".$editValue.",".$deleteValue;
       }
}

function checkStuStatus($userId){
    $connection = dbConnect();
    
    $getQuery = "SELECT * FROM ARRA_tracking WHERE ARRA_tracking_user = '".$userId."'";
    $result = mysqli_query($connection, $getQuery);
    // print_r($result);
    if(mysqli_num_rows($result) > 0){
       
        return true;   
    } else {
       
        return false;   
    }
}

function checkNoticeStuStatus($userId, $noticeId) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT 1 AS acknowledged FROM notice_status
                WHERE user_id = ? AND notice_id = ? AND status = 1";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "ii", $userId, $noticeId); // Bind integer parameters

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $acknowledged = (mysqli_num_rows($result) > 0); // Use boolean check for acknowledgement

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return $acknowledged;
}
function traStatus($trackingNumber) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT ARRA_tracking_status FROM ARRA_tracking WHERE ARRA_tracking_number = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $trackingNumber); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    $accessArea = $row['ARRA_tracking_status'];
    return $accessArea;
  } else {
    // Handle case where tracking number is not found
    return "Tracking Number Not Found"; // Or throw an exception, log a warning, etc.
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}

function countAttachments($noticeID) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT COUNT(*) AS attachment_count FROM attachments WHERE notice_id = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "i", $noticeID); // Bind integer parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row['attachment_count'] > 0) {
    $attachmentCount = $row['attachment_count'];
  } else {
    $attachmentCount = 0; // Or "No Attachments" if you prefer a string
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

  return $attachmentCount;
}
function traTotalStatus($status) {

  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT COUNT(*) AS total_active
               FROM ARRA_tracking
               WHERE ARRA_tracking_status = ?
               AND tracking_sub_status = 'ACTIVE'";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $status); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  return $row['total_active'];

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect

}
function albumTotalStatus($name) {


  $connection = dbConnect(); // Assuming you have the dbConnect function defined as in previous responses

  $sqlQuery = "SELECT COUNT(*) AS total_albums FROM ARRA_album WHERE ARRA_album_for = ?";

  $stmt = mysqli_prepare($connection, $sqlQuery);

  if (!$stmt) {
    die("Failed to prepare statement: " . mysqli_error($connection));
  }

  // Bind parameters using prepared statement for security
  mysqli_stmt_bind_param($stmt, "s", $name); // Bind string parameter

  // Execute the prepared statement
  if (!mysqli_stmt_execute($stmt)) {
    die("Failed to execute statement: " . mysqli_error($connection));
  }

  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_assoc($result);

  if ($row) {
    return $row['total_albums'];
  } else {
    return 0; // No album found
  }

  mysqli_stmt_close($stmt);
  mysqli_close($connection); // Close connection if not handled by dbConnect
}

function getBrowser() {
  $u_agent = $_SERVER['HTTP_USER_AGENT'];
  $platform = 'Unknown';
  $browser = array(
    'name' => 'Unknown',
    'version' => "",
  );

  // Platform detection
  if (preg_match('/linux/i', $u_agent)) {
    $platform = 'linux';
  } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
    $platform = 'mac';
  } elseif (preg_match('/windows|win32/i', $u_agent)) {
    $platform = 'windows';
  }

  // Browser detection and version extraction (combined)
  $pattern = '#(?<browser>MSIE|Firefox|Chrome|Safari|Opera|Netscape)[/ ]+(?<version>[0-9.|a-zA-Z.]*)#i';
  if (preg_match($pattern, $u_agent, $matches)) {
    $browser['name'] = $matches['browser'];
    $browser['version'] = $matches['version'] ?? ""; // Use nullish coalescing for empty version
  }

  return array(
    'userAgent' => $u_agent,
    'name' => $browser['name'],
    'version' => $browser['version'],
    'platform' => $platform,
  );
}

function browserSplit($name){
	$nameEx = explode(" ",$name);
	return $nameEx[0];
}

function clean($string, $replaceSpaceWith = '-', $keepSpecialChars = '') {

  // Optional space replacement
  if ($replaceSpaceWith !== false) {
    $string = str_replace(' ', $replaceSpaceWith, $string);
  }

  // Allow keeping specific characters (optional)
  $pattern = '/[^A-Za-z0-9' . preg_quote($keepSpecialChars, '/') . '\-]/';

  // Remove special characters using regular expression
  $cleanedString = preg_replace($pattern, '', $string);

  return $cleanedString;
}

function get_ip(){
	
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function get_ip_address() {
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }

    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip))
                    return $ip;
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];

    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip) {
    if (strtolower($ip) === 'unknown')
        return false;

    // generate ipv4 network address
    $ip = ip2long($ip);

    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) return false;
        if ($ip >= 167772160 && $ip <= 184549375) return false;
        if ($ip >= 2130706432 && $ip <= 2147483647) return false;
        if ($ip >= 2851995648 && $ip <= 2852061183) return false;
        if ($ip >= 2886729728 && $ip <= 2887778303) return false;
        if ($ip >= 3221225984 && $ip <= 3221226239) return false;
        if ($ip >= 3232235520 && $ip <= 3232301055) return false;
        if ($ip >= 4294967040) return false;
    }
    return true;
}

function formSubmitMail($user,$trackingNumber){
	$connection = dbConnect();
	//$mail_body='http://'.$serverip.':'.$serverport.'/post.php?post_id='.$post_id.'&cat_id='.$cat_id;// Mail body
	//$subject = "New request:".$post_title;
	//$headers = "From: postmaster@localhost";
	//$headers .= "Content-type: text/html \r\n";
	$mail = new PHPMailer();
	
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 0;
	
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	$mail->IsHTML(true);
	
	//Set the hostname of the mail server
    //$mail->Host = '172.16.0.63';
	//$mail->Host = '27.147.195.222';
	//$mail->Host = 'outgoing.ascentbd.com';
    $mail->Host = "smtp.gmail.com";
    $mail->SMTPSecure = 'tls';
	//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	//$mail->Port = 587;
    $mail->Port = 587 ;
	
	//Set the encryption system to use - ssl (deprecated) or tls
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    //$mail->Username = "webmaster@imaginebd.com";
   // $mail->Username = "webmailer@scholasticabd.com";
    // $mail->Username = "no-reply@aurora-intl.org";
    $mail->Username = "mottaleb.jebon@gmail.com";
    //$mail->Username = "manzu.alam01@gmail.com";
	
	//Password to use for SMTP authentication
	//$mail->Password = "Dhaka@1212#";
    //$mail->Password = "4aHqgo9F4$";
    $mail->Password = "tpmqjczlhbrjbkkb";
	//$mail->Password = "WM123";
	//Set who the message is to be sent from
	//$mail->setFrom('webmaster@imaginebd.com',"Aurora International School");
	//$mail->setFrom('webmailer@scholasticabd.com',"Aurora International School");
  //$mail->setFrom('no-reply@aurora-intl.org',"Aurora International School");
    $mail->setFrom('mottaleb.jebon@gmail.com', "Aurora International School");
    //$mail->setFrom('manzu.alam01@gmail.com', "Aurora International School");
	
	//$mail->setFrom('webmaster@imaginebd.com',"Aurora International School");
	//Set an alternative reply-to address
	//$mail->addReplyTo('hypereemede007@gmail.com');
	//Set the subject line
	$mail->Subject = "Thank you for applying to Aurora International School.";
  $mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
	//$mail->AddEmbeddedImage('../../images/auroralogo.png', 'auroralogo');
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
	
	//Replace the plain text body with one created manually
	$value = $user;
    
	

$sqlQuery = "SELECT a.ARRA_tracking_name, b.ARRA_childs_detail_dob
              FROM ARRA_tracking a
              INNER JOIN ARRA_childs_detail b ON b.ARRA_childs_detail_tracking_number = a.ARRA_tracking_number
              WHERE a.ARRA_tracking_number = ?";

$stmt = mysqli_prepare($connection, $sqlQuery);

if (!$stmt) {
  die("Failed to prepare statement: " . mysqli_error($connection));
}

// Bind parameters using prepared statement for security
mysqli_stmt_bind_param($stmt, "s", $trackingNumber); // Bind string parameter

// Execute the prepared statement
if (!mysqli_stmt_execute($stmt)) {
  die("Failed to execute statement: " . mysqli_error($connection));
}

$result = mysqli_stmt_get_result($stmt);

if (mysqli_num_rows($result) === 1) {
  $row = mysqli_fetch_assoc($result);
  $studentName = $row['ARRA_tracking_name'];
  
  $studentDOB = $row['ARRA_childs_detail_dob'];
} else {
  // Handle case where no data is found for the tracking number
  $studentName = null;
  $studentDOB = null;
  // Or you can throw an exception or log a warning
}

mysqli_stmt_close($stmt);

	$nameexplode = explode("@",$studentName);
	$firstName=$nameexplode[0];
	$lastName=$nameexplode[1];
	
	if(trim($value) != ''){ 
          $mail->Body = '<table width="100%">'.
							// '<tr align="center" style="text-align:centre;">
                            //         <td colspan="15">
                                        
                            //             <img src="http://ais.ascentbd.com:2244/arrald/images/auroralogo.png" style="width: 12%; />
                            //         </td>
                            // </tr><br/>'.
                            // <img src="cid:auroralogo" style="width: 12%;" />
                            '<tr align="center" style="text-align:centre;"><td><img src="http://ais.ascentbd.com:2244/arrald/images/auroralogo.png" style="width: 12%;" /></td></tr>'.
                            '<tr style="margin-bottom: 12px; display:block;"><td>Thank you for taking the time to complete and submit the Admission Form for Aurora International School.  Details of your application are provided for your reference below:</td></tr>'.
							'<tr><td>'.
							'<span><strong>Name of student:  </strong> '.$firstName." ".$lastName.'</span><br />'.
							'<span><strong>Date of birth:  </strong> '.$studentDOB.'</span><br />'.
							'<span><strong>Application tracking number:  </strong> '.$trackingNumber.' </span><br />'.
							'</td></tr>'.
							'<tr style="margin-bottom: 12px; text-align:justify;"><td><p>Please print out the payment instruction document to pay the admission processing fee at the AIS campus. After we receive the fee, we will start to review your application. You can expect to hear from us within one week after payment of the processing fee. If you do not receive any email or phone call from us within seven days, please feel free to call us at +88-0184-123-4230 or email us at info@aurora-intl.org.</p></td></tr>'.
							//'<tr style="margin-bottom: 12px;"><a href="http://27.147.195.222:2244/ARRA/dashboard/pages/payments_instruction.php?trackingid='.$trackingNumber.'">Click here to print your payment slip</a></tr>'.
							'<tr style="margin-bottom: 12px;"><td><p>Thank you for your interest in Aurora International School!</p></td></tr>'.
							'<br/><tr style="margin-bottom: 12px;"><td><p>Sincerely,</p></td></tr>'.
							'<tr style="margin-bottom: 12px;"><td>'.
							// '<p style="margin-bottom: -18px;"><span style="font-size: 14px; font-weight: bold;">Israt Jahan</span></p>'.
                            // '<p style="margin-bottom: -18px;"><span style="font-size: 14px; font-weight: bold;">Head of Admissions</span></p>'.
							// '<p style="margin-bottom: -18px;"><span style="font-size: 14px; font-weight: bold;">Student Admissions and Records</span></p>'.
							// '<p style="margin-bottom: -18px;"><span style="font-size: 14px; font-weight: bold;">Nusrat Tabassum </span></p>'.
                            // '<p style="margin-bottom: -18px;"><span style="font-size: 14px; font-weight: bold;">Officer</span></p>'.
							'<p style="margin-bottom: -18px;"><span style="font-size: 14px; font-weight: bold;">Student Admissions and Records</span></p>'.
                            '</td></tr>'.
							'<tr style="margin-bottom: 12px;"><td>'.
							// '<p style="margin-bottom: -12px;"><span style="font-size: 9px;">House No. NE(A)3A, Road No. 74</span></p>'.
							'<p style="margin-bottom: -12px;"><span style="font-size: 9px;">Gulshan 2, Road 83, House NE (L) 5 A & C. Dhaka 1212.</span></p>'.
							// '<p style="margin-bottom: -12px;"><span style="font-size: 9px;">Telephone: 02222283251 and 02222262252</span></p>'.
                            '<p style="margin-bottom: -12px;"><span style="font-size: 9px;">Telephone: 02222283251 and 02222262252</span></p>'.
							'<p style="margin-bottom: -12px;"><span style="font-size: 9px;">Mobile: +88 01841 234230</span></p>'.
							'<p><span style="font-size: 9px;">Website: www.aurora-intl.org</span></p>'.
							'</td></tr><br/>'.
                            // '<tr align="center" style="text-align:centre;"><td><img src="http://ais.ascentbd.com:2244/arrald/images/auroralogo.png" style="width: 12%;" /></td></tr>'.
                            // '<tr align="center" style="text-align:centre;" ><td>'.
                            //     '<img src="../../../images/Auroralog.jpg" style="width: 12%; />'.
							// '</td></tr>'.
						'</table>';
		 
				
			
          $mail->AddAddress($value);
          //$mail->AddAddress('dipak_chakraborty@imaginebd.com');
          //$mail1->ClearAddresses();
          //send the message, check for errors
        
        // $query = mysql_query("SELECT aurora_signup_email FROM users WHERE aurora_user_access  = 'SubAdmin'");
		// if ($query)
		// { 
		//   while($row = mysql_fetch_array($query))
		//   {     
		// 	$mail->AddCC($row[0]);
		//   }  
		// }
    // $mail->AddCC('mottaleb.jebon@gmail.com');
    $mail->AddCC('mmottaleb171246@bscse.uiu.ac.bd');
        // $mail->AddCC('admissions@aurora-intl.org');
        // $mail->AddCC('madiha_murshed@aurora-intl.org');
        // $mail->AddCC('naziat_khan@aurora-intl.org');
        // $mail->AddCC('tasnima_methi@aurora-intl.org');
        // $mail->AddCC('nusrat_tabassum@aurora-intl.org');
        // $mail->AddCC('madiha_murshed@aurora-intl.org');
	// 	  $mail->AddCC('sadequr_rahman@imaginebd.com');
    //    $mail->AddCC('msrkhokoncse@gmail.com');
    //    $mail->AddCC('sadeq@ohs.global');
        // $mail->AddCC('manzu.alam012@gmail.com');
        // $mail->AddBCC("manzu.alam012@gmail.com", "Manzurul alam");
		if (!$mail->send()) {
		
			echo "Mailer Error: " . $mail->ErrorInfo;
			return false;
		} else {
			
			$mail->ClearAddresses();
			return true;
		}
          //echo '<script type="text/javascript">alert("E-mail notification has been sent successfully!");</script>';
        
        }
      
      return true;
}

function sendMail($to, $sub, $body){
  $connection = dbConnect();
	$mail = new PHPMailer();
	$mail->isSMTP();
	 $mail->SMTPDebug = 0;
	
	$mail->Debugoutput = 'html';
	$mail->IsHTML(true);

    //Set the hostname of the mail server
    $mail->Host = "smtp.gmail.com";

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    //$mail->Port = 587;
    $mail->Port = 587;

    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    //$mail->Username = "webmaster@imaginebd.com";
    // $mail->Username = "webmailer@scholasticabd.com";
    $mail->Username = "mottaleb.jebon@gmail.com";

    //Password to use for SMTP authentication
    //$mail->Password = "Dhaka@1212#";
    // $mail->Password = "rlxwczmkojawlbba";
    $mail->Password = "tpmqjczlhbrjbkkb";
    //$mail->Password = "WM123";
    //Set who the message is to be sent from
    //$mail->setFrom('webmaster@imaginebd.com',"Aurora International School");
    //$mail->setFrom('webmailer@scholasticabd.com',"Aurora International School");
    // $mail->setFrom('no-reply@aurora-intl.org', "Aurora International School");
    $mail->setFrom('mottaleb.jebon@gmail.com', "Aurora International School");
    $mail->addAddress('mmottaleb171246@bscse.uiu.ac.bd', 'mottaleb');
	//Username to use for SMTP authentication - use full email address for gmail
    //$mail->Username = "webmaster@imaginebd.com";
    
    //Password to use for SMTP authentication
    //$mail->Password = "WM123";
    //Set who the message is to be sent from
    //$mail->setFrom('webmaster@imaginebd.com',"Aurora International School");
	//Set the subject line
	$mail->Subject = $sub;
	//$mail->AddEmbeddedImage('../../images/auroralogo.png', 'auroralogo');
	
	//Replace the plain text body with one created manually
	$mail->Body = $body;
	
	foreach ($to as $email => $name) {
		
        $mail->AddAddress($name);
    }
    
	
	// $query = mysql_query("SELECT aurora_signup_email FROM users WHERE aurora_user_access  = 'SubAdmin'");
	// if ($query){
		// while($row = mysql_fetch_array($query)){
			// $mail->AddCC($row[0]);echo $row[0]."<br>";
		// }
	// }
	
	//echo $mail->Body;
	
$mail->send();
	
}
?>
