<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $uploadDir = 'uploads/';
    if (!is_dir($uploadDir)) {
        mkdir($uploadDir, 0777, true);
    }

    foreach ($_FILES['files']['name'] as $key => $name) {
        $targetFile = $uploadDir . basename($name);
        if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $targetFile)) {
            echo "The file " . htmlspecialchars(basename($name)) . " has been uploaded.<br>";
        } else {
            echo "Sorry, there was an error uploading your file.<br>";
        }
    }
}
?>

