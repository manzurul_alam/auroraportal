<?php
    session_start();
    include ('function.php');
    dbConnect();

    $oriQuery = "SELECT * FROM category, user_alias where id=creator AND cat_flag=" . $_GET['type'];
    $query = mysqli_query(dbConnect(), $oriQuery) or die(mysqli_error(dbConnect()));
    $i = 1;
    while ($rows = mysqli_fetch_array($query)) {
?>
        <tr id="<?php echo $rows["cat_id"]; ?>" class="odd gradeX">
            <td><?php echo $i++; ?></td>
            <td><?php echo $rows["cat_name"]; ?></td>

            <td>
                <?php if ($_SESSION['alias_id'] == $rows["creator"]) { ?>
                    <button class="btn_del_cat" data-id="<?php echo $rows["cat_id"]; ?>" data-name="<?php echo $rows["cat_name"]; ?>" type="button" class="btn btn-danger"><i class="fa fa-trash-o fa-1x"></i> </button>
                <?php } else echo "by - " . $rows["email"]; ?>
            </td>

        </tr>
<?php
    }
?>
