<style type="text/css">
    /*Custom*/
    .custom-nav-head-li {
        background: #FB593E; 
        color: #FFF;
        padding: 10px;
    }
    .page-header {
        padding-bottom: 9px;
        margin: 0px 20px 10px;
        border-bottom: 1px solid #FB593E;
    }
    h1{font-size: 16px;}
    h2{font-size: 14px;}
    h3{font-size: 14px;}

    .btn-success.btn-outline{ color: #fff; }
    .custom-btn{
        margin-top: 0px;
        cursor: pointer;
        padding-top: 7px;
        padding-bottom: 7px;
        padding-left: 37px;
        padding-right: 37px;
        background: #FB593E;
        border-radius: 8px;
    }
    
    .navbar-header .navbar-brand, .navbar-header .navbar-brand:hover { color: #FFF; }

    .tab-btn {border-radius: 4px; width:inherit; text-align:center; font-size: 8px;}
    .tab-btn a{color: #FFFFFF; background-color: #FB593E;}
    .nav-pills>li.active>a, 
    .nav-pills>li.active>a:focus, 
    .nav-pills>li.active>a:hover {
        color: #FFFFFF; background-color: #FB593E; 
    }
    .nav>li.tab-btn>a:hover {
        text-decoration: none; 
        border-radius: 4px; 
        color: #FFFFFF; 
        background-color: #156059; 
    }
    .nav>li.tab-btn>a {
        text-decoration: none; 
        border-radius: 4px; 
        color: #FFFFFF; 
        background-color: #156059; 
    }

    .btn-width{
        width: 115px;
        text-align: center;
    }
    .submitbtn{
        background-color: #156059 !important;
        color: #fff !important;
    }
    .exitbtn{
        background-color: #009D4E !important;
        color: #fff !important;
    }
    img{
        width: 64%;
    }

    .panel {
        border: 1px solid #156059;

    }

    .nav > li > a {
	    position: relative;
	    display: block;
	    padding: 10px 15px;
	    color: #FFFFFF;
	    background-color: inherit;
	}

	.menutext-left {
		padding-top: 10px;
	}
    .actiontd>a>.btn{padding: 0px; font-size: 6px;}
    .actiontd{width: inherit;}
    table {font-size: 2px; }
    table.dataTable thead > tr > th{padding: 0px;}
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding: 0px;}


    @media(min-width:460px){
        table {font-size: 8px;}
        table.dataTable thead > tr > th{padding: 0px;}
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding: 0px;}
		
    }
    
    @media(min-width:768px){
        h1{font-size: 26px;}
        h2{font-size: 24px;}
        h3{font-size: 24px;}
    }
    @media(min-width:1024px){
        table {font-size: 14px;}
        table.dataTable thead > tr > th{padding: 5px;}
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding: 5px;}
        .actiontd>a>.btn{padding: 6px 12px; font-size: 14px;}
        .actiontd{width: inherit;}
    }
    @media(min-width:1070px){
        table {font-size: 14px;}
        table.dataTable thead > tr > th{padding: 18px;}
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr{padding: 5px;}
    }
</style>
<!-- Unset Tracking number -->
<?php 
if( ( basename($_SERVER['PHP_SELF']) == "userprofile.php" || 
	  basename($_SERVER['PHP_SELF']) == "viewApplyForm.php" || 
	  basename($_SERVER['PHP_SELF']) == "dashboard.php" || 
	  basename($_SERVER['PHP_SELF']) == "index.php" ) ){ 
      
      unset($_SESSION['tracking_number']);
}
?>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="background-color: #156059;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <img src="../images/auroralogo.png"/>
        <a class="navbar-brand">AURORA INTERNATIONAL SCHOOL</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li><button type="button" class="btn btn-new withtext" onclick="window.location.href ='dashboard.php'"><i class="fa fa-home fa-2x"></i>&nbsp; Home</button></li>
        <li><button type="button" class="btn btn-new withtext" onclick="window.location.href ='userprofile.php?user_id=<?php echo $_SESSION['alias_email']; ?>'"><i class="fa fa-user fa-2x"></i>&nbsp; My Profile</button></li>
        <li><button type="button" class="btn btn-new withtext" onclick="window.location.href ='try_logout.php'"><i class="fa fa-sign-out fa-2x"></i>&nbsp; Log Out</button></li>
        <!-- top nav link without text -->
        <li><button type="button" class="btn btn-new notext" onclick="window.location.href ='dashboard.php'"><i class="fa fa-home fa-2x"></i></button></li>
        <li><button type="button" class="btn btn-new notext" onclick="window.location.href ='userprofile.php?user_id=<?php echo $_SESSION['user_id']; ?>'"><i class="fa fa-user fa-2x"></i></button></li>
        <li><button type="button" class="btn btn-new notext" onclick="window.location.href ='try_logout.php'"><i class="fa fa-sign-out fa-2x"></i></button></li>
    </ul>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php  if(isset($_SESSION['tracking_number'])){
                echo "";
                }else{
                    ?>
	                <li class="custom-nav-head-li" onclick="window.location.href='index.php'" onmouseover="this.style.cursor='pointer';"><i class="fa fa-dashboard fa-fw"></i> Homepage</li>
	                <?php 
	                $i=1;
                   $query = mysqli_query(dbConnect(), "SELECT * FROM ARRA_menu ORDER BY ARRA_menu_id") or die(mysqli_error(dbConnect()));
                    while($rows = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                        if(($_SESSION['access'] == "SuperAdmin") OR 
                           ( getAccessArea(getAccessArr(safe($_SESSION['user_id'])),$rows["ARRA_menu_short"]."-R") == $rows["ARRA_menu_short"]."-R") OR 
                           ( getAccessArea(getAccessArr(safe($_SESSION['user_id'])),$rows["ARRA_menu_short"]."-E") == $rows["ARRA_menu_short"]."-E") OR 
                           ( getAccessArea(getAccessArr(safe($_SESSION['user_id'])),$rows["ARRA_menu_short"]."-D") == $rows["ARRA_menu_short"]."-D")
                          ) {
                                    
                            if(( $_SESSION['access'] == "User") AND (checkStuStatus($_SESSION['user_id']))){ ?>
                                <li class="custom-nav-li">
                                    <a href="<?php echo  $rows["ARRA_menu_short"].".php";?>" class="secLink <?php if(basename($_SERVER['PHP_SELF']) == $rows["ARRA_menu_short"].".php") echo "active"; ?>"> <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i><?php echo  $rows["ARRA_menu_detail"];?></div></a>
                                </li> <?php
                            } elseif($_SESSION['access'] == "SuperAdmin" OR $_SESSION['access'] == "SubAdmin" OR $_SESSION['access'] == "Admin" OR $_SESSION['access'] == "User"){ ?>
                                <li class="custom-nav-li">
                                    <a href="<?php echo  $rows["ARRA_menu_short"].".php";?>" class="secLink <?php if(basename($_SERVER['PHP_SELF']) == $rows["ARRA_menu_short"].".php") echo "active"; ?>"> <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i> <?php echo  $rows["ARRA_menu_detail"];?></div></a>
                                </li> <?php
                            }
                    	}
                	} 
	            }

                if($_SESSION['access'] == "User"){
                	$query = mysqli_query(dbConnect(), "SELECT ARRA_applying_user FROM ARRA_applying WHERE ARRA_applying_user = '".$_SESSION['user_id']."'");
                                
                    if(mysqli_num_rows($query)) {
                                    
                                    echo "";
                                
                                }else{
                                    
                                    ?>
                                    <li class="custom-nav-li">
				                        <a href="http://www.aurora-intl.org" target="_blank" id="core" class="secLink"><div class="menutext-left"></div><div class="menutext-right" ><i class="fa fa-dashboard fa-fw"></i> Website</div> </a>
				                    </li>
				                    <li class="custom-nav-li">
				                        <a href="../sample/Procedure_And_Guideline1.pdf" target="_blank" id="yellow" class="secLink"><div class="menutext-left"></div><div class="menutext-right" ><i class="fa fa-dashboard fa-fw"></i> Admission Policy</div> </a>
				                    </li>
				                    <li class="custom-nav-li">
				                        <a href="../sample/FeePaymentPolicies.pdf" target="_blank" id="red" class="secLink"><div class="menutext-left"></div><div class="menutext-right" ><i class="fa fa-dashboard fa-fw"></i> Fee Policy</div> </a>
				                    </li>
				                    <!-- 
				                    <li>
				                        <a href="../sample/InstructionsforAdmissionForm.pdf" target="_blank" id="green" class="secLink"><div class="menutext-left"></div><div class="menutext-right" ><i class="fa fa-dashboard fa-fw"></i> Instructions</div> </a>
				                    </li> 
				                    -->
                    <?php
                    }
                } 

                if(isset($_SESSION['tracking_number']) && (  
            	basename($_SERVER['PHP_SELF']) == "sec0.php" OR 
                basename($_SERVER['PHP_SELF']) == "sec1.php" OR 
                basename($_SERVER['PHP_SELF']) == "sec4.php" OR 
                basename($_SERVER['PHP_SELF']) == "sec7.php" OR 
                basename($_SERVER['PHP_SELF']) == "sec9.php" OR 
                basename($_SERVER['PHP_SELF']) == "sec10.php") && 
                $_SESSION['access'] == "User") { ?>
                    
                    <li class="custom-nav-head-li"><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Application Form </a></li>
                    <li class="custom-nav-li">
                        <a href="sec0.php" id="sec1" class="secLink"><div class="menutext-left">1</div><div class="menutext-right">Basic Information</div> </a>
                    </li>
                    <li class="custom-nav-li">
                        <a href="sec1.php" id="sec2" class="secLink"><div class="menutext-left">2</div><div class="menutext-right">Student's Details</div></a>
                    </li>
                    <li class="custom-nav-li">
                        <a href="sec4.php" id="sec3" class="secLink"><div class="menutext-left">3</div><div class="menutext-right">Family Information</div></a>
                    </li>
                    <li class="custom-nav-li">
                        <a href="sec7.php" id="sec4" class="secLink"><div class="menutext-left">4</div><div class="menutext-right">Sibling Information</div> </a>
                    </li>
                    <li class="custom-nav-li">
                        <a href="sec9.php" id="sec5" class="secLink"><div class="menutext-left">5</div><div class="menutext-right">Learning Profile</div> </a>
                    </li>
                    <li class="custom-nav-li">
                        <a href="sec10.php" id="sec6" class="secLink"><div class="menutext-left">6</div><div class="menutext-right">Supporting Documents</div> </a>
                    </li>
                    <li class="custom-nav-li">
                        <a href="viewApplyForm.php?trackingid=<?php echo $_SESSION['tracking_number'];?>" id="sec12" class="secLink"><div class="menutext-left">7</div><div class="menutext-right">Application Review</div> </a>
                    </li> <?php 
                } ?>
               <!--  <li class="custom-nav-li">
                    <a href="../sample/InstructionsforAdmissionForm.pdf" target="_blank" id="green" class="secLink"><div class="menutext-left"></div><div class="menutext-right" ><i class="fa fa-dashboard fa-fw"></i> Instructions</div> </a>
                </li> -->
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>