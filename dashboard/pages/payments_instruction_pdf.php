<?php 
session_start();

if(!empty($_SESSION['user_id']) AND !empty($_REQUEST["trackingid"])){
include ('function.php');
dbConnect();
$html = "";

$html .= '<!DOCTYPE html><html lang="en"><head><title>..::AIS::..</title></head><body>';
$query = mysqli_query(dbConnect(),"SELECT ARRA_childs_detail_name, ARRA_childs_detail_dob FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_REQUEST["trackingid"]."'");
$childsData = mysqli_fetch_array($query, MYSQLI_ASSOC);
$html .= '<div>';
$html .= '<p style="align:center; text-align:center;"><img src="../../images/auroralogo.png"><br><br></p>';
$html .= '<table align="center" style="border: 1px solid #ddd; width:95%;">';
$html .= '<tr>';
$html .= '<th style="width:250px; text-align:left;">Name of Student</th>';
$html .= '<td>'.str_replace("@", " ", $childsData["ARRA_childs_detail_name"]).'</td>';
$html .= '</tr><tr><th style="width:250px; text-align:left;">Date of Birth</th>';
$html .= '<td>'.$childsData["ARRA_childs_detail_dob"].'</td>';
$html .= '</tr><tr><th style="width:250px; text-align:left;">Application Tracking Number</th>';
$html .= '<td>'.$_REQUEST["trackingid"].'</td>';
$html .= '</tr><tr><th style="width:250px; text-align:left;">Application Processing Fee</th>';
$html .= '<td>BDT 5,000.00</td></tr></table>';
$html .= '<p>&nbsp;</p><p>&nbsp;</p>';
$html .= '<p style="margin-left:25px;font-size:20px"><strong>Instructions:</strong></p>';
$html .= '<ul>';
$html .= '<li>Please print this document and present it to the school office to pay  the admission processing fee. It is important to bring this document when  submitting the payment to ensure we credit the payment correctly and to avoid  delays.</li>
	  <li>Please note that this admission processing fee is non-refundable and the  figure quoted above is inclusive of VAT.</li>
	  <li>The fee should be paid in cash (Bangladeshi Takas only).</li>
	  <li>Please make the payment at the Aurora International School premises: Gulshan 2, Road 83, House NE (L) 5 A & C. Dhaka 1212.</li>
	  <li>Payments will be accepted on working days during office hours (please  check our website at <a href="http://www.aurora-intl.org">www.aurora-intl.org</a> for our current  office hours).</li>
	  <li>If you have any questions, please contact us at +88-0184-123-4230 or <a href="mailto:info@aurora-intl.org">info@aurora-intl.org</a> during office hours  on any working day. </li>
	</ul>
</div>
</body>
</html>';
// include("mpdf/src/Mpdf.php");
require_once ('vendor/autoload.php');
$config = [
    'mode' => 'win-1252',
    'format' => 'A4',
    'default_font_size' => 0,
    'default_font' => '',
    'margin_left' => 20,
    'margin_right' => 15,
    'margin_top' => 48,
    'margin_bottom' => 25,
    'margin_header' => 10,
    'margin_footer' => 10,
    'orientation' => 'P'
];
$mpdf = new \Mpdf\Mpdf($config);
// $mpdf = new mPDF('win-1252', 'A4', '', '', 20, 15, 48, 25, 10, 10);

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

$mpdf->WriteHTML($html);

$mpdf->Output($_REQUEST["trackingid"], 'I');
}else{
  require_once 'login.php';
}
?>
