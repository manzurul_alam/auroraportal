<?php 
session_start();

if (!empty($_SESSION['user_id'])) {
    require_once 'function.php'; // Assuming function.php contains your database connection function

    $mysqli = dbConnect(); // Connect to the database

    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>..::AIS::..</title>
        <style>
            table td div {
                text-align: center;
                margin: 10px;
                padding: 0px 5px;
                cursor: pointer;
                border: 1px solid #FFF;
                border-radius: 6px;
            }

            table td div:hover {
                border: 1px solid #000;
                border-radius: 6px;
            }

            .btn-width-yes {
                width: 100px;
                text-align: center;
                background-color: #449D44!important;
                color: #fff !important;
                margin-right: 10px;
            }

            .btn-width-no {
                width: 70px;
                text-align: center;
                background-color: #286090!important;
                color: #fff !important;
            }

            .btn-width-cancel {
                width: 100px;
                text-align: center;
                background-color: #C9302C !important;
                color: #fff !important;
            }

            .panel-footer {
                background-color: #fff;
            }

            .line {
                height: 45px;
                text-indent: 10px;
            }

            .line:hover {
                background-color: #EEE;
            }
        </style>
    </head>

    <body>
        <?php
        $query = $mysqli->query("SELECT * FROM ARRA_notice WHERE id='" . $mysqli->real_escape_string($_REQUEST['nid']) . "'");
        $row = $query->fetch_assoc();
        ?>
        <h1><?php echo $row["title"]; ?></h1>
        <p><?php echo nl2br($row["body"]); ?></p>
        <p><?php if (isset($row["createlink"]) && $row["createlink"] != "" && $row["createlink"] != "N/A") echo '<a href="' . $row["createlink"] . '" target="_blank">' . $row["createlink"] . '</a>'; ?></p>
        <p><?php if (isset($row["salutation"]) && $row["salutation"] != "" && $row["salutation"] != "N/A") echo '<p>' . $row["salutation"] . '</p>'; ?></p>
        <?php
        $fileQuery = $mysqli->query("SELECT * FROM attachments WHERE notice_id = '" . $mysqli->real_escape_string($row["id"]) . "'");
        if ($fileQuery->num_rows) { ?>
            <hr><b><input type="checkbox" id="select_all" />&nbsp; Attachment(s)</b><br>
            <table width=100%>
                <?php $size = "width=35px height=40px";
                $fileCount = 1;
                while ($f = $fileQuery->fetch_assoc()) { ?>
                    <tr class="line">
                        <td width=15px><input name="chk_download[]" data-id="<?php echo $f["sl"]; ?>" data-appliedfor="<?php echo $row["appliedfor"]; ?>" value="<?php echo $photo_loc; ?>" type="checkbox" class="checkbox" /></td>
                        <td width=40px><?php
                                        $fileExt = explode(".", $f["file_name"]);
                                        if (getImgFileValid($fileExt[1])) { ?>
                                <img src="../noticeboard/<?php echo $f["file_name"]; ?>" <?php echo $size; ?> /><?php }
                                                                                            elseif ($fileExt[1] == "doc" || $fileExt[1] == "docx") { ?><img src="../images/doc.png" <?php echo $size; ?> /><?php } elseif ($fileExt[1] == "xls" || $fileExt[1] == "xlsx") { ?><img src="../images/xls.png" <?php echo $size; ?> /><?php } elseif ($fileExt[1] == "ppt" || $fileExt[1] == "pptx") { ?><img src="../images/ppt.png" <?php echo $size; ?> /><?php } elseif ($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" <?php echo $size; ?> /><?php } else { ?><img src="../images/other.png" <?php echo $size; ?> /><?php } ?>
                        </td>
                        <td><a href="<?php echo "../noticeboard/" . $f["file_name"]; ?>" target="_blank"><b><?php if ($f["file_title"] != "" || $f["file_title"] != NULL) echo $f["file_title"]; else echo $f["file_name"]; ?></b></a></td>
                        <!--<td><?php echo "../noticeboard/".$f["file_name"]; ?></td>-->
                        <td width="100px"><?php echo number_format((filesize("../noticeboard/" . $f["file_name"]) / 1024), 2) . " KB"; ?></td>
                    </tr><?php
                            $fileCount++;
                        }
                    } ?>
            </table>
            <br /><br /><br />
            <div class="panel-footer">
                <button class="btn btn-success btn-width-cancel pull-right" onClick="window.location.reload();">Cancel</button>&nbsp;&nbsp;&nbsp;
                <a href="" id="btn_download_chk"><button class="btn btn-success btn-width-yes pull-right" id="bt_download_chk">Download</button></a>
                <a href="doctopdf.php?id=<?php echo $row["id"]; ?>&type=<?php echo $row["type"]; ?>" target="_blank"><button class="btn btn-yellow btn-width-yes pull-right">Print</button></a>
            </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#select_all').on('click', function() {
                if (this.checked) {
                    $('.checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $('.checkbox').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('.checkbox').on('click', function() {
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $('#select_all').prop('checked', true);
                } else {
                    $('#select_all').prop('checked', false);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#bt_download_chk").prop('disabled', true);
            $(':checkbox').change(function() {
                var count = $('input[name="chk_download[]"]:checked').length;
                if (count > 0) {
                    var files = new Array();
                    var appliedfor = $(this).data("appliedfor");
                    $('input[name="chk_download[]"]:checked').each(function() {

                        files.push($(this).data("id"));


                    });
                    $("#bt_download_chk").prop('disabled', false);
                    $("#btn_download_chk").attr("href", "download.php?appliedfor=" + appliedfor + "&files=" + files);
                    //console.log(jsarray);
                } else {
                    $("#bt_download_chk").prop('disabled', true);
                    $("#btn_download_chk").attr("href", "");
                }
            });

        });
    </script>

    </html>

    <?php
} else {
    require_once 'login.php';
}
?>
