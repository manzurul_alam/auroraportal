<?php
    session_start();

    if (!empty($_SESSION['user_id'])) {
        include ('function.php');
        dbConnect();

        if ($_SESSION['access'] != "User") {
            $album_gallery = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_album_id='" . $_GET["id"] . "'");
            $album_photos_grid = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_album_id='" . $_GET["id"] . "'");
            $album_photos_list = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_album_id='" . $_GET["id"] . "'");
        } else {
            $album_gallery = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_photo_status=1 and ARRA_album_id='" . $_GET["id"] . "'");
            $album_photos_grid = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_photo_status=1 and ARRA_album_id='" . $_GET["id"] . "'");
            $album_photos_list = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_photo_status=1 and ARRA_album_id='" . $_GET["id"] . "'");
        }
        $album_info = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album where ARRA_album_id='" . $_GET["id"] . "'");
    
?>

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 //EN">
	
    <html lang="en">
		
        <head>
			
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
            <title>..::AIS::..</title>
			
            <!-- Bootstrap Core CSS -->
            <link href="../responsive/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link href="../responsive/bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">
			
            <!-- MetisMenu CSS -->
            <link href="../responsive/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
			
            <!-- Custom CSS -->
            <link href="../dist/css/sb-admin-2 before responsive.css" rel="stylesheet">
			
            <!-- Galleria Plugin 
				<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
				<script src="galleria/galleria-1.4.2.min.js"></script>
				<link href="galleria/thumbnail-gallery.css" rel="stylesheet">
			-->
            <!-- Bootstrap Modal Carousel  
				<link rel="stylesheet" href="bootstrap-modal-carousel/dist/css/bootstrap-modal-carousel.min.css">
				<script src="bootstrap-modal-carousel/dist/js/bootstrap-modal-carousel.js"/></script/>
			-->
            <style>
				
                .galleria-lightbox-prevholder{display: none;}
                .galleria-lightbox-nextholder{display: none;}
				
				
                .fixed_width{width: 150px;overflow: auto;}
                .btn-arra{border-color: #156059 !important; color: #FFF !important; background-color: #156059 !important; float: right !important;}
                .active{background-color: #156059 !important; }
                .carousel-indicators li{border-color: #156059 !important;}
                .slide_desr{color: #156059 !important;}
				
			</style> 
            <!-- Add fancyBox -->
            <link rel="stylesheet" href="dist/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
			
			
            <!-- Custom Fonts -->
            <link href="../responsive/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
			
		</head>
		
        <body>
			
            <div id="wrapper">
				
                <!-- Navigation -->
                <?php include('nav before responsive.php') ?>
                <!-- End Navigation -->
				
				
                <div id="page-wrapper">
					
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Pictures</h1>
						</div>
                        <div class="col-lg-12">
                            <?php if ($_SESSION['access'] == "User" || $_SESSION['access'] != "User") { ?>
                                <?php
									if (isset($message)) {
										echo '<div class="row voffset2" id="messageDiv">
										<div class="col-md-6 col-md-offset-4">
										<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>' . $message . '
										</div>
										</div>
										</div>';
										//echo $message;
										unset($message);
									}
								?>
								
                                <div class="panel panel-arra">
                                    <div class="panel-heading">
										
                                        <?php include('loginname.php'); ?>
										
										
									</div>
                                    <!----------------mejba------------->
									<?php if ($_SESSION['access'] == "User") { ?>
    <div class="row custom-padding" style="padding-left: 15px;">
        <div class="col-lg-12 form-group"></div>
        <?php 
            $connection = dbConnect(); // Assuming dbConnect() is your database connection function
            $query = mysqli_query($connection, "SELECT * FROM instruction WHERE type = 'GALLERY'") or die(mysqli_error($connection));
            $row = mysqli_fetch_array($query, MYSQLI_ASSOC);
        ?>
        <div class="col-lg-12 form-group">
            <?php echo "<b style='float: left;'>Instruction:&nbsp;</b>" . htmlspecialchars_decode($row["instruction"]); ?>
        </div>
    </div>
<?php } ?>

									

                                    <!-------------end---mejba------------->
                                    <div class="panel-body">
                                        <div class="row" style="background-color:#A8D4B5;">
											
											
                                            <?php

while ($row = mysqli_fetch_assoc($album_info)) {
	$alb_name = $row['ARRA_album_name'];
	$alb_descr = htmlspecialchars_decode($row['ARRA_album_description']);

	$alb_cat = $row['ARRA_album_category'];
	$alb_pubdate = $row['ARRA_album_publishdate'];

	$query2 = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_album_id='" . $row["ARRA_album_id"] . "'") or die(mysqli_error(dbConnect()));
	$num_rows = mysqli_num_rows($query2);

	echo "<div class=\"col-lg-6\"><b>Title:</b> $alb_name</div>";
	echo "<div class=\"col-lg-6\"><b>Category:</b> $alb_cat</div>";
	echo "<div class=\"col-lg-6\"><b style='float: left;'>Description:&nbsp;</b>$alb_descr</div>";
	echo "<div class=\"col-lg-6\"><b>Published Date:</b> $alb_pubdate</div>";
}
												// while ($row = mysql_fetch_assoc($album_info)) {
												// 	$alb_name = $row['ARRA_album_name'];
												// 	$alb_descr = htmlspecialchars_decode($row['ARRA_album_description']);
													
												// 	//if(strlen($alb_descr)>40){$alb_descr_f=substr($alb_descr,0,40);}
												// 	//$alb_for= $row['ARRA_album_for'];
												// 	$alb_cat = $row['ARRA_album_category'];
												// 	$alb_pubdate = $row['ARRA_album_publishdate'];
												// 	//$alb_credate= $row['ARRA_album_createdate'];
												// 	$query2 = mysql_query("SELECT * FROM ARRA_album_photo where ARRA_album_id='" . $row["ARRA_album_id"] . "'") or die(mysql_error());
												// 	$num_rows = mysql_num_rows($query2);
													
												// 	echo "<div class=\"col-lg-6\"><b>Title:</b> $alb_name</div>";
												// 	//echo "<b>Album For:</b> <p>$alb_for</p>";
												// 	echo "<div class=\"col-lg-6\"><b>Category:</b> $alb_cat</div>";
												// 	echo "<div class=\"col-lg-6\"><b style='float: left;'>Description:&nbsp;</b>$alb_descr</div>";
												// 	echo "<div class=\"col-lg-6\"><b>Published Date:</b> $alb_pubdate</div>";
												// 	//echo "<b>Album Create Date:</b> <p>$alb_credate</p>";
												// 	//echo "<p><b>Number of Photos:</b> $num_rows</p>"; 
												// }
											?>
											
										</div>
										
                                        <!-- Album slideshow -->
										
                                        <div class="modal fade modal-fullscreen force-fullscreen" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="background-color: #227B72;color: #FFFFFF;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" >Album Slideshow</h4>
													</div>
                                                    <div class="modal-body">
                                                        <div id="myCarousel" class="carousel slide carousel-fit" data-ride="carousel">
                                                            <!-- Indicators -->
                                                            <ol class="carousel-indicators" style="margin-bottom: -5%;">
                                                                <?php
																	 $num_rows = mysqli_num_rows($album_gallery);
																	for ($x = 0; $x < $num_rows; $x++) {
																		echo "<li data-target=\"#myCarousel\" data-slide-to=\"$x\" class=\"active\"></li>";
																	}
																?>
															</ol>
															
                                                            <!-- Wrapper for slides -->
															
                                                            <div class="carousel-inner">
																
															<?php
    $ci = 0;
    while ($row = mysqli_fetch_assoc($album_gallery)) {
        if ($ci == 0) {
            echo '<div class="item active">';
        } else {
            echo '<div class="item">';
        }
        $photo_loc = $row['ARRA_photo_location'];
        $fname = basename($photo_loc);
        $dname = dirname($photo_loc);
        $thumb_loc = $dname . "/thumb_" . $fname;
        
        $photo_name = $row['ARRA_photo_name'];
        $photo_descr = $row['ARRA_photo_description'];
        echo "<a title=\"Photo Title: $photo_name , Description: $photo_descr\" class=\"thumbnail fancybox\" rel=\"group\" href=\"$photo_loc\">";
        
        echo "<img style=\"width: 100%; height: 100%;\" class=\"img-responsive\" src=\"$photo_loc\">";
        
        echo '</a>';
        
        echo "</div>";
        $ci++;
    }
?>

																
															</div>
                                                            <!-- Controls -->
                                                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                                <span class="glyphicon glyphicon-chevron-left"></span>
															</a>
                                                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                                <span class="glyphicon glyphicon-chevron-right"></span>
															</a>
														</div>
														
													</div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														
													</div>
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
                                        <br />
                                        <!--
											<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#home">Grid view</a></li>
											<li><a data-toggle="tab" href="#menu1">List view</a></li>
											
											</ul>
											
											<div class="tab-content">
										<div id="home" class="tab-pane fade in active">  -->
                                        <!-- Grid view -->
                                        <div id="div_gridview" class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-arra voffset2">
                                                    <div class="panel-heading">
                                                        Album Content
													</div>
                                                    <div class="panel-body">
                                                        <div class="dataTable_wrapper">
                                                            <table class="table table-bordered table-hover" id="dataTables-grid">
                                                                <thead>
                                                                    <tr class="text-center">
                                                                        <th>Pictures</th>
																		
																	</tr>
																	
																</thead>
                                                                <tbody>
																<?php
    while ($row = mysqli_fetch_assoc($album_photos_grid)) {
        echo '<tr class="col-lg-3 col-md-4 col-xs-6 thumb">';
        echo '<td>';
        $photo_loc = ($row['ARRA_photo_location']);
        $fname = basename($photo_loc);
        $dname = dirname($photo_loc);
        $thumb_loc = $dname . "/thumb_" . $fname;
        $photo_name = substr($row['ARRA_photo_name'], 0, 15);
        $photo_id = $row['ARRA_photo_id'];
        $photo_descr = $row['ARRA_photo_description'];
        echo "<input class=\"chk_grid\" name=\"chk_download[]\" data-id=\"$photo_id\" value=\"$photo_loc\" type=\"checkbox\">";
        echo "<a title=\"Photo Title: $photo_name , Description: $photo_descr\" class=\"thumbnail fancybox\" rel=\"group\" href=\"$photo_loc\">";
        
        echo "<img style=\"width: 100%; height: 100%;\" class=\"img-responsive\" src=\"$thumb_loc\">";
        
        echo '</a>';
        echo "$photo_name";
        echo '</td>';
        echo '</tr>';
    }
?>

																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!--	</div>
											List view 
											<div id="menu1" class="tab-pane fade">
											<div id="div_listview" class="row">
											<div class="col-lg-12">
											<div class="panel panel-arra voffset2">
											<div class="panel-heading">
											Album Content
											</div>
											<div class="panel-body">
											<div class="dataTable_wrapper">
											<table class="table table-bordered table-hover" id="dataTables-list">
											<thead>
											<tr class="text-center">
											<th><input type="checkbox" id="selectall_photo"/>Select All </th>
											
											<th>Name</th>
											<th>Description</th>
											<th>Published Date</th>
											<th>Download</th>
											</tr>
											</thead>
											<tbody>
											<?php
												while ($row = mysqli_fetch_assoc($album_photos_list)) {
													$photo_loc = $row['ARRA_photo_location'];
													$fname = basename($photo_loc);
													$dname = dirname($photo_loc);
													$thumb_loc = $dname . "/thumb_" . $fname;
													$photo_name = $row['ARRA_photo_name'];
													$photo_id = $row['ARRA_photo_id'];
													$photo_descr = $row['ARRA_photo_description'];
													$photo_credate = $row['ARRA_album_publishdate'];
													
													echo '<tr class="">';
													echo "<td><input class=\"chk_list\" name=\"chk_download2[]\" data-id=\"$photo_id\" value=\"$photo_loc\" type=\"checkbox\"></div></td>";
													
													echo "<td><div class=\"fixed_width\">$photo_name</div></td>";
													echo "<td><div class=\"fixed_width\">$photo_descr</div></td>";
													echo "<td><div class=\"fixed_width\">$photo_credate</div></td>";
													echo "<td><a id=\"btn_download_chk3\" href=\"download_gallery.php?files=$photo_id\">Download</a></td>";
													echo '</tr>';
												}
											?>
											</tbody>
											</table>
											</div>
											</div>
											</div>
											</div>
											</div>
										-->
									</div>
									
									
									
									
									
									
								</div>
							</div>
						</div>
						
						
						
					<?php } ?>
					
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
		
		<!-- Modal -->
		<div class="modal fade" id="slideshowModal" tabindex="-1" role="dialog" aria-labelledby="publish" aria-hidden="true">
			
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header btn-arra" >
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel2">Album Slideshow</h4>
					</div>
					<div class="modal-body ">
						<div class="galleria">
							
							<?php
								while ($row = mysqli_fetch_assoc($album_gallery)) {
									$photo_loc = $row['ARRA_photo_location'];
									$fname = basename($photo_loc);
									$dname = dirname($photo_loc);
									$thumb_loc = $dname . "/thumb_" . $fname;
									
									$photo_name = $row['ARRA_photo_name'];
									$photo_descr = $row['ARRA_photo_description'];
									
									//echo "<img src=\"$photo_loc\" data-title=\"$photo_name\" data-description=\"$photo_descr\">";
									echo "<a href=\"$photo_loc\"><img src=\"$thumb_loc\" data-title=\"Title: $photo_name, Description: $photo_descr\" data-description=\"$photo_descr\"></a>";
								}
							?>
							
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default antoclose3 btn-width-cancel" data-dismiss="modal">Cancel</button>
						
					</div>
				</div>
				
			</div>
			
		</div>
		
		
		
		<!-- /#wrapper -->
		
		<!-- jQuery -->
		
		
		<script src="../responsive/bower_components/jquery/dist/jquery.min.js"></script>
		<script src="../responsive/bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
		<!--<script type="text/javascript">
			$(document).ready(function() {
			$('#datetimepicker1').datetimepicker({
			yearOffset:0,
			lang:'en',
			timepicker:false,
			format:'Y-m-d',
			formatDate:'Y-m-d',// and tommorow is maximum date calendar
			//minDate:'-1970/01/021'
			});
			
			});
		</script> -->
		
		<!-- Bootstrap Core JavaScript -->
		<script src="../responsive/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>\
		
		<!-- DataTables JavaScript -->
		<script src="../responsive/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<script src="../responsive/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
		
		<!-- Metis Menu Plugin JavaScript -->
		<script src="../responsive/bower_components/metisMenu/dist/metisMenu.min.js"></script>
		
		<!-- Custom Theme JavaScript -->
		<script src="../responsive/dist/js/sb-admin-2.js"></script>
		<script src="../responsive/dist/js/applyforchk.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function () {
				
				$("#edit_album").hide();
				$("#canceledit_album").hide();
				$("select[name='album_for']").attr('disabled', false);
				$("select[name='album_category']").attr('disabled', false);
				$("#canceledit_album").click(function () {
					$("#edit_album").hide();
					$("#create_album").show();
					$("#canceledit_album").hide();
					$("select[name='album_for']").attr('disabled', false);
					$("select[name='album_category']").attr('disabled', false);
				});
				$(".btn-edit").click(function () {
					$(window).scrollTop(0);
					$("#edit_album").show();
					$("#create_album").hide();
					$("#canceledit_album").show();
					$("select[name='album_for']").attr('disabled', true);
					$("select[name='album_category']").attr('disabled', true);
					var data = $.parseJSON($(this).attr('data-button'));
					$("input[name='album_id_edit']").val(data.id);
					$("input[name='album_for_edit']").val(data.ab_for);
					$("input[name='album_category_edit']").val(data.ab_cat);
					$("select[name='album_for']").val(data.ab_for);
					
					$("select[name='album_category']").val(data.ab_cat);
					$("input[name='album_name']").val(data.ab_name);
					$("textarea[name='album_descr']").val(data.ab_descr);
					$("input[name='album_pubdate']").val(data.ab_pubdate);
				});
				
				// $("#div_gridview").hide('slow');
				// $("#div_listview").hide('slow');
				// $("#btn_gridview").click(function(){
				// $("#div_gridview").show('slow');
				// $("#div_listview").hide('slow');
				// });
				// $("#btn_listview").click(function(){
				// $("#div_gridview").hide('slow');
				// $("#div_listview").show('slow');
				// });
				
				
			});
		</script>
		
		<script type="text/javascript" src="dist/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
		<link rel="stylesheet" href="dist/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
		<script type="text/javascript" src="dist/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<script type="text/javascript" src="dist/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		
		<link rel="stylesheet" href="dist/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
		<script type="text/javascript" src="dist/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<script type="text/javascript">
			$(document).ready(function () {
				
				$('#dataTables-list').dataTable({
					"bFilter": false,
				});
				$('#dataTables-grid').dataTable({
					"bFilter": false,
				});
				
				$("#dataTables-grid_length").closest(".row").append('<button type="button" style="margin-right: 15px ! important;" class="btn btn-arra" data-toggle="modal" data-target="#myModal" data-local="#myCarousel">View in slideshow</button>');
				<?php if ($_SESSION['access'] != "User") { ?>
					$("#dataTables-grid_length").closest(".row").append('<a id="btn_del_chk" href="" style="margin-right: 15px ! important;color: red;">Delete</a>');
				<?php } ?>
				$("#dataTables-grid_length").append('<a id="btn_download_chk" href="" style="padding:15px">Download</a>');
				
				$("#dataTables-list_length").append('<button type="button" class="btn btn-arra" data-toggle="modal" data-target="#myModal" data-local="#myCarousel">View in slideshow</button>');
				$("#dataTables-list_length").append('<a id="btn_download_chk2" href="" style="padding:15px">Download</a>');
				
				$(".fancybox").fancybox({
					helpers: {
						title: {
							type: 'inside',
							position: 'bottom'
						},
					},
					nextEffect: 'fade',
					prevEffect: 'fade'
				});
				
				$("#btn_download_chk").css("display", "none");
				$("#btn_del_chk").css("display", "none");
				//$('.chk_grid').change(function() {
				$("body").on("change", ".chk_grid", function (e) {
					console.log(this);
					var count = $('input[name="chk_download[]"]:checked').length;
					if (count > 0) {
					var files = new Array();
					$('input[name="chk_download[]"]:checked').each(function () {
					
					files.push($(this).data("id"));
					
					});
					
					$("#btn_download_chk").css("display", "");
					$("#btn_download_chk").attr("href", "download_gallery.php?files=" + files);
					$("#btn_del_chk").css("display", "");
					
					$("#btn_del_chk").attr("href", "delete_photos.php?files=" + files + "&aid=" + "<?php echo $_GET['id']; ?>");
					//console.log(jsarray);
					} else {
					$("#btn_download_chk").attr("href", "");
					$("#btn_download_chk").css("display", "none");
					$("#btn_del_chk").attr("href", "");
					$("#btn_del_chk").css("display", "none");
					
					}
					});
					$("#btn_download_chk2").css("display", "none");
					//$('.chk_list').change(function() {
					$("body").on("change", ".chk_list", function (e) {
					console.log(this);
					var count = $('input[name="chk_download2[]"]:checked').length;
					if (count > 0) {
					var files = new Array();
					$('input[name="chk_download2[]"]:checked').each(function () {
					
					files.push($(this).data("id"));
					
					});
					$("#btn_download_chk2").css("display", "");
					$("#btn_download_chk2").attr("href", "download_gallery.php?files=" + files);
					//console.log(jsarray);
					} else {
					
					$("#btn_download_chk2").attr("href", "");
					$("#btn_download_chk2").css("display", "none");
					}
					});
					//$('#selectall_photo').click(function(event) {
					$("body").on("click", "#selectall_photo", function (e) {
					
					if (this.checked) {
					var files = new Array();
					$('.chk_list').prop('checked', true);
					$(".chk_list").each(function () {
					files.push($(this).data("id"));
					});
					
					$("#btn_download_chk2").attr("href", "download_gallery.php?files=" + files);
					$("#btn_download_chk2").css("display", "");
					} else {
					$('.chk_list').prop('checked', false);
					
					$("#btn_download_chk2").attr("href", "");
					$("#btn_download_chk2").css("display", "none");
					}
					});
					
					
					});
					</script>
					
					<!--
					<link rel="stylesheet" href="jQuery-Feature-Carousel/css/feature-carousel.css" charset="utf-8" />
					<script src="jQuery-Feature-Carousel/js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
					<script src="jQuery-Feature-Carousel/js/jquery.featureCarousel.min.js" type="text/javascript" charset="utf-8"></script>
					<script type="text/javascript">
					$(document).ready(function() {
					var jQuery17 = $.noConflict(true);
					var carousel = jQuery17("#carousel").featureCarousel({
					// include options like this:
					// (use quotes only for string values, and no trailing comma after last option)
					// option: value,
					// option: value
					largeFeatureWidth :  500,
					largeFeatureHeight:     300,
					smallFeatureWidth:    100,
					smallFeatureHeight:     100,
					sidePadding:200,
					
					});
					
					});
					</script> -->
					</body>
					
					</html>
					<?php
					} else {
					require_once 'login.php';
					}
					?>
