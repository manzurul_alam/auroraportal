<?php 
session_start();
if(!empty($_SESSION['user_id'])){
    include ('function.php');
    dbConnect();
    $sucId=0;
    $count=0;
    $connection = dbConnect();
    $getAccessArea = mysqli_query($connection, "SELECT aurora_user_access_area FROM users WHERE aurora_signup_username = '" . $_SESSION['user_id'] . "'") or die(mysqli_error($connection));

    if (mysqli_num_rows($getAccessArea) == 1) {
        $getrows = mysqli_fetch_array($getAccessArea);
        $accessArry = $getrows['aurora_user_access_area'];

        $explodeArry = explode(",", $accessArry);
        $pushVal = array();
        foreach ($explodeArry as $value) {
            $newAccArry = explode("-", $value);
            foreach ($newAccArry as $val) {
                if ($newAccArry[0] == $val) {
                    $pushVal[] = $newAccArry[0];
                }
            }
        }
    }

    if(isset($_POST['usersave'])){
        $username = safe(strtolower($_POST['email']));
        $emailalias = $_POST['fname']." ".$_POST['lname'];
        $almobile = $_POST['almobile'];
        $passwordalias = $_POST['passalias'];
        $notification = $_POST['notification'];
        
        if($_POST['accessName'] == ""){
            $accesName = "User";
        } else {
            $accesName = $_POST['accessName'];
        }
        
        // Initialize $access as an array to avoid foreach warning
        $access = []; 
        
        if(isset($_POST['acss']) && is_array($_POST['acss'])) {
            $access = $_POST['acss'];
        }
        
        $string = ""; // Initialize $string variable
        foreach($access as $value){
            if(isset($_POST[$value.'read']) && isset($_POST[$value.'edit'])) {
                if($_POST[$value.'read'] == ""){
                    $string .= $_POST[$value.'edit'].",".$_POST[$value.'delete'].",";
                } else if($_POST[$value.'edit'] == ""){
                    $string .= $_POST[$value.'read'].",".$_POST[$value.'delete'].",";
                } else if($_POST[$value.'delete'] == ""){
                    $string .= $_POST[$value.'read'].",".$_POST[$value.'edit'].",";
                } else {
                    $string .= $_POST[$value.'read'].",".$_POST[$value.'edit'].",".$_POST[$value.'delete'].",";
                }
            }
        }
        
        $newStr = trim($string, ","); // Remove trailing comma from $string
        
        $paswword = md5(safe($_POST['password']));
        $validationKey = rand(11111111,999999999); 
        $aes256Key = md5(trim($validationKey));
        $info = $_POST['firstName'].','.$_POST['lastName'].','.$_POST['mobile'];
        
        if (!empty($username) && !empty($paswword) && !empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['mobile'])) {
            $sqlQuery = "INSERT INTO users (aurora_signup_username, aurora_signup_firstname, aurora_signup_lastname, aurora_signup_email, aurora_signup_mobile, aurora_signup_passwd, aurora_signup_validationKey, aurora_signup_status, aurora_user_access, aurora_signup_creator, aurora_user_access_area) 
                         VALUES ('" . $username . "','" . safe($_POST['firstName']) . "','" . safe($_POST['lastName']) . "','" . safe(strtolower($_POST['email'])) . "','" . safe($_POST['mobile']) . "','" . $paswword . "','" . $aes256Key . "','CONFIRMED','" . $accesName . "','" . $_SESSION['user_id'] . "','" . $newStr . "')";

            $queryResult = mysqli_query($connection, $sqlQuery);

            if ($queryResult) {
                mysqli_query($connection, "INSERT INTO pass_info (user_id, pass, insert_time) VALUES ('" . $username . "','" . $_POST['password'] . "',CURRENT_TIMESTAMP)");

                $query = mysqli_query($connection, "SELECT aurora_sign_id FROM users WHERE aurora_signup_username='" . $username . "'");
                $nid = mysqli_fetch_array($query)['aurora_sign_id'];
                $name = safe($_POST['firstName']) . " " . safe($_POST['lastName']);
                mysqli_query($connection, "INSERT INTO user_alias (user_id, email, password, name, mobilenumber, notification) VALUES ('" . $nid . "','" . $username . "','" . $paswword . "', '" . $name . "', '" . safe($_POST['mobile']) . "','2')");

                if (!empty($_POST['alias']) && !empty($passwordalias) && !empty($emailalias) && !empty($almobile) && !empty($notification)) {
                    mysqli_query($connection, "INSERT INTO user_alias (user_id, email, password, name, mobilenumber, notification) VALUES ('" . $nid . "','" . $_POST['alias'] . "', '" . md5($passwordalias) . "', '" . $emailalias . "','" . $almobile . "','" . $notification . "')");
                }

                $sucId = 1;
            } else {
                echo '<script type="text/javascript">alert("**ERROR : Sorry!!! Sign Up is not completed. Please avoid insert  duplicate entries");</script>';
            }
        } else {
            echo '<script type="text/javascript">alert("**ERROR : Empty field(s).");</script>';
        }
    }

    if(isset($_POST['save'])){
        $username = safe($_POST['email']);
        $paswword = md5(safe($_POST['password']));
        $validationKey = rand(11111111,999999999); 
        $aes256Key = md5(trim($validationKey));
        
        if(!empty($username) && !empty($paswword) && !empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['mobile'])){
            $sqlQuery = "INSERT INTO users (aurora_signup_username,aurora_signup_firstname,aurora_signup_lastname,aurora_signup_email,aurora_signup_mobile,aurora_signup_passwd,aurora_signup_validationKey,aurora_signup_status) 
                        VALUES ('".$username."','".safe($_POST['firstName'])."','".safe($_POST['lastName'])."','".safe($_POST['email'])."','".safe($_POST['mobile'])."','".$paswword."','".$aes256Key."','CONFIRMED')"; 
                        
            $queryResult = mysqli_query($connection,$sqlQuery);
            if($queryResult){
                if(signUpMgs($username,safe($_POST['password']),$aes256Key)==true){
                    $sucId=1;  
                } else {
                    $sucId=2;  
                }
            } else {
                $sucId=2;
            }
        } else {
            echo '<script type="text/javascript">alert("**ERROR : Empty field(s).");</script>';
        }
    }

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../responsive/bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../responsive/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- select2 -->
    <link href="bootstrap-fileinput/css/select/select2.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2 before responsive.css" rel="stylesheet">
    <!-- DATA TABLES -->
    <link href="../responsive/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css"
        rel="stylesheet" type="text/css" />

    <!-- Custom Fonts -->
    <link href="../responsive/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../responsive/favico/favicon-16x16.png">
    <!-- File input -->
    <link href="bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <!-- jQuery v2.1.3 -->
    <script src="../responsive/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<script src="../bower_components/jquery/dist/jquery.min.js"></script>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
			<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>-->
    <script src="bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
    <!-- Lightbox -->
    <link href="lightbox/dist/ekko-lightbox.css" rel="stylesheet">



    <style type="text/css">
    .btn.btn-primary.btn-file {
        background: #196960
    }

    .modal-header {
        background-color: #156059;
        color: #fff;
    }


    .panel-body-custom {
        padding-bottom: 0px;
        padding-top: 0px;
    }

    .btn-danger {
        background-color: #FB593E;
        color: #fff !important;

    }

    .custom-padding {
        padding-top: 0px;
        padding-bottom: 0px;
        padding-left: 31px;
        padding-right: 31px;

    }

    #lecture {
        width: 500px;
        height: 100px;
    }

    #salutation {
        width: 500px;
        height: 100px;
    }

    #email_salutation {
        width: 645px;
        height: 100px;
    }

    #email_body {
        width: 645px;
        height: 100px;
    }

    .tb-btn {
        text-decoration: none;
        border-radius: 4px;
        color: #FFFFFF;
        background-color: #156059;
    }
    </style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav before responsive.php') ?>
        <!-- End Navigation -->


        <div id="page-wrapper">
            <div class="row">
                <!-- Progress Bar 
                            <div class="row voffset2" style="margin-left: 5%;">
                                    <?php //include('appMenu.php'); ?>
                            </div>
                             End Progress Bar -->
                <div class="col-lg-12">
                    <h1 class="page-header">User Management</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-arra">
                        <div class="panel-heading">
                            <?php include('loginname.php'); ?>
                        </div>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li style="width: 130px;text-align: center" class="active"><a
                                        style="color: inherit;border: 1px solid;" href="#home-pills" class="tb-btn"
                                        data-toggle="tab">All User</a>
                                </li>
                                <?php 
                                if($_SESSION['access'] == "SuperAdmin" OR getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"UM-E") == "UM-E" ){  ?>
                                <li style="width: 130px;text-align: center"><a style="color: inherit;border: 1px solid;"
                                        href="#profile-pills" class="tb-btn" data-toggle="tab">Add User</a></li>
                                <?php
                                }
                                ?>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div class="tab-pane fade in active" id="home-pills">
                                    <h2 class="text-center">All User(s)</h2>
                                    <div class="dataTable_wrapper voffset2">
                                        <table class="table table-striped table-bordered table-hover"
                                            id="dataTables-user">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width: 40.8889px;">Sl</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Email</th>
                                                    <th class="text-center" style="width: 136.8889px;">Account Status</th>
                                                    <th class="text-center">Application Status</th>
                                                    <th class="text-center" style="width: 130.8889px;">Access Area</th>
                                                    <th class="text-center" style="width: 130.8889px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                              $i = 1;
                                              $userQUery = "SELECT * FROM users " . getAcess($_SESSION['access'], safe($_SESSION['user_id']));
                                              $userRes = mysqli_query($connection, $userQUery); // Assuming $conn is your mysqli connection
                                              if ($userRes && mysqli_num_rows($userRes) > 0) { // Check if query was successful
                                                  while ($userRow = mysqli_fetch_array($userRes)) { // Use mysqli_fetch_array for mysqli result
                                                      $userName = $userRow["aurora_signup_username"];
                                                      $userAccess = $userRow['aurora_user_access'];
                                                      $accessArea = $userRow['aurora_user_access_area'];
                                                      $accessArray = explode(",", $accessArea);
                                              ?>
                                                <tr>
                                                    <td class="text-center" style="vertical-align: middle">
                                                        <?php echo $i; ?></td>
                                                    <td style="vertical-align: middle">
                                                        <?php echo $userRow["aurora_signup_firstname"] . ' ' . $userRow["aurora_signup_lastname"]; ?>
                                                    </td>
                                                    <td style="vertical-align: middle">
                                                        <?php echo $userRow["aurora_signup_email"]; ?></td>
                                                    <td class="text-center" style="vertical-align: middle">
                                                        <?php if ($userAccess == 'User' && getActiveUser($userName) == 'ACTIVE') {
                                                                                                                          echo "Admitted User";
                                                                                                                      } else echo $userRow["aurora_user_access"]; ?></td>
                                                    <td><?php $data = getTracUser($userName);
                                                              $trackingCount = count($data['Tracking']); // Assign count to variable
                                                              if (!empty($data['Tracking'])) {
                                                                  for ($j = 0; $j < $trackingCount; $j++) {
                                                                      echo '<a style="color:#FB593E" href="viewApplyForm.php?trackingid=' . $data['Tracking'][$j] . '">' . $data['Tracking'][$j] . "</a>(" . $data['Status'][$j] . ")" . "<br />";
                                                                  }
                                                              } else {
                                                                  echo "No Application Found";
                                                              }

                                                              ?></td>
                                                    <td><?php foreach ($accessArray as $value) {
                                                                  echo getAccessDetail($value) . "<br />";
                                                              }  ?></td>
                                                    <td class="text-center" style="vertical-align: middle">
                                                        <?php
                                                              if ($_SESSION['access'] == "SuperAdmin" || getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "UM-E") == "UM-E") { ?>
                                                        <a
                                                            href="updateuser.php?email=<?php echo $userRow["aurora_signup_username"]; ?>&id=<?php echo $userRow["aurora_sign_id"]; ?>">
                                                            <button type="button" class="btn btn-success"><i
                                                                    class="glyphicon glyphicon-pencil"></i></button>
                                                        </a> &nbsp;
                                                        <?php
                                                              }
                                                              if ($_SESSION['access'] == "SuperAdmin" || getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "UM-D") == "UM-D") { ?>
                                                        <a
                                                            href="deleteuser.php?id=<?php echo $userRow["aurora_sign_id"]; ?>">
                                                            <button type="button" class="btn btn-danger"><i
                                                                    class="glyphicon glyphicon-trash"></i></button>
                                                        </a> &nbsp;

                                                        <?php
                                                              }
                                                              if (getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "UM-R") == "UM-R") { ?>
                                                        <!-- <a href="#">
                                                                              <button type="button" class="btn btn-danger" disabled="disabled"><i class="glyphicon glyphicon-zoom-in"></i></button></a> -->

                                                        <?php
                                                              }
                                                              ?>


                                                    </td>

                                                </tr>
                                                <?php
                                                      $i++;
                                                  }
                                              } else { ?>
                                                <tr>
                                                    <td class="text-center" colspan=6>No Data Found...</td>
                                                </tr>
                                                <?php
                                              }

                                              ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile-pills">
                                    <h2 class="text-center">Create new user</h2>
                                    <div class="row voffset5">
                                        <div class="col-lg-12 ">
                                            <?php 
                                               if($sucId == 0){
                                                 echo "";
                                                 }elseif($sucId == 1){
                                                   echo '<div class="row voffset2">
                                                           <div class="col-lg-12">
                                                              <div class="alert alert-success alert-dismissable">
                                                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>Welcome! An Email send to Email address. Please Varify Email at first.
                                                              </div>
                                                           </div>
                                                         </div>';
                                                 }elseif($sucId == 2){
                                                   echo '<div class="row voffset2">
                                                           <div class="col-lg-12">
                                                              <div class="alert alert-success alert-dismissable">
                                                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>Sorry! Sign up is not completed. Thank You
                                                              </div>
                                                           </div>
                                                         </div>';
                                                 }elseif($sucId == 4){
                                                   echo '<div class="row voffset2">
                                                           <div class="col-lg-12">
                                                              <div class="alert alert-success alert-dismissable">
                                                                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>Sorry! This Email already exist. Thank You
                                                              </div>
                                                           </div>
                                                         </div>';
                                                 }
                                            ?>
                                            <div class="col-lg-12 voffset2">
                                                <form method="POST" action="#">
                                                    <fieldset>
                                                        <label>First Name</label>
                                                        <div class="form-group">
                                                            <input class="form-control"
                                                                style=" float:left; width: 94%; margin-right: 2%;"
                                                                placeholder="First Name" name="firstName" type="text"
                                                                value="" required>
                                                            <i class="fa fa-question-circle icon-color-arra fa-2x pull-left"
                                                                style=" margin-top: -0.7%;" data-toggle="modal"
                                                                data-target="#firstNameModal"></i>
                                                        </div>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="firstNameModal" tabindex="-1"
                                                            role="dialog" aria-labelledby="firstNameModalLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header"
                                                                        style="background-color:#156059; color:#FFFFFF;">
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title"
                                                                            id="firstNameModalLabel">First Name(s)</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Please include any middle names (if applicable)
                                                                        with the first name.
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-new"
                                                                            data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                        <label style="width: 100%; margin-top: 1%;">Last Name</label>
                                                        <div class="form-group">
                                                            <input class="form-control"
                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                placeholder="Last Name" name="lastName" type="text"
                                                                value="" required>
                                                            <i class="fa fa-question-circle icon-color-arra fa-2x pull-left"
                                                                style=" margin-top: -0.7%;" data-toggle="modal"
                                                                data-target="#lastNameModal"></i>
                                                        </div>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="lastNameModal" tabindex="-1"
                                                            role="dialog" aria-labelledby="firstNameModalLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header"
                                                                        style="background-color:#156059; color:#FFFFFF;">
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title"
                                                                            id="firstNameModalLabel">Last Name(s)</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Please type your family name.
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-new"
                                                                            data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                        <label style="width: 100%; margin-top: 1%;">Mobile
                                                            Number</label>
                                                        <div class="form-group">
                                                            <input class="form-control"
                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                placeholder="mobile" name="mobile" type="text" value=""
                                                                required>
                                                        </div>
                                                        <label style="width: 100%; margin-top: 1%;">Email</label>
                                                        <div class="form-group">
                                                            <input class="form-control"
                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                placeholder="E-mail" name="email" type="email" autofocus
                                                                required>
                                                        </div>

                                                        <label style="width: 100%; margin-top: 1%;">Password</label>
                                                        <div class="form-group">
                                                            <input class="form-control"
                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                placeholder="Password" name="password" type="password"
                                                                value="" required>
                                                        </div>

                                                        <label style="width: 100%; margin-top: 1%;">Alias</label>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-12" id="appDiv">
                                                                    <div class="col-lg-12">
                                                                        <div class="col-lg-4"><label
                                                                                style="width: 100%; margin-top: 1%;">First
                                                                                Name</label><input class="form-control"
                                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                                placeholder="First Name" name="fname"
                                                                                type="text" value=""></div>
                                                                        <div class="col-lg-4"><label
                                                                                style="width: 100%; margin-top: 1%;">Last
                                                                                Name</label><input class="form-control"
                                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                                placeholder="Last Name" name="lname"
                                                                                type="text" value=""></div>
                                                                        <div class="col-lg-4"><label
                                                                                style="width: 100%; margin-top: 1%;">Email</label><input
                                                                                class="form-control"
                                                                                style="float:left; width: 90%; margin-right: 2%;"
                                                                                placeholder="E-mail" name="alias"
                                                                                type="email" value=""></div>
                                                                        <div class="col-lg-4"><label
                                                                                style="width: 100%; margin-top: 1%;">Mobile</label><input
                                                                                class="form-control"
                                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                                placeholder="Mobile Number"
                                                                                name="almobile" type="text" value="">
                                                                        </div>
                                                                        <div class="col-lg-4"><label
                                                                                style="width: 100%; margin-top: 1%;">Password</label><input
                                                                                class="form-control"
                                                                                style="float:left; width: 94%; margin-right: 2%;"
                                                                                placeholder="Password" name="passalias"
                                                                                type="password" value=""></div>
                                                                        <div class="col-lg-4"><label
                                                                                style="width: 100%; margin-top: 1%;">Notification</label><input
                                                                                style="float:left;" name="notification"
                                                                                type="checkbox" value="1"></div>
                                                                    </div>
                                                                </div>
                                                                <!--                                      <div class="col-lg-1"><i class="fa fa-plus icon-color-arra fa-2x pull-left" style="margin-top: 55%;padding-left: 32%;" id="alias"></i></div>-->

                                                            </div>


                                                        </div>

                                                        <?php if($_SESSION['access'] == "SuperAdmin"){?>
                                                        <div class="form-group">
                                                            <label style="width: 100%; margin-top: 1%;">Acess</label>
                                                            <select class="form-control" name="accessName"
                                                                style="float:left; width: 94%; margin-right: 2%;">
                                                                <option value="Admin">Admin</option>
                                                                <option value="SubAdmin">Sub Admin</option>
                                                                <option value="User">User</option>
                                                            </select>
                                                        </div>
                                                        <?php }elseif($_SESSION['access'] == "Admin"){?>
                                                        <div class="form-group">
                                                            <label style="width: 100%; margin-top: 1%;">Acess</label>
                                                            <select class="form-control" name="accessName"
                                                                style="float:left; width: 94%; margin-right: 2%;">
                                                                <option value="SubAdmin">Sub Admin</option>
                                                                <option value="User">User</option>
                                                            </select>
                                                        </div>
                                                        <?php }elseif($_SESSION['access'] == "SubAdmin"){?>
                                                        <div class="form-group">
                                                            <label style="width: 100%; margin-top: 1%;">Acess</label>
                                                            <select class="form-control" name="accessName"
                                                                style="float:left; width: 94%; margin-right: 2%;">
                                                                <option value="User">User</option>
                                                            </select>
                                                        </div>
                                                        <?php }?>


                                                        <div class="row">
                                                            <div class="col-lg-12"><label
                                                                    style="width: 100%; margin-top: 1%;">Access
                                                                    Area</label></div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <table class="table" id="dataTables-payment"
                                                                        style="float:left; width: 94%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th><input type="checkbox"
                                                                                        id="selecctall" /> Access Name
                                                                                </th>
                                                                                <th class="text-center">Read Only</th>
                                                                                <th class="text-center">Edit</th>
                                                                                <th class="text-center">Delete</th>
                                                                                <th class="text-center">Select All</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                            <?php 
                                        if ($_SESSION['access'] == "SuperAdmin") {
    $i = 1;
    $query = mysqli_query($connection, "SELECT * FROM ARRA_menu ORDER BY ARRA_menu_id") or die(mysqli_error($connection));
    while ($rows = mysqli_fetch_array($query)) { ?>
                                                                            <tr>
                                                                                <td>&nbsp;&nbsp;<input name="acss[]"
                                                                                        type="checkbox"
                                                                                        class="chkselect"
                                                                                        id="<?php echo $rows["ARRA_menu_short"]; ?>"
                                                                                        value="<?php echo $rows["ARRA_menu_short"]; ?>" />
                                                                                    <?php echo $rows["ARRA_menu_detail"]; ?>
                                                                                </td>
                                                                                <td class="text-center"><input
                                                                                        name="<?php echo $rows["ARRA_menu_short"] . "read"; ?>"
                                                                                        class="chkselect1"
                                                                                        id="<?php echo $rows["ARRA_menu_short"] . "-ReadId"; ?>"
                                                                                        type="checkbox"
                                                                                        value="<?php echo $rows["ARRA_menu_short"] . "-R"; ?>"
                                                                                        disabled="disabled"></td>
                                                                                <td class="text-center"><input
                                                                                        name="<?php echo $rows["ARRA_menu_short"] . "edit"; ?>"
                                                                                        class="chkselect1"
                                                                                        id="<?php echo $rows["ARRA_menu_short"] . "-EditId"; ?>"
                                                                                        type="checkbox"
                                                                                        value="<?php echo $rows["ARRA_menu_short"] . "-E"; ?>"
                                                                                        disabled="disabled"></td>
                                                                                <td class="text-center"><input
                                                                                        name="<?php echo $rows["ARRA_menu_short"] . "delete"; ?>"
                                                                                        class="chkselect1"
                                                                                        id="<?php echo $rows["ARRA_menu_short"] . "-DeleteId"; ?>"
                                                                                        type="checkbox"
                                                                                        value="<?php echo $rows["ARRA_menu_short"] . "-D"; ?>"
                                                                                        disabled="disabled"></td>
                                                                                <td class="text-center"><input name=""
                                                                                        class="chkselect1"
                                                                                        type="checkbox" class="select"
                                                                                        id="<?php echo $rows["ARRA_menu_short"] . "-Id"; ?>"
                                                                                        value="<?php echo $rows["ARRA_menu_short"] . "all"; ?>"
                                                                                        disabled="disabled"></td>
                                                                            </tr>
                                                                            <?php
    }
} elseif ($_SESSION['access'] != "SuperAdmin") {
    foreach (array_unique($pushVal) as $valAs) {
        //echo $valAs;
        if ($_SESSION['access'] == "SubAdmin" && $valAs == "UM") {
            echo "";
        } else {
?>
                                                                            <tr>
                                                                                <td><input name="acss[]" type="checkbox"
                                                                                        class="chkselect"
                                                                                        id="<?php echo $valAs; ?>"
                                                                                        value="<?php echo $valAs; ?>" />
                                                                                    <?php echo getAccessDetail($valAs); ?>
                                                                                </td>
                                                                                <td class="text-center"><input
                                                                                        name="<?php echo $valAs . "read"; ?>"
                                                                                        class="chkselect1"
                                                                                        id="<?php echo $valAs . "-ReadId"; ?>"
                                                                                        type="checkbox"
                                                                                        value="<?php echo $valAs . "-R"; ?>"
                                                                                        disabled="disabled"></td>
                                                                                <td class="text-center"><input
                                                                                        name="<?php echo $valAs . "edit"; ?>"
                                                                                        class="chkselect1"
                                                                                        id="<?php echo $valAs . "-EditId"; ?>"
                                                                                        type="checkbox"
                                                                                        value="<?php echo $valAs . "-E"; ?>"
                                                                                        disabled="disabled"></td>
                                                                                <td class="text-center"><input
                                                                                        name="<?php echo $valAs . "delete"; ?>"
                                                                                        class="chkselect1"
                                                                                        id="<?php echo $valAs . "-DeleteId"; ?>"
                                                                                        type="checkbox"
                                                                                        value="<?php echo $valAs . "-D"; ?>"
                                                                                        disabled="disabled"></td>
                                                                                <td class="text-center"><input name=""
                                                                                        class="chkselect1"
                                                                                        type="checkbox" class="select"
                                                                                        id="<?php echo $valAs . "-Id"; ?>"
                                                                                        value="<?php echo $valAs . "all"; ?>"
                                                                                        disabled="disabled"></td>
                                                                            </tr>
                                                                            <?php
        }
    }
}
                                            ?>
                                                                        </tbody>
                                                                    </table>


                                                                </div>
                                                            </div>


                                                        </div>

                                                        <!-- Change this to a button or input when using this as a form -->

                                                        <div class="col-lg-12 text-center">
                                                            <div class="form-group">
                                                                <button type="submit" name="usersave" id="save"
                                                                    class="btn btn-new">CREATE USER</button>
                                                                <button type="submit" class="btn btn-danger"
                                                                    onclick="history.go(-1);"><i
                                                                        class="fa fa-arrow-circle-o-left fa-1x"></i>
                                                                    CANCEL</button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>

                    <div class="panel-footer">

                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->



    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-user').DataTable({
            responsive: true
        });
    });
    </script>


    <script src="../dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        var count = 1;
        $('#alias').click(function(event) {
            var doIt = '<div class="row">';
            doIt +=
                '<div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Email</label><input class="form-control" style="float:left;" placeholder="E-mail" name="alias[]" type="email" value="" required></div>';
            doIt +=
                '<div class="col-lg-3"><label style="width: 100%; margin-top: 1%;">Full Name</label><input class="form-control" style="float:left;" placeholder="E-mail" name="emailalias[]" type="text" value="" required ></div>';
            doIt +=
                '<div class="col-lg-3"><label style="width: 100%; margin-top: 1%;">Passwprd</label><input class="form-control" style="float:left;" placeholder="Password" name="passalias[]" type="password" value="" required ></div>';
            doIt +=
                '<div class="col-lg-1"><label style="width: 100%; margin-top: 1%;">Notification</label><input  style="float:left;" id="notification" name="notification[]" type="checkbox" value="1" ></div>';
            doIt += '</div>';

            count++;
            if (count < 4) {
                $('#appDiv').append(doIt);
            } else {
                $('#alias').attr("disabled", true);
            }

        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#selecctall').click(function(event) { //on click 
            if (this.checked) { // check select status
                $('.chkselect').each(function() { //loop through each checkbox
                    this.checked =
                    true; //select all checkboxes with class "chkselect"               
                });
                $('.chkselect1').each(function() {
                    this.disabled = false;
                    this.checked = true;
                });
            } else {
                $('.chkselect').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "chkselect"   
                });
                $('.chkselect1').each(function() {
                    this.checked = false;
                    this.disabled = true;
                });
            }
        });
    });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("input").click(function(event) {
            var id = event.target.id;
            if ($(this).is(':checked') == true) {
                if (id.substr(-3, 3) == "-Id") {
                    id = id.replace('-Id', '');
                    $('#' + id + '-ReadId').attr('checked', 'true');
                    $('#' + id + '-EditId').attr('checked', 'true');
                    $('#' + id + '-DeleteId').attr('checked', 'true');
                } else {
                    $('#' + id + '-ReadId').removeAttr('disabled');
                    $('#' + id + '-EditId').removeAttr('disabled');
                    $('#' + id + '-DeleteId').removeAttr('disabled');
                    $('#' + id + '-Id').removeAttr('disabled');
                }
            } else if ($(this).is(':checked') == false) {
                if (id.substr(-3, 3) == "-Id") {
                    id = id.replace('-Id', '');
                    $('#' + id + '-ReadId').removeAttr('checked');
                    $('#' + id + '-EditId').removeAttr('checked');
                    $('#' + id + '-DeleteId').removeAttr('checked');
                } else {
                    $('#' + id + '-ReadId').attr('disabled', 'disabled');
                    $('#' + id + '-EditId').attr('disabled', 'disabled');
                    $('#' + id + '-DeleteId').attr('disabled', 'disabled');
                    $('#' + id + '-Id').attr('disabled', 'disabled');
                }
            }
        });
    });
    </script>

</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>