<?php
	session_start();
	include ('function.php');
	dbConnect();
	
	
	if(isset($_GET['q'])){
		$i=1;
		
		if($_SESSION['access'] != "User"){
			$query = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album WHERE ARRA_album_category like '%".$_GET['q']."%' ORDER BY ARRA_album_id DESC") or die(mysqli_error(dbConnect()));
		} else {
			$query = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album WHERE ARRA_album_status='1' AND ARRA_album_category like '%".$_GET['q']."%' ORDER BY ARRA_album_id DESC") or die(mysqli_error(dbConnect()));
		}
		
		$num_rows = mysqli_num_rows($query);
		if($num_rows > 0){
			while($rows = mysqli_fetch_array($query)){ 
				$fullclassname = array(); // Initialize $fullclassname as an empty array
		?>
		
		<tr class="odd gradeX" id="<?php echo $rows["ARRA_album_id"]; ?>">
			<td><?php echo $i++; ?></td>
			<td><?php echo $rows["ARRA_album_name"]; ?></td>
			<?php if($_SESSION['access'] == "Admin" OR $_SESSION['access'] == "SuperAdmin") echo '<td>'.$rows["created_by"].'</td>' ?>
			<td><?php echo $rows["ARRA_album_category"]; ?></td>
			<td><?php echo $rows["ARRA_album_publishdate"]; ?></td>
			<?php if($_SESSION['access'] != "User") {
				$fullclassarr = explode(",",$rows["ARRA_album_for"]);
				foreach($fullclassarr as $fullclass) {
					if($fullclass == "toddler"){$fullclassname[0]="Toddler";}
					if($fullclass == "preschool"){$fullclassname[1]="Preschool";}
					if($fullclass == "elschool"){$fullclassname[2]="Prekindergarten";}
					if($fullclass == "kinder"){$fullclassname[3]="Kindergarten";}
					
				}
				
				echo '<td>'.implode(", ", $fullclassname).'</td>'; // Use implode with $fullclassname
			}
			?>
			
			<td><?php 
				$query2 = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_album_id='".$rows["ARRA_album_id"]."'") or die(mysqli_error(dbConnect())); 
				$num_rows_photo = mysqli_num_rows($query2);
				echo $num_rows_photo;
				echo ' <i class="fa fa-paperclip"></i>';
				$query3 = mysqli_query(dbConnect(), "SELECT * FROM ARRA_album_photo where ARRA_photo_status=1 and ARRA_album_id='".$rows["ARRA_album_id"]."'") or die(mysqli_error(dbConnect())); 
				$num_rows_status = mysqli_num_rows($query3);
			?></td>
			
			<td class="actiontd text-center">
				<?php if($_SESSION['access'] == "SuperAdmin" OR getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"album-R") == "album-R") { ?>
					<a title="View" href="galleryshow.php?id=<?php echo $rows["ARRA_album_id"]; ?>" ><button type="button" class="btn btn-success"><i class="glyphicon glyphicon-zoom-in"></i></button></a> 
				<?php } ?>
				
				<?php if($_SESSION['access'] == "SuperAdmin" OR getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"album-E") == "album-E") { ?>
					<a title='Edit'  ><button  data-button='{"id": "<?php echo $rows["ARRA_album_id"]; ?>", "ab_for":"<?php echo $rows["ARRA_album_for"]; ?>"}' type='button' class='btn btn-warning btn-edit <?php if($rows["ARRA_album_status"] == 1) echo "disabled"; ?>'><i class="glyphicon glyphicon-pencil Edit"></i></button></a>
				<?php } ?>
				<?php if($_SESSION['access'] == "SuperAdmin" OR getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"album-D") == "album-D") { ?>
					<a title='Delete'><button id="btn_del_album" data-id="<?php echo $rows["ARRA_album_id"]; ?>" data-loc="<?php echo $rows["ARRA_album_location"]; ?>" type='button' class='btn btn-danger'><i class="fa fa-trash-o fa-1x"></i> </button></a>
				<?php } ?>
				<?php if($_SESSION['access'] == "SuperAdmin" OR getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"album-E") == "album-E") { ?>
					<?php if($rows["ARRA_album_status"] == 0){?>
						
						<a title='Publish' data-enot= "<?php echo $rows["ARRA_album_emailnot"]; ?>" data-snot= "<?php echo $rows["ARRA_album_smsnot"]; ?>" onclick="goDoSomething(this,$(this).data('value'),$(this).data('enot'),$(this).data('snot'),$(this).data('albumid'),$(this).data('photoid'));" href="#" data-toggle="modal" data-target="#publish" data-value= "<?php echo $rows["ARRA_album_for"]; ?>" data-albumid= "<?php echo $rows["ARRA_album_id"]; ?>" data-photoid= "<?php echo $rows["ARRA_album_id"]; ?>" ><button type='button' class='btn btn-primary publish'><i class="fa fa-eye fa-1x" ></i> </button></a>
					<?php } elseif($rows["ARRA_album_status"] == 1){ ?>
						
						<a title='Unpublish'><button id="btn_unpublish" data-id="<?php echo $rows["ARRA_album_id"]; ?>" type='button' class='btn btn-warning'><i class="fa fa-eye-slash fa-1x"></i> </button></a>
						<a title='Status'><button id="btn_status" data-toggle="modal" data-target="#publish_statusModal" data-id="<?php echo $rows["ARRA_album_id"]; ?>" data-albfor="<?php echo $rows["ARRA_album_for"]; ?>" type='button' class='btn btn-primary'><i class="glyphicon glyphicon-info-sign"></i> </button></a> 
					<?php } ?>
					<?php if($rows["ARRA_album_status"] == 1 && ($num_rows_photo != $num_rows_status) ){ ?>
						<a title='Republish' data-enot= "<?php echo $rows["ARRA_album_emailnot"]; ?>" data-snot= "<?php echo $rows["ARRA_album_smsnot"]; ?>" onclick="goDoSomething(this,$(this).data('value'),$(this).data('enot'),$(this).data('snot'),$(this).data('albumid'),$(this).data('photoid'));" href="#" data-toggle="modal" data-target="#publish" data-value= "<?php echo $rows["ARRA_album_for"]; ?>" data-albumid= "<?php echo $rows["ARRA_album_id"]; ?>" data-photoid= "<?php echo $rows["ARRA_album_id"]; ?>" ><button type='button' class='btn btn-primary publish'><i class="fa fa-retweet" ></i> </button></a>
					<?php } else{ ?>
						
						<a title="No Republish"><button id="btn_notify" type="button" class="btn btn-default disabled"><i class="glyphicon glyphicon-ban-circle"></i> </button></a>
						
						
					<?php } ?>
					
				<?php	} ?>
			</td>
		</tr>
		<?php
			}
		} else { 
		?>
		<tr><td colspan="9" align="center" style="font-size:20px; font-weight:bold;">No albums were found.</td></tr>
		<?php	} 
	}
	
?> 
