<div class="row">
	<div class="col-lg-6 voffset2" >
		<h6 class="text-left">Before completing the application form please read the <a href="../sample/InstructionsforAdmissionForm.pdf" target="_blank" style="color:WHITE; font-size:14px; font-weight:bold;" >instructions </a>carefully</h6>
		<h6 class="text-left">Fields marked with (<strong style="font-weight: bold;color: red;">*</strong>) are mandatory.</h6>
		<h6 class="text-left">Click on (<strong style="font-weight: bold;color: white;">?</strong>) for additional information.</h6></div>
	<div class="col-lg-6">
		<div class="row">
			<div class="col-lg-6 text-right">
				<h3 style="font-weight:bold;">Tracking Number:</h3>
			</div>
			<div class="col-lg-6">
				<h3 style="font-weight:bold;"><?php echo $_SESSION['tracking_number']; ?></h3>
			</div>
		</div>
		<?php 
		/* $query = mysql_query("SELECT * FROM ARRA_progress_bar WHERE tracking_number = '".$_SESSION['tracking_number']."'") or die(mysql_error());
		$progress_bar_value = mysql_fetch_array($query);
		if(basename($_SERVER['PHP_SELF']) == "sec0.php"){
			$page_name = "Basic Information";
			$status = $progress_bar_value[2];
		}
		elseif(basename($_SERVER['PHP_SELF']) == "sec1.php"){
			$page_name = "Student's Details";
			$status = $progress_bar_value[3];
		}
		elseif(basename($_SERVER['PHP_SELF']) == "sec4.php"){
			$page_name = "Family Information";
			$status = $progress_bar_value[4];
		}
		elseif(basename($_SERVER['PHP_SELF']) == "sec7.php"){
			$page_name = "Sibling's Information";
			$status = $progress_bar_value[5];
		}
		elseif(basename($_SERVER['PHP_SELF']) == "sec9.php"){
			$page_name = "Learning Profile";
			$status = $progress_bar_value[6];
		}
		elseif(basename($_SERVER['PHP_SELF']) == "sec10.php"){
			$page_name = "Supporting Documents";
			$status = $progress_bar_value[7];
		} */
		$status = traStatus($_SESSION['tracking_number']);
		$status_color = ($status == "COMPLETE")?"style='color:yellow;font-weight:bold;'":"style='color:red;font-weight:bold;'";
		?>
		<div class="row">
			<div class="col-lg-6 text-right">
				<h4>Application Status :</h4>
			</div>
			<div class="col-lg-6">
				<h4><?php echo "<span  $status_color>$status</span>"; ?></h4>
			</div>
		</div>
	</div>
</div>
