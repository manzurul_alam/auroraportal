<?php 
session_start();

if(!empty($_SESSION['user_id'])){
    if(isset($_REQUEST["id"]) && isset($_REQUEST["type"])){
        
        include ('function.php');
        dbConnect();
        
        $query = mysqli_query(dbConnect(), "SELECT * FROM ARRA_notice WHERE id='".mysqli_real_escape_string(dbConnect(), $_REQUEST['id'])."' AND type = '".mysqli_real_escape_string(dbConnect(), $_REQUEST['type'])."' ") or die(mysqli_error(dbConnect()));
        $row = mysqli_fetch_array($query, MYSQLI_ASSOC);

        $count = 0;
        $html = "";
        $html .= '<!DOCTYPE html>
        <html lang="en">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>... AIS. ..</title>
        <style>
        th{text-align: left;}
        </style>
        </head>
        <body>';
        
        $html .= '<h1>'.$row["title"].'</h1>';
        $html .= '<p>'.$row["body"].'</p>';
        $html .= '<br /><br />'; 
        if(isset($row["createlink"]) && $row["createlink"] != "" && $row["createlink"] != "N/A")
            $html .= '<p><a href="'.$row["createlink"].'" target="_blank">'.$row["createlink"].'</a></p>';
        $html .= '<br />'; 
        if($row["salutation"] == "")
            $html .= '<p>Team AIS</p>';
        else
            $html .= '<p>'.$row["salutation"].'</p>';
        
        $html .= '<br />'; 
        $fileQuery = mysqli_query(dbConnect(), "SELECT * FROM attachments WHERE notice_id = '".mysqli_real_escape_string(dbConnect(), $row["id"])."'")or die(mysqli_error(dbConnect()));
        if(mysqli_num_rows($fileQuery)){
            $html .= "Attachments <ul>";
            while($f = mysqli_fetch_array($fileQuery, MYSQLI_ASSOC)){
                $html .= "<li>".$f["file_title"]." (".$f["file_name"].")</li>";
            }
            $html .= "</ul>";
        }
        $html .= '<div>&nbsp;</div>
        </body>
        </html>';

        require_once ('vendor/autoload.php');

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'win-1252',
            'format' => 'A4',
            'default_font_size' => 10,
            'margin_left' => 20,
            'margin_right' => 15,
            'margin_top' => 20,
            'margin_bottom' => 15,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
        if(isset($_REQUEST["id"]) && $_REQUEST["type"] == 1){
            $mpdf->WriteHTML($html);
            $temp_file_name = 'email_con/ApplicationForm_'.$_SESSION["user_id"]."-".$_REQUEST["id"].'.pdf';
            $mpdf->Output($temp_file_name,'F');
            
            echo "<script>window.location.href='mailto:&body=Please click the link for download the Application form http://172.16.0.99/ARRA/dashboard/pages/$temp_file_name';</script>";
        } else {
            $mpdf->WriteHTML($html);
            $mpdf->Output($_REQUEST["trackingid"], 'I');
        }
    }
} else {
    require_once 'login.php';
}
?>
