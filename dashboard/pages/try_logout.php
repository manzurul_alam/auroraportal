<?php
session_start();
include ('function.php');
dbConnect();

$user_id = $_SESSION['user_id'];
$session_id = $_SESSION['session_id'];
unset($_SESSION['user_id']);
unset($_SESSION['tracking_number']);
unset($_SESSION['session_id']);
$result = session_destroy();

if ($result) {
    $query = mysqli_query(dbConnect(), "UPDATE login_info SET logout_time = CURRENT_TIMESTAMP WHERE session_id = '".$session_id."' AND user_id = '".$user_id."'") or die(mysqli_error(dbConnect()));
    if($query){
        header('Refresh: 3; URL= index.php');
        echo '<center><div style="background-color:#CCFFCC; color:green; width: 500px; margin-top:200px; border:1px solid #47D147;"><p>Successfully logged out. You will be redirected to the Home page.</p>';
        echo '<p>If your browser doesn\'t redirect you shortly, click <a href= "index.php">here</a>.</p></div></center>';
    }
} else {
    $query = mysqli_query(dbConnect(), "UPDATE login_info SET logout_time = CURRENT_TIMESTAMP WHERE session_id = '".$session_id."' AND user_id = '".$user_id."'") or die(mysqli_error(dbConnect()));
    if($query){
        header('Refresh: 3; URL= index.php');
        echo '<center><div style="background-color:goldenrodyellow; color:red; width: 500px; margin-top:200px; border:1px solid red;"><p>Successfully logged out. You will be redirected to the Home page.</p>';
        echo '<p>If your browser doesn\'t redirect you shortly, click <a href= "index.php">here</a>.</p></div></center>';
    }
}
?>