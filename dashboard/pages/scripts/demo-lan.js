﻿/*jslint  browser: true, white: true, plusplus: true */
/*global $, languages */

$(function () {
    'use strict';

    var languagesArray = $.map(languages, function (value, key) { return { value: value, data: key }; });

      

    // Initialize autocomplete with custom appendTo:
    $('.langulist').autocomplete({
        lookup: languagesArray
    });
	$('.languHome').autocomplete({
        lookup: languagesArray
    });
	
	$(document).on('click', function(e){
        $('.langulist').autocomplete({
        lookup: languagesArray
    });
	$('.languHome').autocomplete({
        lookup: languagesArray
    });
	
    })
	
	
	
	
});