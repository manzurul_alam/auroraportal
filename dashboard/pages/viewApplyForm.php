<?php 
session_start();

if(!empty($_SESSION['user_id'])){
$_SESSION['tracking_number'] = $_REQUEST["trackingid"];
include ('function.php');
dbConnect();
//$message = 0;
$count = 0;

if(isset($_REQUEST["form_accept"])){
	$query =  mysqli_query(dbConnect(),"UPDATE ARRA_tracking SET ARRA_tracking_status = '".$_REQUEST["status"]."' WHERE ARRA_tracking_number = '".$_REQUEST["form_accept"]."'") ;
	if($query){
		echo "<script>window.location.href='AIP.php';</script>";
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="khokon" >

    <title>... AIS. ..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        <div id="page-wrapper">
            <div class="row">
				<!-- Progress Bar 
                        <div class="row voffset2" style="margin-left: 5%;">
							<?php //include('appMenu.php'); ?>
						</div>
                         End Progress Bar -->
                <div class="col-lg-12">
                    <h1 class="page-header">View and print application</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!--
				<div class="col-md-12">
					<label style="margin-top: -15px;margin-bottom: 20px;">Please check to ensure that you have completed all the required fields in the form with accurate and correct information. Please review the information given below, and when you are sure that all the entries are correct, please click on the "CONFIRM" button.</label>
				</div>
				-->
				<div class="col-lg-12">
                    <div class="panel panel-arra">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-11">
									<h4>Section 01. Basic Information</h4>
								</div>
								<div class="col-lg-1">
									<?php if($_SESSION['access'] == "User"){ ?>
										<button type="button" onClick="window.location.href='sec0.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-default"><i class="fa fa-edit fa-1x"></i> Edit</button>
									<?php } ?>
								</div>
								
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<?php 
								// Default variable
								$required_span = "<span class='mandatory-black'>*</span> ";
								$required_count = 0;
								$query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_applying WHERE ARRA_applying_tracking = '".$_REQUEST["trackingid"]."'");
								$applyingData = mysqli_fetch_array($query, MYSQLI_ASSOC); ?>
								<table class="table table-striped table-bordered table-hover">
								<tr>
    <th width="350px"><?php echo $required_span; ?>Tracking Number</th>
    <td><b><?php echo isset($applyingData["ARRA_applying_tracking"]) ? $applyingData["ARRA_applying_tracking"] : ''; ?></b></td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Applying for Grade/Level</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($applyingData["ARRA_applying_for"]) ? getClassType($applyingData["ARRA_applying_for"]) : ''; ?>
    </td>
</tr>

<tr>
    <th><?php if(isset($applyingData["ARRA_applying_for"]) && ($applyingData["ARRA_applying_for"] == "toddler" || $applyingData["ARRA_applying_for"] == "preschool")) echo $required_span; ?>Preferred hours</th>
    <td <?php if(isset($applyingData["ARRA_applying_for"]) && ($applyingData["ARRA_applying_for"] == "toddler" || $applyingData["ARRA_applying_for"] == "preschool")) echo "id='check_required".$required_count++."'"; ?>>
        <?php echo isset($applyingData["ARRA_applying_preTime"]) ? $applyingData["ARRA_applying_preTime"] : ''; ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Intended starting date</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($applyingData["ARRA_applying_inTime"]) ? $applyingData["ARRA_applying_inTime"] : ''; ?>
    </td>
</tr>

								</table>
								<?php 
if(isset($applyingData["ARRA_applying_has_school"]) && $applyingData["ARRA_applying_has_school"]) { 
    $q =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_stu_pre_school WHERE ARRA_stu_pre_school_tracking = '".$applyingData["ARRA_applying_tracking"]."'");
    if($q && mysqli_num_rows($q) > 0) {
?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo $required_span; ?>Name of the past school</th>
                <th><?php echo $required_span; ?>Academic Year</th>
                <th><?php echo $required_span; ?>Grade / Class</th>
                <th><?php echo $required_span; ?>Language of instruction</th>
                <th><?php echo $required_span; ?>Location</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 1; 
            while($rowData = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
            ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td id="check_required<?php echo $required_count++; ?>"><?php echo $rowData["ARRA_stu_pre_school_name"]; ?></td>
                <td id="check_required<?php echo $required_count++; ?>"><?php echo $rowData["ARRA_stu_pre_school_aca_year"]; ?></td>
                <td id="check_required<?php echo $required_count++; ?>"><?php echo $rowData["ARRA_stu_pre_school_class_dtl"]; ?></td>
                <td id="check_required<?php echo $required_count++; ?>"><?php echo $rowData["ARRA_stu_pre_schoolcol_lan"]; ?></td>
                <td id="check_required<?php echo $required_count++; ?>"><?php echo $rowData["ARRA_stu_pre_school_loc"]; ?></td>
            </tr>
            <?php 
            } 
            ?>
        </tbody>
    </table>
<?php
    }
}
?>

							</div>
						</div>
						<div class="panel-footer">
                            &nbsp;
                        </div>
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="panel panel-arra-light">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-11">
									<h4>Section 02. Student's Details</h4>
								</div>
								<div class="col-lg-1">
									<?php if($_SESSION['access'] == "User"){ ?>
										<button type="button" onClick="window.location.href='sec1.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-default"><i class="fa fa-edit fa-1x"></i> Edit</button>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<?php 
								$query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_REQUEST["trackingid"]."'");
								$childsData = mysqli_fetch_array($query, MYSQLI_ASSOC); ?>
								<table class="table table-striped table-bordered table-hover">
								<tr>
    <th width="350px"><?php echo $required_span; ?>Student’s Name</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($childsData["ARRA_childs_detail_name"]) ? str_replace("@", " ", $childsData["ARRA_childs_detail_name"]) : ''; ?>
    </td>
</tr>

<tr>
    <th>Preferred Name (if applicable)</th>
    <td>
        <?php echo isset($childsData["ARRA_childs_detail_prename"]) ? $childsData["ARRA_childs_detail_prename"] : ''; ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Date of Birth</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($childsData["ARRA_childs_detail_dob"]) ? $childsData["ARRA_childs_detail_dob"] : ''; ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Place of Birth</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($childsData["ARRA_childs_detail_pob"]) ? $childsData["ARRA_childs_detail_pob"] : ''; ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Gender</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($childsData["ARRA_childs_detail_gender"]) ? ($childsData["ARRA_childs_detail_gender"] == "male" ? "Male" : "Female") : ''; ?>
    </td>
</tr>
<tr>
    <th><?php echo $required_span; ?>Primary Nationality</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($childsData["ARRA_childs_detail_CN"]) ? $childsData["ARRA_childs_detail_CN"] : ''; ?>
    </td>
</tr>

<tr>
    <th>Secondary Nationality</th>
    <td>
        <?php 
        if (isset($childsData["ARRA_childs_detail_SN"])) {
            $ls = explode(",", $childsData["ARRA_childs_detail_SN"]); 
            if ($ls[0] != "") {
                for ($i = 0; $i < count($ls); $i++) {
                    echo $ls[$i] . "<br>";
                }
            } else {
                echo "NA";
            }
        } else {
            echo "NA"; // Handle the case where $childsData["ARRA_childs_detail_SN"] is null or not set
        }
        ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Current Address</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($childsData["ARRA_childs_detail_cur_add"]) ? htmlspecialchars($childsData["ARRA_childs_detail_cur_add"]) : "NA"; ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Permanent Address</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php 
            if (isset($childsData["ARRA_childs_detail_per_add_flag"]) && $childsData["ARRA_childs_detail_per_add_flag"]) {
                echo isset($childsData["ARRA_childs_detail_cur_add"]) ? htmlspecialchars($childsData["ARRA_childs_detail_cur_add"]) : "NA";
            } else {
                echo isset($childsData["ARRA_childs_detail_per_add"]) ? htmlspecialchars($childsData["ARRA_childs_detail_per_add"]) : "NA";
            }
        ?>
    </td>
</tr>

									<!--
									<?php if(isset($childsData["ARRA_childs_detail_bnationality"])){ ?> 
									<tr>
										<th colspan=2>Birth Certificate</th>
									</tr>
									<tr>
										<th>Certificate No</th>
										<td><?php echo $childsData["ARRA_childs_detail_bcertificate_no"]; ?></td>
									</tr>
									<?php } ?>
									-->
								</table>
											
								<!-- NID or Citizen Certificate -->
								<?php $i=1; $q =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_stu_nid WHERE ARRA_stu_nid_tracking = '".$childsData["ARRA_childs_detail_tracking_number"]."'") ;
									if(mysqli_num_rows($q) > 0) { ?>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan=3>NID/Citizen Certificate</th>
										</tr>
										<tr>
											<th>#</th>
											<th>Nationality</th>
											<th>Certificate No</th>
										</tr>
									</thead>
								<?php 
									while($rowData = mysqli_fetch_array($q, MYSQLI_ASSOC)){ ?>
									<tbody>
										<tr>
											<td><?php echo $i++; ?></td>
											<td><?php echo $rowData["ARRA_stu_nid_nationaliy"]; ?></td>
											<td><?php echo $rowData["ARRA_stu_nid_certificate"]; ?></td>
										</tr>
									</tbody>
									<?php } ?>
								</table><?php
								} ?>
							</div>
						</div>
						<div class="panel-footer">
                            &nbsp;
                        </div>
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="panel panel-ms">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-11">
									<h4>Section 03. Family Information</h4>
								</div>
								<div class="col-lg-1">
									<?php if($_SESSION['access'] == "User"){ ?>
										<button type="button" onClick="window.location.href='sec4.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-default"><i class="fa fa-edit fa-1x"></i> Edit</button>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<?php 
								$live_query =  mysqli_query(dbConnect(),"SELECT ARRA_childs_detail_livewith, ARRA_childs_detail_family_situation FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number='".$_REQUEST["trackingid"]."'") ;
								$live_with = mysqli_fetch_array($live_query, MYSQLI_ASSOC);
								//echo $live_with["ARRA_childs_detail_family_situation"];
								$con = 0;
								$con_query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_contact_detail WHERE tracking_number ='".$_REQUEST["trackingid"]."'") ;
								while($cd = mysqli_fetch_array($con_query)){
									$contact_category[$con] = $cd[1];
									$contact_status[$con] = $cd[2];
									$contact_number[$con] = $cd[3];
									$contact_type[$con] = $cd[4];
									$contact_priority[$con++] = $cd[5];
								}
								$query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_family_detail WHERE tracking_number ='".$_REQUEST["trackingid"]."'") ; ?>
								<table class="table table-striped table-bordered table-hover">
									<?php $i=0; while($childsData = mysqli_fetch_array($query, MYSQLI_ASSOC))
									{ ?>
										<?php if($i == 0) $i++; else { ?>
										<tr>
											<td colspan=2>&nbsp;</td>
										</tr>
										<?php } ?>
										<tr>
											<th width=350px><?php echo $required_span; ?><?php echo ucfirst($childsData["relation"])."'s"; ?> Name</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $childsData["firstname"]." ".$childsData["lastname"]; ?></td>
										</tr>
										<!--<tr>
											<th><?php echo ucfirst($childsData["relation"]); ?> Status</th>
											<td><?php echo $childsData["status"]; ?></td>
										</tr>-->
										<tr>
											<th><?php echo $required_span; ?>Nationality</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $childsData["nationality"]; ?></td>
										</tr>
										<tr>
											<th>Secondary Nationality</th>
											<td>
											<?php 
											$ls = explode(",", $childsData["secnationality"]); 
													if($ls[0] != ""){
														for($i=0; $i<count($ls); $i++){
															echo $ls[$i]."<br>";
														}
													}
													else{
														echo "NA";
													}
											?>
											</td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Highest Educational Qualification</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $childsData["higheduqua"]; ?></td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Occupation</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $childsData["occupation"]; ?></td>
										</tr>
										<!--
										<tr>
											<th><?php echo ucfirst($childsData["relation"]); ?> work in Ascent Group?</th>
											<td><?php echo $childsData["workinascent"]; ?></td>
										</tr>
										<tr>
											<th>Sister Concern</th>
											<td><?php echo $childsData["sisterconcern"]; ?></td>
										</tr>
										-->
										<tr>
											<th>Organization</th>
											<td><?php echo $childsData["organization"]; ?></td>
										</tr>
										<tr>
											<th>Designation</th>
											<td><?php echo $childsData["designation"]; ?></td>
										</tr>
										<tr>
											<th>Office Address</th>
											<td><?php echo $childsData["officeaddress"]; ?></td>
										</tr>
										<tr>
											<th>Mailing Address</th>
											<td><?php 
											switch($childsData["mailingaddress"]){
												case "fatherca": case "moca":
													echo "Student Present Address"; break;
												case "fatheroa": case "mooa":
													echo $childsData["officeaddress"]; break;
												case "fatherpa": case "mopa":
													echo "Student Permanent Address"; break;
												case "fatherother": case "moother":
													echo $childsData["otheraddress"]; break;
											}
											
											//echo $childsData["mailingaddress"]; 
											?></td>
										</tr>
										<tr>
											<th>Telephone Number</th>
											<td >
												<?php 
												// Check if $contact_number is set and not empty
													if (isset($contact_number) && is_array($contact_number) && count($contact_number) > 0) {
														for ($con = 0; $con < count($contact_number); $con++) {
															// Check if $contact_status and $contact_category have corresponding values
															if (isset($contact_status[$con]) && isset($contact_category[$con]) && $contact_status[$con] == $childsData["relation"] && $contact_category[$con] == "telephone") {
																echo $contact_number[$con] . "(" . $contact_type[$con] . "-" . $contact_priority[$con] . ")<br />";
															}
														}
													} else {
														// Handle the case when $contact_number is not set or empty
														echo "No telephone numbers available.";
													}
												
												?>
											</td>
										</tr>
										<tr>
											<th>Mobile Number</th>
											<td >
												<?php 
												// Check if $contact_number is set and is not empty
													if (isset($contact_number) && is_array($contact_number) && count($contact_number) > 0) {
														for ($con = 0; $con < count($contact_number); $con++) {
															// Check if $contact_status and $contact_category have corresponding values
															if (isset($contact_status[$con]) && isset($contact_category[$con]) && $contact_status[$con] == $childsData["relation"] && $contact_category[$con] == "mobile") {
																echo $contact_number[$con] . "(" . $contact_type[$con] . "-" . $contact_priority[$con] . ")<br />";
															}
														}
													} else {
														// Handle the case when $contact_number is not set or empty
														echo "No mobile numbers available.";
													}
												
												?>
											</td>
										</tr>
										<tr>
											<th>Email Address</th>
											<td >
												<?php 
												// Check if $contact_number is set and is not empty
													if (isset($contact_number) && is_array($contact_number) && count($contact_number) > 0) {
														for ($con = 0; $con < count($contact_number); $con++) {
															// Check if $contact_status and $contact_category have corresponding values
															if (isset($contact_status[$con]) && isset($contact_category[$con]) && $contact_status[$con] == $childsData["relation"] && $contact_category[$con] == "email") {
																echo $contact_number[$con] . "(" . $contact_type[$con] . "-" . $contact_priority[$con] . ")<br />";
															}
														}
													} else {
														// Handle the case when $contact_number is not set or empty
														echo "No email addresses available.";
													}
												
												?>
											</td>
										</tr><?php 
									} ?>
									<tr>
										<td colspan=2>&nbsp;</td>
									</tr>
									<tr>
    <th><?php echo $required_span; ?>Student lives with</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php 
        if (isset($live_with["ARRA_childs_detail_livewith"])) {
            $lw = $live_with["ARRA_childs_detail_livewith"];
            if ($lw == "LocalGuardian") {
                echo "Local guardian";
            } elseif ($lw == "BothParents") {
                echo "Both parents";
            } else {
                echo $lw;
            }
        } else {
            echo "NA"; // Handle the case where $live_with["ARRA_childs_detail_livewith"] is null or not set
        }
        ?>
    </td>
</tr>

									<?php 
									if($live_with["ARRA_childs_detail_livewith"] == "LocalGuardian"){ 
									$guardian_query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_guardian_detail WHERE tracking_number = '".$_REQUEST["trackingid"]."'") ;
									while($guardian_data = mysqli_fetch_array($guardian_query))
									{ ?>
										<tr>
											<th width=350px><?php echo $required_span; ?>Guardian’s Name </th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $guardian_data["fullname"]; ?></td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Nationality</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $guardian_data["nationality"]; ?></td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Highest Educational Qualification</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php echo $guardian_data["higheduqua"]; ?></td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Relationship</th>
											<td id="check_required<?php echo $required_count++; ?>"><?php 
												switch($guardian_data["relationstu"]){
													case "puncle": echo "Uncle (Paternal)"; break;
													case "muncle": echo "Uncle (Maternal)"; break;
													case "paunt": echo "Aunt (Paternal)"; break;
													case "maunt": echo "Aunt (Maternal)"; break;
													case "pgrandparen": echo "Grandparent (Paternal)"; break;
													case "mgrandparent": echo "Grandparent (Maternal)"; break;
													case "brother": echo "Brother"; break;
													case "sister": echo "Sister"; break;
													case "cousin": echo "Cousin"; break;
													case "other": echo $guardian_data["relationother"]; break;
												} ?>
											</td>
										</tr>
										<tr>
											<th>Occupation</th>
											<td><?php echo $guardian_data["occupation"]; ?></td>
										</tr>
										<tr>
											<th>Organization</th>
											<td><?php echo $guardian_data["organization"]; ?></td>
										</tr>
										<tr>
											<th>Designation</th>
											<td><?php echo $guardian_data["designation"]; ?></td>
										</tr>
										<tr>
											<th>Office Address</th>
											<td><?php echo $guardian_data["officeaddress"]; ?></td>
										</tr>
										<tr>
											<th>Present Address</th>
											<td><?php echo $guardian_data["presentaddress"]; ?></td>
										</tr>
										<tr>
											<th>Permanent Address</th>
											<td><?php echo $guardian_data["permanentaddress"]; ?></td>
										</tr>
										<tr>
											<th>Mailing Address</th>
											<td>
											<?php 
											if( $guardian_data["mailingaddress"] == "gupr" ){
												echo "Same as present address"; 
											}elseif( $guardian_data["mailingaddress"] == "guoa" ){
												echo "Same as office address"; 
											}elseif( $guardian_data["mailingaddress"] == "gupe" ){
												echo "Same as permanent address"; 
											}
											elseif( $guardian_data["mailingaddress"] == "guother" ){
												echo $guardian_data["otheraddress"]; 
											}
											?>
											</td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Telephone Number</th>
											<td id="check_required<?php echo $required_count++; ?>">
												<?php 
												 if (!empty($contact_number)) {
													for ($con = 0; $con < count($contact_number); $con++) {
														$gr = explode(".", $contact_status[$con]);
														if ($gr[0] == "guardian" && $contact_category[$con] == "telephone") {
															echo $contact_number[$con] . "(" . $contact_type[$con] . "-" . $contact_priority[$con] . ")<br />";
														}
													} 
												} else {
													echo "No telephone numbers available.";
												}
												
												?>
											</td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Mobile Number</th>
											<td id="check_required<?php echo $required_count++; ?>">
												<?php 
												for($con=0; $con<count($contact_number); $con++){
													$gr = explode(".",$contact_status[$con]);
													if($gr[0] == "guardian"){
														if($contact_category[$con] == "mobile"){
															echo $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
														}
													}
												} 
												
												?>
											</td>
										</tr>
										<tr>
											<th><?php echo $required_span; ?>Email Address</th>
											<td id="check_required<?php echo $required_count++; ?>">
												<?php 
												for($con=0; $con<count($contact_number); $con++){
													$gr = explode(".",$contact_status[$con]);
													if($gr[0] == "guardian"){
														if($contact_category[$con] == "email"){
															echo $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
														}
													}
												} 
												
												?>
											</td>
										</tr>
									<?php } 
									}
									?>
									<tr>
										<td colspan=2>&nbsp;</td>
									</tr>
									<tr>
    <th>Student’s family structure and current living situation</th>
    <td>
        <?php echo isset($live_with["ARRA_childs_detail_family_situation"]) ? htmlspecialchars($live_with["ARRA_childs_detail_family_situation"]) : "NA"; ?>
    </td>
</tr>

								</table>
							</div>
						</div>
						<div class="panel-footer">
                            &nbsp;
                        </div>
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="panel panel-arra">		
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-11">
									<h4>Section 04. Sibling's Information</h4>
								</div>
								<div class="col-lg-1">
									<?php if($_SESSION['access'] == "User"){ ?>
										<button type="button" onClick="window.location.href='sec7.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-default"><i class="fa fa-edit fa-1x"></i> Edit</button>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<?php 
								$query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_stu_siblings WHERE ARRA_stu_tracking ='".$_REQUEST["trackingid"]."'") ;
								if(mysqli_num_rows($query)>0){
									while($rows = mysqli_fetch_array($query, MYSQLI_ASSOC)){ ?>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<th width=350px>Sibling's Name</th>
										<td><?php echo $rows["ARRA_stu_siblings_name"]; ?></td>
									</tr>
									<tr>
										<th>Sibling's Gender</th>
										<td><?php echo $rows["ARRA_stu_siblings_gender"]; ?></td>
									</tr>
									<tr>
										<th>Sibling's Birthday</th>
										<td><?php echo $rows["ARRA_stu_siblings_dob"]; ?></td>
									</tr>
									<tr>
										<th>Is sibling studying in AIS? </th>
										<td><?php echo ($rows["ARRA_stu_siblings_studing_aurora"] == "sibInAuroraYes")?"Yes":"No"; ?></td>
									</tr>
									<?php if($rows["ARRA_stu_siblings_studing_aurora"] == "sibInAuroraYes") { ?>
									<tr>
										<th>Tracking Number </th>
										<td><?php echo $rows["ARRA_stu_sib_tracking"]; ?></td>
									</tr>
									<?php } ?>
									<tr>
										<th>Sibling's current school</th>
										<td><?php echo $rows["ARRA_stu_siblings_current_school"]; ?></td>
									</tr>
									<tr>
										<th>Sibling's intended starting date in AIS</th>
										<td><?php echo $rows["ARRA_stu_siblings_indate"]; ?></td>
									</tr>
									<tr>
										<th>Interested to enroll</th>
										<td><?php echo ($rows["ARRA_stu_inenroll"] == "intenYes")?"Yes":"No"; ?></td>
									</tr>
								</table><?php }
								} else {
									echo "No siblings";
								}
							?>
							</div>
						</div>
						<div class="panel-footer">
                            &nbsp;
                        </div>
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="panel panel-arra-light">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-11">
									<h4>Section 05. Learning Profile</h4>
								</div>
								<div class="col-lg-1">
									<?php if($_SESSION['access'] == "User"){ ?>
										<button type="button" onClick="window.location.href='sec9.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-default"><i class="fa fa-edit fa-1x"></i> Edit</button>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<?php 
								$query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_learning_profile WHERE ARRA_learning_profile_tracking ='".$_REQUEST["trackingid"]."'") ;
								$rows = mysqli_fetch_array($query, MYSQLI_ASSOC); ?>
								<table class="table table-striped table-bordered table-hover">
								<tr>
    <th width="350px"><?php echo $required_span; ?>Child's Mother Language</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php echo isset($rows["ARRA_learning_profile_mother_lan"]) ? $rows["ARRA_learning_profile_mother_lan"] : ''; ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Language Proficiency</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php 
        if (isset($rows["ARRA_learning_profile_langspoken"]) && isset($rows["ARRA_learning_profile_langprof"])) {
            $ls = explode(",", $rows["ARRA_learning_profile_langspoken"]);
            $lp = explode(",", $rows["ARRA_learning_profile_langprof"]);
            for ($i = 0; $i < count($ls); $i++) {
                if ($ls[$i] != "") {
                    echo $ls[$i] . " = " . $lp[$i] . "<br>";
                }
            }
        } else {
            echo "NA"; // Display "NA" or handle the case when data is not available
        }
        ?>
    </td>
</tr>

									<tr>
										<th><?php echo $required_span; ?>Evaluated by diagnostician or other specialist</th>
										<td id="check_required<?php echo $required_count++; ?>"><?php if(isset($rows["ARRA_learning_profile_specialist"]) AND $rows["ARRA_learning_profile_specialist"] == "SpecialistYes"){
												echo $rows["ARRA_learning_profile_sp_detail"];
											}else{
												echo "NA";
											}
											?>
										</td>
									</tr>
									<tr>
										<th><?php echo $required_span; ?>Tested for learning, behavioral, emotional or physical disability</th>
										<td id="check_required<?php echo $required_count++; ?>"><?php if(isset($rows["ARRA_learning_profile_disability"]) AND $rows["ARRA_learning_profile_disability"] == "DisabilityYes"){
												echo $rows["ARRA_learning_profile_ad_detail"];
											}else{
												echo "NA";
											}
											?>
										</td>
									</tr>
									<tr>
    <th><?php echo $required_span; ?>Received any special services</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php 
        if (isset($rows["ARRA_learning_profile_recvss"])) {
            $ls = explode(",", $rows["ARRA_learning_profile_recvss"]);
            if ($ls[0] != "") {
                for ($i = 0; $i < count($ls); $i++) {
                    echo getSpecialServices($ls[$i], $_REQUEST["trackingid"]) . "<br>";
                }
            } else {
                echo "NA";
            }
        } else {
            echo "NA"; // Display "NA" or handle the case when $rows["ARRA_learning_profile_recvss"] is not set
        }
        ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Child needs additional support</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php 
        if (isset($rows["ARRA_learning_profile_ad_sprt"]) && $rows["ARRA_learning_profile_ad_sprt"] == "AdditionalYes") {
            echo $rows["ARRA_learning_profile_detail"];
        } else {
            echo "NA";
        }
        ?>
    </td>
</tr>

<tr>
    <th><?php echo $required_span; ?>Medical information</th>
    <td id="check_required<?php echo $required_count++; ?>">
        <?php 
        if (isset($rows["ARRA_learning_profile_minfo"]) && $rows["ARRA_learning_profile_minfo"] == "MedicalYes") {
            echo $rows["ARRA_learning_profile_minfo_detail"];
        } else {
            echo "NA";
        }
        ?>
    </td>
</tr>

								</table>
							</div>
						</div>
						<div class="panel-footer">
                            &nbsp;
                        </div>
					</div>
				</div>
				<div class="col-lg-12">
                    <div class="panel panel-ms">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-11">
									<h4>Section 06. Supporting Documents</h4>
								</div>
								<div class="col-lg-1">
									<?php if($_SESSION['access'] == "User"){ ?>
										<button type="button" onClick="window.location.href='sec10.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-default"><i class="fa fa-edit fa-1x"></i> Edit</button>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
							<?php 
							$i = 1 ;
							$query =  mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number ='".$_REQUEST["trackingid"]."'") ;
							while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)){
								$document_list[$i] = $row["ARRA_file_type"];
								$document_link[$i++] = $row["ARRA_file_name"];
								//$document_list[$i++] = getDocTypeName($row["ARRA_file_type"]);
								//print_r($document_list);
							}
							?>
							<table class="table table-striped table-bordered table-hover">
								<tr>
									<th width=350px><?php echo $required_span; ?>Student Photo</th>
									<td id="check_required<?php echo $required_count++; ?>">
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											$key = array_search("SP", $document_list);
											// Check if $key is found and is greater than 0
											if ($key !== false && $key > 0) {
												// Check access level and generate the appropriate link
												echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$key]."' target='_blank'>".$document_link[$key]."</a>" : $document_link[$key];
											}
										} 
										?>
									</td>
								</tr>
								<tr>
									<th><?php echo $required_span; ?>Student Birth Certificate</th>
									<td id="check_required<?php echo $required_count++; ?>">
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											$key = array_search("SBC", $document_list);
											// Check if $key is found and is greater than 0
											if ($key !== false && $key > 0) {
												// Check access level and generate the appropriate link
												echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$key]."' target='_blank'>".$document_link[$key]."</a>" : $document_link[$key];
											}
										}
										?>
									</td>
								</tr>
								<tr>
									<th>Student Individual Passport</th>
									<td>
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											// Find all keys corresponding to the value "SPP"
											$list = array_keys($document_list, "SPP");
											// Loop through each key and display the corresponding document link
											for ($i = 0; $i < count($list); $i++) {
												// Check access level and generate the appropriate link
												echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$list[$i]]."' target='_blank'>".$document_link[$list[$i]]."</a><br />" : $document_link[$list[$i]]."<br />";
											}
										} else {
											// Handle the case when $document_list is not set or is not an array
											echo "Document list is not available.";
										}
										?>
									</td>
								</tr>
								<tr>
									<th><?php echo $required_span; ?>Parents/Guardian’s Identification Document</th>
									<td id="check_required<?php echo $required_count++; ?>">
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											// Find all keys corresponding to the value "PGD"
											$list = array_keys($document_list, "PGD");
											// Loop through each key and display the corresponding document link
											for ($i = 0; $i < count($list); $i++) {
												// Check access level and generate the appropriate link
												echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$list[$i]]."' target='_blank'>".$document_link[$list[$i]]."</a><br />" : $document_link[$list[$i]]."<br />";
											}
										} 
										?>
									</td>
								</tr>
							
								<tr>
									<th>Work Permit</th>
									<td>
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											// Find all keys corresponding to the value "WP"
											$list = array_keys($document_list, "WP");
											// Loop through each key and display the corresponding document link
											for ($i = 0; $i < count($list); $i++) {
												// Check access level and generate the appropriate link
												echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$list[$i]]."' target='_blank'>".$document_link[$list[$i]]."</a><br />" : $document_link[$list[$i]]."<br />";
											}
										} else {
											// Handle the case when $document_list is not set or is not an array
											echo "Document list is not available.";
										}
										?>
									</td>
								</tr>
								<tr>
									<th>Student Report Card</th>
									<td>
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											// Find all keys corresponding to the value "SRC"
											$list = array_keys($document_list, "SRC");
											// Loop through each key and display the corresponding document link
											for ($i = 0; $i < count($list); $i++) {
												// Check if the document link exists in $document_link array
												if (isset($document_link[$list[$i]])) {
													// Check access level and generate the appropriate link
													echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$list[$i]]."' target='_blank'>".$document_link[$list[$i]]."</a><br />" : $document_link[$list[$i]]."<br />";
												} else {
													// Handle the case when the document link is not found
													echo "Document link is not available.<br />";
												}
											}
										} else {
											// Handle the case when $document_list is not set or is not an array
											echo "Document list is not available.";
										}
										?>
									</td>
								</tr>
								<tr>
									<th>Immunization & Vaccination Records</th>
									<td>
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											// Find all keys corresponding to the value "IVR"
											$list = array_keys($document_list, "IVR");
											// Loop through each key and display the corresponding document link
											for ($i = 0; $i < count($list); $i++) {
												// Check if the index exists in $document_link array
												if (isset($document_link[$list[$i]])) {
													// Check access level and generate the appropriate link
													echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$list[$i]]."' target='_blank'>".$document_link[$list[$i]]."</a><br />" : $document_link[$list[$i]]."<br />";
												} else {
													// Handle the case when the document link is not found
													echo "Document link is not available.<br />";
												}
											}
										} else {
											// Handle the case when $document_list is not set or is not an array
											echo "Document list is not available.";
										}
										?>
									</td>
								</tr>
								<tr>
									<th>Reports / Evaluations </th>
									<td>
										<?php
										// Check if $document_list is set and is an array
										if (isset($document_list) && is_array($document_list)) {
											// Find all keys corresponding to the value "PD"
											$list = array_keys($document_list, "PD");
											// Loop through each key and display the corresponding document link
											for ($i = 0; $i < count($list); $i++) {
												// Check if the index exists in $document_link array
												if (isset($document_link[$list[$i]])) {
													// Check access level and generate the appropriate link
													echo ($_SESSION['access'] != "User") ? "<a href='../documents/".$_REQUEST["trackingid"]."/".$document_link[$list[$i]]."' target='_blank'>".$document_link[$list[$i]]."</a><br />" : $document_link[$list[$i]]."<br />";
												} else {
													// Handle the case when the document link is not found
													echo "Document link is not available.<br />";
												}
											}
										} else {
											// Handle the case when $document_list is not set or is not an array
											echo "Document list is not available.";
										}
										?>
									</td>
								</tr>
								
							</table>
							</div>
						</div>
						<div class="panel-footer">
                            &nbsp;
                        </div>
                    </div>
					<!-- /.panel -->
                </div>
				<!-- TAG PERSON POP UP WINDOW START-->
				<!--<div id="light" class="white_content">
				<a href = "javascript:void(0)"  onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';window.location.reload();"><div class="btn">Cancel</div></a>
					
				</div>
				<div id="fade" class="black_overlay"></div>-->
				<!-- TAG PERSON POP UP WINDOW END-->
				
				<div class="col-lg-12 text-center">
					<input type="hidden" name="hid_req_count" id="hid_req_count" value="<?php echo $required_count; ?>">
					<?php if($_SESSION['access'] == "User"){ ?>
					<!--<button type="submit" onClick="window.location.href='terms.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-ok"></i> Confirm</button>-->
					<button type="submit" id="submit" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-ok"></i> CONFIRM</button>
					<?php } else { ?>
					<!--<button type="button" onClick="window.location.href='?form_accept=<?php echo $_REQUEST["trackingid"]; ?>&status=SL'" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-ok"></i> SHORT LISTED</button>
					<button type="button" onClick="window.location.href='?form_accept=<?php echo $_REQUEST["trackingid"]; ?>&status=FR'" class="btn btn-outline btn-danger"><i class="glyphicon glyphicon-remove"></i> REJECTED</button>-->
					<!--<button type="button" onClick="window.location.href='applicationform_print.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-print"></i> PRINT</button>-->
					<button type="button" onClick="window.print()" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-print"></i>PRINT</button>
					<button type="button" onClick="window.open('viewApplyFormToPDF.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>');" class="btn btn-outline btn-footer"><i class="fa fa-download"></i>VIEW & DOWNLOAD</button>
					<button type="button" onClick="window.open('viewApplyFormToPDF.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>&email=1');" class="btn btn-outline btn-footer"><i class="fa fa-download"></i>EMAIL</button>
					<?php } ?>
					<button type="button" class="btn btn-outline btn-danger" onclick="history.go(-1);"><i class="fa fa-arrow-circle-o-left fa-1x"></i> CANCEL</button>
					<br><br><br><br>
				</div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
	
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    
	<!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    
	<script type="text/javascript">
      $(document).ready(function() {
		var flag = 0;
        $("#submit").click(function( event ) {
			var required_count = $("#hid_req_count").val();
			for(var i=0; i<required_count; i++){
				if( !$.trim( $('#check_required'+i).html() ).length ){
					$('#check_required'+i).css('opacity', '.4');
					$('#check_required'+i).css('background-color','#f8f400');
					flag++;
				}
			}
			if(flag>0){
				alert( flag+" mandatory fields have no data." );
				flag = 0;
			}
			
			else
				window.location.href='terms.php?trackingid=<?php echo $_REQUEST["trackingid"]; ?>';
			event.preventDefault();
		});

      });
    </script>
	<script type="text/javascript">
      // $(document).ready(function() {
        // var submit = $('#tag');
        // submit.on('click', function(e) {
          // document.getElementById('light').style.display='block';
          // document.getElementById('fade').style.display='block';
        // });

      // });
    </script>
    <script type="text/javascript">
    function redirect()
    {
    var url = "http://www.(url).com";
    window.location(url);
    //setTimeout('window.location.href="apg.php"', 0);
    }
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>