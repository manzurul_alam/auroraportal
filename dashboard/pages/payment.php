<?php 
session_start();

if(!empty($_SESSION['user_id'])){
//$_SESSION['tracking_number'] = "";
include ('function.php');
$conn=dbConnect();
//$message = 0;
$count = 0;

if(isset($_POST['save'])){
    if(isset($_POST["trackingId"]) AND 
        isset($_POST["payFor"]) AND 
        isset($_POST["paymentDate"]) AND 
        isset($_POST["paymentAmount"]) AND 
        isset($_POST["trxNo"]) AND 
        isset($_POST["trackingStatus"]) AND 
        $_POST["trackingId"] != "" AND 
        $_POST["payFor"] != "" AND 
        $_POST["paymentDate"] != "" AND 
        $_POST["paymentAmount"] != "" AND 
        $_POST["trxNo"] != "" AND 
        $_POST["trackingStatus"] != "" ){

        if(isset($_POST["payFrom"])){
            if($_POST["payFrom"] == "School" AND $_POST["branchName"] == " ") { 
                $message = "Please insert branch name."; 
            }
            elseif($_POST["payFrom"] == "Cash in Bank"  AND ($_POST["bankName"] == " " OR $_POST["branchName"] == " ")) { 
                $message = "Please insert the bank and branch name."; 
            }
            elseif($_POST["payFrom"] == "Fund Transfer" OR $_POST["payFrom"] == "Mobile Banking") { 
                $message = "This payment system is not incorporated."; 
            }
            else{
                if(isset($_POST["hidPaymentId"]) AND $_POST["hidPaymentId"] != ''){
                    mysqli_query($conn, "UPDATE ARRA_payment_info SET
                        `ARRA_tracking_number` = '".$_POST["trackingId"]."', 
                        `ARRA_payment_type` = '".$_POST["payFor"]."',
                        `ARRA_payment_amount` = '".$_POST["paymentAmount"]."',
                        `ARRA_payment_date` = '".$_POST["paymentDate"]."',
                        `ARRA_payment_authorize_by` = '".$_SESSION['user_id']."',
                        `ARRA_payment_process` = '".$_POST["payFrom"]."', 
                        `ARRA_payment_trx_no` = '".$_POST["trxNo"]."',
                        `ARRA_bank_name` = '".$_POST["bankName"]."',
                        `ARRA_branch_name` = '".$_POST["branchName"]."'
                        WHERE `ARRA_payment_id` = '".$_POST["hidPaymentId"]."'");

                    mysqli_query($conn, "UPDATE ARRA_tracking SET ARRA_tracking_status = '".$_POST["trackingStatus"]."' WHERE ARRA_tracking_number = '".$_POST["trackingId"]."'");    
                    $message = "Data Successfully Updated...";
                }
				elseif(mysqli_query($conn, "INSERT INTO `ARRA_payment_info` (`ARRA_payment_id`, `ARRA_tracking_number`, `ARRA_payment_type`, `ARRA_payment_amount`, `ARRA_payment_date`, `ARRA_payment_authorize_by`, `ARRA_payment_process`, `ARRA_payment_trx_no`, `ARRA_bank_name`, `ARRA_branch_name`, `ARRA_last_update`) VALUES (NULL, '".$_POST["trackingId"]."', '".$_POST["payFor"]."', '".$_POST["paymentAmount"]."', '".$_POST["paymentDate"]."', '".$_SESSION['user_id']."', '".$_POST["payFrom"]."', '".$_POST["trxNo"]."', '".$_POST["bankName"]."', '".$_POST["branchName"]."', CURRENT_TIMESTAMP)")){
					$idQuery = mysqli_query($conn, "SELECT MAX(ARRA_payment_id) id FROM ARRA_payment_info");
					$id = mysqli_fetch_array($idQuery);
					mysqli_query($conn, "UPDATE ARRA_tracking SET ARRA_tracking_status = '".$_POST["trackingStatus"]."' WHERE ARRA_tracking_number = '".$_POST["trackingId"]."'");
					$message = "Successfully Saved. Payment ID is: ".$id["id"];
				}
				else{
					$message = "Data insert error !!!";
				}
            } 
        }   
    }
    else{
        $message = "Mandatory field missing.";
    }
}
elseif(isset($_REQUEST["paymentid"])){
	$query = mysqli_query($conn, "SELECT api.*, at.ARRA_tracking_status FROM ARRA_payment_info api, ARRA_tracking at WHERE api.ARRA_tracking_number = at.ARRA_tracking_number AND api.ARRA_payment_id = '".$_REQUEST["paymentid"]."'");

    if(mysqli_num_rows($query) > 0){
        $editPaymentData = mysqli_fetch_array($query);
    }
    else{
        $message = "No Data Found";
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Payment</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                           Add Payment
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<!-- /.panel-heading -->
									<div class="panel-body">
										<!-- Tab panes -->
										<div class="tab-content voffset4">
											<div class="tab-pane fade in active" id="home-pills">
												<div class="row">
													<div class="col-lg-12">
														<?php 
														 if(isset($message)){
															echo '<div class="row voffset2">
																	<div class="col-md-6 col-md-offset-3">
																		<div class="alert alert-success alert-dismissable">
																			<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>'.$message.'
																		</div>
																	</div>
																</div>';
															 //echo $message;
															 unset($message);
														  }
														?>
														<form role="form" method="POST" action="#">
															<fieldset>
																<div class="col-md-6">
																	<label>Payment ID(System Generated)</label>
																	<input type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_id"])) echo "value=".$editPaymentData["ARRA_payment_id"]; ?> disabled /> 
																	<input name="hidPaymentId" type="hidden" <?php if(isset($editPaymentData["ARRA_payment_id"])) echo "value='".$editPaymentData["ARRA_payment_id"]."'"; else echo "value=''"; ?> /> 
																</div>
																<div class="col-md-6">
																	<label>ARRA Tracking ID</label>
																	<input name="trackingId" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_tracking_number"])) echo "value=".$editPaymentData["ARRA_tracking_number"]; elseif(isset($_REQUEST["trackingid"])) echo "value=".$_REQUEST["trackingid"]; ?> required tabindex=1 autofocus /> 
																</div>
																<div class="col-md-6">
																	<label>Pay For</label>
																	<input name="payFor" type="text" list="payForList" class="form-control" autocomplete="off" <?php if(isset($editPaymentData["ARRA_payment_type"])) echo "value=".$editPaymentData["ARRA_payment_type"]; ?> required tabindex=2 />
																	<datalist id="payForList">
																		<option>Admission Fees</option>
																		<option>Application Fees</option>
																		<option>Late Fees</option>
																		<option>Semester Fees</option>
																	</datalist>
																</div>
																<div class="col-md-6">
																	<label>Payment Date</label>
																	<input name="paymentDate" type="text" id="datetimepicker1" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_date"])) echo "value=".$editPaymentData["ARRA_payment_date"]; ?> required tabindex=3 /> 
																</div>
																<div class="col-md-6">
																	<label>Payment Amount</label>
																	<input name="paymentAmount" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_amount"])) echo "value=".$editPaymentData["ARRA_payment_amount"]; ?> required tabindex=4 /> 
																</div>
																<div class="col-md-6">
																	<label>Transaction Reference No</label>
																	<input name="trxNo" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_trx_no"])) echo "value=".$editPaymentData["ARRA_payment_trx_no"]; ?> required tabindex=5 /> 
																</div>

																<div class="col-md-6">
    <label>Tracking Status</label>
    <input name="trackingStatus" type="text" list="trackingStatusList" autocomplete="off" class="form-control" 
        <?php if(isset($editPaymentData["ARRA_tracking_status"])) echo "value=\"" . $editPaymentData["ARRA_tracking_status"] . "\""; ?> 
        required tabindex=6 />
    <datalist id="trackingStatusList">
        <?php 
        $ARRA_status = array();
        $ARRA_dateline = array();
        $i = 0;
        // Assuming you have a $conn variable for the database connection
        $query = mysqli_query($conn, "SELECT * FROM ARRA_dateline_status ORDER BY ARRA_dateline_id DESC") or die(mysqli_error($conn));
        while($rows = mysqli_fetch_array($query)){ ?>
            <option value="<?php echo $rows["ARRA_status"]; ?>"><?php echo getStatusDetail($rows["ARRA_status"]); ?></option>
        <?php } ?>
    </datalist>
</div>

																
																<div class="col-md-6">
																	<label>Payment From</label>
																	<input name="payFrom" type="text" list="payFromList" class="form-control" autocomplete="off" <?php if(isset($editPaymentData["ARRA_payment_process"])) echo "value=".$editPaymentData["ARRA_payment_process"]; ?> required tabindex=7 />
																	<datalist id="payFromList">
																		<option>Corporate Office</option>
																		<option>School</option>
																		<option>Cash in Bank</option>
																		<option>Fund Transfer</option>
																		<option>Mobile Banking</option>
																	</datalist>
																</div>
																<div class="col-md-6">
																	<label>Bank Name</label>
																	<input name="bankName" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_bank_name"])) echo "value='".$editPaymentData["ARRA_bank_name"]."'"; else echo "value=' '"; ?> tabindex=8 /> 
																</div>
																<div class="col-md-6">
																	<label>Branch Name (For School/ Bank)</label>
																	<input name="branchName" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_branch_name"])) echo "value='".$editPaymentData["ARRA_branch_name"]."'"; else echo "value=' '"; ?> tabindex=9 /> 
																</div>
																<div class="col-md-12">
																	<div>&nbsp;</div>
																	<button type="submit" name="save" id="save" class="btn btn-outline btn-success"><i class="fa fa-database fa-1x"></i> SAVE</button>&nbsp;
																	<button type="button" class="btn btn-outline btn-danger" onclick="history.go(-1);"><i class="fa fa-arrow-circle-o-left fa-1x"></i> CANCEL</button>

																</div>
																
															</fieldset>
														</form>
													</div><!-- /.col-lg-4 -->
												</div><!-- /.row --> 
												   
											</div>
											<div class="tab-pane fade" id="all-child-pills">
											   
											</div>
										</div>
									</div>
									<!-- /.panel-body -->
								</div>
								<!-- /.panel -->
                            </div>
							<!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
					
					<!-- nested panel -->
					<div class="panel panel-danger">
                        <div class="panel-heading">
                           View All Payments
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Following Tracking Number(s) has been generated
										</div>
										<!-- /.panel-heading -->
										<div class="panel-body">
											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="dataTables-payment">
													<thead>
														<tr class="text-center">
															<th width=70px>SL</th>
															<th>Pmt.ID</th>
															<th>Tracking No</th>
															<th>Pay For</th>
															<th>Payment Date</th>
															<th>Amount</th>
															<th>TRX No</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php $i=1;
													$query = mysqli_query($conn, "SELECT * FROM ARRA_payment_info ORDER BY ARRA_payment_id DESC") or die(mysqli_error($connection));
														while($rows = mysqli_fetch_array($query)){ ?>
														<tr class="odd gradeX">
															<td><?php echo $i++; ?></td>
															<td><?php echo $rows["ARRA_payment_id"]; ?></td>
															<td><?php echo $rows["ARRA_tracking_number"]; ?></td>
															<td><?php echo $rows["ARRA_payment_type"]; ?></td>
															<td><?php echo $rows["ARRA_payment_date"]; ?></td>
															<td><?php echo $rows["ARRA_payment_amount"]; ?></td>
															<td><?php echo $rows["ARRA_payment_trx_no"]; ?></td>
															<td class="text-center">
    <?php
    if ($_SESSION['access'] == "SuperAdmin" || getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "PSE") == "PSE") {
    ?>
        <a href="?paymentid=<?php echo $rows["ARRA_payment_id"]; ?>" title='Edit'>
            <button type='button' class='btn btn-success'><i class='glyphicon glyphicon-pencil'></i></button>
        </a>&nbsp;
    <?php
    }
    ?>
    
    <a href="payment_con_modal_print.php?paymentid=<?php echo $rows["ARRA_payment_id"]; ?>" title='Apply' <?php if(isset($status) && $status == 'Initiation') echo "style='pointer-events: none;'"; ?>>
        <button type='button' class='btn btn-success'><i class='glyphicon glyphicon-print'></i></button>
    </a>
</td>

														</tr>	
															<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
										<!-- /.panel-body -->
									</div>
									<!-- /.panel -->
								</div>
								<!-- /.col-lg-12 -->
							</div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel (nested) -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datetimepicker1').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>\
	
	<!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<script src="../dist/js/applyforchk.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
			$('#dataTables-payment').DataTable({
				responsive: true
			});
			
             document.getElementById("myButton").onclick = function () {
                location.href = "apg.php";
            };
                              
          });
    </script>
    
    <script type="text/javascript">
    function redirect()
    {
    var url = "http://www.(url).com";
    window.location(url);
    //setTimeout('window.location.href="apg.php"', 0);
    }
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>