<?php
session_start();
include ('function.php'); // Assuming this file contains your database connection code
dbConnect(); // Assuming this function establishes a database connection

// Check if 'type' parameter is set and not empty
if(isset($_GET['type']) && $_GET['type'] !== '') {
    $type = mysqli_real_escape_string(dbConnect(), $_GET['type']);

    $q = mysqli_query(dbConnect(), "SELECT cat_id FROM category WHERE cat_name = '$type'") or die(mysqli_error(dbConnect()));
    $row = mysqli_fetch_assoc($q);

    // Check if a category with the given name exists
    if($row) {
        $catid = $row['cat_id'];

        $oriQuery = "SELECT * FROM subcategory, user_alias WHERE id=creator AND catid=$catid";
        $query = mysqli_query(dbConnect(), $oriQuery) or die(mysqli_error(dbConnect()));
        $i = 1;

        while($rows = mysqli_fetch_assoc($query)){ ?>
            <tr id="<?php echo $rows["subcat_id"]; ?>" class="odd gradeX">
                <td><?php echo $i++; ?></td>
                <td><?php echo $rows["subcat_name"]; ?></td>
                <td>
                    <?php if($_SESSION['alias_id'] == $rows["creator"]) { ?>
                        <button class="btn_del_subcat" data-id="<?php echo $rows["subcat_id"]; ?>" data-name="<?php echo $rows["subcat_name"]; ?>" type="button" class="btn btn-danger"><i class="fa fa-trash-o fa-1x"></i> </button>
                    <?php } else echo "by - ".$rows["email"]; ?>
                </td>
            </tr>    
        <?php }
    } else {
        echo "Category not found.";
    }
} else {
    echo "Category type parameter is missing or empty.";
}
?>
