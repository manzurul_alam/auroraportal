<?php 
session_start();

if (!empty($_SESSION['user_id'])) {
    include('function.php');
    dbConnect();
    $message = "";

    if (isset($_POST['save'])) {
        $trackingId = $_POST["trackingId"];
        $trackingStatus = $_POST["trackingStatus"];

        $query = mysqli_query(dbConnect(), "UPDATE ARRA_tracking SET ARRA_tracking_status = '$trackingStatus' WHERE ARRA_tracking_number = '$trackingId'");
        if ($query) {
            $message = "Data Successfully Updated...";
        } else {
            $message = "Mandatory field missing.";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>..::AIS::..</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <div id="wrapper">
        <?php include('nav.php') ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Change Status</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                           Change Status
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php 
                                    if (!empty($message)) {
                                        echo '<div class="row voffset2">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <div class="alert alert-success alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>'.$message.'
                                                    </div>
                                                </div>
                                            </div>';
                                    }
                                    ?>
                                    <form role="form" method="POST" action="#">
                                        <fieldset>
                                            <div class="col-md-6">
                                                <label>ARRA Tracking ID</label>
                                                <input readonly name="trackingId" type="text" class="form-control" value="<?php if(isset($_REQUEST["trackingid"])) echo $_REQUEST["trackingid"]; ?>" required tabindex="1" autofocus /> 
                                            </div>
                                            <div class="col-md-6">
                                                <label>Tracking Status</label>
                                                <input name="trackingStatus" type="text" value="<?php if(isset($_REQUEST["status"])) echo $_REQUEST["status"]; ?>" list="trackingStatusList" class="form-control" required tabindex="2" autocomplete="off" />
                                                <datalist id="trackingStatusList">
                                                    <?php 
                                                    $query = mysqli_query(dbConnect(), "SELECT ARRA_status FROM ARRA_dateline_status ORDER BY ARRA_dateline_id DESC") or die(mysqli_error(dbConnect()));
                                                    while($rows = mysqli_fetch_array($query)){ ?>
                                                        <option value="<?php echo $rows["ARRA_status"]; ?>"><?php echo  getStatusDetail($rows["ARRA_status"]); ?></option>
                                                    <?php } ?>
                                                </datalist>
                                            </div>
                                            <div class="col-md-12">
                                                <div>&nbsp;</div>
                                                <button type="submit" name="save" id="save" class="btn btn-outline btn-success"><i class="fa fa-database fa-1x"></i> Update Status</button>&nbsp;
                                                <a href="AIP.php" title='Change Status'>
                                                    <button type='button' class='btn btn-danger'><i class="fa fa-arrow-circle-o-left fa-1x"></i> CANCEL</button>
                                                </a>&nbsp;
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>
</body>
</html>

<?php
} else {
    require_once 'login.php';
}
?>
