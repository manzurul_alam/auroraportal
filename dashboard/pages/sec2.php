
<?php
session_start();
if(!empty($_SESSION['user_id']) && $_SESSION['tracking_number']){
include ('function.php');
dbConnect();
if(isset($_POST['save'])){
    
    if(!empty($_POST['schoolName']) && !empty($_POST['startDate']) && !empty($_POST['grade']) && !empty($_POST['finishDate']) && !empty($_POST['location']) && !empty($_POST['loi'])){
       $stuSql = "UPDATE student_details SET a_stu_cuschool = '".$_POST['schoolName']."',a_stu_startdate = '".$_POST['startDate']."',a_stu_finishdate = '".$_POST['finishDate']."',a_stu_grade = '".$_POST['grade']."',a_stu_location = '".$_POST['location']."',a_stu_language = '".$_POST['loi']."' WHERE a_stu_trackingNumber= '".$_SESSION['tracking_number']."'";
       $applyResult = mysql_query($stuSql) or die(mysql_error());
        if($applyResult){
            echo "<script type='text/javascript'>window.location='sec3.php';</script>";
        } 
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">SECTION 2: -- Education History</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            History of your child's education (Tracking Number: <?php echo $_SESSION['tracking_number']; ?>)
                        </div>
                        <div class="panel-body">
                        <form role="form" method="POST" action="#">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Have child attended any other school before?</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="beforeschool" id="beforeschoolyes" value="beforeschoolYes" >Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="beforeschool" id="beforeschoolno" value="beforeschoolNo">No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="beforeschoolid">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Name of school</label>
                                                <input id="schoolName" name="schoolName" class="form-control" placeholder="Type Here..." />
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Academic Year</label>
                                                <input type="text" name="startDate" class="form-control" id="academicyear"  name="academicyear" value="" placeholder="Type Academic Year..." />
                                                
                                            </div>
                                        </div>
										
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Grade / Class</label>
                                                <input id="grade" name="grade" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Language of instruction</label>
                                                <input id="loi" name="loi" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Location</label>
                                                <input id="location" name="location" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                    
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            
                            <div class="row">
                                <div  class="col-lg-12">
                                    <div class="col-lg-12 text-center">
                                            <div class="form-group">
                                                <button type="submit" name="save" id="save" class="btn btn-outline btn-success">CONTINUE <i class="fa fa-hdd-o fa-1x"></i></button>
                                                <button type="submit" class="btn btn-outline btn-danger">SAVE & QUIT <i class="fa fa-database fa-1x"></i></button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            </form>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
              $('#datetimepicker2').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
	<script type="text/javascript">
        $(document).ready(function() {
              $('#datetimepicker3').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#beforeschoolid').hide();
        $("input").click(function(event) {
				var id = event.target.id;
				
                console.log($("#"+id).val());
				
                if ($("#"+id).val() == "beforeschoolYes"){
					$("#beforeschoolid").show("slow");
				}else if($("#"+id).val() == "beforeschoolNo"){
					$("#beforeschoolid").hide("slow");
				}
			});   
    });
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>