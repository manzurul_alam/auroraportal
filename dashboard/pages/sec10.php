<?php
session_start();
if(!empty($_SESSION['user_id']) && $_SESSION['tracking_number']){
include ('function.php');
dbConnect();

$total = rand(1,100);
$current = rand(1,$total);
$percent = round(($current/$total)*100);

$xpname = $_SESSION['tracking_number'];
if(!is_dir('../documents/'.$xpname)){
	mkdir('../documents/'.$xpname, 0777, true);
	chmod('../documents/', 0777);
}

$path ="../documents/".$xpname."/" ; // Upload directory

if(isset($_REQUEST["removeDoc"])){
	$query = mysqli_query(dbConnect(),"DELETE FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_name = '".$_REQUEST["removeDoc"]."'");
	if($query){
		unlink($path.$_REQUEST["removeDoc"]);
		$message = "File Remove Successfully...";
	}
}
if(isset($_REQUEST["removeAll"])){
	$query = mysqli_query(dbConnect(),"DELETE FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' ");
	if($query){
		
		array_map('unlink', glob("$path*.*"));
		
	}
	$message = "All Files Removed Successfully...";
	echo "<script type='text/javascript'>window.location='dashboard.php';</script>";
}
elseif(isset($_POST["but_std_photo_upload"])){
	
	// stu photo rename
    $std_uploaded_doc =$_FILES["stuPhoto"]["tmp_name"];
    $std_uploaded_doc_path = $_FILES["stuPhoto"]["name"];
	$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path, PATHINFO_EXTENSION);
	if(getImgFileValid($std_uploaded_doc_ext)){
		$std_uploaded_doc_new =($xpname."_SP_".time().'.'.$std_uploaded_doc_ext);
		
		if(is_uploaded_file($std_uploaded_doc)){
			if(move_uploaded_file($std_uploaded_doc,$path.$std_uploaded_doc_new)){
				$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'SP',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
				if($query){
					$message = "Successfully Uploaded...";
					//====================================== Progress Bar Calculation =========================================//
					mysqli_query(dbConnect(),"UPDATE ARRA_progress_bar SET sec6 = sec6+'20' WHERE tracking_number = '".$_SESSION['tracking_number']."'") ;
					//====================================== /.Progress Bar Calculation =======================================//
					
				}
				else
					$message = "File saving error!!!";
			}
			else
				$message = "Move upload file error!!!";
		}
		else
			$message = "Upload error!!!";
	}
	else
		$message = "Error, file format incorrect. only jpg or png support";
}
elseif(isset($_POST["but_std_birth_certificate"])){
	
	// std Birth Certificate
    $std_uploaded_doc =$_FILES["stdBirthCertificate"]["tmp_name"];
    $std_uploaded_doc_path = $_FILES["stdBirthCertificate"]["name"];
	$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path, PATHINFO_EXTENSION);
	if(getDocFileValid($std_uploaded_doc_ext)){
		$std_uploaded_doc_new =($xpname."_SBC_".time().'.'.$std_uploaded_doc_ext);
		
		if(is_uploaded_file($std_uploaded_doc)){
			if(move_uploaded_file($std_uploaded_doc,$path.$std_uploaded_doc_new)){
				$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'SBC',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
				if($query){
					$message = "Successfully Uploaded...";
					//====================================== Progress Bar Calculation =========================================//
					mysqli_query(dbConnect(),"UPDATE ARRA_progress_bar SET sec6 = sec6+'20' WHERE tracking_number = '".$_SESSION['tracking_number']."'") ;
					//====================================== /.Progress Bar Calculation =======================================//
				}
				else
					$message = "File saving error!!!";
			}
			else
				$message = "Move upload file error!!!";
		}
		else
			$message = "Upload error!!!";
	}
	else
		$message = "Error, file format incorrect.";
}
elseif(isset($_POST["but_std_passport"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["stuPassport"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["stuPassport"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_SPP_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'SPP',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query){
						$message = "Successfully Uploaded...";
						//====================================== Progress Bar Calculation =========================================//
						mysqli_query(dbConnect(),"UPDATE ARRA_progress_bar SET sec6 = sec6+'20' WHERE tracking_number = '".$_SESSION['tracking_number']."'") ;
						//====================================== /.Progress Bar Calculation =======================================//
					}
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}
elseif(isset($_POST["but_pgd"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["fatherPassport"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["fatherPassport"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_PGD_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'PGD',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query)
						$message = "Successfully Uploaded...";
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}
elseif(isset($_POST["but_mother_passport"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["motherPassport"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["motherPassport"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_MP_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'MP',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query)
						$message = "Successfully Uploaded...";
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}

elseif(isset($_POST["but_work_permit"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["workPermit"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["workPermit"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_WP_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'WP',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query)
						$message = "Successfully Uploaded...";
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}

elseif(isset($_POST["but_std_reports"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["reportCard"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["reportCard"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_SRC_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'SRC',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query){
						$message = "Successfully Uploaded...";
						//====================================== Progress Bar Calculation =========================================//
						mysqli_query(dbConnect(),"UPDATE ARRA_progress_bar SET sec6 = sec6+'20' WHERE tracking_number = '".$_SESSION['tracking_number']."'") ;
						//====================================== /.Progress Bar Calculation =======================================//
					}
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}

elseif(isset($_POST["but_ivr_records"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["vaccinationReport"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["vaccinationReport"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_IVR_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'IVR',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query){
						$message = "Successfully Uploaded...";
						//====================================== Progress Bar Calculation =========================================//
						mysqli_query(dbConnect(),"UPDATE ARRA_progress_bar SET sec6 = sec6+'20' WHERE tracking_number = '".$_SESSION['tracking_number']."'") ;
						//====================================== /.Progress Bar Calculation =======================================//
					}
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}
elseif(isset($_POST["but_specialist"])){
	
	// std Birth Certificate
    $std_uploaded_doc = $_FILES["disability"]["tmp_name"];
	$std_uploaded_doc_path = $_FILES["disability"]["name"];
	for($i = 0; $i < count($std_uploaded_doc); $i++){
		$std_uploaded_doc_ext = pathinfo($std_uploaded_doc_path[$i], PATHINFO_EXTENSION);
		if(getDocFileValid($std_uploaded_doc_ext)){
			$std_uploaded_doc_new =($xpname."_PD_".time().$i.'.'.$std_uploaded_doc_ext);
			
			if(is_uploaded_file($std_uploaded_doc[$i])){
				if(move_uploaded_file($std_uploaded_doc[$i], $path.$std_uploaded_doc_new)){
					$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_documents (`id` , `ARRA_tracking_number` ,`ARRA_file_type` ,`ARRA_file_name` ,`last_update` ) VALUES ( NULL ,  '".$_SESSION['tracking_number']."',  'PD',  '".$std_uploaded_doc_new."', CURRENT_TIMESTAMP)") ;
					if($query)
						$message = "Successfully Uploaded...";
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Error, file format incorrect.";
			break;
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="khokon" >

            <title>..::AIS::..</title>

            <!-- Bootstrap Core CSS -->
            <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

            <!-- MetisMenu CSS -->
            <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

            <!-- Custom CSS -->
            <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

            <!-- Custom Fonts -->
            <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    
	<style>
	/*Copied from bootstrap */
	.btn {
		display: inline-block;
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: normal;
		line-height: 1.42857143;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
	}
	/*Also */
	.btn-success {
		color: #fff;
		background-color: #5cb85c;
		border-color: #4cae4c;
	}
	/* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
	.fileinput-button {
		position: relative;
		overflow: hidden;
	}
	/*Also*/
	.fileinput-button input {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		opacity: 0;
		-ms-filter:'alpha(opacity=0)';
		font-size: 200px;
		direction: ltr;
		cursor: pointer;
	}
	.rptrmvbtn{
		padding: 6px 6px !important;
	}
	</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            <div class="row">
                <!-- Progress Bar 
                        <div class="row voffset2" style="margin-left: 5%;">
							<?php //include('appMenu.php'); ?>
						</div>
                         End Progress Bar -->
                <div class="col-lg-12">
                    <h1 class="page-header">Section 6. Supporting Documents</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-arra" style="border-color: #156059;">
                        <div class="panel-heading">
							<?php include('headpanel.php'); ?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
								<?php 
								 if(isset($message)){
									echo '<div class="row voffset2" id="messageDiv">
											<div class="col-md-6 col-md-offset-3">
												<div class="alert alert-success alert-dismissable">
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>'.$message.'
												</div>
											</div>
										</div>';
									 //echo $message;
									 unset($message);
								  }
								?>
                                     <div class="col-md-12">
										<label style="margin-top: -20px;margin-bottom: 20px;">Please scan and upload all the required documents.</label>
										<label style="margin-top: -20px;margin-bottom: 20px;">For every document please click on "Choose file" to select appropriate document from your hard drive. If you need to add more than one document, please click on "Add more". After you have selected all the files that you want to upload for a particular question, please click the "Upload" button to attach them to the application. </label>
									</div>
									 <div class="col-lg-12 bordernew tooltip-demo">
                                            <div class="row">
                                                
												<div class="col-lg-6">
                                                    <div class="form-group">
														<label  ><span class="mandatory-read">*</span> Student's Passport-size Photo </label><br />
														<?php 
														$q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'SP'") ; 
														if(mysqli_num_rows($q)>0){ 
															$row = mysqli_fetch_array($q, MYSQLI_ASSOC); ?>
														<img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px />
														<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
														<?php }
														else { ?>
														<form role="form1" method="POST" action="#" enctype='multipart/form-data'>
															<div class="col-lg-12 marginBottom">
																<div class="col-lg-6">
																	<input type="file" name="stuPhoto" data-toggle="tooltip" data-placement="top" title="Student’s Passport-size Photo – a current photograph (within 3 months) ">
																</div>
																<div class="col-lg-6"><button type="submit" name="but_std_photo_upload" id="but_std_photo_upload" class="btn btn-new marginvertical" >Upload</button></div>
															</div>
														</form><?php 
														} ?>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 ">
                                                    <div class="form-group">
														<label><span class="mandatory-read">*</span> Student's Birth Certificate</label><br />
                                                        <?php 
														$q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'SBC'") ; 
														if(mysqli_num_rows($q)>0){ 
															$row = mysqli_fetch_array($q, MYSQLI_ASSOC); 
															$fileExt = explode(".", $row["ARRA_file_name"]);
															?>
														<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
														elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
														elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
														<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
														<?php }
														else { ?>
														<form role="form2" method="POST" action="#" enctype='multipart/form-data'>
															<div class="col-lg-12 marginBottom">
																<div class="col-lg-6"><input type="file" name="stdBirthCertificate" data-toggle="tooltip" data-placement="top" title="Original birth certificate in English (translated and certified by an official translator, if not in English)"></div>
																<div class="col-lg-6"><button type="submit" name="but_std_birth_certificate" id="but_std_birth_certificate" class="btn btn-new marginvertical" <?php if(isset($std_uploaded_doc_sbc_flag) and $std_uploaded_doc_sbc_flag) echo "disabled"; ?>>Upload</button></div>
															</div>
														</form><?php 
														} ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 bordernew">
                                            <div class="row">
											<div class="col-lg-12">
												<label><!--<span class="mandatory-read">*</span>-->Does the student have an individual passport?</label>
											</div>
											<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'SPP'") ; 
											if(mysqli_num_rows($q)>0){ 
												while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
													$fileExt = explode(".", $row["ARRA_file_name"]); ?>
													<div class="col-lg-6" style="height:100px;">
													<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
													elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
													elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
													<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
													</div><?php 
												}
											} else { ?>
												<div class="col-lg-12">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="studentPassport" id="SPYes" value="SPYes" >Yes
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="studentPassport" id="SPNo" value="SPNo">No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
												
                                                <div class="col-lg-12" id="studentPassport">
                                                    <form role="form3" method="POST" action="#" enctype='multipart/form-data'>
														<div class="col-lg-12">
															<div class="row" id="passportDivAppend">
																<div class="form-group">
																	<label>If yes, please upload a scanned copy of the photo page and visa page (applicable for non Bangldeshis only) </label>
																	<div class="col-lg-12 marginBottom">
																		<div class="col-lg-6"><input type="file" name="stuPassport[]"></div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-12" style="margin-bottom: 10px;">
															<div class="col-lg-6"><button type="button" class="btn btn-new" id="passaddbtnclick">Add More &nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
															<div class="col-lg-6" style="  margin-top: 6px;"><button type="submit" name="but_std_passport" class="btn btn-new marginvertical">Upload</button></div>
														</div>
													</form>
                                                </div><?php 
											}
											?>
                                            </div>
                                        </div>
                                        <?php
											$getSchoolStatus = mysqli_query(dbConnect(),"SELECT `ARRA_applying_has_school` FROM ARRA_applying WHERE ARRA_applying_tracking =  '".$_SESSION['tracking_number']."' AND ARRA_applying_has_school =  '1'") ;
											if(mysqli_num_rows($getSchoolStatus)==1){
												?>

															<div class="col-lg-12 bordernew">
																<div class="row">
																<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'SRC'") ; 
																if(mysqli_num_rows($q)>0){ 
																	while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
																		$fileExt = explode(".", $row["ARRA_file_name"]); ?>
																		<div class="col-lg-12">
																			<label>Report Card(s)</label>
																		</div>
																		<div class="col-lg-6" style="height:100px;">
																		<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
																		elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
																		elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
																		<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
																		</div><?php 
																	}
																} else { ?>
																	<div class="col-lg-12">
																	   <div class="form-group">
																			<div class="questions">    
																			<label style="float:left;width:50%;">Please upload past school, pre-school or day care report card:</label>  
																				
																			</div>     
																			<!--<div class="form-group">
																				<label class="radio-inline">
																					<input type="radio" name="reportCardsName" id="RCYes" value="RCYes" >Yes
																				</label>
																				<label class="radio-inline">
																					<input type="radio" name="reportCardsName" id="RCNo" value="RCNo" >No
																				</label>
																			</div>-->
																		</div>
																	</div>
																	<div class="col-lg-12">
																	<!--<div class="form-group">
																		<label>If, yes, please attach the progress report card.</label>
																	</div>-->
																	<form role="form7" method="POST" action="#" enctype='multipart/form-data'>
																		<div class="row" id="reportAppDiv">
																			<div class="form-group">
																				<div class="col-lg-12 marginBottom">
																					<div class="col-lg-6"><input type="file" name="reportCard[]"></div>
																				</div>
																			</div>
																		</div>
																		<div class="col-lg-12" style="margin-bottom: 10px;">
																			<div class="col-lg-6"><button type="button" class="btn btn-new" id="reportbtnclick">Add More &nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
																			<div class="col-lg-6" style="margin-top: 6px;"><button type="submit" name="but_std_reports" class="btn btn-new marginvertical">Upload</button></div>
																		</div>
																	</form>
																	</div><?php 
																}
																?>
																</div>
															</div>
														</div>
													
												<?php
											}
										?>
                                        <div class="col-lg-12 bordernew">
                                            <div class="col-lg-12">
                                                    <div class="row">
														<div class="col-lg-12">
														<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'IVR'") ; 
														if(mysqli_num_rows($q)>0){ 
															while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
																$fileExt = explode(".", $row["ARRA_file_name"]); ?>
																<div class="col-lg-12">
																	<label>Immunization/ Vaccination records</label>
																</div>
																<div class="col-lg-6" style="height:100px;">
																<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
																elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
																elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
																<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
																</div><?php 
															}
														} else { ?>
															<div class="col-lg-6">
															    <div class="form-group">
																	<label> Please upload complete immunization records/ vaccination records for the student (see the list provided in the instructions).</label>
																	<!--<div class="form-group">
																		<label class="radio-inline">
																			<input type="radio" name="vaccinationRecords" id="VRYes" value="VRYes" >Yes
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="vaccinationRecords" id="VRNo" value="VRNo" >No
																		</label>
																	</div>-->
																</div>
															</div>
															<div class="col-lg-12" id="vaccinationRecords">
																<form role="form8" method="POST" action="#" enctype='multipart/form-data'>
																	<div class="row" id="vacciappDiv">
																		<div class="form-group">
																		<div class="col-lg-12 marginBottom">
																			<div class="col-lg-6"> <input type="file" name="vaccinationReport[]"></div>
																		</div>
																	</div>
																	</div>
																	<div class="col-lg-12" style="margin-bottom: 10px;">
																		<div class="col-lg-6"><button type="button" class="btn btn-new" id="vaccibtnclick">Add More &nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
																		<div class="col-lg-6" style="margin-top: 6px;"><button type="submit" name="but_ivr_records" class="btn btn-new marginvertical">Upload</button></div>
																	</div>
																</form>
															</div><?php 
														}
														?>
                                                    </div>
                                            </div>
                                            
                                        </div>
										</div>
                                        <div class="col-lg-12 bordernew">
                                            
                                                <div class="col-lg-12" >
													<label><span style="color:red">*</span> Parents/Guardian’s Identification Document</label>
													<div style="float:right; margin-right:60%;"><i class="fa fa-question-circle icon-color-arra fa-2x pull-left" data-toggle="modal" data-target="#pgd"></i></div>
													<br />
													<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'PGD'") ; 
													if(mysqli_num_rows($q)>0){ 
														while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
															$fileExt = explode(".", $row["ARRA_file_name"]); ?>
															<div style="margin:10px 0px;">
															<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
															elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
															elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
															<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
															</div><?php 
														}
													} else { ?>
													<form role="form4" method="POST" action="#" enctype='multipart/form-data'>
	                                                    <div class="col-lg-12" id="fatherPPappDiv">
	                                                        <div class="form-group">
	                                                            <div class="col-lg-12 marginBottom">
	                                                                <div class="col-lg-6"><input type="file" name="fatherPassport[]"></div>
	                                                            </div>
	                                                        </div>
	                                                    </div> 
	                                                    <div class="col-lg-12" style="margin-bottom: 10px;">
	                                                         <div class="col-lg-6"><button type="button" class="btn btn-new" id="faaddbtnclick">Add More &nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
	                                                         <div class="col-lg-6" style="  margin-top: 6px;"><button type="submit" name="but_pgd" class="btn btn-new marginvertical">Upload</button></div>
	                                                    </div>
													</form><?php 
													}
													?>
                                                </div>
                                                <!--
                                                <div class="col-lg-6">
													<label>Mother's Passport (if not a Bangladeshi national)</label><br />
												<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'MP'") ; 
												if(mysqli_num_rows($q)>0){ 
													while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
														$fileExt = explode(".", $row["ARRA_file_name"]); ?>
														<div style="margin:10px 0px;">
														<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
														elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
														elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
														<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
														</div><?php 
													}
												} else { ?>
												<form role="form5" method="POST" action="#" enctype='multipart/form-data'>
                                                    <div class="col-lg-12" id="motherPPappDiv">
                                                        <div class="form-group">
                                                            <div class="col-lg-12 marginBottom">
                                                                <div class="col-lg-6"><input type="file" name="motherPassport[]"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12" style="margin-bottom: 10px;margin-left: 8%;">     
                                                        <div class="col-lg-6" ><button type="button" class="btn btn-new" id="moaddbtnclick">Add More &nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
                                                        <div class="col-lg-6" style="margin-top: 6px;"><button type="submit" name="but_mother_passport" class="btn btn-new marginvertical">Upload</button></div>
                                                   </div>
												</form><?php 
												}
												?>
                                                </div>-->
                                            
                                            
                                        </div>
										<div class="modal fade" id="pgd" tabindex="-1" role="dialog" aria-labelledby="pgdl" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header"  style="background-color:#156059; color:#FFFFFF;">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title" id="pgdl">Parents/Guardian’s Identification Document</h4>
													</div>
													<div class="modal-body">
														For foreign nationals, please scan and upload the photo page of the passport(s). For Bangladeshi nationals, please scan and upload the National ID cards or photo page of the passport.
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-new" data-dismiss="modal">Close</button>
													</div>
												</div>
												<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
										</div>
										<div class="col-lg-12 bordernew">
														<div class="col-lg-12">
																<div class="row">
																<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'WP'") ; 
																if(mysqli_num_rows($q)>0){ 
																	while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
																		$fileExt = explode(".", $row["ARRA_file_name"]); ?>
																		<div class="col-lg-12">
																			<label>Work permit</label>
																		</div>
																		<div class="col-lg-6" style="height:100px;">
																		<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
																		elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
																		elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
																		<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
																		</div><?php 
																	}
																} else { ?>
																   <div class="col-lg-6">
																	   <div class="form-group">
																			 <div class="questions">
																				<label style="width:84%;float:left;">Does either parent have a work permit for working in Bangladesh? (Applicable for non-resident Bangladeshis only.)</label>  
																			    <div style="float:right; margin-right:8%;"><i class="fa fa-question-circle icon-color-arra fa-2x pull-left" data-toggle="modal" data-target="#workit"></i></div>
																			</div>
																		    <div class="form-group">
																				<label class="radio-inline">
																					<input type="radio" name="workPermit" id="WPYes" value="WPYes" >Yes
																				</label>
																				<label class="radio-inline">
																					<input type="radio" name="workPermit" id="WPNo" value="WPNo" >No
																				</label>
																			</div>
																		<!-- Modal Dialog -->	
																		<div class="modal fade" id="workit" tabindex="-1" role="dialog" aria-labelledby="workitl" aria-hidden="true">
																			<div class="modal-dialog">
																				<div class="modal-content">
																					<div class="modal-header"  style="background-color:#156059; color:#FFFFFF;">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																						<h4 class="modal-title" id="workitl">Work Permit</h4>
																					</div>
																					 <div class="modal-body">
																					   For any parent who is a foreign national and is in Bangladesh on a work permit, please scan and upload a copy of the work permit.
																					 </div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-new" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				
																			</div>
																			
																		</div>
																				
																		</div>
																	</div>
																	<div class="col-lg-6" id="workPermit">
																	<form role="form6" method="POST" action="#" enctype='multipart/form-data'>
																		<div class="form-group">
																			<label>If yes, please attach a copy of the work permit.</label>
																		</div>
																		<div class="row" id="workpermitdiv" >
																			<div class="form-group">
																				<div class="col-lg-12 marginBottom">
																					<div class="col-lg-6"><input type="file" name="workPermit[]"></div>
																				</div>
																			</div>
																		</div>
																		
																		<div class="col-lg-12" style="margin-bottom: 10px;">
																		   <div class="col-lg-6" > <button type="button" class="btn btn-new" id="workpermitbtnclick">Add More &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
																			<div class="col-lg-6" style="margin-top: 6px;"><button type="submit" name="but_work_permit" class="btn btn-new marginvertical">Upload</button></div>
																		</div>
																												

																		
																		
																	</form>
																	</div><?php 
																}
																?>
																</div>
															</div>
													</div> 
										<!-- /.modal -->
                                        <?php
											/*
											$getCNStatus = mysqli_query(dbConnect(),"SELECT ARRA_childs_detail_CN FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_childs_detail_CN='Bangladesh'") ;;
											if(mysqli_num_rows($getCNStatus)==1){
												echo "";
											}else{
												?>
													<div class="col-lg-12 bordernew">
														<div class="col-lg-12">
																<div class="row">
																<?php $q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'WP'") ; 
																if(mysqli_num_rows($q)>0){ 
																	while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
																		$fileExt = explode(".", $row["ARRA_file_name"]); ?>
																		<div class="col-lg-12">
																			<label>Work permit</label>
																		</div>
																		<div class="col-lg-6" style="height:100px;">
																		<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
																		elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
																		elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
																		<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
																		</div><?php 
																	}
																} else { ?>
																   <div class="col-lg-6">
																	   <div class="form-group">
																			 <div class="questions">
																				<label style="width:84%;float:left;">Does either parent have a work permit for working in Bangladesh? (Applicable for non-resident Bangladeshis only.)</label>  
																			    <div style="float:right; margin-right:8%;"><i class="fa fa-question-circle icon-color-arra fa-2x pull-left" data-toggle="modal" data-target="#workit"></i></div>
																			</div>
																		    <div class="form-group">
																				<label class="radio-inline">
																					<input type="radio" name="workPermit" id="WPYes" value="WPYes" >Yes
																				</label>
																				<label class="radio-inline">
																					<input type="radio" name="workPermit" id="WPNo" value="WPNo" >No
																				</label>
																			</div>
																		<!-- Modal Dialog -->	
																		<div class="modal fade" id="workit" tabindex="-1" role="dialog" aria-labelledby="workitl" aria-hidden="true">
																			<div class="modal-dialog">
																				<div class="modal-content">
																					<div class="modal-header"  style="background-color:#156059; color:#FFFFFF;">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																						<h4 class="modal-title" id="workitl">Work Permit</h4>
																					</div>
																					 <div class="modal-body">
																					   For any parent who is a foreign national and is in Bangladesh on a work permit, please scan and upload a copy of the work permit.
																					 </div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-new" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				
																			</div>
																			
																		</div>
																				
																		</div>
																	</div>
																	<div class="col-lg-6" id="workPermit">
																	<form role="form6" method="POST" action="#" enctype='multipart/form-data'>
																		<div class="form-group">
																			<label>If yes, please attach a copy of the work permit.</label>
																		</div>
																		<div class="row" id="workpermitdiv" >
																			<div class="form-group">
																				<div class="col-lg-12 marginBottom">
																					<div class="col-lg-6"><input type="file" name="workPermit[]"></div>
																				</div>
																			</div>
																		</div>
																		
																		<div class="col-lg-12" style="margin-bottom: 10px;">
																		   <div class="col-lg-6" > <button type="button" class="btn btn-new" id="workpermitbtnclick">Add More &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
																			<div class="col-lg-6" style="margin-top: 6px;"><button type="submit" name="but_work_permit" class="btn btn-new marginvertical">Upload</button></div>
																		</div>
																												

																		
																		
																	</form>
																	</div><?php 
																}
																?>
																</div>
															</div>
													</div> 

												<?php
											}*/
										?>
										<?php 
										$getLPSpecialist = mysqli_query(dbConnect(),"SELECT ARRA_learning_profile_specialist FROM ARRA_learning_profile WHERE ARRA_learning_profile_tracking = '".$_SESSION['tracking_number']."' AND ARRA_learning_profile_specialist ='SpecialistYes'") ;
										if(mysqli_num_rows($getLPSpecialist) == 1){
											$q = mysqli_query(dbConnect(),"SELECT * FROM ARRA_documents WHERE ARRA_tracking_number = '".$_SESSION['tracking_number']."' AND ARRA_file_type = 'PD'") ; 
											if(mysqli_num_rows($q)>0){ ?>
											<div class="col-lg-12 bordernew">
											<label>Please upload copies of reports/evaluations by any specialists, if applicable.</label><br /><?php 
											while($row = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
													$fileExt = explode(".", $row["ARRA_file_name"]); ?>
													<div class="col-lg-6" style="height:100px;">
													<?php if(getImgFileValid($fileExt[1])) { ?><img src="../documents/<?php echo $_SESSION['tracking_number']."/".$row["ARRA_file_name"]; ?>" width=100px height=80px /><?php } 
													elseif($fileExt[1] == "doc" OR $fileExt[1] == "docx") { ?><img src="../images/doc.png" width=75px height=85px /><?php } 
													elseif($fileExt[1] == "pdf") { ?><img src="../images/pdf.png" width=75px height=85px /><?php } ?>
													<a href="?removeDoc=<?php echo $row["ARRA_file_name"]; ?>"<button type="button" class="btn btn-warning marginvertical" >Remove Photo</button></a>
													</div><?php 
												}?>
											</div><?php
											} else { ?>
											<div class="col-lg-12 bordernew" id="testbe">
												<div class="row">
													<div class="col-lg-12">
														<label style="float:left;width:50%; margin-right: 2%;">Please upload copies of reports/evaluations by any specialists, if applicable.</label>
														<i class="fa fa-question-circle icon-color-arra fa-2x pull-left" data-toggle="modal" data-target="#specialist" style="margin-top:-1.5%"></i>
														<div class="form-group">
															<form role="form2" method="POST" action="#" enctype='multipart/form-data'>
																<div class="col-lg-12 marginBottom" id="disabilityDocPan">
																	<div class="row marginBottom">
																		<div class="col-lg-6"><input type="file" name="disability[]"></div>
																	</div>
																</div>
															<div class="col-lg-6 marginBottom">
																<div class="col-lg-6"><button type="button" class="btn btn-new marginvertical" id="addbtnclick_disabilityDoc">Add More &nbsp;<i class="glyphicon glyphicon-plus"></i></button></div>
																<div class="col-lg-6"><button type="submit" name="but_specialist" class="btn btn-new marginvertical">Upload</button></div>
																	
															</div>
															</form>
														</div>
                        			<!-- Modal Dialog -->	
																		<div class="modal fade" id="specialist" tabindex="-1" role="dialog" aria-labelledby="specialistl" aria-hidden="true">
																			<div class="modal-dialog">
																				<div class="modal-content">
																					<div class="modal-header"  style="background-color:#156059; color:#FFFFFF;">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																						<h4 class="modal-title" id="specialistl">Specialist information</h4>
																					</div>
																					 <div class="modal-body">
																					   Applicable if the student has been assessed or treated by any specialists, or has any medical conditions.</div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-new" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				<!-- /.modal-content -->
																			</div>
																			<!-- /.modal-dialog -->
																		</div>
																<!-- Modal -->    
                            
													</div>
												</div>
											</div><?php 
										} } ?>
                                        <!--
                                        <div class="col-lg-12">
                                            <div class="row">
                                               <div class="col-lg-6">
                                                   <div class="form-group">
                                                        <label>Has work permit of the Father/Mother if they are non-resident bangladeshis?</label>
                                                        <div class="form-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="takeSpecialistName" id="TSYes" value="TSYes" >Yes
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="takeSpecialistName" id="TSNo" value="TSNo">No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Please attach your work permit</label>
                                                        <input type="file">
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        -->
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                     <div class="form-group">
                                                        <label class="checkbox-inline">
                                                           <!-- <input type="checkbox" id="termchk" name="cb1" value="bcer">I agree with the <a><strong>Terms and Conditions</strong></a> and the <a><strong>Privacy Policy</strong></a> -->
                                                        </label>
                                                     </div>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         
                                        <div class="col-lg-12 text-center">
                                            <div class="row" style="margin-top:100px;">
												<div class="col-lg-1">
													<i onClick="window.location.href='sec7.php'" title="Previous" class="fa fa-arrow-circle-left icon-color-arra fa-2x"></i>
												</div>
												<div class="col-lg-10">
													<?php if(isset($_REQUEST["trackingid"])) {?>
													<a href="viewApplyForm.php?trackingid=<?php echo $_SESSION['tracking_number']; ?>">
														<button type="submit" name="save" id="save" class="btn btn-outline btn-new">UPDATE NOW <i class="fa fa-hdd-o fa-1x"></i></button>
													</a>
													<?php } else { ?>
													<a href="viewApplyForm.php?trackingid=<?php echo $_SESSION['tracking_number']; ?>">
														<button type="submit" name="save" id="save" class="btn btn-outline btn-footer">SAVE & CONTINUE <i class="fa fa-save fa-1x"></i></button>
													</a>
													<a href="dashboard.php">
														<button type="submit" name="save" id="save" class="btn btn-outline btn-footer">SAVE & EXIT <i class="fa fa-home fa-1x"></i></button>
													</a>
													<a href="?removeAll">
													<button type="submit" name="save" id="save" class="btn btn-outline btn-danger">CANCEL <i class="fa fa-home fa-1x"></i></button>
													</a>
													<?php } ?>
												</div>
												<div class="col-lg-1">
													&nbsp;
												</div>
                                            </div>
                                        </div>
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
			$("#studentPassport").css("display", "none");
			$("input").click(function(event) {
				var id = event.target.id;
				if ($("#"+id).val() == "SPYes"){
					$("#studentPassport").show("slow");
				}else if($("#"+id).val() == "SPNo"){
					$("#studentPassport").hide("slow");
                    //$("#testyesfield").replaceWith($("#testyesfield").clone());
				}
			});
									  
		});
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
			$("#workPermit").css("display", "none");
			$("input").click(function(event) {
				var id = event.target.id;
				if ($("#"+id).val() == "WPYes"){
					$("#workPermit").show("slow");
				}else if($("#"+id).val() == "WPNo"){
					$("#workPermit").hide("slow");
                    //$("#testyesfield").replaceWith($("#testyesfield").clone());
				}
			});
									  
		});
    </script>
    <script type="text/javascript">
        // $(document).ready(function() {
			// $("#reportCards").css("display", "none");
			// $("input").click(function(event) {
				// var id = event.target.id;
				// if ($("#"+id).val() == "RCYes"){
					// $("#reportCards").show("slow");
				// }else if($("#"+id).val() == "RCNo"){
					// $("#reportCards").hide("slow");
                    // //$("#testyesfield").replaceWith($("#testyesfield").clone());
				// }
			// });
									  
		// });
    </script>
    <script type="text/javascript">/*
        $(document).ready(function() {
			$("#vaccinationRecords").css("display", "none");
			$("input").click(function(event) {
				var id = event.target.id;
				if ($("#"+id).val() == "VRYes"){
					$("#vaccinationRecords").show("slow");
				}else if($("#"+id).val() == "VRNo"){
					$("#vaccinationRecords").hide("slow");
                    //$("#testyesfield").replaceWith($("#testyesfield").clone());
				}
			});
		});*/
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
			$('#messageDiv').delay(5000).fadeOut('slow');
		});
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("a").on("click",".secLink",function(event) {
                event.preventDefault();
                var id = event.target.id;
                if(id == "sec1"){
                    $("#"+id).css("display","block");
                }
                
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //alert($(location).attr('pathname'));
            var loc = window.location;
            var currentURL =  loc.pathname;
            var arr = currentURL.split('/');
            console.log(arr[4]);
            if(arr[4] == "sec10.php" ){
                $("#sec6div").show("slow");
                $("#sec6div").css("font-weight","bloder");
                $("#sec6div").css("color","rgb(255,197,120)");
                
                
            }
            
        });
    </script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        
        $('#passaddbtnclick').click(function(){
            var did = '<div class="form-group" id="pasrmvbtn'+count+'">'
            +'<label>Please attach another Passport</label>'
            +'<div class="col-lg-12 marginBottom">'
            +'<div class="col-lg-6"><input type="file" name="stuPassport[]"></div>'
            +'<button type="button" class="btn btn-danger btn-circle pull-right pprmvbtn" id="rmvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
            +'</div>'
            +'</div>';
            $('#passportDivAppend').append(did);
            count++;
        }); 
    });
    </script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        
        $('#faaddbtnclick').click(function(){
            var did = '<div class="form-group" id="fappfarmvbtn'+count+'">'
            +'<label>Please attach another document</label>'
            +'<div class="col-lg-12 marginBottom">'
            +'<div class="col-lg-6"><input type="file" name="fatherPassport[]"></div>'
            +'<button type="button" class="btn btn-danger btn-circle pull-right farmvbtn" id="farmvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
            +'</div>'
            +'</div>';
            $('#fatherPPappDiv').append(did);
            count++;
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        
        $('#moaddbtnclick').click(function(){
            var did = '<div class="form-group" id="moppmormvbtn'+count+'">'
            +'<label>Please attach another Passport</label>'
            +'<div class="col-lg-12 marginBottom">'
            +'<div class="col-lg-6"><input type="file" name="fatherPassport[]"></div>'
            +'<button type="button" class="btn btn-danger btn-circle pull-right mormvbtn" id="mormvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
            +'</div>'
            +'</div>';
            $('#motherPPappDiv').append(did);
            count++;
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".farmvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#fapp"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".mormvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#mopp"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".pprmvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#pas"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        
        $('#workpermitbtnclick').click(function(){
            var did = '<div class="form-group" id="nrdrmvbtn'+count+'">'
            +'<div class="col-lg-12 marginBottom">'
            +'<div class="col-lg-6"><input type="file" name="workPermit[]"></div>'
            +'<button type="button" class="btn btn-danger btn-circle pull-right nrrmvbtn" id="drmvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
            +'</div>'
            +'</div>';
            $('#workpermitdiv').append(did);
            count++;
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".nrrmvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#nr"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        
        $('#reportbtnclick').click(function(){
            var did = '<div class="form-group" id="divrptrmvbtn'+count+'">'
            +'<label>Please attach another report card</label>'
            +'<div class="col-lg-12 marginBottom">'
            +'<div class="col-lg-6"><input type="file" name="reportCard[]"></div>'
            +'<button type="button" style="padding:6px 6px" class="btn btn-danger btn-circle pull-right rptrmvbtn" id="rptrmvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
            +'</div>'
            +'</div>';
            $('#reportAppDiv').append(did);
            count++;
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".rptrmvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#div"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        
        $('#vaccibtnclick').click(function(){
            var did = '<div class="form-group" id="divvacirmvbtn'+count+'">'
            +'<label>Please attach another Vaccination records</label>'
            +'<div class="col-lg-12 marginBottom">'
            +'<div class="col-lg-6"><input type="file" name="vaccinationReport[]"></div>'
            +'<button type="button" style="6px 6px" class="btn btn-danger btn-circle pull-right rptrmvbtn" id="vacirmvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
            +'</div>'
            +'</div>';
            $('#vacciappDiv').append(did);
            count++;
        }); 
    });
    </script>
	<script type="text/javascript">
	$(document).ready(function() {
        var countrow=1;
        
        $('#addbtnclick_disabilityDoc').click(function(){
            var did = '<div class="row marginBottom" id="disabilityrmv'+countrow+'">'
            +'<div class="col-lg-6"><input type="file" name="disability[]"></div>'
            +'<div class="col-lg-6">'
            +'<button type="button" class="btn btn-danger btn-circle pull-right disabilityrmvbtn" id="rmv'+countrow+'"><i class="glyphicon glyphicon-trash"></i></button>'
			+'</div>'
			+'</div>';
            $('#disabilityDocPan').append(did);
            countrow++;
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".disabilityrmvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#disability"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
	
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".rptrmvbtn",function(event) {
        //  event.preventDefault();
            var id = $(this).attr('id');
          console.log(id);
          $("#div"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
	<script>
                // tooltip demo
                $('.tooltip-demo').tooltip({
                    selector: "[data-toggle=tooltip]",
                    container: "body"
                })

                // popover demo
                $("[data-toggle=popover]")
                .popover()
            </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>