<?php 
session_start();

if(!empty($_SESSION['user_id']) AND !empty($_REQUEST["trackingid"])){
include ('function.php');
dbConnect();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        <div id="page-wrapper">
            
			<div class="row">
                <div class="col-lg-12">
                    <h4><br></h4>
                </div>
                
            </div>
            <div class="row">
				<form action= "" method="POST" >
				<?php $query = mysql_query("SELECT ARRA_childs_detail_name, ARRA_childs_detail_dob FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_REQUEST["trackingid"]."'");
				$childsData = mysql_fetch_array($query, MYSQL_ASSOC); ?>
					<div class="col-lg-12" id="viewterms">
						<div class="panel panel-green">
							<div class="panel-body">							
								<p class="text-center"><img src="../../images/auroralogo.png"><br><br></p>
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<tr>
											<th width=250px>Name of Student</th>
											<td><?php echo str_replace("@", " ", $childsData["ARRA_childs_detail_name"]); ?></td>
										</tr>
										<tr>
											<th>Date of Birth:</th>
											<td><?php echo $childsData["ARRA_childs_detail_dob"]; ?></td>
										</tr>
										<tr>
											<th>Application Tracking Number:</th>
											<td><?php echo $_REQUEST["trackingid"]; ?></td>
										</tr>
										<tr>
											<th>Application Process Fee:</th>
											<td>BDT <?php echo "5,000.00"; ?></td>
										</tr>
									</table>
								</div>
								<p>&nbsp;</p><p>&nbsp;</p>
								<p style="margin-left:25px;font-size:20px"><strong>Instructions:</strong></p>
								<ul>
								  <li>Please print this document and present it to the school office to pay  the admission processing fee. It is important to bring this document when  submitting the payment to ensure we credit the payment correctly and to avoid  delays.</li>
								  <li>Please note that this admission processing fee is non-refundable.</li>
								  <li>The fee should be paid in cash (Bangladeshi Takas only).</li>
								  <li>Please make the payment at the Aurora International School premises: House  # NE(A)3A, Road # 74, Gulshan 2, Dhaka.</li>
								  <li>Payments will be accepted on working days during office hours (please  check our website at <a href="http://www.aurora-intl.org">www.aurora-intl.org</a> for our current  office hours).</li>
								  <li>If you have any questions, please contact us at +88-0184-123-4230 or <a href="mailto:info@aurora-intl.org">info@aurora-intl.org</a> during office hours  on any working day. </li>
								</ul>
							</div>
						</div>
						
					</div>
					<div class="col-lg-12 text-center" id="viewbuttons">
						<!--<button  type="submit" id="clickagreed"  name="continue" type="button" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-ok"></i>AGREE & SUBMIT</button>
						<button type="button" onClick="window.location.href='terms.php?saveandexit=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-footer">DISAGREE & EXIT<i class="fa fa-home fa-1x"></i></button>
						<button type="button" onClick="window.location.href='terms.php?cancel=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-danger"><i class="fa fa-arrow-circle-o-left fa-1x"></i>CANCEL</button>-->
						<button type="button" onClick="window.print()" class="btn btn-outline btn-footer">PRINT<i class="fa fa-home fa-1x"></i></button>
						<br><br><br><br>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
	
	<!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
		<script type="text/javascript">
		/*
		$(document).ready(function() {
			$("#viewsucess").hide();
				$("#clickagreed" ).click(function() {
				$("#viewsucess").show();
				$("#viewterms").hide();
				$("#viewbuttons").hide();
				$.get("terms.php",{'func':'1'},function(data){ //alert(data);
				});
				
				
				
				
				
				
				var name = $("#applyName").val();
				//console.log(name);
				var date = $("#datetimepicker2").val();
				//console.log(date);
				var tId = $("#tId").val();
				//console.log(tId);
				//alert(name +","+ date +","+ tId);
				
				//Ajax Use for bad coding style.
				$.ajax({
					url: 'ajaxupdate.php',
					type: 'POST',
					data: {name: name, date:date, tId:tId }
				});
				
			});
		});*/
    </script>
	<script type="text/javascript">
		// $(document).ready(function(){
			// $('#clickagreed').attr('disabled',true);
			// $('#applyName').keyup(function(){
				// if($(this).val().length !=0)
					// $('#clickagreed').attr('disabled', false);            
				// else
					// $('#clickagreed').attr('disabled',true);
			// })
		// });
	</script>
	
	<script type="text/javascript">
		$('#datetimepicker2').datetimepicker({
			yearOffset:0,
			lang:'en',
			timepicker:false,
			format:'Y-m-d',
            formatDate:'Y-m-d',// and tommorow is maximum date calendar
			minDate:'-1970/01/01', // yesterday is minimum date
			maxDate:'+1970/01/01' // and tommorow is maximum date calendar
		});
    </script>
	
	<script>
                // tooltip demo
                $('.tooltip-demo').tooltip({
                    selector: "[data-toggle=tooltip]",
                    container: "body"
				})
				
                // popover demo
                $("[data-toggle=popover]")
                .popover()
			</script>
</body>
</html>
<?php
}else{
  require_once 'login.php';
}
?>
