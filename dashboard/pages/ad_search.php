<?php
session_start();
include ('function.php');
dbConnect();

if($_SESSION['access'] != "User"){
	$status = "status = '1' OR status = '0'";
}else{
	$status = "status = '1'";
}


if($_GET["type"] == "FILE"){
	$conspan = 9;
	$text = "No documents were found.";
} else {
	$conspan = 8;
	$text = "No notices were found.";
}

	
if($_GET['title'] OR $_GET['category'] OR $_GET['from_date'] OR $_GET['to_date'] OR $_GET['appliedfor'] OR $_GET['subcategory']){
	
	$type = $_GET['type'];
	
	$subQuery = "WHERE type = '".$type."' ";
	
	if($_GET['title'] != ""){
		$subQuery .= " AND title LIKE '%".$_GET['title']."%'";
	}

	if($_GET['category'] != ""){
		$subQuery .= " AND category = '".$_GET['category']."'";
	}
	
	if($_GET['subcategory'] != ""){
		$subQuery .= " AND subcategory = '".$_GET['subcategory']."'";
	}

	if($_GET['from_date'] != ""){
		$subQuery .= " AND publish >= '".$_GET['from_date']."'";
	}

	if($_GET['to_date'] != ""){
		$subQuery .= " AND publish <= '".$_GET['to_date']."'";
	}

	if($_GET['appliedfor'] != ""){
		//$subQuery .= " AND appliedfor = '".$_GET['appliedfor']."'";
		$subQuery .= " AND classname LIKE '%".$_GET['appliedfor']."%'";
	}

	$i=1;
	//echo $subQuery;
	$oriQuery = "SELECT * FROM noticewithclassview ".$subQuery." ORDER BY id DESC";
	
	$query = mysqli_query(dbConnect(),$oriQuery) ;
	if(mysqli_num_rows($query)>0) {
		while($rows = mysqli_fetch_array($query)){ ?>
		
		<tr class="odd gradeX">
			<td><?php echo $i++; ?></td>
			<td><?php echo $rows["title"]; ?></td>
			<?php if($_SESSION['access'] == "Admin" OR $_SESSION['access'] == "SuperAdmin") echo '<td>'.$rows["created_by"].'</td>' ?>
			<td><?php echo $rows["category"]; ?></td>
			<?php if($rows['type'] == "FILE") { ?><td><?php echo $rows["subcategory"]; ?></td><?php } ?>
			<td><?php echo $rows["publish"]; ?></td>
			<?php if($_SESSION['access'] != "User") {
				if($rows["appliedfor"] == "Group") {
					$rows["appliedfor"] = "";
					$q = mysqli_query(dbConnect(),"SELECT * FROM multiple_classes WHERE notice_id = '".$rows["id"]."' ORDER BY id") ;
					while($r = mysqli_fetch_array($q, MYSQLI_ASSOC)){
						$rows["appliedfor"] .= $r["class"] . ", " ;
					}
				}
				$rows["appliedfor"] = rtrim($rows["appliedfor"], ", ");
				echo '<td>'. getClassType($rows["appliedfor"]) .'</td>';
			} ?>
			<?php echo '<td>'. countAttachments($rows["id"]).'&nbsp;<i class="fa fa-paperclip"></i></td>'; ?>
			<?php if($rows['type'] == "FILE" AND $_SESSION['access'] == "User"){ ?><td><a href="<?php echo $rows["createlink"]; ?>" target="_blank">Goto Link</a></td><?php } ?>
			<td class="text-center">
				<a href="viewDocuments.php?nid=<?php echo $rows["id"]; ?>" data-title="View Document" data-toggle="lightbox" data-parent="" data-gallery="remoteload" title='View Document'><button type='button' class='btn btn-success'><i class="fa fa-search"></i> </button></a>
				<?php if($_SESSION['access'] != "User") { ?>
					<a title='Edit'><button data-button='{"id": "<?php echo $rows["id"]; ?>", "appliedfor":"<?php echo $rows["appliedfor"]; ?>","category": "<?php echo $rows["category"]; ?>","titlee": "<?php echo $rows["title"]; ?>","bodyy": "<?php echo $rows["body"]; ?>","cdate": "<?php echo $rows["expireon"]; ?>","clink": "<?php echo $rows["createlink"]; ?>"}' type='button' class='btn btn-warning btn-edit'><i class="glyphicon glyphicon-pencil Edit"></i></button></a>
					<a title='Delete'><button type='button' class='btn btn-danger btn_del_album' data-loc="FU.php?removeDocId=<?php echo $rows["id"]; ?>&dir=<?php echo $rows["appliedfor"]; ?>"><i class="fa fa-trash-o fa-1x"></i> </button></a>
					<?php if($rows["status"] == 0){ ?>
						<a data-toggle="modal" href="#confirmModal" onClick="callConfirm('<?php echo $rows["appliedfor"]; ?>', '<?php echo $rows["id"]; ?>')"><button type='button' class='btn btn-primary publish'><i class="fa fa-eye fa-1x" ></i> </button></a>
						<!--<a href="#" data-toggle="modal" <?php if($rows["email_not"] == 0 && $rows["sms_not"] == 0 ){ echo "";}else{ echo 'data-target="#publish"'; }   ?> data-value= "<?php echo $rows["appliedfor"]; ?>" data-id= "<?php echo $rows["id"]; ?>" data-enot= "<?php echo $rows["email_not"]; ?>" data-snot= "<?php echo $rows["sms_not"]; ?>" onclick = showHint($(this).data('value'),$(this).data('enot'),$(this).data('snot'),$(this).data('id')) title='Publish'><button type='button' class='btn btn-primary publish'><i class="fa fa-eye fa-1x" ></i> </button></a>-->
						<?php } elseif($rows["status"] == 1){ ?>
						<a title='Unpublish'><button type='button' class='btn btn-warning btn_unpublish' data-loc="FU.php?unpublish=<?php echo $rows["id"]; ?>"><i class="fa fa-eye-slash fa-1x"></i> </button></a>
						<?php	}
					} ?>															
					<?php if($rows["type"] == "NOTICE"){ if($_SESSION['access'] != "User"){?>
						<?php if($rows["reminder"] == 1 && $rows["status"] == 0){
							echo '<a title="No Notify"><button id="btn_notify" type="button" class="btn btn-default disabled"><i class="glyphicon glyphicon-ban-circle"></i> </button></a>';
					} 
							elseif($rows["reminder"] == 1 && $rows["status"] == 1){ ?>		
							<a href="#" data-toggle="modal" data-value= "<?php echo $rows["appliedfor"]; ?>" data-target="#publishModal" data-id= "<?php echo $rows["id"]; ?>" onclick = notifyReminder($(this).data('id'),$(this).data('value')) title='Reminder'><button type='button' class='btn btn-primary notify'><i class="glyphicon glyphicon-bell" ></i> </button></a>
			
					<?php } ?>
					<?php } } ?>
			</td>
		</tr>	
			<?php
		}
	} else {
		echo "<tr><td colspan=$conspan align=center style='font-size:20px; font-weight:bold;'>$text</td></tr>";
	}
	
	
}

?>