<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #156059;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>


    </div>
    <div style="float: left;height: 78px;width: 52%;margin-left:2%">
        <div style="float: left;height: 78px;width: 13%;"><img src="../images/auroralogo.png" style="padding: 0px;width: 98%;<!--padding: 7px 1px 6px 8px;-->float: left;" /></div>
        <a class="navbar-brand" style="color: #FFFFFF;margin-top: 1%;font-size:30px;">AURORA INTERNATIONAL SCHOOL </a>
    </div>
    <?php if ((basename($_SERVER['PHP_SELF']) == "userprofile.php" || basename($_SERVER['PHP_SELF']) == "viewApplyForm.php") && $_SESSION['access'] != "User") {

        unset($_SESSION['tracking_number']);
    } ?>

    <div style="float: left;height: 50px;width: 46%;  margin-top: 1%;">
        <?php include('appMenu.php'); ?>
    </div>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <?php if (isset($_SESSION['tracking_number'])) {
                    echo "";
                } else {
                ?>
                    <li>
                        <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Homepage</a>
                    </li>

                    <li>
                        <!--<ul class="nav2">-->
                        <ul class="nav nav-second-level">
                            <?php $i = 1;
                            $query = mysqli_query(dbConnect(),"SELECT * FROM ARRA_menu ORDER BY ARRA_menu_id") or die(mysql_error());

                            while ($rows = mysqli_fetch_array($query)) {
                                if (($_SESSION['access'] == "SuperAdmin") or
                                    (getAccessArea(getAccessArr(safe($_SESSION['user_id'])), $rows["ARRA_menu_short"] . "-R") == $rows["ARRA_menu_short"] . "-R") or
                                    (getAccessArea(getAccessArr(safe($_SESSION['user_id'])), $rows["ARRA_menu_short"] . "-E") == $rows["ARRA_menu_short"] . "-E") or
                                    (getAccessArea(getAccessArr(safe($_SESSION['user_id'])), $rows["ARRA_menu_short"] . "-D") == $rows["ARRA_menu_short"] . "-D")
                                ) {
                                    if (($_SESSION['access'] == "User") and (checkStuStatus($_SESSION['user_id']))) {

                                        //------------------------- CHECK MENU WHILE PLAYING ON MENU ACCESS FROM DB --------------------------------//
                                        //------------------------- CHECK MENU WHILE PLAYING ON MENU ACCESS FROM DB --------------------------------//
                                        //------------------------- CHECK MENU WHILE PLAYING ON MENU ACCESS FROM DB --------------------------------//
                            ?>
                                        <li>
                                            <a href="<?php echo  $rows["ARRA_menu_short"] . ".php"; ?>" class="secLink <?php if (basename($_SERVER['PHP_SELF']) == $rows["ARRA_menu_short"] . ".php") echo "active"; ?>">
                                                <div class="menutext-left"></div>
                                                <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i><?php echo  $rows["ARRA_menu_detail"]; ?></div>
                                            </a>
                                        </li>

                                    <?php
                                    } elseif ($_SESSION['access'] == "SuperAdmin" or $_SESSION['access'] == "SubAdmin" or $_SESSION['access'] == "Admin" or $_SESSION['access'] == "User") {
                                    ?>
                                        <li>
                                            <a href="<?php echo  $rows["ARRA_menu_short"] . ".php"; ?>" class="secLink <?php if (basename($_SERVER['PHP_SELF']) == $rows["ARRA_menu_short"] . ".php") echo "active"; ?>">
                                                <div class="menutext-left"></div>
                                                <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i> <?php echo  $rows["ARRA_menu_detail"]; ?></div>
                                            </a>
                                        </li>
                                    <?php
                                    }
                                } else {
                                    echo "";
                                }
                            }
                            //------------------------- CHECK MENU WHILE PLAYING OUT OF APPLICATION FORM --------------------------------//
                            //------------------------- CHECK MENU WHILE PLAYING OUT OF APPLICATION FORM --------------------------------//
                            //------------------------- CHECK MENU WHILE PLAYING OUT OF APPLICATION FORM --------------------------------//

                            $query = mysqli_query(dbConnect(),"SELECT ARRA_applying_user FROM ARRA_applying WHERE ARRA_applying_user = '" . $_SESSION['user_id'] . "'");

                            if (mysqli_num_rows($query)) {

                                echo "";
                            } else {
                                if ($_SESSION['access'] == "User") {

                                    ?>

                                    <li>
                                        <a href="http://www.aurora-intl.org" target="_blank" id="core" class="secLink">
                                            <div class="menutext-left"></div>
                                            <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i> Website</div>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="../sample/Procedure_And_Guideline1.pdf" target="_blank" id="yellow" class="secLink">
                                            <div class="menutext-left"></div>
                                            <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i> Admission Policy</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../sample/Fee_Payment_Policies_2022-23.pdf" target="_blank" id="red" class="secLink">
                                            <div class="menutext-left"></div>
                                            <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i> Fee Policy</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../sample/InstructionsforAdmissionForm.pdf" target="_blank" id="green" class="secLink">
                                            <div class="menutext-left"></div>
                                            <div class="menutext-right"><i class="fa fa-dashboard fa-fw"></i> Instructions</div>
                                        </a>
                                    </li>
                            <?php

                                }
                            }


                            ?>



                        </ul>
                    </li>
                <?php

                }

                //------------------------- CHECK MENU WHILE PLAYING ON APPLICATION FORM --------------------------------//
                //--------------------------CHECK MENU WHILE PLAYING ON APPLICATION FORM---------------------------------//
                //--------------------------CHECK MENU WHILE PLAYING ON APPLICATION FORM---------------------------------//
                //--------------------------CHECK MENU WHILE PLAYING ON APPLICATION FORM---------------------------------//
                if (
                    isset($_SESSION['tracking_number']) &&
                    (basename($_SERVER['PHP_SELF']) == "sec0.php" or
                        basename($_SERVER['PHP_SELF']) == "sec1.php" or
                        basename($_SERVER['PHP_SELF']) == "sec4.php" or
                        basename($_SERVER['PHP_SELF']) == "sec7.php" or
                        basename($_SERVER['PHP_SELF']) == "sec9.php" or
                        basename($_SERVER['PHP_SELF']) == "userprofile.php" or
                        basename($_SERVER['PHP_SELF']) == "viewApplyForm.php" or
                        basename($_SERVER['PHP_SELF']) == "sec10.php") &&
                    $_SESSION['access'] == "User"
                ) {
                ?>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Application Form<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="sec0.php" id="sec1" class="secLink">
                                    <div class="menutext-left">1</div>
                                    <div class="menutext-right">Basic Information</div>
                                </a>
                            </li>
                            <li>
                                <a href="sec1.php" id="sec2" class="secLink">
                                    <div class="menutext-left">2</div>
                                    <div class="menutext-right">Student's Details</div>
                                </a>
                            </li>
                            <li>
                                <a href="sec4.php" id="sec3" class="secLink">
                                    <div class="menutext-left">3</div>
                                    <div class="menutext-right">Family Information</div>
                                </a>
                            </li>
                            <li>
                                <a href="sec7.php" id="sec4" class="secLink">
                                    <div class="menutext-left">4</div>
                                    <div class="menutext-right">Sibling Information</div>
                                </a>
                            </li>
                            <li>
                                <a href="sec9.php" id="sec5" class="secLink">
                                    <div class="menutext-left">5</div>
                                    <div class="menutext-right">Learning Profile</div>
                                </a>
                            </li>
                            <li>
                                <a href="sec10.php" id="sec6" class="secLink">
                                    <div class="menutext-left">6</div>
                                    <div class="menutext-right">Supporting Documents</div>
                                </a>
                            </li>
                            <li>
                                <a href="viewApplyForm.php?trackingid=<?php echo $_SESSION['tracking_number']; ?>" id="sec12" class="secLink">
                                    <div class="menutext-left">7</div>
                                    <div class="menutext-right">Application Review</div>
                                </a>
                            </li>


                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                <?php
                } else {
                    echo "";
                }
                ?>


            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>

    <!-- /.navbar-static-side -->
</nav>