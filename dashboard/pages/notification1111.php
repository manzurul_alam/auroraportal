				<?php 
				// Create all notification box
				
				
                function drawAppListnBox(){
				?> 
				<div class="col-lg-33 hover" id="clickable" href="AIP.php">
					<div class="panel panel-arra">
						<div class="panel-heading text-center">
							 <h2 style="font-size:15px;font-weight: bold;">View Applications</h2>
						</div>
						<div class="panel-body text-center">
							
							<img class="imgCtrl" src="../images/applyIcon.png" />	
						</div>
						
					</div>
				</div>
				<?php
				}
				function drawInstructionBox(){
				?> 
				<div class="col-lg-33 hover"  onclick="window.open('../sample/InstructionsforAdmissionForm.pdf')">
					<div class="panel panel-ms">
						<div class="panel-heading text-center">
							 <h2 style="font-size:15px;font-weight: bold;">Instructions</h2>
						</div>
						<div class="panel-body text-center">
								<img class="imgCtrl" src="../images/instruction.png" />
						</div>
						
					</div>
				</div>
				<?php
				}
				
				function drawWebBox(){
				?> 
				<div class="col-lg-33 hover" onclick="window.open('http://www.aurora-intl.org')">
					<div class="panel panel-core">
						<div class="panel-heading text-center">
							 <h2 style="font-size:15px;font-weight: bold;">Website</h2>
						</div>
						<div class="panel-body text-center">
							<img class="imgCtrl" src="../images/website.png" />	
								
						</div>
						
					</div>
				</div>
				<?php
				}
				
				function drawAdPolicyBox(){
				?> 
				<div class="col-lg-33 hover"  onclick="window.open('../sample/Procedure_And_Guideline1.pdf')">
					<div class="panel panel-arra3">
						<div class="panel-heading text-center">
							 <h2 style="font-size:15px;font-weight: bold;">Admission Policy</h2>
						</div>
						<div class="panel-body text-center">
							<img class="imgCtrl" src="../images/ad_policy.png" />
						</div>
						
					</div>
				</div>
				<?php
				}
            
                function drawNoticeUploadBox(){
                ?> 
                <div class="col-lg-33 hover" id="clickable" href="NTC.php" >
                    <div class="panel panel-ms">
                        <div class="panel-heading text-center">
                             <h2 style="font-size:15px;font-weight: bold;">Notice (s)</h2>
                        </div>
                        <div class="panel-body text-center">
                            <img class="imgCtrl" src="../images/notice.png" />
                        </div>
                        
                    </div>
                </div>
                <?php
                }
				
                function drawAlbumGalleryBox(){
                ?> 
                <div class="col-lg-33 hover" id="clickable" href="gallery.php"">
                    <div class="panel panel-ps">
                        <div class="panel-heading text-center">
                             <h2 style="font-size:15px;font-weight: bold;">Gallery (s)</h2>
                        </div>
                        <div class="panel-body text-center">
                            <img class="imgCtrl" src="../images/album.png" />
                        </div>
                        
                    </div>
                </div>
                <?php
                }
				    function drawDocUploadBox(){
                ?> 
                <div class="col-lg-33 hover" id="clickable" href="FU.php">
                    <div class="panel panel-es">
                        <div class="panel-heading text-center">
                             <h2 style="font-size:15px;font-weight: bold;">Key Document (s)</h2>
                        </div>
                        <div class="panel-body text-center">
                            <img class="imgCtrl" src="../images/documents.png" />
                        </div>
                        
                    </div>
                </div>
                <?php
                }
				function drawAlbumUploadBox(){
                ?> 
                <div class="col-lg-33 hover" id="clickable" href="album.php"">
                    <div class="panel panel-ps">
                        <div class="panel-heading text-center">
                             <h2 style="font-size:15px;font-weight: bold;">Gallery (s)</h2>
                        </div>
                        <div class="panel-body text-center">
                            <img class="imgCtrl" src="../images/album.png" />
                        </div>
                        
                    </div>
                </div>
                <?php
                }
				function drawFeePolicyBox(){
				?> 
				<div class="col-lg-33 hover"  onclick="window.open('../sample/FeePaymentPolicies.pdf')">
					<div class="panel panel-arra2">
						<div class="panel-heading text-center">
							 <h2 style="font-size:15px;font-weight: bold;">Fee Policy</h2>
						</div>
						<div class="panel-body text-center">
								
								<img class="imgCtrl" src="../images/policy.png" />
						</div>
						
					</div>
				</div>
				<?php
				}
				function drawLoginBox(){ ?>
                <div class="col-lg-3 col-md-6" id="clickable" href="loginInfo.php">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-th-list fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Log</div>
                                    
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                    <span class="pull-left"><a href="loginInfo.php">Click Here</a></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> <?php }
				
				function drawMenuBox(){ ?>
                <div class="col-lg-3 col-md-6" id="clickable" href="menucontrol.php">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-th-list fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Menu</div>
                                    
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                    <span class="pull-left"><a href="menucontrol.php">Click Here</a></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> <?php }
                
                function drawUserBox(){ ?>
				<div class="col-lg-3 col-md-6" id="clickable" href="alluser.php">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-user fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">User</div>
                                    
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                    <span class="pull-left"><a href="alluser.php">Click Here</a></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> <?php }
				
				function drawUserBox2(){ ?>
				<div class="col-lg-33 hover" id="clickable" href="UM.php">
					<div class="panel panel-arra1">
						<div class="panel-heading text-center">
							 <h2 style="font-size:15px;font-weight: bold;">User Management</h2>
						</div>
						<div class="panel-body text-center">
							
							<img class="imgCtrl" src="../images/user.png" />	
						</div>
						
					</div>
				</div> <?php }
				function drawFeesBox(){ ?>
				<div class="col-lg-3 col-md-6" id="clickable" href="payment.php">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-dollar fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Fees</div>
                                   
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="payment.php" target="_blank">Add Payment</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div><?php }
				
				function drawApplyBox(){ ?>
				<div class="col-lg-3 col-md-6" id="clickable" href="AIP.php">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa  fa-pencil fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Apply</div>
                                    
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                 <span class="pull-left"><a href="AIP.php">Admission Inquiry Poll</a></span>
                                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div><?php }
                
				function drawAISBox() { ?>
				<div class="col-lg-3 col-md-6" id="clickable" href="../../index.html#contact">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-phone-square fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">AIS</div>
                                   
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="../../index.html#contact" target="_blank">Click Here</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> <?php }
                function drawStatusBox() { ?>
                <div class="col-lg-3 col-md-6" id="clickable" href="dands.php">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="glyphicon glyphicon-cog fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Status</div>
                                   
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="dands.php" target="_blank">Click Here</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> <?php } ?>
				<?php 
				if($_SESSION['access'] == "SuperAdmin") { 
					
					drawApplyBox();
					drawMenuBox();
                    drawStatusBox();
					drawLoginBox();	
                    drawDocUploadBox();		
                    drawAISBox();
                    drawUserBox();
                    drawFeesBox();		
				} 
				elseif($_SESSION['access'] == "Admin" OR $_SESSION['access'] == "SubAdmin"){
					if((getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"AIP-R") == "AIP-R") OR (getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"AIP-E") == "AIP-E") OR (getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"AIP-D") == "AIP-D") ){
                      drawAppListnBox();
                    }
                    drawNoticeUploadBox();
                    drawDocUploadBox(); 
                    drawAlbumUploadBox(); 
				

					if((getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"UM-R") == "UM-R") OR (getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"UME") == "UME") OR (getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"UMD") == "UMD") ){
                      drawUserBox2();
					  
                    }
					
                    	drawWebBox();
					drawAdPolicyBox();
					drawFeePolicyBox();
					drawInstructionBox();
                    
                    
				}
				elseif($_SESSION['access'] == "User"){
                    if((getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"AIP-R") == "AIP-R") OR (getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"AIP-E") == "AIP-E") OR (getAccessArea(getAccessArr(safe($_SESSION['user_id'])),"AIP-D") == "AIP-D") ){
                      drawAppListnBox();
                    }
                    drawNoticeUploadBox();
                    drawDocUploadBox(); 
                    drawAlbumGalleryBox();
                    drawWebBox();
                    drawAdPolicyBox();
                    drawFeePolicyBox();
                    drawInstructionBox();
                    
				}
                ?>