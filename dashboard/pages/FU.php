<?php
session_start();

if (!empty($_SESSION['user_id'])) {
	//$_SESSION['tracking_number'] = "";
	include('function.php');
	dbConnect();
	//$message = 0;
	$today = date("Y-m-d");
	if (!is_dir('../noticeboard')) {
		mkdir('../noticeboard', 0777, true);
		// mkdir('../noticeboard/All', 0777, true);
		// mkdir('../noticeboard/Toddler', 0777, true);
		// mkdir('../noticeboard/Preschool', 0777, true);
		// mkdir('../noticeboard/Kinder', 0777, true);
		// mkdir('../noticeboard/Prekindergarten', 0777, true);
		// mkdir('../noticeboard/Group', 0777, true);
		chmod('../noticeboard/', 0777);
	}
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		switch ($_GET['func']) {
			case 'delete_category':

				if (isset($_GET["dc_id"])) {
					$q = mysqli_query(dbConnect(),"SELECT cat_id FROM category WHERE cat_name = '" . $_GET['cat_id'] . "'") or die(mysqli_errno());
					$catid = mysqli_result($q, 0);
					mysqli_query(dbConnect(),"DELETE from subcategory where subcat_id=" . $_GET['dc_id'] . " AND catid=" . $catid . "");
				}
				break;

			default:
				// Do nothing?
		}
	}
	if (isset($_REQUEST["removeDocId"])) {
		$query = mysqli_query(dbConnect(),"DELETE FROM ARRA_notice WHERE id = '" . $_REQUEST["removeDocId"] . "'");
		if ($query) {
			$query = mysqli_query(dbConnect(),"SELECT file_name FROM attachments WHERE notice_id = '" . $_REQUEST["removeDocId"] . "'");
			while ($r = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
				unlink("../noticeboard/" . $r["file_name"]);
			}
			$query = mysqli_query(dbConnect(),"DELETE FROM attachments WHERE notice_id = '" . $_REQUEST["removeDocId"] . "'");
			$query = mysqli_query(dbConnect(),"DELETE FROM multiple_classes WHERE notice_id = '" . $_REQUEST["removeDocId"] . "'");
			echo "<script>window.location.assign('FU.php');</script>";
		}
	} elseif (isset($_POST['save'])) {
		if ($_POST["id"]) {
			$classname = $_POST["applicable_for"][0];
			mysqli_query(dbConnect(),"DELETE FROM multiple_classes WHERE notice_id = '" . $_POST["id"] . "'") or die(mysqli_error());
			if (count($_POST["applicable_for"]) > 1) {
				$classname = "Group";
				foreach ($_POST["applicable_for"] as $key => $value) {
					mysqli_query(dbConnect(),"INSERT INTO multiple_classes VALUES (NULL , '" . $_POST["id"] . "',  '$value')") or die(mysqli_error());
				}
			}
			$query = mysqli_query(dbConnect(),"UPDATE ARRA_notice SET appliedfor = '$classname', filelink = '/$classname/', category = '" . xss_cleaner($_POST["cateName"]) . "', subcategory = '" . xss_cleaner($_POST["subCateName"]) . "', title = '" . xss_cleaner($_POST["subject"]) . "', body = '" . xss_cleaner($_POST["lecture"]) . "', expireon = '" . $_POST["createdate"] . "', createlink = '" . $_POST["createlink"] . "' WHERE id = '" . $_POST["id"] . "'") or die(mysqli_error());

			//For notice log
			mysqli_query(dbConnect(),"INSERT INTO file_log VALUES (NULL ,  '" . $_SESSION['user_id'] . "',  '" . $_POST["id"] . "',  'Update', CURRENT_TIMESTAMP)") or die(mysqli_error());

			foreach ($_FILES["uploadFile"]["tmp_name"] as $key => $tmp_name) {
				// echo $_FILES["uploadFile"]["name"][$key]. "<br>";
				if ($_FILES["uploadFile"]["size"][$key] < 1048576) {
					if (isset($classname)) {

						$path = "../noticeboard/"; // Upload directory

						$uploaded_lecture = $_FILES["uploadFile"]["tmp_name"][$key];
						$uploaded_lecture_path = $_FILES["uploadFile"]["name"][$key];
						$uploaded_lecture_ext = pathinfo($uploaded_lecture_path, PATHINFO_EXTENSION);
						$uploaded_lecture_new = ("FILE_" . time() . $key . '.' . $uploaded_lecture_ext);
						if (is_uploaded_file($uploaded_lecture)) {
							if (move_uploaded_file($uploaded_lecture, $path . $uploaded_lecture_new)) {
								$query = mysqli_query(dbConnect(),"INSERT INTO attachments VALUES ( NULL , '" . $_POST["id"] . "', '$uploaded_lecture_new', '" . $_POST['input_descr'][$key] . "', 0)") or die(mysqli_error());
							} else {
								$message = "Move upload file error!!!";
							}
						} else {
							if ($uploaded_lecture == "") break;
							$message = "Upload error!!!";
						}
					} else {
						$message = "Mandatory field missing !!!";
					}
				} else {
					$message = "Keep the file size under 1MB!!!";
				}
			}
			//Reload the page
			echo "<script>window.location.assign('FU.php');</script>";
		} else { //New insert
			$classname = $_POST["applicable_for"][0];
			if (count($_POST["applicable_for"]) > 1) {
				$classname = "Group";
				foreach ($_POST["applicable_for"] as $key => $value) {
					mysqli_query(dbConnect(),"INSERT INTO multiple_classes VALUES (NULL , '" . $_POST["id"] . "',  '$value')") or die(mysqli_error());
				}
			}

			$query = mysqli_query(dbConnect(),"INSERT INTO ARRA_notice VALUES (NULL, 'FILE', '" . xss_cleaner($_POST["subject"]) . "', '" . xss_cleaner($_POST["lecture"]) . "', '', '0000-00-00', '" . $classname . "', '/" . $classname . "/', '" . $_POST["createlink"] . "','" . $_POST["createdate"] . "', '0','1','0','1', '" . xss_cleaner($_POST["cateName"]) . "', '" . xss_cleaner($_POST["subCateName"]) . "','" . $_SESSION['name'] . "')") or die(mysqli_error(dbConnect()));

			if ($query) {
				$message = "Successfully Uploaded...";
				$query = mysqli_query(dbConnect(),"SELECT id FROM ARRA_notice WHERE type='FILE' AND title='" . xss_cleaner($_POST["subject"]) . "' AND expireon='" . $_POST["createdate"] . "'") or die(mysqli_error());
				// $nid = mysql_result($query, 0);
				$row = mysqli_fetch_assoc($query);
                $nid = $row['id'];
				//For notice log
				mysqli_query(dbConnect(),"INSERT INTO file_log VALUES (NULL ,  '" . $_SESSION['user_id'] . "',  '$nid',  'Upload', CURRENT_TIMESTAMP)") or die(mysqli_error());

				foreach ($_POST["applicable_for"] as $key => $value) {
					mysqli_query(dbConnect(),"INSERT INTO multiple_classes VALUES (NULL , '$nid',  '$value')") or die(mysqli_error());
				}
				foreach ($_FILES["uploadFile"]["tmp_name"] as $key => $tmp_name) {
					// echo $_FILES["uploadFile"]["name"][$key]. "<br>";
					if ($_FILES["uploadFile"]["size"][$key] < 1048576) {
						if (isset($classname)) {

							$path = "../noticeboard/"; // Upload directory

							$uploaded_lecture = $_FILES["uploadFile"]["tmp_name"][$key];
							$uploaded_lecture_path = $_FILES["uploadFile"]["name"][$key];
							$uploaded_lecture_ext = pathinfo($uploaded_lecture_path, PATHINFO_EXTENSION);
							$uploaded_lecture_new = ("FILE_" . time() . $key . '.' . $uploaded_lecture_ext);
							//echo $_POST["input_descr".$input_count];
							if (is_uploaded_file($uploaded_lecture)) {
								if (move_uploaded_file($uploaded_lecture, $path . $uploaded_lecture_new)) {
									$query = mysqli_query(dbConnect(),"INSERT INTO attachments VALUES ( NULL , '$nid', '$uploaded_lecture_new', '" . $_POST['input_descr'][$key] . "', 0)") or die(mysqli_error());
								} else {
									$message = "Move upload file error!!!";
								}
							} else {
								if ($uploaded_lecture == "") break;
								$message = "Upload error!!!";
							}
						} else {
							$message = "Mandatory field missing !!!";
						}
					} else {
						$message = "Keep the file size under 1MB!!!";
					}
				}
			} else {
				$message = "File saving error!!!";
			}

			// if($message != "Successfully Uploaded..."){
			// 	mysqli_query(dbConnect(),"DELETE FROM ARRA_notice WHERE id = '$nid'");
			// 	mysqli_query(dbConnect(),"DELETE FROM attachments WHERE notice_id = '$nid'");
			// }else{
			// 	echo "<script>window.location.assign('FU.php');</script>";
			// 	// echo "<script>window.location.assign('FU.php?message=Successfully Uploaded...');</script>";
			// }

			echo "<script>window.location.assign('FU.php');</script>";
		}
	} elseif (isset($_REQUEST["unpublish"])) {
		$query = mysqli_query(dbConnect(),"UPDATE ARRA_notice SET status = '0', publish='0000-00-00' WHERE id = '" . $_REQUEST["unpublish"] . "'");
		if ($query) {
			//$message = "Successfully Unpublish !!!";
			echo "<script>window.location.assign('FU.php');</script>";
		}
	} elseif (isset($_POST["instructions"])) {
		$it = htmlspecialchars($_POST["textinstructions"], ENT_QUOTES);
		if ($_POST["id"])
			mysqli_query(dbConnect(),"UPDATE instruction SET instruction = '" . $it . "' WHERE id = '" . $_POST["id"] . "'") or die(mysqli_error());
		else
			mysqli_query(dbConnect(),"INSERT INTO instruction VALUES ('', '" . $_POST["type"] . "', '" . $it . "')") or die(mysqli_error());
		echo "<script>window.location.assign('FU.php');</script>";
	}

?>
	<!DOCTYPE html>

	<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>..::AIS::..</title>

		<!-- Bootstrap Core CSS -->
		<link href="../responsive/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="../responsive/bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

		<!-- MetisMenu CSS -->
		<link href="../responsive/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

		<!-- select2 -->
		<link href="bootstrap-fileinput/css/select/select2.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="../dist/css/sb-admin-2 before responsive.css" rel="stylesheet">
		<!-- DATA TABLES -->
		<link href="../responsive/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- Custom Fonts -->
		<link href="../responsive/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="icon" type="image/png" sizes="16x16" href="../responsive/favico/favicon-16x16.png">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- File input -->
		<link href="bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
		<!-- jQuery v2.1.3 -->
		<script src="../responsive/bower_components/jquery/dist/jquery.min.js"></script>
		<!--<script src="../bower_components/jquery/dist/jquery.min.js"></script>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
			<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>-->
		<script src="bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
		<!-- Lightbox -->
		<link href="lightbox/dist/ekko-lightbox.css" rel="stylesheet">



		<style>
			.btn.btn-primary.btn-file {
				background: #196960
			}

			.modal-header {
				background-color: #156059;
				color: #fff;
			}

			.nav1>li>a {
				background-color: #fff;
				color: black;

			}

			.nav1>li>a:hover {
				background-color: #fff;
				color: black;
				text-decoration: underline;

			}

			.panel-body-custom {
				padding-bottom: 0px;
				padding-top: 0px;
			}

			.btn-danger {
				background-color: #FB593E;
				color: #fff !important;

			}

			.custom-padding {
				padding-top: 0px;
				padding-bottom: 0px;
				padding-left: 31px;
				padding-right: 31px;

			}

			#lecture {
				width: 500px;
				height: 100px;
			}

			#salutation {
				width: 480px;
				height: 100px;
			}

			#email_salutation {
				width: 645px;
				height: 100px;
			}

			#email_body {
				width: 645px;
				height: 100px;
			}
		</style>
	</head>

	<body>

		<div id="wrapper">

			<!-- Navigation -->
			<?php include('nav before responsive.php') ?>
			<!-- End Navigation -->


			<div id="page-wrapper">

				<div class="row">
					<div class="col-lg-12">
						<input type=hidden id=test value="{ y: '1' , legendText: 'All' , indexLabel: 'All' }, { y: '2' , legendText: 'Toddler' , indexLabel: 'Toddler' }" />
						<h1 class="page-header">Key Documents</h1>
					</div>
					<div class="col-lg-12">
					<?php
if (isset($message) || isset($_REQUEST["message"])) {
    if (isset($_REQUEST["message"])) {
        $message = $_REQUEST["message"];
    }
    echo '<div class="row voffset2" id="messageDiv">
            <div class="col-md-6 col-md-offset-4">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>' . $message . '
                </div>
            </div>
        </div>';
    unset($message);
}
?>
						<div class="panel panel-arra">
							<div class="panel-heading">
								<?php include('loginname.php'); ?>
							</div>
							<div class="row custom-padding" style="margin-bottom:-14px;">
								<div class="col-lg-12 form-group"></div>

								<?php $q = mysqli_query(dbConnect(),"SELECT * FROM instruction WHERE type = 'FILE'") or die(mysqli_error());
								$instructionrow = mysqli_fetch_array($q, MYSQLI_ASSOC);
								if ($_SESSION['access'] == "User") { ?>
									<div class="col-lg-12 form-group">
										<?php echo "<b style='float: left;text-align:justify;'>Instruction:&nbsp;</b>" . htmlspecialchars_decode($instructionrow["instruction"]); ?>
									</div>
								<?php } ?>
							</div>
							<div class="panel-body">
								<!-- Nav tabs -->
								<ul class="nav nav-pills">
									<?php if ($_SESSION['access'] != "User") {  ?>
										<li class="tab-btn active"><a id="pill_view" href="#home-pills" data-toggle="tab">All Documents</a></li>
										<li class="tab-btn"><a id="pill_add" href="#add-pills" data-toggle="tab">Add New Document</a></li> <?php	} else { ?>
										<!--<li  style="width:160px; text-align:center"><a href="#view-pills" data-toggle="tab">View Documents</a></li>-->
								</ul>
							<?php }	?>
							<div class="tab-content">

								<?php if ($_SESSION['access'] != "User") {  ?>
									<div class="tab-pane fade in active" id="home-pills" style="margin-bottom:-4px">
										<div class="row">&nbsp;</div>
										<div class="row">&nbsp;</div>

									</div><?php
										} ?>

								<?php if ($_SESSION['access'] != "User") { ?>
									<div class="tab-pane fade" id="add-pills">
										<div class="row">&nbsp;</div><br />
										<form role="form1" id="form1" method="POST" action="#" enctype='multipart/form-data'>
											<input name="id" type="hidden" class="form-control" id="notice_id_edit" value="" />
											<fieldset>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Applicable for</label>
													</div>
													<div class="col-md-6">
														<select name="applicable_for[]" id="applicable_for" multiple="multiple" class="form-control select2_multiple" required>
															<option value="Toddler" class="Toddler">Toddler ( Age 2+ )</option>
															<option value="Preschool" class="Preschool">Pre School ( Age 3+ )</option>
															<option value="Prekindergarten" class="Prekindergarten">Pre Kindergarten ( Age 4+ )</option>
															<option value="Kinder" class="Kinder">Kindergarten ( Age 5+ )</option>
															<option value="Class-1" class="Class-1">Class One</option>
															<option value="Class-2" class="Class-2">Class Two</option>
															<option value="Class-3" class="Class-3">Class Three</option>
															<option value="Class-4" class="Class-4">Class Four</option>
															<option value="Class-5" class="Class-5">Class Five </option>
															<option value="Class-6" class="Class-6">Class Six </option>
														</select>
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Category</label>
													</div>
													<div class="col-md-6">
														<select name="cateName" id="cateName" class="form-control" required style="width:88.5%;float: left; margin-right: 2%;" onChange="loadsubcat()">
															<option value="">Select...</option>
															<?php $catquery = "SELECT cat_name FROM category WHERE cat_flag = '3'";
															$catQueryResult = mysqli_query(dbConnect(),$catquery);
															while ($rowCat = mysqli_fetch_array($catQueryResult)) {
															?>
																<option value="<?php echo $rowCat['cat_name']; ?>"><?php echo $rowCat['cat_name']; ?></option>
															<?php
															} ?>
														</select>
														<a onCLick='callmodal("#addNewCategory")' title="Add New Category"><i style="margin-top: 2%;font-size: 21px;" class="fa fa-plus fa-x"></i></a>
														<a id="open_del_cat" onCLick='callmodal("#dltNewCategory")' title="Delete Category"><i style="margin-top: 2%;font-size: 21px;color: RED;" class="fa fa-minus fa-x"></i></a>
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Sub-Category</label>
													</div>
													<div class="col-md-6">
														<select name="subCateName" id="subCateName" class="scn form-control" style="width:88.5%;float: left; margin-right: 2%;">
															<option value="">Select your sub-category</option>
															<?php $subcatResult = mysqli_query(dbConnect(),"SELECT subcat_name FROM subcategory");
																while ($catRow = mysqli_fetch_array($subcatResult)) {
																	echo '<option value="' . $catRow['subcat_name'] . '">' . $catRow['subcat_name'] . '</option>';
																}
																?>
														</select>
														<a onCLick='callmodal("#addNewSubCategory")' title="Add New Sub Category"><i style="margin-top: 2%;font-size: 21px;" class="fa fa-plus"></i></a>
														<a id="open_del_subcat" onCLick='callmodal("#dltSubCategory");' title="Add New Sub Category"><i style="margin-top: 2%;font-size: 21px;color: RED;" class="fa fa-minus"></i></a>
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Document Title</label>
													</div>
													<div class="col-md-6">
														<input type="text" name="subject" id="subject" class="form-control" />
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Document Description</label>
													</div>
													<div class="col-md-6">
														<textarea name="lecture" id="lecture" class="form-control"></textarea>
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Document Link</label>
													</div>
													<div class="col-md-6">
														<input type="text" name="createlink" id="createlink" class="form-control" />
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Create Date</label>
													</div>
													<div class="col-md-6">
														<input value="<?php echo $today; ?>" type="text" class="form-control" disabled />
														<input name="createdate" value="<?php echo $today; ?>" type="hidden" class="form-control" id="datetimepicker1" />
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														<label>Select File</label>
													</div>
													<div class="col-md-6">
														<div id="load_images"></div>
														<input type="file" name="uploadFile[]" id="document_upload" multiple class="file" data-overwrite-initial="false">
													</div>
												</div>
												<div class="col-lg-12 form-group">
													<div class="col-md-4">
														&nbsp;
													</div>
													<div class="col-md-6">
														<button type="button" name="save" id="edit_doc" class="btn btn-outline btn-success btn-width submitbtn"><i class="fa fa-database fa-1x"></i> UPDATE</button>
														<button type="button" class="btn btn-outline btn-success btn-width submitbtn falsesave"><i class="fa fa-upload fa-1x"></i> SAVE</button>
														<button type="submit" style="display:none" name="save" id="save"> SAVE</button>
														<button type="button" name="exit_dashboard" id="exit_dashboard" class="btn btn-outline btn-success btn-width exitbtn"><i class="fa fa-upload fa-1x"></i> EXIT</button>
													</div>
												</div>
												<div class="col-lg-12 form-group" style="margin-bottom:20px"></div>
											</fieldset>
										</form>
									</div><?php
										} else { ?>
									<div class="tab-pane fade in active" id="view-pills" style="padding: 11px;  margin-top: -50px; ">
										<div class="row">&nbsp;</div>

									</div><?php
										} ?>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-arra">
										<div class="panel-heading" id="datatable-panel-heading">
											All Document(s)
										</div>
										<div class="panel-body">
											<div class="dataTable_wrapper">
												<div class="panel panel-default">
													<div class="panel-heading2">
														<div class="row">
															<div class="col-lg-2">
																<p style="font-size: 25px;line-height: 1em;font-weight: bold;margin-bottom: 15px;">Search</p>
																<small onmouseover="this.style.cursor='pointer';this.style.color='RED';" onmouseout="this.style.color='black';" onclick="Show_Div(adse,adse1)" id="adclick">Advanced Search</small>
															</div>
															<div class="col-lg-10" id="adse" style="display:none;">
																<div class="row">
																	<div class="col-lg-4">
																		<div class="col-md-12">
																			<label>Title</label>
																		</div>
																		<div class="col-md-12">
																			<input type="text" name="title" id="title" class="form-control" />
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="col-md-12">
																			<label>Category</label>
																		</div>
																		<div class="col-md-12">
																			<select name="ad_cat" id="ad_cat" class="form-control">
																				<option value="">Select category</option>
																				<?php if ($_SESSION['access'] != "User") $catQuery = "SELECT cat_name FROM category WHERE cat_flag = '3'"; //"SELECT `category`, `status` FROM `ARRA_notice` WHERE `type` = 'FILE' GROUP BY `category`";
																				else $catQuery = "SELECT cat_name FROM category WHERE cat_flag = '3'"; //"SELECT `category`, `status` FROM `ARRA_notice` WHERE `status` = '1' AND `type` = 'FILE' GROUP BY `category`";
																				$catResult = mysqli_query(dbConnect(),$catQuery);
																				while ($catRow = mysqli_fetch_array($catResult)) {
																					echo '<option value="' . $catRow['cat_name'] . '">' . $catRow['cat_name'] . '</option>';
																				}
																				?>
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="col-md-12">
																			<label>Sub-Category</label>
																		</div>
																		<div class="col-md-12">
																			<select id="subcat" class="form-control">
																				<option value="">Select sub-category</option>
																				<?php if ($_SESSION['access'] != "User") $subcatQuery = "SELECT `subcategory`, `status` FROM `ARRA_notice` WHERE `type` = 'FILE' GROUP BY `subcategory`";
																				else $subcatQuery = "SELECT `subcategory`, `status` FROM `ARRA_notice` WHERE `status` = '1' AND `type` = 'FILE' GROUP BY `subcategory`";
																				$subcatResult = mysqli_query(dbConnect(),$subcatQuery);
																				while ($catRow = mysqli_fetch_array($subcatResult)) {
																					echo '<option value="' . $catRow['subcategory'] . '">' . $catRow['subcategory'] . '</option>';
																				}
																				?>
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="col-md-12">
																			<label>Published Date From</label>
																		</div>
																		<div class="col-md-12">
																			<input name="exdate" type="text" class="form-control" id="datetimepicker11" name="month" />
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="col-md-12">
																			<label>Published Date To</label>
																		</div>
																		<div class="col-md-12">
																			<input name="exdate" type="text" class="form-control" id="datetimepicker2" name="month" />
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="col-md-12">
																			<label>Applicable For</label>
																		</div>
																		<div class="col-md-12">
																			<select name="ad_appliedfor" id="ad_appliedfor" class="form-control">
																				<option value="">Select class</option>
																				<option value="Toddler">Toddler ( Age 2+ )</option>
																				<option value="Preschool">Pre School ( Age 3+ )</option>
																				<option value="Prekindergarten">Pre Kindergarten ( Age 4+ )</option>
																				<option value="Kinder">Kindergarten ( Age 5+ )</option>
																				<option value="Class-1">Class One ( Age 6+ )</option>
																				<option value="Class-2">Class Two </option>
																				<option value="Class-3">Class Three </option>
																				<option value="Class-4">Class Four </option>
																				<option value="Class-5">Class Five </option>
																				<option value="Class-6">Class Six </option>
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-3">
																		<div class="col-md-12">
																			<label>&nbsp;&nbsp;</label>
																		</div>
																		<div class="col-md-12">
																			<div class="row">
																				<div class="col-md-6 pull-left">
																					<button type="submit" name="ad_search" id="ad_search" class="btn btn-outline btn-success btn-width-search" style="background-color:#156059;color:#FFFFFF;"><i class="fa fa-binoculars fa-1x"></i> Search</button>
																				</div>
																				<div class="col-md-6">
																					<button class="btn btn-outline btn-success custom-btn" style="margin-left:10px" onmouseover="this.style.cursor='pointer';" onClick="window.location.reload()" name="reset" id="reset">View All</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-lg-10" id="adse1" style="display:block;">

																<form role="form1" method="POST" action="#" enctype='multipart/form-data'>
																	<div class="col-lg-5">
																		<div class="col-md-12">
																			<label>Category</label>
																		</div>
																		<div class="col-md-12">
																			<select name="searchcat" class="form-control" onchange="showCate(this.value,'category')">
																				<option disabled="" selected="" value="">Select category</option>
																				<?php if ($_SESSION['access'] != "User") $catQuery = "SELECT cat_name FROM category WHERE cat_flag = '3'"; //"SELECT `category`, `status` FROM `ARRA_notice` WHERE `type` = 'FILE' GROUP BY `category`";
																				else $catQuery = "SELECT cat_name FROM category WHERE cat_flag = '3'"; //"SELECT `category`, `status` FROM `ARRA_notice` WHERE `status` = '1' AND `type` = 'FILE' GROUP BY `category`";
																				$catResult = mysqli_query(dbConnect(),$catQuery);
																				while ($catRow = mysqli_fetch_array($catResult)) {
																					echo '<option value="' . $catRow['cat_name'] . '">' . $catRow['cat_name'] . '</option>';
																				}
																				?>
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-5">
																		<div class="col-md-12">
																			<label>Sub-Category</label>
																		</div>
																		<div class="col-md-12">
																			<select name="searchsubcat" id="searchsubcat" class="form-control" onchange="showCate(this.value,'subcategory')">
																				<option disabled="" selected="" value="">Select sub-category</option>
																				<?php if ($_SESSION['access'] != "User") $subcatQuery = "SELECT `subcategory`, `status` FROM `ARRA_notice` WHERE `type` = 'FILE' GROUP BY `subcategory`";
																				else $subcatQuery = "SELECT `subcategory`, `status` FROM `ARRA_notice` WHERE `status` = '1' AND `type` = 'FILE' GROUP BY `subcategory`";
																				$subcatResult = mysqli_query(dbConnect(),$subcatQuery);
																				while ($catRow = mysqli_fetch_array($subcatResult)) {
																					echo '<option value="' . $catRow['subcategory'] . '">' . $catRow['subcategory'] . '</option>';
																				}
																				?>
																			</select>
																		</div>
																	</div>

																	<div class="col-lg-2 ">
																		<div class="col-md-12">
																			&nbsp;
																		</div>
																		<div class="col-md-12">
																			<button class="btn btn-outline btn-success custom-btn" style="margin-left:-25px" onmouseover="this.style.cursor='pointer';" onClick="window.location.reload()" name="reset" id="reset">View All</button>
																		</div>
																	</div>
																</form>
															</div>

														</div>
													</div>


												</div>
												<table class="datatable table table-striped table-bordered table-hover1" id="dataTables1">
													<thead>
														<tr class="text-center">
															<th>#</th>
															<th>Title</th>
															<?php if ($_SESSION['access'] == "Admin" or $_SESSION['access'] == "SuperAdmin")  
															// echo '<th>Created By</th>'; 
														?>
															<th>Category</th>
															<th>Sub-Category</th>
															<th style="white-space: nowrap;">Published Date</th>
															<?php if ($_SESSION['access'] != "User") echo '<th>Applicable For</th>'; ?>
															<?php echo '<th><i class="fa fa-paperclip"></i></th>'; ?>
															<?php if ($_SESSION['access'] == "User") { ?><th>File Link</th><?php } ?>
															<th class="actiontd text-center" style="width: 200.8889px;">Action</th>
														</tr>
													</thead>
													<tbody id="txtHint">
														<?php $i = 0;
														if ($_SESSION['access'] != "User") {
															$query = mysqli_query(dbConnect(),"SELECT * FROM ARRA_notice WHERE type = 'FILE' ORDER BY id DESC") or die(mysqli_error());
														} else {
															$query = mysqli_query(dbConnect(),"SELECT * FROM ARRA_notice WHERE type = 'FILE' AND status = '1' ORDER BY id DESC") or die(mysqli_error());
														}
														while ($rows = mysqli_fetch_array($query)) { ?>
															<tr class="odd gradeX" <?php if ($_SESSION['access'] == "User") {
																						if (checkNoticeStuStatus($_SESSION['user_id'], $rows["id"]) == true) echo 'style=background-color:#FFFFFF;';
																						else echo "style=background-color:#D6F3DF;";
																					} ?>>
																<td class="text-center"><?php echo ++$i; ?></td>
																<td><?php echo $rows["title"]; ?></td>
																<?php if ($_SESSION['access'] == "Admin" or $_SESSION['access'] == "SuperAdmin") 
																// echo '<td>' . $rows["created_by"] . '</td>' 
															
															?>
																
																<td><?php echo $rows["category"]; ?></td>
																

																<td><?php echo $rows["subcategory"]; ?></td>
																<td><?php echo $rows["publish"]; ?></td>
																<?php if ($_SESSION['access'] != "User") {
																	if ($rows["appliedfor"] == "Group") {
																		$rows["appliedfor"] = "";
																		$q = mysqli_query(dbConnect(),"SELECT * FROM multiple_classes WHERE notice_id = '" . $rows["id"] . "' ORDER BY id") or die(mysqli_error());
																		while ($r = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
																			$rows["appliedfor"] .= $r["class"] . ", ";
																		}
																	}
																	$rows["appliedfor"] = rtrim($rows["appliedfor"], ", ");
																	echo '<td>' . getClassType($rows["appliedfor"]) . '</td>';
																} ?>
																<?php echo '<td style="width:8%">' . countAttachments($rows["id"]) . '&nbsp;<i class="fa fa-paperclip"></i></td>'; ?>
																<?php if ($_SESSION['access'] == "User") { ?><td style="width:12%"><a href="<?php echo $rows["createlink"]; ?>" target="_blank"><?php if ($rows["createlink"] != "") echo "Go to Link"; ?></a></td><?php } ?>
																<td class="actiontd text-center">
																	<?php if ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-R") == "FU-R") { ?>
																		<a href="viewDocuments.php?nid=<?php echo $rows["id"]; ?>" data-title="View Document" data-toggle="lightbox" data-parent="" data-gallery="remoteload" title='View Document'><button type='button' class='btn btn-success' data-value="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $rows["id"]; ?>" onclick=readNotice($(this).data('value'),$(this).data('id'))><i class="fa fa-search"></i> </button></a> <?php } ?>
																	<?php if ($_SESSION['access'] != "User") { ?>
																		<?php if ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-E") == "FU-E") { ?>
																			<a title='Edit'><button onclick="console.log($(this).data('button'))" data-button='{"id": "<?php echo $rows["id"]; ?>", "appliedfor":"<?php echo $rows["appliedfor"]; ?>"}' type='button' class='btn btn-warning btn-edit <?php if ($rows["status"] == 1) echo "disabled"; ?>'><i class="glyphicon glyphicon-pencil Edit"></i></button></a><?php } ?>
																		<?php if ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-D") == "FU-D") { ?>
																			<a title='Delete'><button type='button' class='btn btn-danger btn_del_album' data-loc="FU.php?removeDocId=<?php echo $rows["id"]; ?>&dir=<?php echo $rows["appliedfor"]; ?>"><i class="fa fa-trash-o fa-1x"></i> </button></a><?php } ?>
																		<?php if ($rows["status"] == 0) { ?>
																			<?php if ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-E") == "FU-E") { ?>
																				<a data-toggle="modal" title='publish' onClick="publishDocument('withemail','<?php echo $rows["appliedfor"]; ?>', '<?php echo $rows["id"]; ?>')"><button type='button' class='btn btn-primary publish'><i class="fa fa-eye fa-1x"></i> </button></a><?php } ?>
																		<?php } elseif ($rows["status"] == 1) { ?>
																			<?php if ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-E") == "FU-E") { ?>
																				<a title='Unpublish'><button type='button' class='btn btn-warning btn_unpublish' data-loc="FU.php?unpublish=<?php echo $rows["id"]; ?>"><i class="fa fa-eye-slash fa-1x"></i> </button></a><?php } ?>
																	<?php	}
																	} ?>
																	<?php if ($rows["reminder"] == 1 && $rows["status"] == 0 && ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-E") == "FU-E")) {
																		echo '<a title="No Notify"><button id="btn_notify" type="button" class="btn btn-default disabled"><i class="glyphicon glyphicon-ban-circle"></i> </button></a>';
																	} elseif ($rows["reminder"] == 1 && $rows["status"] == 1 && ($_SESSION['access'] == "SuperAdmin" or getAccessArea(getAccessArr(safe($_SESSION['user_id'])), "FU-E") == "FU-E")) { ?>
																		<a href="#" data-toggle="modal" data-value="<?php echo $rows["appliedfor"]; ?>" onclick="notifyReminder('withemail','<?php echo $rows["appliedfor"]; ?>', '<?php echo $rows["id"]; ?>')" title='Reminder'><button type='button' class='btn btn-primary notify'><i class="glyphicon glyphicon-bell"></i> </button></a>
																	<?php } ?>
																</td>

															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
										<div class="panel-footer">
											<div class="row">
												<?php if ($_SESSION['access'] != "User") { ?>
													<div class="col-lg-3 margin-top-10"><button type='button' class='btn btn-success'><i class="fa fa-search fa-1x"></i> </button> View Document</div>
													<div class="col-lg-3 margin-top-10"><button type='button' class='btn btn-warning'><i class="glyphicon glyphicon-pencil"></i> </button> Edit Document</div>
													<div class="col-lg-3 margin-top-10"><button type='button' class='btn btn-danger'><i class="fa fa-trash-o fa-1x"></i> </button> Delete Document</div>
													<div class="col-lg-3 margin-top-10"><button type='button' class='btn btn-primary publish'><i class="fa fa-eye fa-1x"></i> </button> Publish Document</div>
													<div class="col-lg-3 margin-top-10"><button type='button' class='btn btn-warning'><i class="fa fa-eye-slash fa-1x"></i> </button> Unpublish Document</div>
												<?php
												} else {
												?>
													<div class="col-lg-3 margin-top-10"><button type='button' class='btn btn-success'><i class="fa fa-search fa-1x"></i> </button> View Document</div>
												<?php } ?>


											</div>
											<div class="col-lg-12 form-group"></div>
											<?php if ($_SESSION['access'] != "User") { ?>
												<form action="FU.php" method="POST">
													<div class="col-lg-12 form-group">
														<div class="col-md-2">
															<label>Instructions</label>
														</div>
														<div class="col-md-10">
															<textarea name="textinstructions" id="textinstructions" class="form-control"><?php echo $instructionrow["instruction"]; ?></textarea>
															<input type="hidden" name="type" value="FILE">
															<input type="hidden" name="id" value="<?php echo $instructionrow["id"]; ?>">
														</div>
														<div class="col-md-2 col-md-offset-10">
															<br>
															<input type="submit" value="<?php if ($instructionrow["id"]) echo "Update";
																						else echo "Save"; ?>" name="instructions" class="form-control submitbtn" />
														</div>
													</div>
												</form>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							</div>
						</div><!-- /.col-lg-12 -->
					</div><!-- /.row -->
				</div><!-- /#page-wrapper -->
			</div><!-- /#wrapper -->
			<!-- ======================================================		MODAL =========================================== -->
			<input type=hidden id="documentID">
			<input type=hidden id="documentClass">

			<div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-labelledby="publish" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header" style="background-color: #227B72;color: #FFFFFF;">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title" id="publish">Documents</h4>
						</div>
						<div class="modal-body">
							<form name="publish_form" action="" class="form-horizontal calender" role="form">
								<div class="col-md-12">
									<input type="hidden" id="noticeId" value="">
									<div class="col-lg-12 form-group">
										<div class="col-md-2">
											<label id="SubjectTitle" for="Subject">Title:</label>
										</div>
										<div class="col-lg-10">
											<input type="text" class="col-md-7 form-control" id="email_subject" name="email_subject">
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<div class="col-md-2">
											<label>Body</label>
										</div>
										<div class="col-lg-10" id="textDiv">
											<textarea class="form-control" id="email_body" name="email_body"></textarea>
										</div>
									</div>
									<div class="col-lg-12 form-group" id="searchClassDiv" style="display:none;">
										<div class="col-md-2">
											<label for="Subject">Search By Class:</label>
										</div>
										<div class="col-lg-10">
											<select name="searchClass" id="searchClass" class="col-md-7 searchClass" onchange="selectClass($(this).val())" style="height:40px;"></select>
											<p class="col-md-2 btn btn-outline btn-success custom-btn" style="margin-left:10px" onmouseover="this.style.cursor='pointer';" onClick="document.getElementById('searchClass').selectedIndex = '0'; selectClass('allclass')">View All</p>
										</div>
									</div>
									<div class="col-lg-12 form-group">
										<div class="col-md-2">
											<label for="Subject">Search By Student Name:</label>
										</div>
										<div class="col-lg-10">
											<input type="text" name="searchClass" class="col-md-7 searchClass" onkeyup="searchByName($(this).val())" style="height:40px;">
											<p class="col-md-2 btn btn-outline btn-success custom-btn" onCLick='previewSelection()' style="margin-left:10px" id="selectListBtn">Preview</p>
										</div>
									</div>
									<div class="col-lg-12 numberofselecteditem"></div>
								</div>
								<!-- <div style="overflow-x:scroll; width:100%;"> -->
								<table class="table table-striped table-bordered table-hover" id="dataTables-email">
									<thead>
										<tr class="text-center">
											<th width=30px>SL</th>
											<th width=140px>Student Name</th>
											<th width=140px>Date Of Birth</th>
											<th width=140px>Class</th>
											<th width=180px>Prime Contact</th>
											<th width=180px>Alias Contact</th>
											<th width=70px>Publish <input type="checkbox" id="sap"></th> <!-- sap = select all publish -->
											<th width=70px>Email <input type="checkbox" id="sae"> </th> <!-- sae = select all email -->
										</tr>
									</thead>
									<tbody id="listHint"></tbody>
								</table>
								<!-- </div> -->
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-width-yes antosubmit3">Send & Publish</button>
							<button type="button" class="btn btn-default btn-width-cancel antoclose3 " data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="addNewCategory" tabindex="-1" role="dialog" aria-labelledby="addNewCategoryLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#156059; color:#FFFFFF;">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="addNewCategoryLabel">Add New Category</h4>
						</div>
						<div class="modal-body">
							<form>
								<div class="form-group ">
									<label for="Subject">Category:</label>
									<input type="text" name="catname" class="form-control" id="catname">
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary catsubmit btn-width-yes">Save</button>
							<button type="button" class="btn btn-default catcancel btn-width-cancel" data-dismiss="modal">Cancel</button>

						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>

			<div class="modal fade" id="addNewSubCategory" tabindex="-1" role="dialog" aria-labelledby="addNewSubCategoryLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#156059; color:#FFFFFF;">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="addNewSubCategoryLabel">Add New Sub Category</h4>
						</div>
						<div class="modal-body">
							<form>
								<div class="form-group">
									<label for="Subject">Category:</label>
									<input type="text" id="takeCatName" class="form-control" />
									<input type="hidden" id="takeCatId" />
								</div>
								<div class="form-group">
									<label for="Subject">Sub Category:</label>
									<input type="text" id="subcatname" class="form-control">
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary subcatsubmit btn-width-yes">Save</button>
							<button type="button" class="btn btn-default catcancel btn-width-cancel" data-dismiss="modal">Cancel</button>

						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<div class="modal fade" id="dltNewCategory" tabindex="-1" role="dialog" aria-labelledby="addNewCategoryLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#156059; color:#FFFFFF;">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="addNewCategoryLabel">Delete Category</h4>
						</div>
						<div class="modal-body">
							<div id="div_delcat">
								<form id="delcatForm" action="" class="form-horizontal" role="form">
									<input type="hidden" id="catdel_id" />
									<div class="form-group" style="display:block;">

										<div class="col-lg-12">
											<table class="table table-bordered" id="dataTables-cat">

												<thead>
													<tr class="text-center">
														<th>#</th>
														<th>Name</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody id="tbody_catdel">

												</tbody>
											</table>
										</div>
									</div>


								</form>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default catcancel btn-width-cancel" data-dismiss="modal">Cancel</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>

			<div class="modal fade" id="dltSubCategory" tabindex="-1" role="dialog" aria-labelledby="addNewCategoryLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#156059; color:#FFFFFF;">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="addNewCategoryLabel">Delete Sub Category</h4>
						</div>
						<div class="modal-body">
							<div id="div_delcat">
								<form id="delcatForm" action="" class="form-horizontal" role="form">
									<input type="hidden" id="catdel_id" />


									<div class="form-group" style="display:block;">

										<div class="col-lg-12">
											<table class="table table-bordered" id="dataTables-cat">

												<thead>
													<tr class="text-center">
														<th>#</th>
														<th>Name</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody id="tbody_subcatdel">

												</tbody>
											</table>
										</div>
									</div>


								</form>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default catcancel btn-width-cancel" data-dismiss="modal">Cancel</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- =========================================== End Modal ======================================================= -->

			<!-- Lightbox -->
			<script src="lightbox/dist/ekko-lightbox.js"></script>
			<script src="../responsive/bower_components/bootbox/bootbox.min.js"></script>
			<script type="text/javascript">
				$(document).ready(function($) {
					$("body").on("click", ".btn_del_cat", function(e) {

						var did = $(this).attr("data-id");
						var dval = $(this).attr("data-name");

						$.ajax({
							url: 'catCheck.php',
							type: 'POST',
							data: {
								catId: did,
								catName: dval
							},
							dataType: "json",
							success: function(emailData, txtSta) {
								console.log(emailData.msg);
								if (emailData.msg == "HAS") {
									alert("You cann't delete this Category");
								} else if (emailData.msg == "NO") {
									$("tr#" + did).hide('slow');
									$("#cateName option[value='" + dval + "']").remove();
								}
							},
							error: function(jqXHR, txtSta, errorThrown) {
								console.log(jqXHR);
								console.log(txtSta);
								console.log(errorThrown);
							}
						});



					});

					$("body").on("click", ".btn_del_subcat", function(e) {

						var catid = $("#takeCatId").val();
						var did = $(this).attr("data-id");
						var dval = $(this).attr("data-name");

						$.ajax({
							url: 'subCatCheck.php',
							type: 'POST',
							data: {
								catId: did,
								catName: dval
							},
							dataType: "json",
							success: function(emailData, txtSta) {
								console.log(emailData.msg);
								if (emailData.msg == "HAS") {
									alert("You cann't delete this Category");
								} else if (emailData.msg == "NO") {
									$("tr#" + did).hide('slow');
									$("#cateName option[value='" + dval + "']").remove();
								}
							},
							error: function(jqXHR, txtSta, errorThrown) {
								console.log(jqXHR);
								console.log(txtSta);
								console.log(errorThrown);
							}
						});


					});

					$("body").on("click", "a#open_del_cat", function(e) {
						var xmlhttp3 = new XMLHttpRequest();
						xmlhttp3.onreadystatechange = function() {
							if (xmlhttp3.readyState == 4 && xmlhttp3.status == 200) {
								all_emails_publish = xmlhttp3.responseText;
								document.getElementById("tbody_catdel").innerHTML = xmlhttp3.responseText;
							}
						}
						xmlhttp3.open("POST", "search_category.php?type=3", true);
						xmlhttp3.send();
					});

					$("body").on("click", "a#open_del_subcat", function(e) {

						var catid = $("#takeCatId").val();
						var xmlhttp2 = new XMLHttpRequest();
						//alert(catid);
						xmlhttp2.onreadystatechange = function() {
							if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
								all_emails_publish = xmlhttp2.responseText;
								document.getElementById("tbody_subcatdel").innerHTML = xmlhttp2.responseText;
							}
						}
						xmlhttp2.open("POST", "search_subcategory.php?type=" + catid, true);
						xmlhttp2.send();
					});
					// delegate calls to data-toggle="lightbox"
					$(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
						event.preventDefault();
						return $(this).ekkoLightbox({
							onShown: function() {
								if (window.console) {
									return console.log('Checking our the events huh?');
								}
							},
							onNavigate: function(direction, itemIndex) {
								if (window.console) {
									return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
								}
							}
						});
					});

					//Programatically call
					$('#open-image').click(function(e) {
						e.preventDefault();
						$(this).ekkoLightbox();
					});
					$('#open-youtube').click(function(e) {
						e.preventDefault();
						$(this).ekkoLightbox();
					});

					// navigateTo
					$(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
						event.preventDefault();
						return $(this).ekkoLightbox({
							onShown: function() {

								var a = this.modal_content.find('.modal-footer a');
								if (a.length > 0) {

									a.click(function(e) {

										e.preventDefault();
										this.navigateTo(2);

									}.bind(this));

								}

							}
						});
					});


				});
			</script>
			<!-- /Lightbox -->

			<!-- Bootstrap Core JavaScript -->
			<script src="../responsive/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


			<!-- Metis Menu Plugin JavaScript -->
			<script src="../responsive/bower_components/metisMenu/dist/metisMenu.min.js"></script>

			<!-- Custom Theme JavaScript -->
			<script src="../responsive/dist/js/sb-admin-2.js"></script>

			<script src="../responsive/dist/js/applyforchk.js"></script>

			<script type="text/javascript">
				var myDataArray = [];
			</script>
			<script type="text/javascript" src="../responsive/dist/js/nicEdit.js"></script>
			<script type="text/javascript">
				bkLib.onDomLoaded(function() {
					nicEditors.allTextAreas()
				});
			</script>
			<script type="text/javascript">
				function readNotice(notify, classfor) {
					$.ajax({
						url: 'readNotice.php',
						type: 'POST',
						data: {
							notice_id: classfor,
							user_id: notify
						},
						dataType: "json",
						success: function(emailData, txtSta) {
							$("#email_subject").val('Reminder : ' + emailData.email_title);
							//$("#SubjectTitle").text("Reminder Title: ");
							//$("#email_body").val(emailData.email_body);
							//$(".email_body").hide();

						},
						error: function(jqXHR, txtSta, errorThrown) {
							// Some code to debbug e.g.:               
							console.log(classfor);
							console.log(jqXHR);
							console.log(txtSta);
							console.log(errorThrown);
						}

					});
				}
			</script>
			<script type="text/javascript">
				var footerTemplate = '<div class="file-thumbnail-footer">\n' +
					'   <div style="margin:5px 0">\n' +
					'       <input name="input_descr[]" class="kv-input kv-new form-control input-sm {TAG_CSS_NEW}" value="" placeholder="Enter description...">\n' +
					// '       <input class="kv-input kv-init form-control input-sm {TAG_CSS_INIT}" value="{TAG_VALUE}" placeholder="Enter description...">\n' +
					'   </div>\n' +
					'   {actions}\n' +
					'</div>';
				$("#document_upload").fileinput({
					// uploadUrl: 'uploadfile.php', // you must set a valid URL here else you will get an error
					uploadAsync: false,
					overwriteInitial: false,
					maxFileSize: 1000,
					maxFilesNum: 10,
					showUpload: false,
					layoutTemplates: {
						footer: footerTemplate
					}
					//  slugCallback: function(filename) {
					//      return filename.replace('(', '_').replace(']', '_');
					//  },
					//  uploadExtraData: function() {
					//         return {
					//             applicable_for: $("#applicable_for").val().join(', '),
					//             id: $("#id").val(),
					// subject: $("#subject").val(),
					// lecture: $("#lecture").val(),
					// createlink: $("#createlink").val(),
					// cateName: $("#cateName").val(),
					// subCateName: $("#subCateName").val(),
					// input_descr: $("#input_descr").val()
					//         };
					//     }
				});

				function loadsubcat() {
					$("#takeCatName").val($("#cateName").val());
					$("#takeCatId").val($("#cateName").val());

					var catid = $("#takeCatId").val();
					$.ajax({
						url: 'subCatAdd.php',
						type: 'POST',
						data: {
							catid: catid
						},
						dataType: "html",
						success: function(data) {
							//alert("New Sub Catgory Insert successfully");
							$("#subCateName").html("");
							$("#subCateName").append(data);
							//$('#addNewSubCategory').modal('hide');
							//location.href = "FU.php";
						},
						error: function(jqXHR, textStatus, errorThrown) {
							// Some code to debbug e.g.:
							console.log(str);
							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
						}
					});
					return false;
				}
			</script>
			<script type="text/javascript">
				function callmodal(modalid) {
					if (modalid == "#addNewSubCategory")
						loadsubcat();
					$(modalid).modal("show");
				}
			</script>
			<script type="text/javascript">
				function notifyReminder(notify, classfor, id) {
					$("#publish-title").html("Documents Reminder");
					$('#publishModal').modal("show");
					myDataArray = [];
					$("#sap").prop("checked", false);
					$("#sae").prop("checked", false);
					//console.log(notify+" "+classfor+" "+id);
					var classfor = classfor;
					var id = id;

					if (notify == "withemail") {
						var myArr = classfor.split(",");
						//console.log("line 1003 = " + myArr.length);
						var myOpt = '';
						$('#searchClass').html("");
						if (myArr.length > 1) {
							$('#searchClassDiv').css('display', 'block');
							myOpt += '<option value="allclass">Select All</option>';
							$.each(myArr, function(key, value) {
								myOpt += '<option value="' + value + '">' + value + '</option>';
							});
							$('#searchClass').html(myOpt);
							classfor = myArr[0];
						} else {
							$('#searchClassDiv').css('display', 'none');
							$('#searchClass').html("");
						}
						// Email title and body
						$.ajax({
							url: 'getTitle.php',
							type: 'POST',
							data: {
								notice_id: id,
								reminder: 'Yes'
							},
							dataType: "json",
							success: function(emailData, txtSta) {
								$("#email_subject").val(emailData.email_title);
								nicEditors.findEditor("email_body").setContent(emailData.email_body);
							},
							error: function(jqXHR, txtSta, errorThrown) {
								console.log(classfor);
								console.log(jqXHR);
								console.log(txtSta);
								console.log(errorThrown);
							}
						});

						$("#noticeId").val(id);

						$.ajax({
							url: 'getEList.php',
							type: 'POST',
							data: {
								arr: myArr,
								notice_id: id,
								pr: 0
							},
							dataType: "json",
							success: function(edata, txtSta) {
								//console.log("line 1047= " + myArr);
								// console.log(edata);
								var checkall = true;
								$.each(edata, function(key, value) {
									myDataArray.push(edata[key]);
									if (edata[key]['publish'] == 0) checkall = false;
								});
								if (checkall) {
									$("#sap").prop("checked", true);
									$("#sae").prop("checked", true);
								} else {
									$("#sap").prop("checked", false);
									$("#sae").prop("checked", false);
								}
								selectClass(myArr);
							},
							error: function(jqXHR, txtSta, errorThrown) {
								console.log(classfor);
								console.log(jqXHR);
								console.log(txtSta);
								console.log(errorThrown);
							}

						});
					}
				}
			</script>

			<script type="text/javascript">
				function publishDocument(notify, classfor, id) {
					$('#publishModal').modal("show");
					myDataArray = [];
					$("#sap").prop("checked", false);
					$("#sae").prop("checked", false);
					// console.log(notify+" "+classfor+" "+id);
					var classfor = classfor;
					var id = id;

					if (notify == "withemail") {
						var myArr = classfor.split(",");
						//console.log("line 1003 = " + myArr.length);
						var myOpt = '';
						$('#searchClass').html("");
						if (myArr.length > 1) {
							$('#searchClassDiv').css('display', 'block');
							myOpt += '<option value="allclass">Select All</option>';
							$.each(myArr, function(key, value) {
								myOpt += '<option value="' + value + '">' + value + '</option>';
							});
							$('#searchClass').html(myOpt);
							classfor = myArr[0];
						} else {
							$('#searchClassDiv').css('display', 'none');
							$('#searchClass').html("");
						}
						// Email title and body
						$.ajax({
							url: 'getTitle.php',
							type: 'POST',
							data: {
								notice_id: id,
								reminder: 'Yes'
							},
							dataType: "json",
							success: function(emailData, txtSta) {
								$("#email_subject").val(emailData.email_title);
								nicEditors.findEditor("email_body").setContent(emailData.email_body);
							},
							error: function(jqXHR, txtSta, errorThrown) {
								console.log("line 1101: " + classfor);
								console.log("line 1102: " + jqXHR);
								console.log("line 1103: " + txtSta);
								console.log("line 1104: " + errorThrown);
							}
						});

						$("#noticeId").val(id);

						$.ajax({
							url: 'getEList.php',
							type: 'POST',
							data: {
								arr: myArr,
								pr: 1
							},
							dataType: "json",
							success: function(edata, txtSta) {
								//console.log("line 1047= " + myArr);
								//console.log(edata);
								$.each(edata, function(key, value) {
									myDataArray.push(edata[key]);
								});

								selectClass(myArr);
							},
							error: function(jqXHR, txtSta, errorThrown) {
								console.log("line 1125: " + classfor);
								console.log("line 1126: " + jqXHR);
								console.log("line 1127: " + txtSta);
								console.log("line 1128: " + errorThrown);
							}

						});
					}
				}

				function selectClass(classfor) {
					$("#listHint").html("");
					var emailList = "";
					var sl = 1;
					// console.log($.isArray(classfor));
					if (classfor == "allclass") { // For all array view
						for (var i = 0; i < myDataArray.length; i++) {
							emailList += '<tr>' +
								'<td>' + sl + '</td>' +
								'<td>' + myDataArray[i].student_name + '</td>' +
								'<td>' + myDataArray[i].dob + '</td>' +
								'<td>' + myDataArray[i].school + '</td>' +
								'<td>' + myDataArray[i].email_list + ' (' + myDataArray[i].first_name + ' ' + myDataArray[i].last_name + ') ' + '</td>' +
								'<td>' + myDataArray[i].alias + '</td>' +
								'<td><input onClick="publishclicks(\'' + myDataArray[i].tracking + '\')" id="chkPub' + i + '" class="chkBoxp chkBoxChk" type="checkbox" ' + (myDataArray[i].publish == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
								'<td><input onClick="emailclicks(\'' + myDataArray[i].tracking + '\')" id="chkEmail' + i + '" class="chkBoxe chkBoxChk" type="checkbox" ' + (myDataArray[i].email == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
								'</tr>';
							sl++;
						}
					} else if ($.isArray(classfor)) { // For multi class view
						$.each(classfor, function(key, value) {
							var apply = value.toLowerCase().trim();
							if (apply == 'prekindergarten') {
								apply = 'elschool';
							}
							for (var i = 0; i < myDataArray.length; i++) {
								if (myDataArray[i].school == apply) {
									emailList += '<tr>' +
										'<td>' + sl + '</td>' +
										'<td>' + myDataArray[i].student_name + '</td>' +
										'<td>' + myDataArray[i].dob + '</td>' +
										'<td>' + myDataArray[i].school + '</td>' +
										'<td>' + myDataArray[i].email_list + ' (' + myDataArray[i].first_name + ' ' + myDataArray[i].last_name + ') ' + '</td>' +
										'<td>' + myDataArray[i].alias + '</td>' +
										'<td><input onClick="publishclicks(\'' + myDataArray[i].tracking + '\')" id="chkPub' + i + '" class="chkBoxp chkBoxChk" type="checkbox" ' + (myDataArray[i].publish == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
										'<td><input onClick="emailclicks(\'' + myDataArray[i].tracking + '\')" id="chkEmail' + i + '" class="chkBoxe chkBoxChk" type="checkbox" ' + (myDataArray[i].email == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
										'</tr>';
									sl++;
								}
							}
						});
					} else { // For single class view
						var apply = classfor.toLowerCase().trim();
						if (apply == 'prekindergarten') {
							apply = 'elschool';
						}
						for (var i = 0; i < myDataArray.length; i++) {
							if (myDataArray[i].school == apply) {
								emailList += '<tr>' +
									'<td>' + sl + '</td>' +
									'<td>' + myDataArray[i].student_name + '</td>' +
									'<td>' + myDataArray[i].dob + '</td>' +
									'<td>' + myDataArray[i].school + '</td>' +
									'<td>' + myDataArray[i].email_list + ' (' + myDataArray[i].first_name + ' ' + myDataArray[i].last_name + ') ' + '</td>' +
									'<td>' + myDataArray[i].alias + '</td>' +
									'<td><input onClick="publishclicks(\'' + myDataArray[i].tracking + '\')" id="chkPub' + i + '" class="chkBoxp chkBoxChk" type="checkbox" ' + (myDataArray[i].publish == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
									'<td><input onClick="emailclicks(\'' + myDataArray[i].tracking + '\')" id="chkEmail' + i + '" class="chkBoxe chkBoxChk" type="checkbox" ' + (myDataArray[i].email == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
									'</tr>';
								sl++;
							}
						}
					}
					$(".numberofselecteditem").html("");
					$("#listHint").html(emailList);
				}

				function searchByName(name) {
					$("#listHint").html("");
					var emailList = "";
					var sl = 1;
					// console.log($.isArray(classfor));
					if (name == "") { // For all array view
						for (var i = 0; i < myDataArray.length; i++) {
							emailList += '<tr>' +
								'<td>' + sl + '</td>' +
								'<td>' + myDataArray[i].student_name + '</td>' +
								'<td>' + myDataArray[i].dob + '</td>' +
								'<td>' + myDataArray[i].school + '</td>' +
								'<td>' + myDataArray[i].email_list + ' (' + myDataArray[i].first_name + ' ' + myDataArray[i].last_name + ') ' + '</td>' +
								'<td>' + myDataArray[i].alias + '</td>' +
								'<td><input onClick="publishclicks(\'' + myDataArray[i].tracking + '\')" id="chkPub' + i + '" class="chkBoxp chkBoxChk" type="checkbox" ' + (myDataArray[i].publish == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
								'<td><input onClick="emailclicks(\'' + myDataArray[i].tracking + '\')" id="chkEmail' + i + '" class="chkBoxe chkBoxChk" type="checkbox" ' + (myDataArray[i].email == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
								'</tr>';
							sl++;
						}
					} else { // For name view
						name = name.toLowerCase();
						console.log(name);
						for (var i = 0; i < myDataArray.length; i++) {
							var fullname = (myDataArray[i].first_name).toLowerCase();
							fullname += (myDataArray[i].last_name).toLowerCase();
							// console.log(fullname.indexOf(name));
							if (fullname.indexOf(name) != -1) {
								emailList += '<tr>' +
									'<td>' + sl + '</td>' +
									'<td>' + myDataArray[i].student_name + '</td>' +
									'<td>' + myDataArray[i].dob + '</td>' +
									'<td>' + myDataArray[i].school + '</td>' +
									'<td>' + myDataArray[i].email_list + ' (' + myDataArray[i].first_name + ' ' + myDataArray[i].last_name + ') ' + '</td>' +
									'<td>' + myDataArray[i].alias + '</td>' +
									'<td><input onClick="publishclicks(\'' + myDataArray[i].tracking + '\')" id="chkPub' + i + '" class="chkBoxp chkBoxChk" type="checkbox" ' + (myDataArray[i].publish == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
									'<td><input onClick="emailclicks(\'' + myDataArray[i].tracking + '\')" id="chkEmail' + i + '" class="chkBoxe chkBoxChk" type="checkbox" ' + (myDataArray[i].email == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
									'</tr>';
								sl++;
							}
						}
					}
					sl = sl - 1;
					$(".numberofselecteditem").html("");
					$("#listHint").html(emailList);
				}

				function previewSelection() {
					$("#listHint").html("");
					var emailList = "";
					var sl = 1;
					for (var i = 0; i < myDataArray.length; i++) {
						if (myDataArray[i].publish == 1) {
							emailList += '<tr>' +
								'<td>' + sl + '</td>' +
								'<td>' + myDataArray[i].student_name + '</td>' +
								'<td>' + myDataArray[i].dob + '</td>' +
								'<td>' + myDataArray[i].school + '</td>' +
								'<td>' + myDataArray[i].email_list + ' (' + myDataArray[i].first_name + ' ' + myDataArray[i].last_name + ') ' + '</td>' +
								'<td>' + myDataArray[i].alias + '</td>' +
								'<td><input onClick="publishclicks(\'' + myDataArray[i].tracking + '\')" id="chkPub' + i + '" class="chkBoxp chkBoxChk" type="checkbox" ' + (myDataArray[i].publish == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
								'<td><input onClick="emailclicks(\'' + myDataArray[i].tracking + '\')" id="chkEmail' + i + '" class="chkBoxe chkBoxChk" type="checkbox" ' + (myDataArray[i].email == 1 ? 'checked' : '') + ' value="' + myDataArray[i].tracking + '" ></td>' +
								'</tr>';
							sl++;
						}
					}
					sl = sl - 1;
					$(".numberofselecteditem").html(sl + " of " + i + " are selected.");
					$("#listHint").html(emailList);
				}

				function publishclicks(val) {
					if ($('.chkBoxp:checked').length == $('.chkBoxp').length) {
						$('#sap').prop('checked', true);
						$('#sae').prop('checked', true);
					} else {
						$('#sap').prop('checked', false);
						$('#sae').prop('checked', false);
					}

					//console.log(myDataArray);
					for (var i = 0; i < myDataArray.length; i++) {
						if (myDataArray[i].tracking == val) {
							if (myDataArray[i].publish == 1) {
								myDataArray[i].publish = 0;
								// myDataArray[i].email = 0;
								// $("#chkEmail"+i).prop("checked", false);
							} else {
								myDataArray[i].publish = 1;
								// myDataArray[i].email = 1;
								// $("#chkEmail"+i).prop("checked", true);
							}
						}
					}
				}

				function emailclicks(val) {
					if ($('.chkBoxe:checked').length == $('.chkBoxe').length) {
						$('#sae').prop('checked', true);
						$('#sap').prop('checked', true);
					} else {
						$('#sae').prop('checked', false);
						$('#sap').prop('checked', false);
					}

					for (var i = 0; i < myDataArray.length; i++) {
						if (myDataArray[i].tracking == val) {
							if (myDataArray[i].email == 1) {
								myDataArray[i].email = 0;
								// myDataArray[i].publish = 0;
								// $("#chkPub"+i).prop("checked", false);
							} else {
								myDataArray[i].email = 1;
								// myDataArray[i].publish = 1;
								// $("#chkPub"+i).prop("checked", true);
							}
						}
					}
					// console.log(myDataArray);
				}

				$(document).ready(function() {

					$('#sap').on('click', function() {
						if (this.checked) {
							$('.chkBoxp').each(function() {
								this.checked = true;
							});
							for (var i = 0; i < myDataArray.length; i++) {
								myDataArray[i].publish = 1;
							}
						} else {
							$('.chkBoxp').each(function() {
								this.checked = false;
							});
							for (var i = 0; i < myDataArray.length; i++) {
								myDataArray[i].publish = 0;
							}
						}
					});

					// $('.chkBoxChk').on('click',function(){
					// 	alert("asdas");
					// });

					$('#sae').on('click', function() {
						if (this.checked) {
							$('.chkBoxe').each(function() {
								this.checked = true;
							});
							for (var i = 0; i < myDataArray.length; i++) {
								myDataArray[i].email = 1;
							}
						} else {
							$('.chkBoxe').each(function() {
								this.checked = false;
							});
							for (var i = 0; i < myDataArray.length; i++) {
								myDataArray[i].email = 0;
							}
						}
					});

				});


				var emailFlag = 0;

				$(".antosubmit3").click(function() {
					// console.log(myDataArray);

					for (var i = 0; i < myDataArray.length; i++) {
						if (myDataArray[i].publish == 1) {
							emailFlag++;
						}
					}

					if (emailFlag > 0) {
						var emailArr = myDataArray;
						var emailTitle = $("#email_subject").val();
						var emailBody = $("#email_body").val();
						var noticeID = $("#noticeId").val();
						var message = "";
						console.log(myDataArray);
						$.ajax({
							url: 'notEmail.php',
							type: 'POST',
							data: {
								noticeID: noticeID,
								noticeType: "File",
								emailTitle: emailTitle,
								emailBody: emailBody,
								emailArr: emailArr
							},
							dataType: "json",

							success: function(data, textStatus) {
								alert("Notice Update Status : " + data['msg']);
								location.href = "FU.php";
							},
							error: function(jqXHR, textStatus, errorThrown) {
								location.href = "FU.php";
							}
						});
					} else {
						alert("You can't publish this document without select any account for publish!!!");
					}
				});

				/* 
				function returnschoolnamesmall(val){
					var apply = val.toLowerCase().trim();
					if(apply == 'prekindergarten')
						apply = 'elschool';
					}
					return apply;
				}*/
				function returnschoolname(val) {
					var apply = val.toUpperCase().trim();
					if (apply == 'Elschool') {
						apply = 'Prekindergarten';
					}
					return apply;
				}

				function callConfirm(classfor, id) {
					$("#documentID").val(id);
					$("#documentClass").val(classfor);
				}
			</script>


			<!-- Taken form NTC, Date 04-10-2015-->
			<script type="text/javascript">
				$(".antoclose3").click(function() {
					$("#email_to").html("");
					$("#number_to").html("");
					//alert("as");
				});
				$(".nav.nav-tabs li a").click(function() {

					var search_text = ($(this).attr("href"));

					var myDTtable = $('#dataTables1').DataTable();
					myDTtable.search(search_text.substr(1)).draw();

				});
			</script>
			<script type="text/javascript">
				$(".smschk").click(function() {
					$(".email").css("display", "none");
					$(".sms").css("display", "block");
					$("#email_to").html(""); //$("#number_to").html("");
					$("#email_load").html("");


					//alert("as");
				});
				$(".emailchk").click(function() {
					$(".sms").css("display", "none");
					$(".email").css("display", "block");
					$("#number_to").html("");
					$(".smsbody").css("display", "none");
					$(".msgbody").val("");

					//$("#number_to").html("");
					//alert("as");
				});
			</script>
			<!-- jquery ui 1.11.4 -->
			<script src="../responsive/bower_components/jquery/dist/jquery-ui-1.11.4.js"></script>


			<script type="text/javascript">
				$(document).ready(function() {
					$("#ad_search").click(function() {
						var title = $("#title").val();
						var categor = $("#ad_cat").val();
						var subcategory = $("#subcat").val();
						var from_date = $("#datetimepicker11").val();
						var to_date = $("#datetimepicker2").val();
						var appliedfor = $("#ad_appliedfor").val();



						// console.log("Else Ok");
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
							if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
							}
						}
						xmlhttp.open("POST", "ad_search.php?title=" + title + "&category=" + categor + "&subcategory=" + subcategory + "&from_date=" + from_date + "&to_date=" + to_date + "&appliedfor=" + appliedfor + "&type=FILE", true);
						xmlhttp.send();

					});

				});
			</script>
			<script>
				function showDate(str, value) {
					if (str.length == 0) {
						document.getElementById("txtHint").innerHTML = "";
						return;
					} else {
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
							if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
							}
						}
						xmlhttp.open("GET", "gethint.php?type=FILE&value=" + value + "&q=" + str, true);
						xmlhttp.send();
					}
				}
			</script>
			<script>
				function showCate(str, value) {
					if (str.length == 0) {
						document.getElementById("txtHint").innerHTML = "";
						return;
					} else {
						$("#datatable-panel-heading").html(str);
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
							if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								document.getElementById("txtHint").innerHTML = xmlhttp.responseText;

								if (value == "category") {
									// console.log(str);
									$.ajax({
										url: 'subCatAdd.php',
										type: 'POST',
										data: {
											catid: str
										},
										dataType: "html",
										success: function(data) {
											$("#searchsubcat").html("");
											$("#searchsubcat").append(data);
										},
										error: function(jqXHR, textStatus, errorThrown) {
											// Some code to debbug e.g.:
											console.log(str);
											console.log(jqXHR);
											console.log(textStatus);
											console.log(errorThrown);
										}
									});
								}
							}
						}
						xmlhttp.open("GET", "gethint.php?type=FILE&value=" + value + "&q=" + str, true);
						xmlhttp.send();
					}
				}
			</script>
			<script>
				function showClass(str, value) {
					if (str.length == 0) {
						document.getElementById("txtHint").innerHTML = "";
						return;
					} else {
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
							if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
							}
						}
						xmlhttp.open("GET", "gethint.php?type=FILE&value=" + value + "&q=" + str, true);
						xmlhttp.send();
					}
				}
			</script>
			<script type="text/javascript">
				function Show_Div(Div_id, Div_id2) {
					if (false == $(Div_id).is(':visible')) {
						$(Div_id).show(250);
						$(Div_id2).hide(250);
						$("#adclick").text("Basic Search");
					} else {
						$(Div_id).hide(250);
						$(Div_id2).show(250);
						$("#adclick").text("Advanced Search");
					}
				}
			</script>


			<script language="Javascript">
				$(function() {
					jQuery(document.body).on('click', "input[type='checkbox']", function(e) {
						e.stopImmediatePropagation();
						//console.log(e.target.id);
						var id = e.target.id;
						if ($(this).is(':checked') == true) {
							if (id == "select_email_load") {
								$('#email_load option').prop('selected', true);
							}
							if (id == "select_email_to") {
								$('#email_to option').prop('selected', true);
							}
						} else {
							if (id == "select_email_load") {
								$('#email_load option').removeProp('selected', true);
							}
							if (id == "select_email_to") {
								$('#email_to option').removeProp('selected');
							}
						}
					});
				});

				function SelectMoveRows(SS1, SS2) {
					$('#select_email_load').removeAttr('checked');
					$('#select_email_to').removeAttr('checked');
					var SelID = '';
					var SelText = '';
					// Move rows from SS1 to SS2 from bottom to top
					for (i = SS1.options.length - 1; i >= 0; i--) {
						if (SS1.options[i].selected == true) {
							SelID = SS1.options[i].value;
							SelText = SS1.options[i].text;
							var newRow = new Option(SelText, SelID);
							SS2.options[SS2.length] = newRow;
							SS1.options[i] = null;
						}
					}
					SelectSort(SS2);
				}

				function SelectSort(SelList) {
					var ID = '';
					var Text = '';
					for (x = 0; x < SelList.length - 1; x++) {
						for (y = x + 1; y < SelList.length; y++) {
							if (SelList[x].text > SelList[y].text) {
								// Swap rows
								ID = SelList[x].value;
								Text = SelList[x].text;
								SelList[x].value = SelList[y].value;
								SelList[x].text = SelList[y].text;
								SelList[y].value = ID;
								SelList[y].text = Text;
							}
						}
					}
				}
			</script>

			<script src="../responsive/bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#datetimepicker1').datetimepicker({
						yearOffset: 0,
						lang: 'en',
						timepicker: false,
						format: 'Y-m-d',
						formatDate: 'Y-m-d'
					});
				});
			</script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#datetimepicker11').datetimepicker({
						yearOffset: 0,
						lang: 'en',
						timepicker: false,
						format: 'Y-m-d',
						formatDate: 'Y-m-d'
					});
				});
			</script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#datetimepicker2').datetimepicker({
						yearOffset: 0,
						lang: 'en',
						timepicker: false,
						format: 'Y-m-d',
						formatDate: 'Y-m-d', // and tommorow is maximum date calendar
						//minDate:'-1970/01/021'
					});
				});
			</script>
			<script type="text/javascript">
				$(".antosubmit3").click(function() {
					if ($('#email_to').has('option').length > 0 && $('#number_to').has('option').length == 0) {

						var emailArr = [];
						var emailTitle = $("#email_subject").val();
						var emailBody = $("#email_body").val();
						var noticeID = $("#docId").val();
						var message = "";

						$("#email_to option").each(function() {
							emailArr.push($(this).val());
						});
						// console.log(emailArr);

						if (emailArr == "") {
							message = "Select Minimum 1 E-mail";
						} else if (emailTitle == "") {
							message = "Type Email Title";
						} else if (emailBody == "") {
							message = "Type Email Body";
						}

						if (emailArr == "" && emailTitle == "" && emailBody == "") {
							$("#msgDiv").html('<div class="row voffset2" id="messageDiv">' +
								'<div class="col-md-6 col-md-offset-4">' +
								'<div class="alert alert-success alert-dismissable">' +
								'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">' +
								'<i class="glyphicon glyphicon-remove"></i></button>' + message +
								'</div>' +
								'</div>' +
								'</div>');
						} else {

							$.ajax({
								url: 'notEmail.php',
								type: 'POST',
								data: {

									noticeID: noticeID,
									emailTitle: emailTitle,
									emailBody: emailBody,
									emailArr: emailArr
								},
								dataType: "json",

								success: function(data, textStatus) {
									alert("Document Upload Status : " + data['msg']);
									location.href = "FU.php";
								},
								error: function(jqXHR, textStatus, errorThrown) {
									// Some code to debbug e.g.:
									console.log(str);
									console.log(jqXHR);
									console.log(textStatus);
									console.log(errorThrown);
								}
							});
							return false;

						}



					} else if ($('#number_to').has('option').length > 0 && $('#email_to').has('option').length == 0) {

						var numArr = [];
						var msgBody = $("#msg_body").val();

						$("#number_to option").each(function() {
							numArr.push($(this).val());
						});
						// console.log(emailArr);
					} else if ($('#email_to').has('option').length > 0 && $('#number_to').has('option').length > 0) {

						var emailArr = [];
						var numArr = [];
						var emailTitle = $("#email_subject").val();
						var emailBody = $("#email_body").val();
						var msgBody = $("#msg_body").val();

						$("#email_to option").each(function() {
							emailArr.push($(this).val());
						});
						// console.log(emailArr);


						$("#number_to option").each(function() {
							numArr.push($(this).val());
						});
						// console.log(numArr);
					}
				});
			</script>

			<script type="text/javascript">
				$(".catsubmit").click(function() {
					var emailTitle = $("#catname").val();
					$.ajax({
						url: 'catAdd.php',
						type: 'POST',
						data: {
							cat_type: 3,
							cat: emailTitle
						},
						dataType: "html",
						success: function(data, textStatus) {
							$("#cateName").append(data);
							$('#addNewCategory').modal('hide');
							$("#catname").val('');
						},
						error: function(jqXHR, textStatus, errorThrown) {
							// Some code to debbug e.g.:
							console.log(str);
							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
						}
					});
					return false;

				});

				$(".subcatsubmit").click(function() {
					var catid = $("#takeCatId").val();
					var subcatname = $("#subcatname").val();
					$.ajax({
						url: 'subCatAdd.php',
						type: 'POST',
						data: {
							catid: catid,
							subcat: subcatname
						},
						dataType: "html",
						success: function(data) {
							//alert("New Sub Catgory Insert successfully");
							$("#subCateName").append(data);
							$('#addNewSubCategory').modal('hide');
							//location.href = "FU.php";
							$("#subcatname").val('');
						},
						error: function(jqXHR, textStatus, errorThrown) {
							// Some code to debbug e.g.:
							console.log(str);
							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
						}
					});
					return false;
				});
			</script>

			<script src="../responsive/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
			<script src="../responsive/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
			<script type="text/javascript">
				$(function() {
					$('#dataTables1').DataTable({
						'searching': false,
						// 'responsive': true,
						'ordering': false,
						'bLengthChange': false
						// 'sScrollX': '100%',
						// 'aoColumns' : [
						// 	{ 'sWidth': '30px' },
						// 	{ 'sWidth': '120px' },
						// 	{ 'sWidth': '80px' },
						// 	{ 'sWidth': '80px' },
						// 	{ 'sWidth': '70px' },
						// 	{ 'sWidth': '180px' },
						// 	{ 'sWidth': '150px' }
						// ]
					});
					/*
					$('#dataTables-email').DataTable({
						searching: false,
				        responsive: true,
				        ordering: false,
				        bLengthChange: false,
				        bInfo: false,
				        paging: false,
				        sScrollX: '100%',
						aoColumns : [
							{ 'sWidth': '30px' },
							{ 'sWidth': '140px' },
							{ 'sWidth': '140px' },
							{ 'sWidth': '180px' },
							{ 'sWidth': '70px' },
							{ 'sWidth': '70px' }
						]
					});
					*/
				});
			</script>


			<script type="text/javascript">
				$(document).ready(function() {
					$("#edit_doc").hide();
					$("body").on("click", ".btn-edit", function() {
						$(".falsesave").hide();
						$("#edit_doc").show();
						$(".select2_multiple *").prop("selected", false);
						$(".select2_multiple").trigger("change");

						$('.nav-pills a[href="#add-pills"]').tab('show');
						$(window).scrollTop(0);

						var data = $.parseJSON($(this).attr('data-button'));


						$("#notice_id_edit").val(data.id);
						if (data.ab_for != "") {
							var c = (data.appliedfor).split(", ");
							$.each(c, function(key) {
								$(".select2_multiple ." + c[key]).prop("selected", "selected");
							});
							$(".select2_multiple").trigger("change");
						}

						$.ajax({
							url: 'getTitle.php',
							type: 'POST',
							//data : {notice_id:data.id},
							data: {
								notice_id: data.id,
								reminder: 'Yes'
							},
							dataType: "json",
							success: function(getData, textStatus) {
								//console.log("");

								$("#cateName").val(getData.category);
								var html = "<option value=" + getData.subcategory + " selected>" + getData.subcategory + "</option>";
								$("#subCateName").append(html);
								$("#subject").val(getData.email_title);
								$("#datetimepicker1").val(getData.expireon);
								$("#createlink").val(getData.createlink);

								nicEditors.findEditor("lecture").setContent(getData.email_body);
								//nicEditors.findEditor( "salutation" ).setContent( getData.salutation );

							},
							error: function(jqXHR, textStatus, errorThrown) {
								// Some code to debbug e.g.:
								console.log(str);
								console.log(jqXHR);
								console.log(textStatus);
								console.log(errorThrown);
							}

						});


						$("#cateName").val(data.category);
						var html = "<option value=" + data.subCategory + " selected>" + data.subCategory + "</option>";
						$("#subCateName").append(html);
						$("#subject").val(data.titlee);
						$("#lecture").val(data.bodyy);
						$("#datetimepicker1").val(data.cdate);
						$("#createlink").val(data.clink);
						//console.log(data.subCategory);
						$.ajax({
							url: 'getAttachment.php',
							type: 'POST',
							data: {
								id: data.id
							},
							dataType: "json",

							success: function(data, textStatus) {
								var imghtml = '';
								$.each(data, function(key, value) {
									imghtml += '<div class="file-preview" id="div_photo_' + data[key].attachid + '"><div class=" file-drop-zone">' +
										'<div class="file-preview-thumbnails">'

										+
										'<div class="file-preview-frame" id="" data-fileindex="0">' +
										'<img src="../noticeboard/' + data[key].file_name + '" class="file-preview-image" title="' + data[key].file_title + '" alt="' + data[key].file_title + '" style="max-width:300px; height:160px;">' +
										'<div class="file-thumbnail-footer">' +
										'<div style="margin:5px 0">' +
										'<input name="inputfiledes[]" class="kv-input kv-new form-control input-sm " value="' + data[key].file_title + '" disabled placeholder="Enter Title...">' +
										'<input class="kv-input kv-init form-control input-sm hide" value="' + data[key].file_title + '" placeholder="Enter description..."></div>' +
										'<div class="file-actions">' +
										'<div class="file-footer-buttons">' +
										'<button type="button" class="kv-file-remove btn btn-xs btn-default rmvphoto" title="Remove file" onClick=removeattach("' + data[key].attachid + '")><i class="glyphicon glyphicon-trash text-danger"></i></button>' +
										'</div>' +
										'<div class="clearfix"></div>' +
										'</div>' +
										'</div>' +
										'</div>' +
										'</div>' +
										'<div class="clearfix"></div><div class="file-preview-status text-center text-success"></div>' +
										'<div style="display: none;" class="kv-fileinput-error file-error-message"></div>' +
										'</div></div>';

								});


								$("#load_images").html(imghtml);

							},
							error: function(jqXHR, textStatus, errorThrown) {
								// Some code to debbug e.g.:
								// console.log(str);
								console.log(jqXHR);
								console.log(textStatus);
								console.log(errorThrown);
							}
						});
					});
				});

				function removeattach(id) {
					$("#div_photo_" + id).hide();
					$.ajax({
						url: 'deleterows.php',
						type: 'POST',
						data: {
							id: id
						},
						dataType: "json",
						success: function(data, textStatus) {
							console.log(textStatus);
						}
					});
				}
			</script>

			<script src="../responsive/bower_components/bootstrap/dist/js/bootstrap.js"></script>
			<script type="text/javascript">
				$(function() {
					$("body").on("click", "button.btn_del_album", function() {
						var url = $(this).attr("data-loc");
						bootbox.dialog({
							message: "Do you want to delete this Document?",
							title: "Confirmation",
							buttons: {
								yes: {
									label: "Yes",
									className: "btn-success",
									callback: function() {
										if (url != "") {
											window.location.assign(url);
										}

									}
								},
								no: {
									label: "No",
									className: "btn-danger",
									callback: function() {

									}
								}
							}
						});
					});

					$("body").on("click", "button.btn_unpublish", function() {
						var url = $(this).attr("data-loc");
						bootbox.dialog({
							message: "Do you want to unpublish this Document?",
							title: "Confirmation",
							buttons: {
								yes: {
									label: "Yes",
									className: "btn-success",
									callback: function() {
										if (url != "") {
											window.location.assign(url);
										}

									}
								},
								no: {
									label: "No",
									className: "btn-danger",
									callback: function() {

									}
								}
							}
						});
					});

					$("body").on("click", "button.falsesave", function(e) {
						e.preventDefault();
						bootbox.dialog({
							message: "Do you want to save this document?",
							title: "Confirmation",
							buttons: {
								yes: {
									label: "Yes",
									className: "btn-success",
									callback: function() {
										$("#save").trigger("click");
									}
								},
								no: {
									label: "No",
									className: "btn-danger",
									callback: function() {
										alert("No");
									}
								}
							}
						});
					});

					$("body").on("click", "button#edit_doc", function(e) {
						e.preventDefault();
						bootbox.dialog({
							message: "Do you want to update this document?",
							title: "Confirmation",
							buttons: {
								yes: {
									label: "Yes",
									className: "btn-success",
									callback: function() {
										$("#save").trigger("click");
									}
								},
								no: {
									label: "No",
									className: "btn-danger",
									callback: function() {
										alert("No");
									}
								}
							}
						});
					});

					$("#canceledit_doc").click(function() {
						bootbox.dialog({
							message: "Do you want to cancel?",
							title: "Confirmation",
							buttons: {
								yes: {
									label: "Yes",
									className: "btn-success",
									callback: function() {
										$("#save").trigger("click");
										location.href = 'index.php';
									}
								},
								no: {
									label: "No",
									className: "btn-danger",
									callback: function() {
										alert("No");
									}
								}
							}
						});

					});

					$("#exit_dashboard").click(function() {
						bootbox.dialog({
							message: "Do you want to exit?",
							title: "Confirmation",
							buttons: {
								yes: {
									label: "Yes",
									className: "btn-success",
									callback: function() {
										location.href = 'index.php';


									}
								},
								no: {
									label: "No",
									className: "btn-danger",
									callback: function() {

									}
								}

							}
						});

					});


				});
			</script>

			<!-- select2 -->
			<script src="bootstrap-fileinput/js/select/select2.full.js"></script>
			<script>
				$(function() {
					$(".select2_multiple").select2({
						maximumSelectionLength: 10,
						placeholder: "Select",
						allowClear: true
					});
					$(".select2_multiple").select2({
						width: '100%'
					});
				});
			</script>
	</body>

	</html>
<?php
} else {
	require_once 'login.php';
}
?>