<?php 
	session_start();
	
	if(!empty($_SESSION['user_id'])){
		//$_SESSION['tracking_number'] = "";
		include ('function.php');
		dbConnect();
		//$message = 0;
		
		function make_thumb($src, $dest, $desired_width) {
			
			
			/* read the source image */
			$source_image = imagecreatefromjpeg($src);
			$width = imagesx($source_image);
			$height = imagesy($source_image);
			
			/* find the "desired height" of this thumbnail, relative to the desired width  */
			$desired_height = floor($height * ($desired_width / $width));
			
			/* create a new, "virtual" image */
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			
			/* copy source image at a resized size */
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			
			/* create the physical thumbnail image to its destination */
			imagejpeg($virtual_image, $dest);
			
		}
		function resize($img,$dir){
			/*
				only if you script on another folder get the file name
				$r =explode("/",$img);
				$name=end($r);
			*/
			//new folder
			
			$vdir_upload = $dir;
			list($width_orig, $height_orig) = getimagesize($img);
			//ne size
			$dst_width = 110;
			$dst_height = ($dst_width/$width_orig)*$height_orig;
			$im = imagecreatetruecolor($dst_width,$dst_height);
			$image = imagecreatefromjpeg($img);
			imagecopyresampled($im, $image, 0, 0, 0, 0, $dst_width, $dst_height, $width_orig, $height_orig);
			//modive the name as u need
			imagejpeg($im,$vdir_upload);
			//save memory
			imagedestroy($im);
		}
		
		if(!is_dir('../album')){
			mkdir('../album', 0777, true);
			mkdir('../album/all', 0777, true);
			mkdir('../album/toddler', 0777, true);
			mkdir('../album/preschool', 0777, true);
			mkdir('../album/kinder', 0777, true);
			mkdir('../album/elschool', 0777, true);
			chmod('../album/', 0777);
		}
		
		if(isset($_REQUEST["removeDoc"])){
			$query = mysql_query("DELETE FROM ARRA_album_photo WHERE ARRA_photo_id = '".$_REQUEST["removeDoc"]."'");
			if($query){
				unlink($_REQUEST["removeDoc"]);
				$message .= "Successfully Removed !!!";
			}
		}
		if(isset($_REQUEST["unpublish"])){
			$query = mysql_query("UPDATE ARRA_album set ARRA_album_status=0 WHERE ARRA_album_id = '".$_REQUEST["unpublish"]."'");
			if($query){
				
				$message .= "Album Successfully Unpublished...";
			}
			
		}
		if(isset($_REQUEST["removeAlbum"])){
			$query = mysql_query("DELETE FROM ARRA_album WHERE ARRA_album_location = '".$_REQUEST["removeAlbum"]."'");
			if($query){
				$location=$_REQUEST["removeAlbum"];
				array_map('unlink', glob("$location/*.*"));
				rmdir($location);
				$message .= "Album Successfully Removed !!!";
			}
		}
		if(isset($_POST['create_category'])){
			//$location_cat="../album/" . $_POST["cat_for"] . "/" . $_POST["cat_name"];
			//if(!is_dir($location_cat)){
			//mkdir($location_cat, 0777, true);		
			$query = mysql_query("INSERT INTO category VALUES ( NULL ,1, '".$_POST["cat_name"]."', NULL)");
			if($query){
				$message .= "Category successfully created...";
			}else{$message .= "Already exists !!!";}
			//}	
		}
		if(isset($_GET['func']) && !empty($_GET['func'])){
			switch($_GET['func']) {
				case 'create_category':
				
				if (isset($_COOKIE["c_catname"])){
					$query = mysql_query("INSERT INTO category VALUES ( NULL ,1, '".$_COOKIE['c_catname']."', NULL)");
					if($query){
						$message .= "Category successfully created...";
					}else{$message .= "Already exists !!!";}
					//}	
				}
                break;
				case 'album_unpublish':
				
				if (isset($_COOKIE["c_unpubid"])){
					$query = mysql_query("UPDATE ARRA_album set ARRA_album_status=0 WHERE ARRA_album_id = '".$_COOKIE['c_unpubid']."'");
					if($query){
						
						$message .= "Album Successfully Unpublished...";
					}
				}
                break;
				case 'photo_publish':
				
				if (isset($_COOKIE["c_photopubid"])){
					$query = mysql_query("UPDATE ARRA_album_photo SET ARRA_photo_status=1 WHERE ARRA_photo_id='".$_COOKIE['c_photopubid']."' ");
					
					
				}
                break;
				case 'album_publish':
				
				if (isset($_COOKIE["c_photopubid"])){
					$datenow = date_create()->format('Y-m-d');
					$query = mysql_query("UPDATE ARRA_album SET ARRA_album_status=1,ARRA_album_publishdate='".$datenow."' WHERE ARRA_album_id='".$_COOKIE['c_albumpubid']."' ");
					
				}
                break;
				
				case 'album_delete':
				
				if(isset($_COOKIE["c_del_album_loc"])){
					$query = mysql_query("DELETE FROM ARRA_album WHERE ARRA_album_location = '".$_COOKIE["c_del_album_loc"]."'");
					if($query){
						$location=$_COOKIE["c_del_album_loc"];
						array_map('unlink', glob("$location/*.*"));
						rmdir($location);
						$message .= "Album Successfully Removed !!!";
					}
				}
                break;
				
				default:
                // Do nothing?
			}
			
		}
		if(isset($_POST['create_album'])){
			//if(isset($_POST["album_category"])){
			$location="../album/" . $_POST["album_for"] . "/" . $_POST["album_category"] . "/" . $_POST["album_name"];
			$album_id = null;
			
			if(!is_dir($location)){	
				mkdir($location, 0777, true); 
				$datenow = date_create()->format('Y-m-d');
				$query = mysql_query("INSERT INTO ARRA_album VALUES ( NULL ,'".$_POST["album_for"]."', '".$_POST["album_category"]."', '".$_POST["album_name"]."' , '".$_POST["album_descr"]."' , '".$location."','".$_POST["album_pubdate"]."', NULL,'".$_POST["email_not"]."','".$_POST["sms_not"]."',0)") or die(mysql_error()); 
				if($query){
					$message .= "Album Successfully Created...";
				}
				//$album_id = mysql_insert_id();
			}
			if(is_dir($location)){
				extract($_POST);
				$error=array();
				
				$result = mysql_query("SELECT ARRA_album_id FROM ARRA_album WHERE ARRA_album_location='".$location."' ") or die(mysql_error()); 
				if($result){
					$row = mysql_fetch_row($result);
					$album_id= $row[0];
					//$message .= $album_id;
				}
				
				//$extension=array("bmp","jpeg","jpg","png","gif");
				$extension=array("jpeg","jpg");
				$i_desc=0;
				foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name)
				{
					$file_name=$_FILES["files"]["name"][$key];
					$file_tmp=$_FILES["files"]["tmp_name"][$key];
					$ext=pathinfo($file_name,PATHINFO_EXTENSION);
					// match allowed extentions
					if(in_array($ext,$extension))
					{
						if(filesize($file_tmp) < 4000000){
							if(!file_exists($location."/".$file_name))
							{
								$maxDim = 800;
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									if( $ratio > 1) {
										$width = $maxDim;
										$height = $maxDim/$ratio;
										} else {
										$width = $maxDim*$ratio;
										$height = $maxDim;
									}
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									$width = 400;
									$height = 300;
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/thumb_".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								//move_uploaded_file($file_tmp,$location."/".$file_name);
								chmod($location."/".$file_name, 0777);
								
								$query = mysql_query("INSERT INTO ARRA_album_photo VALUES ( NULL ,'".$album_id."', '".$file_name."', '".$_POST['input_descr'.$i_desc]."' , '".$location."/".$file_name."', NULL,0)") or die(mysql_error()); 
								if($query){
									$message .= "Successfully Uploaded...";
								}
							}
							else
							{
								$maxDim = 800;
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									if( $ratio > 1) {
										$width = $maxDim;
										$height = $maxDim/$ratio;
										} else {
										$width = $maxDim*$ratio;
										$height = $maxDim;
									}
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									$width = 400;
									$height = 300;
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/thumb_".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								//unlink($location."/".$file_name);
								//move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],$location."/".$file_name);
								
								chmod($location."/".$file_name, 0777);
								//make_thumb($location."/".$file_name,$location."/".$file_name."_tmp",80);
								
							}
						}else{$message .= "Image size should be less than 4MB...";}
					}
					else
					{
						array_push($error,"$file_name, ");
						$message .= "Image format not supported...";
					}
					$i_desc++;
				}
			}
		}
		if(isset($_POST['edit_album'])){
			
			$location="../album/" . $_POST["album_for_edit"] . "/" . $_POST["album_category_edit"] . "/" . $_POST["album_name"];
			$album_id = null;
			if(!is_dir($location)){
				mkdir($location, 0777, true);
				$datenow = date_create()->format('Y-m-d');
				$query = mysql_query("INSERT INTO ARRA_album VALUES ( NULL ,'".$_POST["album_for_edit"]."', '".$_POST["album_category_edit"]."', '".$_POST["album_name"]."' , '".$_POST["album_descr"]."' , '".$location."','".$_POST["album_pubdate"]."', NULL,'".$_POST["email_not"]."','".$_POST["sms_not"]."',0)") or die(mysql_error()); 
				if($query){
					$message .= "Album Successfully Created...";
				}
			}
			if(is_dir($location)){
				$result = mysql_query("SELECT ARRA_album_id FROM ARRA_album WHERE ARRA_album_location='".$location."' ") or die(mysql_error()); 
				if($result){
					$row = mysql_fetch_row($result);
					$album_id= $row[0];
					$message .= $album_id;
				}
				extract($_POST);
				$error=array();
				//$extension=array("bmp","jpeg","jpg","png","gif");
				$extension=array("jpeg","jpg");
				foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name)
				{
					$file_name=$_FILES["files"]["name"][$key];
					$file_tmp=$_FILES["files"]["tmp_name"][$key];
					$ext=pathinfo($file_name,PATHINFO_EXTENSION);
					// match allowed extentions
					if(in_array($ext,$extension))
					{	
						if(filesize($file_tmp) < 4000000){
							if(!file_exists($location."/".$file_name))
							{
								$maxDim = 800;
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									if( $ratio > 1) {
										$width = $maxDim;
										$height = $maxDim/$ratio;
										} else {
										$width = $maxDim*$ratio;
										$height = $maxDim;
									}
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									$width = 400;
									$height = 300;
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/thumb_".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								//move_uploaded_file($file_tmp,$location."/".$file_name);
								chmod($location."/".$file_name, 0777);
								$query = mysql_query("INSERT INTO ARRA_album_photo VALUES ( NULL ,'".$album_id."', '".$file_name."', '".$_POST["album_descr"]."' , '".$location."/".$file_name."', NULL,0)") or die(mysql_error()); 
								if($query){
									$message .= "Successfully Uploaded...";
								}
							}
							else
							{
								$maxDim = 800;
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									if( $ratio > 1) {
										$width = $maxDim;
										$height = $maxDim/$ratio;
										} else {
										$width = $maxDim*$ratio;
										$height = $maxDim;
									}
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								$maxDim = 100;
								list($width, $height, $type, $attr) = getimagesize( $file_tmp );
								if ( $width > $maxDim || $height > $maxDim ) {
									//$target_filename = $file_tmp;
									$fn = $file_tmp;
									$size = getimagesize( $fn );
									$ratio = $size[0]/$size[1]; // width/height
									$width = 400;
									$height = 300;
									$src = imagecreatefromstring( file_get_contents( $fn ) );
									$dst = imagecreatetruecolor( $width, $height );
									imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
									imagedestroy( $src );
									imagejpeg( $dst, $location."/thumb_".$file_name ); // adjust format as needed
									imagedestroy( $dst );
								}
								//$filename=basename($file_name,$ext);
								//$newFileName=$filename.time().".".$ext;
								//unlink($location."/".$file_name);
								//move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],$location."/".$file_name);
								
								chmod($location."/".$file_name, 0777);
								
							}
						}else{$message = "Image size should be less than 4MB...";}
					}
					else
					{
						array_push($error,"$file_name, ");
						$message = "Image format not supported...";
					}
				}
			}
			
			
		}
		
		$album_categories = mysql_query("SELECT * FROM category where cat_flag=1");
	?>

<!DOCTYPE html>
	<html lang="en">		
		<head>			
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="description" content="">
			<meta name="author" content="">
			
			<title>..::Practice::..</title>
			
			<!-- Bootstrap Core CSS -->
			<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
			<link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">
			
			<!-- Bootstrap File Input -->
			
			<link href="bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
			<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
			<!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.
			This must be loaded before fileinput.min.js -->
			<script src="bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
			<script src="bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
			<!-- bootstrap.js below is only needed if you wish to use the feature of viewing details 
			of text file preview via modal dialog -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
			
			<!-- MetisMenu CSS -->
			<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
			
			
			
			<!-- Custom Fonts -->
			<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
			<link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
			<meta name="theme-color" content="#ffffff">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
			<style>
				.btn.btn-primary.btn-file { background:#196960 }
				.modal { overflow: auto !important; }
				.modal-header{border-color: #156059;color: #FFF;background-color: #156059;}
				
			</style>	
			<!-- Custom CSS -->
			<link href="../dist/css/sb-admin-2.css" rel="stylesheet">
			<script type="text/javascript" src="jsbar/canvasjs.min.js"></script>
			
		</head>
		
		<body>
			
			<div id="wrapper">
				
				<!-- Navigation -->
				<?php include('nav.php') ?>
				<!-- End Navigation -->
				
				
				<div id="page-wrapper">
					
					<div class="row">
						<div class="col-lg-12">
							<h1 class="page-header">Practices</h1>
						</div>
						<div class="col-lg-12">
							<?php if($_SESSION['access'] != "User") { ?>
								<?php 
									if(isset($message)){
										echo '<div class="row voffset2" id="messageDiv">
										<div class="col-md-6 col-md-offset-4">
										<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>'.$message.'
										</div>
										</div>
										</div>';
										//echo $message;
										unset($message);
									}
								?>
								
								<div class="panel panel-arra">
									<div class="panel-heading">
										<div class="row">
											<div class="col-lg-6 voffset2" >
												
												<?php 
													if(isset($message) AND $message!=""){
													?>
													<div class="alert alert-danger alert-dismissable" style="  height: 50px; padding: 0px 25px 0px 135px;">
														<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
														<a href="#" class="alert-link"><?php echo $message;?></a>.
													</div>
													<?php
														
													}
												?>
												
												<!--
													<h6 class="text-left">Before completing the application form please read the <a href="../sample/InstructionsforAdmissionForm.pdf" target="_blank" style="color:WHITE; font-size:14px; font-weight:bold;">instructions </a>carefully.</h6>
													<h6 class="text-left">Fields marked with (<strong style="font-weight: bold;color: red;">*</strong>) are mandatory.</h6>
													<h6 class="text-left">Click on (<strong style="font-weight: bold;color: white;">?</strong>) for additional information.</h6>
												-->
											</div>
											<div class="col-lg-6">
												<h3 class="text-right" style="padding-top: 0%;" >Login as <?php echo $_SESSION['name']; ?></h3>
											</div>
										</div>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-arra">
													<div class="panel-heading">
														List of Albums
													</div>
													<div class="panel-body">
														<div class="dataTable_wrapper">
															<table class="datatable table table-striped table-bordered table-hover" id="dataTables-albums">
																<tfoot>
																	<tr>
																		<th>Name</th>
																		<th>For</th>
																		<th>Position</th>
																		<th>Office</th>
																		<th>Age</th>
																		<th>Start date</th>
																		<th>Salary</th>
																	</tr>
																</tfoot>
																<thead>
																	<tr class="text-center">
																		<th width=70px>SL</th>
																		<!--<th>For</th>-->
																		<th>Title</th>
																		<th>Category</th>
																		<th>Create Date</th>
																		<th>Publish Date</th>
																		<th>Number Of Photos</th>
																		<th>Action</th>
																	</tr>
																</thead>
																<tbody>
																	<?php $i=1;
																		$query = mysql_query("SELECT * FROM ARRA_album order by ARRA_album_createdate desc") or die(mysql_error());
																		while($rows = mysql_fetch_array($query)){ ?>
																		<tr class="odd gradeX" id="<?php echo $rows["ARRA_album_id"]; ?>">
																			<td><?php echo $i++; ?></td>
																			<!--<td><?php //echo $rows["ARRA_album_for"]; ?></td>-->
																			<td><?php echo $rows["ARRA_album_name"]; ?></td>
																			<td><?php echo $rows["ARRA_album_category"]; ?></td>
																			
																			<td><?php echo substr($rows["ARRA_album_createdate"],0,10); ?></td>
																			
																			<td><?php echo $rows["ARRA_album_publishdate"]; ?></td>
																			<td><?php $query2 = mysql_query("SELECT * FROM ARRA_album_photo where ARRA_album_id='".$rows["ARRA_album_id"]."'") or die(mysql_error()); 
																				$num_rows = mysql_num_rows($query2);
																				echo  $num_rows;
																			?></td>
																			
																			<td class="text-center">
																				<a title="View" target="_blank" href="galleryshow.php?id=<?php echo $rows["ARRA_album_id"]; ?>" ><button type="button" class="btn btn-info"><i class="glyphicon glyphicon-zoom-in"></i></button></a>
																				<?php if($_SESSION['access'] != "User") { ?>
																					<button data-button='{"id": "<?php echo $rows["ARRA_album_id"]; ?>", "ab_for":"<?php echo $rows["ARRA_album_for"]; ?>","ab_cat": "<?php echo $rows["ARRA_album_category"]; ?>","ab_name": "<?php echo $rows["ARRA_album_name"]; ?>","ab_descr": "<?php echo $rows["ARRA_album_description"]; ?>","ab_pubdate": "<?php echo $rows["ARRA_album_publishdate"]; ?>","ab_emailnot": "<?php echo $rows["ARRA_album_emailnot"]; ?>","ab_smsnot": "<?php echo $rows["ARRA_album_smsnot"]; ?>"}' type='button' class='btn btn-success btn-edit'><i class="glyphicon glyphicon-pencil"></i></button>
																					<a title='Delete'  ><button id="btn_del_album" data-loc="<?php echo $rows["ARRA_album_location"]; ?>" type='button' class='btn btn-danger'><i class="fa fa-trash-o fa-1x"></i> </button></a>
																					<!--<button id="btn-publish" data-value="<?php echo $rows["ARRA_album_for"]; ?>" data-sendemail="<?php echo $rows["ARRA_album_sendemail"]; ?>" onclick = "showsendemail($(this).data('value'))" data-toggle="modal" data-target="#PublishModal" type='button' class='btn btn-warning'><i class="fa fa-upload fa-1x"></i> </button> -->
																					<?php if($rows["ARRA_album_status"] == 0){ ?>
																						<a title='Publish' data-enot= "<?php echo $rows["ARRA_album_emailnot"]; ?>" data-snot= "<?php echo $rows["ARRA_album_smsnot"]; ?>" onclick="goDoSomething(this,$(this).data('value'),$(this).data('enot'),$(this).data('snot'),$(this).data('albumid'),$(this).data('photoid'));" href="#" data-toggle="modal" data-target="#publish" data-value= "<?php echo $rows["ARRA_album_for"]; ?>" data-albumid= "<?php echo $rows["ARRA_album_id"]; ?>" data-photoid= "<?php echo $rows["ARRA_album_id"]; ?>" ><button type='button' class='btn btn-primary publish'><i class="fa fa-eye fa-1x" ></i> </button></a>
																						<?php } elseif($rows["ARRA_album_status"] == 1){ ?>
																						
																						<a title='Unpublish'><button id="btn_unpublish" data-id="<?php echo $rows["ARRA_album_id"]; ?>" type='button' class='btn btn-warning'><i class="fa fa-eye-slash fa-1x"></i> </button></a>
																						
																					<?php } ?>
																					<!--
																						<?php if($rows["ARRA_album_emailnot"] == 0 && $rows["ARRA_album_smsnot"] == 0 ){
																							echo '<a title="No Notify"><button id="btn_notify" type="button" class="btn btn-default disabled"><i class="glyphicon glyphicon-ban-circle"></i> </button></a>';
																						} else{ ?>
																						<a href="#" data-toggle="modal" data-target="#notify" data-value= "<?php echo $rows["ARRA_album_for"]; ?>" data-id= "<?php echo $rows["ARRA_album_id"]; ?>" data-enot= "<?php echo $rows["ARRA_album_emailnot"]; ?>" data-snot= "<?php echo $rows["ARRA_album_smsnot"]; ?>" onclick = showHint2($(this).data('value'),$(this).data('enot'),$(this).data('snot'),$(this).data('id')) title='Notify'><button type='button' class='btn btn-primary publish'><i class="glyphicon glyphicon-bell" ></i> </button></a>
																						<?php } ?> -->
																				<?php	} ?>
																			</td>
																		</tr>	
																		<?php
																		}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							<?php } ?>
							
						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /#page-wrapper -->

				<!-- /#wrapper -->
				
				<!-- jQuery -->
				<script src="../bower_components/jquery/dist/jquery.min.js"></script>
				<script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
				<!-- Bootstrap Core JavaScript -->
				<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
				<!-- Bootstrap Bootbox -->
				<script src="../bower_components/bootbox/bootbox.min.js"></script>

				<!-- DataTables JavaScript -->
				<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
				<script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
				
				<!-- Metis Menu Plugin JavaScript -->
				<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
				
				<!-- Custom Theme JavaScript -->
				<script src="../dist/js/sb-admin-2.js"></script>
				<script src="../dist/js/applyforchk.js"></script>
				<script>
				$(function()
					{
						$('#dataTables-albums').dataTable( { responsive: true,"bSort" : false } );
					}
				</script>

	</body>

</html>
<?php
}else{
require_once 'login.php';
}
?>																																																																																																																																													