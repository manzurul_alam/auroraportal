<table width="100%">

    <tr style="margin-bottom: 12px; text-align: center;" align="center" valign="middle">
        <img src="../../images/Auroralog.jpg" style="width: 12%; margin:0% 0% 0%  45%" />
    </tr>
    <tr style="margin-bottom: 12px; text-align:justify;">
        <p>Thank you for taking the time to complete and submit the Admission Form for Aurora International School. Details of your application are provided for your reference below:</p>
    </tr>
    <tr style="margin-bottom: 12px;">
        <span><strong>Name of student: </strong>'.$infoex[0].' '.$infoex[1].' </span><br />
        <span><strong>Date of birth: </strong>'.$user.' </span><br />
        <span><strong>Application tracking number: </strong>'.$user.' </span><br />
    </tr>
    <tr style="margin-bottom: 12px;text-align:justify;">
        <p>Please print out the payment instruction document to pay the admission processing fee at the AIS campus. After we receive the fee, we will start to review your application. You can expect to hear from us within one week after payment of the processing fee. If you do not receive any email or phone call from us within seven days, please feel free to call us at +88-0184-123-4230 or email us at info@aurora-intl.org.</p>
    </tr>
    <tr style="margin-bottom: 12px;">
        <p>Thank you for your interest in Aurora International School!</p>
    </tr>
    <tr style="margin-bottom: 12px;">
        <p>Sincerely,</p>
    </tr>
    <!-- <tr style="margin-bottom: 12px;">
        <p style="margin-bottom: -12px;">Israt Jahan</p>
        <p style="margin-bottom: -12px;">Head of Admissions</p>
        <p>Student Admissions and Records</p>
    </tr> -->
    <tr style="margin-bottom: 12px;">
        <p style="margin-bottom: -12px;">Nusrat Tabassum</p>
        <p style="margin-bottom: -12px;">Officer</p>
        <p>Student Admissions and Records</p>
    </tr>
    <tr style="margin-bottom: 12px;">
        <!-- <p style="margin-bottom: -12px;"><span style="font-size: 9px;">House No. NE(A)3A, Road No. 74</span></p> -->
        <p style="margin-bottom: -12px;"><span style="font-size: 9px;">Gulshan 2, Road 83, House NE (L) 5 A & C. Dhaka 1212</span></p>
        <p style="margin-bottom: -12px;"><span style="font-size: 9px;">Telephone: +88 (02) 985 2251-4</span></p>
        <p style="margin-bottom: -12px;"><span style="font-size: 9px;">Mobile: +88 01841 234230</span></p>
        <p><span style="font-size: 9px;">Website: www.aurora-intl.org</span></p>
    </tr>
</table>