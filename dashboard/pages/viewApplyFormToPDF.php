<?php 
session_start();

if(!empty($_SESSION['user_id'])){
$_SESSION['tracking_number'] = $_REQUEST["trackingid"];
include ('function.php');
dbConnect();
$count = 0;
$html = "";
$html .= '<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>... AIS. ..</title>
<style>
th{text-align: left;}
</style>
</head>
<body>';
	// $query = mysqli_query("SELECT * FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_REQUEST["trackingid"]."'");
	// $childsData = mysqli_fetch_array($query, MYSQLI_ASSOC); 
	// $html .='<table width="100%">
	$conn = mysqli_connect("localhost", "root", "", "arranew");

$query = mysqli_query($conn, "SELECT * FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_REQUEST["trackingid"]."'");
$childsData = mysqli_fetch_array($query, MYSQLI_ASSOC);
$html .='<table width="100%"> // continue your HTML code here

	<tr>
		<td rowspan=2><img src="../../images/auroralogo.png" width=70px height=60px></td>
		<td colspan=2><span style="font-size:30px;">AURORA INTERNATIONAL SCHOOL</span></td>
	</tr>
	<tr>
		<td>Student Name: <b>'.str_replace("@", " ", $childsData["ARRA_childs_detail_name"]).'</b></td>
		<td style="text-align:right; padding-right:60px;">Tracking No: <b>'.$_REQUEST["trackingid"].'</b></td>
	</tr>
	</table>
	<hr>
	<h4 style="background:#CDCDCD; text-indent:20px; padding:5px;">Section 01. Basic Information</h4>';
	$required_span = "";
$required_count = 0;
$query = mysqli_query($conn, "SELECT * FROM ARRA_applying WHERE ARRA_applying_tracking = '".$_REQUEST["trackingid"]."'");
$applyingData = mysqli_fetch_array($query, MYSQLI_ASSOC);
$html .= '<table>
    <tr>
        <th width="350px"><span style="color:#F00;">*</span> Tracking Number</th>
        <td><b>'. ($applyingData["ARRA_applying_tracking"] ?? '') .'</b></td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Applying for Grade/Level</th>
        <td>'. (isset($applyingData["ARRA_applying_for"]) ? getClassType($applyingData["ARRA_applying_for"]) : '') .'</td>
    </tr>
    <tr>
        <th>'. ((isset($applyingData["ARRA_applying_for"]) && ($applyingData["ARRA_applying_for"] == "toddler" || $applyingData["ARRA_applying_for"] == "preschool")) ? $required_span . ' Preferred hours' : 'Preferred hours') .'</th>
        <td>'. ($applyingData["ARRA_applying_preTime"] ?? '') .'</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Intended starting date</th>
        <td>'. ($applyingData["ARRA_applying_inTime"] ?? '') .'</td>
    </tr>
</table>';


if(isset($applyingData["ARRA_applying_has_school"]) && $applyingData["ARRA_applying_has_school"]) {
    $html .= '<table>
        <thead>
            <tr>
                <th>#</th>
                <th><span style="color:#F00;">*</span> Name of the past school</th>
                <th><span style="color:#F00;">*</span> Academic Year</th>
                <th><span style="color:#F00;">*</span> Grade / Class</th>
                <th><span style="color:#F00;">*</span> Language of instruction</th>
                <th><span style="color:#F00;">*</span> Location</th>
            </tr>
        </thead>';
    // Add table rows and data here
    $html .= '</table>';
	$i=1; $q = mysqli_query(dbConnect(), "SELECT * FROM ARRA_stu_pre_school WHERE ARRA_stu_pre_school_tracking = '".$applyingData["ARRA_applying_tracking"]."'") or die(mysql_error());
	while($rowData = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
	$html .= 
		'<tbody>
			<tr>
				<td>'.$i++.'</td>
				<td>'.$rowData["ARRA_stu_pre_school_name"].'</td>
				<td>'.$rowData["ARRA_stu_pre_school_aca_year"].'</td>
				<td>'.$rowData["ARRA_stu_pre_school_class_dtl"].'</td>
				<td>'.$rowData["ARRA_stu_pre_schoolcol_lan"].'</td>
				<td>'.$rowData["ARRA_stu_pre_school_loc"].'</td>
			</tr>
		</tbody>';
	}
	$html .= '</table>';
	}
	$html .= '
	<div>
	&nbsp;
	</div>
	<h4 style="background:#CDCDCD; text-indent:20px; padding:5px;">Section 02. Student\'s Details</h4>';
	//$query = mysql_query("SELECT * FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number = '".$_REQUEST["trackingid"]."'");
	//$childsData = mysql_fetch_array($query, MYSQL_ASSOC); 
	//this two line pull up
	$html .= '
	<table>
		<tr>
			<th width=350px><span style="color:#F00;">*</span> Student\'s Name</th>
			<td>';
				$html .= str_replace("@", " ", $childsData["ARRA_childs_detail_name"]);
			$html .= '</td>
		</tr>
		<tr>
			<th>Preferred Name (if applicable)</th>
			<td>'.$childsData["ARRA_childs_detail_prename"].'</td>
		</tr>
		<tr>
			<th><span style="color:#F00;">*</span> Date of Birth</th>
			<td>'.$childsData["ARRA_childs_detail_dob"].'</td>
		</tr>
		<tr>
			<th><span style="color:#F00;">*</span> Place of Birth</th>
			<td>'.$childsData["ARRA_childs_detail_pob"].'</td>
		</tr>
		<tr>
			<th><span style="color:#F00;">*</span> Gender</th>
			<td>';
				$html .= ($childsData["ARRA_childs_detail_gender"] == "male")?"Male":"Female";
			$html .= '</td>
		</tr>
		<tr>
			<th><span style="color:#F00;">*</span> Primary Nationality</th>
			<td>'.$childsData["ARRA_childs_detail_CN"].'</td>
		</tr>
		<tr>
			<th>Secondary Nationality</th>
			<td>';
				$ls = explode(",", $childsData["ARRA_childs_detail_SN"]); 
				if($ls[0] != ""){
					for($i=0; $i<count($ls); $i++){
						$html .= $ls[$i]."<br>";
					}
				}
				else{
					$html .= "NA";
				}
			$html .= '</td>
		</tr>
		<tr>
			<th><span style="color:#F00;">*</span> Current Address</th>
			<td>'.$childsData["ARRA_childs_detail_cur_add"].'</td>
		</tr>
		<tr>
			<th><span style="color:#F00;">*</span> Permanent Address</th>
			<td>'; 
				if($childsData["ARRA_childs_detail_per_add_flag"]) 
					$html .= $childsData["ARRA_childs_detail_cur_add"]; 
				else 
					$html .= $childsData["ARRA_childs_detail_per_add"]; 
			$html .= '</td>
		</tr>
	</table>';

	$i = 1;
$q = mysqli_query($conn, "SELECT * FROM ARRA_stu_nid WHERE ARRA_stu_nid_tracking = '".$childsData["ARRA_childs_detail_tracking_number"]."'") or die(mysqli_error($conn));

	if(mysqli_num_rows($q) > 0) {
	$html .= '<table>
	<thead>
		<tr>
			<th colspan=3>NID/Citizen Certificate</th>
		</tr>
		<tr>
			<th>#</th>
			<th>Nationality</th>
			<th>Certificate No</th>
		</tr>
	</thead>';
	while($rowData = mysqli_fetch_array($q, MYSQLI_ASSOC)){ 
	$html .= '<tbody>
		<tr>
			<td>'.$i++.'</td>
			<td>'.$rowData["ARRA_stu_nid_nationaliy"].'</td>
			<td>'.$rowData["ARRA_stu_nid_certificate"].'</td>
		</tr>
	</tbody>';
	}
	$html .= '</table>';
	}
	$html .= '<div>&nbsp;</div>
	<h4 style="background:#CDCDCD; text-indent:20px; padding:5px;">Section 03. Family Information</h4>';
	$live_query = mysqli_query($conn, "SELECT ARRA_childs_detail_livewith, ARRA_childs_detail_family_situation FROM ARRA_childs_detail WHERE ARRA_childs_detail_tracking_number='".$_REQUEST["trackingid"]."'") or die(mysqli_error($conn));

	$live_with = mysqli_fetch_array($live_query, MYSQLI_ASSOC);
	$con = 0;
	$con_query = mysqli_query($conn, "SELECT * FROM ARRA_contact_detail WHERE tracking_number ='".$_REQUEST["trackingid"]."'") or die(mysqli_error($conn));

	while($cd = mysqli_fetch_array($con_query)){
		$contact_category[$con] = $cd[1];
		$contact_status[$con] = $cd[2];
		$contact_number[$con] = $cd[3];
		$contact_type[$con] = $cd[4];
		$contact_priority[$con++] = $cd[5];
	}
	// Assuming $conn is your database connection
$conn = mysqli_connect("localhost", "root", "", "arranew");

$query = mysqli_query($conn, "SELECT * FROM ARRA_family_detail WHERE tracking_number ='".$_REQUEST["trackingid"]."'") or die(mysqli_error($conn));

	$html .= '<table>';
	$i=0; while($childsData = mysqli_fetch_array($query, MYSQLI_ASSOC))
	{
		if($i == 0) $i++; else {
			$html .= '
			<tr>
				<td colspan=2>&nbsp;</td>
			</tr>';
		}
	$html .= '
	<tr>
		<th width=350px><span style="color:#F00;">*</span> '.ucfirst($childsData["relation"])."'s".' Name</th>
		<td>'.$childsData["firstname"]." ".$childsData["lastname"].'</td>
	</tr>
	<tr>
		<th><span style="color:#F00;">*</span> Nationality</th>
		<td>'.$childsData["nationality"].'</td>
	</tr>
	<tr>
		<th>Secondary Nationality</th>
		<td>';
		$ls = explode(",", $childsData["secnationality"]); 
		if($ls[0] != ""){
			for($i=0; $i<count($ls); $i++){
				$html .= $ls[$i]."<br>";
			}
		}
		else{
			$html .= "NA";
		}
		$html .= '</td>
	</tr>
	<tr>
		<th><span style="color:#F00;">*</span> Highest Educational Qualification</th>
		<td>'.$childsData["higheduqua"].'</td>
	</tr>
	<tr>
		<th><span style="color:#F00;">*</span> Occupation</th>
		<td>'.$childsData["occupation"].'</td>
	</tr>
	<tr>
		<th>Organization</th>
		<td>'.$childsData["organization"].'</td>
	</tr>
	<tr>
		<th>Designation</th>
		<td>'.$childsData["designation"].'</td>
	</tr>
	<tr>
		<th>Office Address</th>
		<td>'.$childsData["officeaddress"].'</td>
	</tr>
	<tr>
		<th>Mailing Address</th>
		<td>';
		switch($childsData["mailingaddress"]){
			case "fatherca": case "moca":
			$html .= "Student Present Address"; break;
			case "fatheroa": case "mooa":
			$html .= $childsData["officeaddress"]; break;
			case "fatherpa": case "mopa":
			$html .= "Student Permanent Address"; break;
			case "fatherother": case "moother":
			$html .= $childsData["otheraddress"]; break;
		}
		$html .= '</td>
	</tr>
	<tr>
		<th>Telephone Number</th>
		<td>';
		$contact_number = [];
		for($con=0; $con<count($contact_number); $con++){
			if($contact_status[$con] == $childsData["relation"]){
				if($contact_category[$con] == "telephone"){
					$html .= $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
				}
			}
		}
		$html .= '</td>
	</tr>

        <tr>
		<th>Mobile Number</th>
		<td>';
		for($con=0; $con<count($contact_number); $con++){
			if($contact_status[$con] == $childsData["relation"]){
				if($contact_category[$con] == "mobile"){
					$html .= $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
				}
			}
		}
		$html .= '</td>
	</tr>
	<tr>
		<th>Email Address</th>
		<td>';
		for($con=0; $con<count($contact_number); $con++){
			if($contact_status[$con] == $childsData["relation"]){
				if($contact_category[$con] == "email"){
					$html .= $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
				}
			}
		}
		$html .= '</td>
	</tr>';
	}
	$html .= '
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
	<tr>
		<th><span style="color:#F00;">*</span> Student lives with</th>
		<td>';
		$lw= $live_with["ARRA_childs_detail_livewith"]; 
		if($lw=="LocalGuardian"){
			$html .= "Local guardian" ;
		}
		elseif($lw=="BothParents"){ 
			$html .= "Both parents" ;
		}
		else{
			$html .= $live_with["ARRA_childs_detail_livewith"];
		}
		$html .= '</td>
	</tr>';
	if($live_with["ARRA_childs_detail_livewith"] == "LocalGuardian"){ 
		$guardian_query = mysqli_query(dbConnect(), "SELECT * FROM ARRA_guardian_detail WHERE tracking_number = '".$_REQUEST["trackingid"]."'") or die(mysql_error());
		while($guardian_data = mysqli_fetch_array($guardian_query))
		{
			$html .= '<tr>
				<th width=350px><span style="color:#F00;">*</span> Guardian\'s Name </th>
				<td>'.$guardian_data["fullname"].'</td>
			</tr>
			<tr>
				<th><span style="color:#F00;">*</span> Nationality</th>
				<td>'.$guardian_data["nationality"].'</td>
			</tr>
			<tr>
				<th><span style="color:#F00;">*</span> Highest Educational Qualification</th>
				<td>'.$guardian_data["higheduqua"].'</td>
			</tr>
			<tr>
				<th><span style="color:#F00;">*</span> Relationship</th>
				<td>';
				switch($guardian_data["relationstu"]){
					case "puncle": $html .= "Uncle (Paternal)"; break;
					case "muncle": $html .= "Uncle (Maternal)"; break;
					case "paunt": $html .= "Aunt (Paternal)"; break;
					case "maunt": $html .= "Aunt (Maternal)"; break;
					case "pgrandparen": $html .= "Grandparent (Paternal)"; break;
					case "mgrandparent": $html .= "Grandparent (Maternal)"; break;
					case "brother": $html .= "Brother"; break;
					case "sister": $html .= "Sister"; break;
					case "cousin": $html .= "Cousin"; break;
					case "other": $html .= $guardian_data["relationother"]; break;
				}
				$html .= '</td>
			</tr>
			<tr>
				<th>Occupation</th>
				<td>'.$guardian_data["occupation"].'</td>
			</tr>
			<tr>
				<th>Organization</th>
				<td>'.$guardian_data["organization"].'</td>
			</tr>
			<tr>
				<th>Designation</th>
				<td>'.$guardian_data["designation"].'</td>
			</tr>
			<tr>
				<th>Office Address</th>
				<td>'.$guardian_data["officeaddress"].'</td>
			</tr>
			<tr>
				<th>Home Address</th>
				<td>'.$guardian_data["homeaddress"].'</td>
			</tr>
			<tr>
				<th>Mailing Address</th>
				<td>';
				if( $guardian_data["mailingaddress"] == "gupa" ){
					$html .= "Same as home address"; 
				}
				elseif( $guardian_data["mailingaddress"] == "guoa" ){
					$html .= "Same as office address"; 
				}
				elseif( $guardian_data["mailingaddress"] == "guother" ){
					$html .= $guardian_data["otheraddress "]; 
				}
				else{
					$html .= $guardian_data["mailingaddress "]; 
				}
				$html .= '</td>
			</tr>
			<tr>
				<th><span style="color:#F00;">*</span> Telephone Number</th>
				<td>';
				for($con=0; $con<count($contact_number); $con++){
					$gr = explode(".",$contact_status[$con]);
					if($gr[0] == "guardian"){
						if($contact_category[$con] == "telephone"){
							$html .= $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
						}
					}
				}
				$html .= '</td>
			</tr>
			<tr>
				<th><span style="color:#F00;">*</span> Mobile Number</th>
				<td>';
				for($con=0; $con<count($contact_number); $con++){
					$gr = explode(".",$contact_status[$con]);
					if($gr[0] == "guardian"){
						if($contact_category[$con] == "mobile"){
							$html .= $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
						}
					}
				}
				$html .= '</td>
			</tr>
			<tr>
				<th><span style="color:#F00;">*</span> Email Address</th>
				<td>';
				for($con=0; $con<count($contact_number); $con++){
					$gr = explode(".",$contact_status[$con]);
					if($gr[0] == "guardian"){
						if($contact_category[$con] == "email"){
							$html .= $contact_number[$con]."(".$contact_type[$con]."-".$contact_priority[$con].")<br />";
						}
					}
				}
				$html .= '</td>
			</tr>';
		}
	}
	$html .= '
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
	<tr>
		<th>Student\'s family structure and current living situation</th>
		<td>'.$live_with["ARRA_childs_detail_family_situation"].'</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<h4 style="background:#CDCDCD; text-indent:20px; padding:5px;">Section 04. Sibling\'s Information</h4>';
	// Assuming you have already established a database connection and stored it in a variable like $conn
$query = mysqli_query($conn, "SELECT * FROM ARRA_stu_siblings WHERE ARRA_stu_tracking ='".$_REQUEST["trackingid"]."'") or die(mysqli_error());

	if(mysqli_num_rows($query)>0){
	while($rows = mysqli_fetch_array($query, MYSQLI_ASSOC)){ 
	$html .= '
	<table>
		<tr>
			<th width=350px>Sibling\'s Name</th>
			<td>'.$rows["ARRA_stu_siblings_name"].'</td>
		</tr>
		<tr>
			<th>Sibling\'s Gender</th>
			<td>'.$rows["ARRA_stu_siblings_gender"].'</td>
		</tr>
		<tr>
			<th>Sibling\'s Birthday</th>
			<td>'.$rows["ARRA_stu_siblings_dob"].'</td>
		</tr>
		<tr>
			<th>Is sibling studying in AIS? </th>
			<td>'; $html .= ($rows["ARRA_stu_siblings_studing_aurora"] == "sibInAuroraYes")?"Yes":"No"; $html .= '</td>
		</tr>';
		if($rows["ARRA_stu_siblings_studing_aurora"] == "sibInAuroraYes") {
		$html .='<tr>
					<th>Tracking Number </th>
					<td>'.$rows["ARRA_stu_sib_tracking"].'</td>
				</tr>';
		}
		$html .='<tr>
			<th>Sibling\'s current school</th>
			<td>'.$rows["ARRA_stu_siblings_current_school"].'</td>
		</tr>
		<tr>
			<th>Sibling\'s intended starting date in AIS</th>
			<td>'.$rows["ARRA_stu_siblings_indate"].'</td>
		</tr>
		<tr>
			<th>Interested to enroll</th>
			<td>'; $html .= ($rows["ARRA_stu_inenroll"] == "intenYes")?"Yes":"No"; $html .= '</td>
		</tr>
	</table>'; }
	} else {
	$html .= "No siblings";
	}
	$html .= '<div>&nbsp;</div>
	<h4 style="background:#CDCDCD; text-indent:20px; padding:5px;">Section 05. Learning Profile</h4>';
	// Assuming you have already established a database connection and stored it in a variable like $conn
$query = mysqli_query($conn, "SELECT * FROM ARRA_learning_profile WHERE ARRA_learning_profile_tracking ='".$_REQUEST["trackingid"]."'") or die(mysqli_error());

	$rows = mysqli_fetch_array($query, MYSQLI_ASSOC);
	$html .= '<table>
    <tr>
        <th width="350px"><span style="color:#F00;">*</span> Child\'s Mother Language</th>
        <td>'. ($rows["ARRA_learning_profile_mother_lan"] ?? '') .'</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Language Proficiency</th>
        <td>';
if (isset($rows["ARRA_learning_profile_langspoken"]) && isset($rows["ARRA_learning_profile_langprof"])) {
    $ls = explode(",", $rows["ARRA_learning_profile_langspoken"]);
    $lp = explode(",", $rows["ARRA_learning_profile_langprof"]);
    for ($i = 0; $i < count($ls); $i++) {
        if ($ls[$i] != "") {
            $html .= $ls[$i] . " = " . $lp[$i] . "<br>";
        }
    }
} else {
    $html .= "NA";
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Evaluated by diagnostician or other specialist</th>
        <td>';
if (isset($rows["ARRA_learning_profile_specialist"]) && $rows["ARRA_learning_profile_specialist"] == "SpecialistYes") {
    $html .= $rows["ARRA_learning_profile_sp_detail"];
} else {
    $html .= "NA";
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Tested for learning, behavioral, emotional or physical disability</th>
        <td>';
if (isset($rows["ARRA_learning_profile_disability"]) && $rows["ARRA_learning_profile_disability"] == "DisabilityYes") {
    $html .= $rows["ARRA_learning_profile_ad_detail"];
} else {
    $html .= "NA";
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Received any special services</th>
        <td>';
$ls = explode(",", $rows["ARRA_learning_profile_recvss"] ?? '');
if ($ls[0] != "") {
    for ($i = 0; $i < count($ls); $i++) {
        $html .= getSpecialServices($ls[$i], $_REQUEST["trackingid"]) . "<br>";
    }
} else {
    $html .= "NA";
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Child needs additional support</th>
        <td>';
if (isset($rows["ARRA_learning_profile_ad_sprt"]) && $rows["ARRA_learning_profile_ad_sprt"] == "AdditionalYes") {
    $html .= $rows["ARRA_learning_profile_detail"];
} else {
    $html .= "NA";
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Medical information</th>
        <td>';
if (isset($rows["ARRA_learning_profile_minfo"]) && $rows["ARRA_learning_profile_minfo"] == "MedicalYes") {
    $html .= $rows["ARRA_learning_profile_minfo_detail"];
} else {
    $html .= "NA";
}
$html .= '</td>
    </tr>
</table>
	<div>&nbsp;</div>
	<h4 style="background:#CDCDCD; text-indent:20px; padding:5px;">Section 06. Supporting Documents</h4>';
	$i = 1 ;
	// Assuming you have already established a database connection and stored it in a variable like $conn
$query = mysqli_query($conn, "SELECT * FROM ARRA_documents WHERE ARRA_tracking_number ='".$_REQUEST["trackingid"]."'") or die(mysqli_error($conn));

	while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)){
		$document_list[$i] = $row["ARRA_file_type"];
		$document_link[$i++] = $row["ARRA_file_name"];
	}
	$html .= '<table>
    <tr>
        <th width="350px"><span style="color:#F00;">*</span> Student Photo</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $key = array_search("SP", $document_list);
    if ($key !== false && isset($document_link[$key])) {
        $html .= $document_link[$key];
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Student Birth Certificate</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $key = array_search("SBC", $document_list);
    if ($key !== false && isset($document_link[$key])) {
        $html .= $document_link[$key];
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th>Student Individual Passport</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $list = array_keys($document_list, "SPP");
    foreach ($list as $item) {
        if (isset($document_link[$item])) {
            $html .= $document_link[$item] . "<br />";
        }
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Parents/Guardian’s Identification Document</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $list = array_keys($document_list, "PGD");
    foreach ($list as $item) {
        if (isset($document_link[$item])) {
            $html .= $document_link[$item] . "<br />";
        }
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th>Work Permit</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $list = array_keys($document_list, "WP");
    foreach ($list as $item) {
        if (isset($document_link[$item])) {
            $html .= $document_link[$item] . "<br />";
        }
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th>Student Report Card</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $list = array_keys($document_list, "SRC");
    foreach ($list as $item) {
        if (isset($document_link[$item])) {
            $html .= $document_link[$item] . "<br />";
        }
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th><span style="color:#F00;">*</span> Immunization & Vaccination Records</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $list = array_keys($document_list, "IVR");
    foreach ($list as $item) {
        if (isset($document_link[$item])) {
            $html .= $document_link[$item] . "<br />";
        }
    }
}
$html .= '</td>
    </tr>
    <tr>
        <th>Reports / Evaluations</th>
        <td>';
if (isset($document_list) && is_array($document_list)) {
    $list = array_keys($document_list, "PD");
    foreach ($list as $item) {
        if (isset($document_link[$item])) {
            $html .= $document_link[$item] . "<br />";
        }
    }
}
$html .= '</td>
    </tr>
</table>
	<div>&nbsp;</div>
</body>
</html>';
require_once ('vendor/autoload.php');

  $mpdf = new \Mpdf\Mpdf([
            'mode' => 'win-1252',
            'format' => 'A4',
            'default_font_size' => 10,
            'margin_left' => 20,
            'margin_right' => 15,
            'margin_top' => 20,
            'margin_bottom' => 15,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
	if(isset($_REQUEST["email"]) AND $_REQUEST["email"] == 1){
		$mpdf->WriteHTML($html);
		$temp_file_name = 'email_con/ApplicationForm_'.$_SESSION["user_id"]."-".$_REQUEST["trackingid"].'.pdf';
		$mpdf->Output($temp_file_name,'F');
		
		echo "<script>window.location.href='mailto:&body=Please click the link for download the Application form http://172.16.0.99/ARRA/dashboard/pages/$temp_file_name';</script>";
	}else {
		$mpdf->WriteHTML($html);
		$mpdf->Output($_REQUEST["trackingid"],'I');
	}
}else{
require_once 'login.php';
}
?>