<?php
session_start();
if(!empty($_SESSION['user_id']) && $_SESSION['tracking_number']){
include ('function.php');
dbConnect();
if(isset($_POST['save'])){
     if(!empty($_POST['name']) && !empty($_POST['nationality']) && !empty($_POST['g2srelation']) && !empty($_POST['hiedu']) && !empty($_POST['occupation']) && !empty($_POST['organization']) && !empty($_POST['designation']) && !empty($_POST['oaddr']) && !empty($_POST['teloff']) && !empty($_POST['telhome']) && !empty($_POST['mobileoff']) && !empty($_POST['mobilehome'])  && !empty($_POST['emailoff']) && !empty($_POST['emailper']) && !empty($_POST['mailingaddr'])){
       $stuSql = "UPDATE family_details SET g_name = '".$_POST['name']."',g_nationality = '".$_POST['nationality']."',g_highest_edu = '".$_POST['hiedu']."',g_g2student_relation = '".$_POST['g2srelation']."',g_occupation = '".$_POST['occupation']."',g_organization = '".$_POST['organization']."',g_designation = '".$_POST['designation']."',g_off_address = '".$_POST['oaddr']."',g_tel_off = '".$_POST['teloff']."',g_tel_home = '".$_POST['telhome']."',g_mobile_off = '".$_POST['mobileoff']."',g_mobile_home = '".$_POST['mobilehome']."',g_email_off = '".$_POST['emailoff']."',g_email_personal = '".$_POST['emailper']."',g_mailling_address = '".$_POST['mailingaddr']."' WHERE student_tracking_number = '".$_SESSION['tracking_number']."'";
       $applyResult = mysql_query($stuSql) or die(mysql_error());
        if($applyResult){
            echo "<script type='text/javascript'>window.location='sec9.php';</script>";
        } 
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">SECTION 8 -- Guardian's Details</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Information (Tracking Number: <?php echo $_SESSION['tracking_number']; ?>)
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="POST" action="#">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>( This section is only applicable if the student does not live with parents )</label>
                                            </div>
                                        <!-- /.col-lg-12 -->
                                        </div>
                                        <hr />
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input name="name" class="form-control" placeholder="Type Here..." />
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nationality</label>
                                                <input name="nationality" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Highest Educational Qualification ( degree, institution name and year of graduation )</label>
                                                <input name="hiedu" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12">
                                            <div class="row"><div class="col-lg-12"><label>Relationship to Student</label></div></div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios1" value="puncle" >Uncle (Parental)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios2" value="paunt">Aunt (Parental)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios3" value="pgrandparen">Grandparent (Parental)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios3" value="brother">Brother
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios3" value="other">Other
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios1" value="muncle">Uncle (Maternal)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios2" value="maunt">Aunt (Maternal)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios3" value="mgrandparent">Grandparent (Maternal)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios3" value="sister">sister
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="g2srelation" id="optionsRadios3" value="cousin">Cousin
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Occupation</label>
                                                <input name="occupation" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Organization</label>
                                                <input name="organization" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input name="designation" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-12">
                                            <div class="form-group">
												<label>Office Address</label>
												<textarea name="oaddr" class="form-control" rows="3"></textarea>
											</div>
										</div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Telephone ( Office )</label>
                                                <input name="teloff" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Telephone ( Home )</label>
                                                <input name="telhome" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Mobile ( Office )</label>
                                                <input name="mobileoff" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Mobile ( Home )</label>
                                                <input name="mobilehome" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Email ( Office )</label>
                                                <input name="emailoff" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Email ( Personal )</label>
                                                <input name="emailper" class="form-control" placeholder="Type Here..."/>
                                            </div>
                                        </div>
										<div class="col-lg-12">
                                            <div class="form-group">
												<label>Mailing Address</label>
												<textarea name="mailingaddr" class="form-control" rows="3"></textarea>
											</div>
										</div>
                                        <div class="col-lg-12 text-center">
                                            <div class="form-group">
                                                <button type="submit" name="save" id="save" class="btn btn-outline btn-success">CONTINUE <i class="fa fa-hdd-o fa-1x"></i></button>
                                                <button type="submit" class="btn btn-outline btn-danger">SAVE & QUIT <i class="fa fa-database fa-1x"></i></button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
              $('#datetimepicker2').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>