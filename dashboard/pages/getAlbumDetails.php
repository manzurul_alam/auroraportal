<?php
	session_start();
	if(!empty($_SESSION['user_id'])){
		include ('function.php');
		dbConnect();
		
		$emailData = array();
		
		if ($_POST['reminder'] == 'Yes') {
			$emailQuery = "SELECT * FROM ARRA_album WHERE ARRA_album_id = '".$_POST['notice_id']."'";
			$emailResult = mysqli_query($emailQuery);
			if ($emailResult) {
				$emailRow = mysqli_fetch_array($emailResult);
				$emailData['ARRA_album_id'] = $emailRow['ARRA_album_id'];
				$emailData['ARRA_album_for'] = $emailRow['ARRA_album_for'];
				$emailData['ARRA_album_category'] = xss_undo($emailRow['ARRA_album_category']);
				$emailData['ARRA_album_name'] = xss_undo($emailRow['ARRA_album_name']);
				$emailData['ARRA_album_description'] = xss_undo($emailRow['ARRA_album_description']);
				$emailData['ARRA_album_location'] = $emailRow['ARRA_album_location'];
				$emailData['ARRA_album_publishdate'] = $emailRow['ARRA_album_publishdate'];
				$emailData['ARRA_album_createdate'] = $emailRow['ARRA_album_createdate'];
				$emailData['ARRA_album_emailnot'] = $emailRow['ARRA_album_emailnot'];
				$emailData['ARRA_album_smsnot'] = $emailRow['ARRA_album_smsnot'];
				$emailData['ARRA_album_status'] = $emailRow['ARRA_album_status'];
				$emailData['ARRA_album_draft'] = $emailRow['ARRA_album_draft'];
			}
			
			
			$pubData = array();
			
			$pubResult = mysqli_query("SELECT publish_for FROM publish WHERE type=2 and type_id = '".$_POST['notice_id']."'");
			if ($pubResult) {
				while ($row = mysqli_fetch_array($pubResult, MYSQLI_NUM)) {
					if($row[0]=='All'){
						$emailData['pub_all'] = 1;
						$ab_for_arr = explode(",",$_POST['album_for']);
		
						$allResult = mysqli_query("SELECT ARRA_applying_tracking FROM emailTo where ARRA_applying_for IN ('" . implode("','", $ab_for_arr) . "') ");
						if ($allResult) {
							while ($row4all = mysqli_fetch_array($allResult, MYSQLI_NUM)) {
								
								array_push($pubData,$row4all[0]);
							}
						}
					}else{
					$emailData['pub_all'] = 0;
					array_push($pubData,$row[0]);
					}
				}
			}
			
			$eResult = mysqli_query("SELECT tracking FROM email WHERE type=2 and type_id = '".$_POST['notice_id']."'");
			if ($eResult) {
				while ($row = mysqli_fetch_array($eResult, MYSQLI_NUM)) {
					
					if($row[0]=='All'){
						$emailData['email_all'] = 1;
						$ab_for_arr = explode(",",$_POST['album_for']);
		
						$allResult = mysqli_query("SELECT ARRA_applying_tracking FROM emailTo where ARRA_applying_for IN ('" . implode("','", $ab_for_arr) . "') ");
						if ($allResult) {
							while ($row4all = mysqli_fetch_array($allResult, MYSQLI_NUM)) {
								
								array_push($pubData,$row4all[0]);
							}
						}
					}else{
					$emailData['email_all'] = 0;
					array_push($pubData,$row[0]);
					}
				}
			}
			
			$emailData['ARRA_publish_trackings'] = implode(",",$pubData);
			
			mysqli_free_result($emailResult);
			mysqli_free_result($eResult);
			mysqli_free_result($pubResult);
		}
		
		
		header('Content-Type: application/json');
				echo json_encode($emailData);
				
				}else{
				require_once 'login.php';
	}
	?>	