

<?php

include('function.php');
dbConnect();

if (isset($_POST['alb_loc'])) {
    $alb_loc = $_POST['alb_loc'];

    // Use mysqli instead of mysql functions
    $conn = dbConnect(); // Ensure dbConnect() returns a mysqli connection

    // Use prepared statements for the first query
    $stmt = $conn->prepare("DELETE FROM ARRA_album WHERE ARRA_album_location = ?");
    $stmt->bind_param("s", $alb_loc);
    $query = $stmt->execute();
    $stmt->close();

    if ($query) {
        // Remove files and directory
        array_map('unlink', glob("$alb_loc/*.*"));
        rmdir($alb_loc);

        // Use prepared statements for the second query
        $stmt2 = $conn->prepare("DELETE FROM ARRA_album_photo WHERE ARRA_photo_location LIKE ?");
        $alb_loc_like = $alb_loc . '%';
        $stmt2->bind_param("s", $alb_loc_like);
        $stmt2->execute();
        $stmt2->close();

        $message .= "Album Successfully Removed !!!";
    }
}
// include ('function.php');
// dbConnect();
//     if (isset($_POST['alb_loc'])) {

//        $query = mysql_query("DELETE FROM ARRA_album WHERE ARRA_album_location = '".$_POST['alb_loc']."'");
// 					if($query){
// 						$location=$_POST['alb_loc'];
// 						array_map('unlink', glob("$location/*.*"));
// 						rmdir($location);
// 						$query2 = mysql_query("DELETE FROM ARRA_album_photo WHERE ARRA_photo_location LIKE '".$_POST['alb_loc']."%'");
// 						$message .= "Album Successfully Removed !!!";
// 					}

//     }

?>