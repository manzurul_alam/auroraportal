<?php 
session_start();

if(!empty($_SESSION['user_id'])){
//$_SESSION['tracking_number'] = "";
include ('function.php');
$conn = dbConnect();
//$message = 0;
$count = 0;

if(isset($_REQUEST["paymentid"])){
	$query = mysqli_query($conn, "SELECT api.*, at.ARRA_tracking_status FROM ARRA_payment_info api, ARRA_tracking at WHERE api.ARRA_tracking_number = at.ARRA_tracking_number AND api.ARRA_payment_id = '".$_REQUEST["paymentid"]."'");

	if(mysqli_num_rows($query) > 0){
		$editPaymentData = mysqli_fetch_array($query);
	}
	else{
		$message = "No Data Found";
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php //include('nav.php') ?>
        <!-- End Navigation -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Payment</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-danger" id="site_content">
                        <div class="panel-heading">
                           Payment Confirmation
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<!-- /.panel-heading -->
									<div class="panel-body">
										<!-- Tab panes -->
										<div class="tab-content voffset4">
											<div class="tab-pane fade in active" id="home-pills">
												<div class="row">
													<div class="col-lg-12">
														<?php 
														 if(isset($message)){
															echo '<div class="row voffset2">
																	<div class="col-md-6 col-md-offset-3">
																		<div class="alert alert-success alert-dismissable">
																			<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>'.$message.'
																		</div>
																	</div>
																</div>';
															 //echo $message;
															 unset($message);
														  }
														?>
														<form role="form" method="POST" action="#">
															<fieldset>
																<div class="col-md-6">
																	<label>Payment ID(System Generated)</label>
																	<input type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_id"])) echo "value=".$editPaymentData["ARRA_payment_id"]; ?> disabled /> 
																	<input name="hidPaymentId" type="hidden" <?php if(isset($editPaymentData["ARRA_payment_id"])) echo "value='".$editPaymentData["ARRA_payment_id"]."'"; else echo "value=''"; ?> /> 
																</div>
																<div class="col-md-6">
																	<label>ARRA Tracking ID</label>
																	<input name="trackingId" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_tracking_number"])) echo "value=".$editPaymentData["ARRA_tracking_number"]; ?> required tabindex=1 autofocus /> 
																</div>
																<div class="col-md-6">
																	<label>Pay For</label>
																	<input name="payFor" type="text" list="payForList" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_type"])) echo "value=".$editPaymentData["ARRA_payment_type"]; ?> required tabindex=2 />
																</div>
																<div class="col-md-6">
																	<label>Payment Date</label>
																	<input name="paymentDate" type="text" id="datetimepicker1" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_date"])) echo "value=".$editPaymentData["ARRA_payment_date"]; ?> required tabindex=3 /> 
																</div>
																<div class="col-md-6">
																	<label>Payment Amount</label>
																	<input name="paymentAmount" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_amount"])) echo "value=".$editPaymentData["ARRA_payment_amount"]; ?> required tabindex=4 /> 
																</div>
																<div class="col-md-6">
																	<label>Transaction Reference No</label>
																	<input name="trxNo" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_trx_no"])) echo "value=".$editPaymentData["ARRA_payment_trx_no"]; ?> required tabindex=5 /> 
																</div>
																<div class="col-md-6">
																	<label>Tracking Status</label>
																	<input name="trackingStatus" type="text" list="trackingStatusList" class="form-control" <?php if(isset($editPaymentData["ARRA_tracking_status"])) echo "value=".$editPaymentData["ARRA_tracking_status"]; ?> required tabindex=6 />
																</div><div class="col-md-6">
																	<label>Payment From</label>
																	<input name="payFrom" type="text" list="payFromList" class="form-control" <?php if(isset($editPaymentData["ARRA_payment_process"])) echo "value=".$editPaymentData["ARRA_payment_process"]; ?> required tabindex=7 />
																</div>
																<div class="col-md-6">
																	<label>Bank Name</label>
																	<input name="bankName" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_bank_name"])) echo "value='".$editPaymentData["ARRA_bank_name"]."'"; else echo "value=' '"; ?> tabindex=8 /> 
																</div>
																<div class="col-md-6">
																	<label>Branch Name (For School/ Bank)</label>
																	<input name="branchName" type="text" class="form-control" <?php if(isset($editPaymentData["ARRA_branch_name"])) echo "value='".$editPaymentData["ARRA_branch_name"]."'"; else echo "value=' '"; ?> tabindex=9 /> 
																</div>
																<div class="col-md-12">
																	<div>&nbsp;</div>
																	<input type="button" class="btn btn-outline btn-success" id="print_button" value="Print" />
																	<!--button type="submit" id="print_button"><i class="fa fa-print fa-1x"></i> PRINT</button>&nbsp;-->
																	<a href="#" class="btn btn-outline btn-danger" onclick="history.back();"><i class="fa fa-arrow-circle-o-left fa-1x"></i> CANCEL</a>

																</div>
																
															</fieldset>
														</form>
													</div><!-- /.col-lg-4 -->
												</div><!-- /.row --> 
												   
											</div>
											<div class="tab-pane fade" id="all-child-pills">
											   
											</div>
										</div>
									</div>
									<!-- /.panel-body -->
								</div>
								<!-- /.panel -->
                            </div>
							<!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery 
	Stop it temporarily 
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    -->
	
	<!-- Jquery Print Function -->
	<script src="jquery-1.4.3.min.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript" src="jquery.jqprint.0.3.js"></script>
	<script>
		$(function(){
			$("#print_button").click( function() {
			   
				$("#site_content").jqprint();
			});
		});
	</script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>\
	
	
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<script src="../dist/js/applyforchk.js"></script>
    
	
    <script type="text/javascript">
    function redirect()
    {
    var url = "http://www.(url).com";
    window.location(url);
    //setTimeout('window.location.href="apg.php"', 0);
    }
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>