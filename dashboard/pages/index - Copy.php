<?php
session_start();
if(!empty($_SESSION['user_id']) && $_SESSION['access_status'] != "NA"){
//$_SESSION['tracking_number'] = "";
include ('function.php');
dbConnect();

////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- FavIcon for all devices -->
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script type="text/javascript">
	window.onload = function () {
		var chart1 = new CanvasJS.Chart("chartContainer1", 
		{
		  title:{
			text: "AURORA Student Summery"              
		  },
		  data: [//array of dataSeries              
			{ //dataSeries object
			 /*** Change type "column" to "bar", "area", "line" or "pie"***/
			 type: "column",
			 dataPoints: [
			 { label: "Toddler", y: <?php echo arra_summery("toddler", '2015-01-01', '2015-12-31'); ?>,
			   click: function(e){ $('#toddlerModal').modal('show'); } },
			 { label: "Pre School", y: <?php echo arra_summery("preschool", '2015-01-01', '2015-12-31'); ?>,
			   click: function(e){ $('#preschoolModal').modal('show'); } },
			 { label: "Pre Kindergarten", y: <?php echo arra_summery("elschool", '2015-01-01', '2015-12-31'); ?>,
			   click: function(e){ $('#elschoolModal').modal('show'); } },
			 { label: "Kindergarten", y: <?php echo arra_summery("kinder", '2015-01-01', '2015-12-31'); ?>,
			   click: function(e){ $('#kinderModal').modal('show'); } }
			 ]
		   }
		   ]
		});

		chart1.render();
		
		
		var chart2 = new CanvasJS.Chart("chartContainer2",
		{
		  title:{
			text: "Toddler"
		  },
		  legend : {
			fontSize: 13,
			fontColor: "black",
		  },
		   data: [
		  {
			 type: "pie",
			 indexLabelFontColor: "black",
		   showInLegend: true,
		   dataPoints: [
		   {  y: 1717786, legendText:"Admitted" , indexLabel: "Admitted"},
		   {  y: 1176121, legendText:"Initiator" , indexLabel: "Initiator"},
		   {  y: 1727161, legendText:"Ready for Interview", indexLabel: "RFI" },
		   {  y: 4303364, legendText:"Ready for Admission" , indexLabel: "RFA"}
		   ]
		 }
		 ]
	   });

		chart2.render();
		
		var chart3 = new CanvasJS.Chart("chartContainer3",
		{
		  title:{
			text: "Pre School"
		  },
		  legend : {
			fontSize: 13,
			fontColor: "black",
		  },
		   data: [
		  {
			 type: "pie",
			 indexLabelFontColor: "black",
		   showInLegend: true,
		   dataPoints: [
		   {  y: 4181563, legendText:"Initiator", indexLabel: "Initiator" },
		   {  y: 2175498, legendText:"Admitted", indexLabel: "Admitted" },
		   {  y: 3125844, legendText:"Ready for Interview", indexLabel: "RFI" },
		   {  y: 1176121, legendText:"Ready for Admission" , indexLabel: "RFA"}
		   ]
		 }
		 ]
	   });

		chart3.render();
		
		var chart4 = new CanvasJS.Chart("chartContainer4",
		{
		  title:{
			text: "Pre Kindergarten"
		  },
		  legend : {
			fontSize: 13,
			fontColor: "black",
		  },
		   data: [
		  {
			 type: "pie",
			 indexLabelFontColor: "black",
		   showInLegend: true,
		   dataPoints: [
		   {  y: 2181, legendText:"Initiator", indexLabel: "Initiator" },
		   {  y: 218, legendText:"Admitted", indexLabel: "Admitted" },
		   {  y: 314, legendText:"Ready for Interview", indexLabel: "RFI" },
		   {  y: 1121, legendText:"Ready for Admission" , indexLabel: "RFA"}
		   ]
		 }
		 ]
	   });

		chart4.render();
		
		var chart5 = new CanvasJS.Chart("chartContainer5",
		{
		  title:{
			text: "Kindergarten"
		  },
		  legend : {
			fontSize: 13,
			fontColor: "black",
		  },
		   data: [
		  {
			 type: "pie",
			 indexLabelFontColor: "black",
		   showInLegend: true,
		   dataPoints: [
		   {  y: 545, legendText:"Initiator", indexLabel: "Initiator" },
		   {  y: 217, legendText:"Admitted", indexLabel: "Admitted" },
		   {  y: 312, legendText:"Ready for Interview", indexLabel: "RFI" },
		   {  y: 117, legendText:"Ready for Admission" , indexLabel: "RFA"}
		   ]
		 }
		 ]
	   });

		chart5.render();
		
	  }
	</script>
	<script type="text/javascript" src="jsbar/canvasjs.min.js"></script>
  
  
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->

        
        <div id="page-wrapper">
             <div class="row">
					<!-- Progress Bar 
							<div class="row voffset2" style="margin-left: 5%;">
								<?php //include('appMenu.php'); ?>
							</div>
							 End Progress Bar -->
					<div class="col-lg-12">
						<h1 class="page-header">Homepage</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
                <div class="col-lg-12">
                    <?php if($_SESSION['access'] == "SuperAdmin"){?>
					<div class="panel panel-default">
						
						<div class="panel-heading">
							 <h2><i class="fa fa-bar-chart-o fa-fw"></i>Intro</h2>
						</div>
						<div class="panel-body">
							
							Select Range
							<select name="fdate" id="fdate"/>
								<option value=2015 selected="selected">2015</option>
								<?php for($i=2014; $i<2020; $i++) {
									echo "<option value=$i>$i</option>";
								}?>
							</select> To 
							<select name="tdate" id="tdate" />
								<option value=2015 selected="selected">2015</option>
								<?php for($i=2014; $i<2020; $i++) {
									echo "<option value=$i>$i</option>";
								}?>
							</select>
							
							<div id="chartContainer1" style="width: 100%; height: 400px;"></div>
							<div style="float:left; width:100%; height:auto;">
								<div id="chartContainer2" style="height:250px; width:24%; float:left; margin:5px;"></div>
								<div id="chartContainer3" style="height:250px; width:24%; float:left; margin:5px;"></div>
								<div id="chartContainer4" style="height:250px; width:24%; float:left; margin:5px;"></div>
								<div id="chartContainer5" style="height:250px; width:24%; float:left; margin:5px;"></div>
							</div>
							
						</div>
						<?php }else{ echo "";}?>
						<!-- Modal -->
						<?php if($_SESSION['access'] == "SuperAdmin"){?>
						<?php include("ARRA_dashboard_modal.php"); ?>
						<?php draw_modal_data("Toddler", "toddler"); ?>
						<?php draw_modal_data("Pre Kindergarten", "elschool"); ?>
						<?php draw_modal_data("Kindergarten", "kinder"); ?>
						<?php draw_modal_data("Pre School", "preschool"); ?>
						
						
						<!-- /.modal -->
					</div>
					<?php }else{ echo "";}?>
				</div>
                <!-- /.col-lg-12 -->
             <!-- /.row -->
            <div class="col-md-12"><p>Please click on the appropriate icon below in order to access information about Aurora International School.</p></div>	
            <div class="row">
                <?php include('notification.php') ?>
            </div>
            
            <!-- Start introduction -->
            
            
            <!-- End introduction -->
            
            
            <!-- /.row -->
            <div class="row">
                
                <!-- /.col-lg-8 -->
                
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
	
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
			//alert("test " + $('#resultip').val());
			
			$('#dataTables-toddler').DataTable({
				responsive: true
			});
			$('#dataTables-elschool').DataTable({
				responsive: true
			});
			$('#dataTables-kinder').DataTable({
				responsive: true
			});
			$('#dataTables-preschool').DataTable({
				responsive: true
			});
			
			$("div").delegate( "#clickable", "click", function() {
				console.log(this);
				window.document.location = $(this).attr("href");
			});
        });
		
		$( "#tdate" ).change(function() {
			$.get("get_arra_summery.php?fd="+$("#fdate").val()+"-01-01&td="+$("#tdate").val()+"-12-31",
			function(data1,status){
				//$('#resultip').html(data1);
				var testData = $.parseJSON(data1);
				//alert(testData.elschool);
				var chart1 = new CanvasJS.Chart("chartContainer1", 
				{
				  title:{
					text: "AURORA Student Summery"              
				  },
				  data: [//array of dataSeries              
					{ 
					 type: "column",
					 dataPoints: [
					 { label: "Toddler", y: parseInt(testData.toddler),
					   click: function(e){ $('#toddlerModal').modal('show'); } },
					 { label: "Pre School", y: parseInt(testData.preschool),
					   click: function(e){ $('#preschoolModal').modal('show'); } },
					 { label: "Pre Kindergarten", y: parseInt(testData.elschool),
					   click: function(e){ $('#elschoolModal').modal('show'); } },
					 { label: "Kindergarten", y: parseInt(testData.kinder),
					   click: function(e){ $('#kinderModal').modal('show'); } }
					 ]
				   }
				   ]
				});
				
				chart1.render();
			});
		});
		
    </script>
	<!-- <input type=hidden id="resultip" value="7" />-->
	<!--<div id="resultip" /></div>-->
</body>

</html>
<?php
}elseif(!empty($_SESSION['user_id']) && $_SESSION['access_status'] == "NA"){
	echo "<script>window.location.href ='dashboard.php';</script>";
}else{
  require_once 'login.php';
}
?>