<?php 
session_start();

if(!empty($_SESSION['user_id'])){
//$_SESSION['tracking_number'] = "";
include ('function.php');
dbConnect();
//$message = 0;

if(!is_dir('../noticeboard')){
	mkdir('../noticeboard', 0777, true);
	mkdir('../noticeboard/all', 0777, true);
	mkdir('../noticeboard/toddler', 0777, true);
	mkdir('../noticeboard/preschool', 0777, true);
	mkdir('../noticeboard/kinder', 0777, true);
	mkdir('../noticeboard/elschool', 0777, true);
	chmod('../noticeboard/', 0777);
}

if(isset($_REQUEST["removeDoc"])){
	$query = mysql_query("DELETE FROM ARRA_notice WHERE id = '".$_REQUEST["lid"]."'");
	if($query){
		unlink("../noticeboard/".$_REQUEST["dir"]."/".$_REQUEST["removeDoc"]);
		$message = "Successfully Removed !!!";
	}
}
elseif(isset($_POST['save'])){
	if($_FILES["uploadFile"]["size"] < 1048576){
		if(isset($_POST["applicable_for"]) AND isset($_POST["subject"]) AND isset($_POST["lecture"])){
		
			$path = "../noticeboard/".$_POST["applicable_for"]."/" ; // Upload directory
			
			$uploaded_lecture =$_FILES["uploadFile"]["tmp_name"];
			$uploaded_lecture_path = $_FILES["uploadFile"]["name"];
			$uploaded_lecture_ext = pathinfo($uploaded_lecture_path, PATHINFO_EXTENSION);
			$uploaded_lecture_new =("NOTICE_".time().'.'.$uploaded_lecture_ext);
			
			//exec('ffmpeg -i '.$uploaded_lecture_path.' -f flv -s 320x240 '.'output-'.$uploaded_lecture_path.'');
			
			if(is_uploaded_file($uploaded_lecture)){
				if(move_uploaded_file($uploaded_lecture,$path.$uploaded_lecture_new)){
					
					$query = mysql_query("INSERT INTO ARRA_notice VALUES ( NULL ,'NOTICE', '".$_POST["subject"]."',  '".$_POST["lecture"]."',  '".$_POST["applicable_for"]."', '".$uploaded_lecture_new."', '/".$_POST["applicable_for"]."/','".$_POST["exdate"]."', '".$_POST["status"]."')") or die(mysql_error());
					if($query)
						$message = "Successfully Uploaded...";
					else
						$message = "File saving error!!!";
				}
				else
					$message = "Move upload file error!!!";
			}
			else
				$message = "Upload error!!!";
		}
		else{
			$message = "Mandatory field missing !!!";
		}
    }
	else{
		$message = "Keep the file size under 1MB!!!";
	}
 }elseif(isset($_REQUEST["statusid"])){
    $query = mysql_query("SELECT * FROM ARRA_dateline_status WHERE ARRA_dateline_id =  '".$_REQUEST["statusid"]."'");
    if(mysql_num_rows($query) > 0){
        $editPaymentData = mysql_fetch_array($query);
    }
    else{
        $message = "No Data Found";
    }
}
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            
            <div class="row">
				<div class="col-lg-12">
                   <h1 class="page-header">Notice Board</h1>
                </div>
                <div class="col-lg-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							List of Notice
						</div>
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataTables-payment">
									<thead>
										<tr class="text-center">
											<th width=70px>SL</th>
											<th>Title</th>
											<th>Body</th>
											<th>Applicable For</th>
											<th>File Link</th>
											<th>Thumbnail</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php $i=1;
										$query = mysql_query("SELECT * FROM ARRA_notice WHERE type = 'NOTICE' ORDER BY id DESC") or die(mysql_error());
										while($rows = mysql_fetch_array($query)){ ?>
										<tr class="odd gradeX">
											<td><?php echo $i++; ?></td>
											<td><?php echo $rows["title"]; ?></td>
											<td><?php echo $rows["body"]; ?></td>
											<td><?php echo $rows["appliedfor"]; ?></td>
											<td><?php if($rows["filename"] == "N/A"){ echo "File not included";}else{ ?><a href="<?php echo "../noticeboard/".$rows["appliedfor"]."/".$rows["filename"]; ?>" target="_blank">Click Here</a><?php } ?></td>
											<td><?php echo $rows["expireon"]; ?></td>
											<td class="text-center">
												<a href="#" title='Download'><button type='button' class='btn btn-success'><i class="fa fa-download fa-1x"></i> </button></a>
												<?php if($_SESSION['access'] != "User") { ?>
												<a href="?removeDoc=<?php echo $rows["filename"]; ?>&lid=<?php echo $rows["id"]; ?>&dir=<?php echo $rows["applicable"]; ?>" title='Delete'><button type='button' class='btn btn-danger'><i class="fa fa-trash-o fa-1x"></i> </button></a>
												<?php } ?>
											</td>
										</tr>	
											<?php
										}
										?>
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datetimepicker1').datetimepicker({
            yearOffset:0,
            lang:'en',
            timepicker:false,
            format:'Y-m-d',
            formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>\
    
    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/applyforchk.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
            $('#dataTables-payment').DataTable({
                responsive: true
            });
            
             document.getElementById("myButton").onclick = function () {
                location.href = "apg.php";
            };
                              
          });
    </script>
    
    <script type="text/javascript">
    function redirect()
    {
    var url = "http://www.(url).com";
    window.location(url);
    //setTimeout('window.location.href="apg.php"', 0);
    }
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>