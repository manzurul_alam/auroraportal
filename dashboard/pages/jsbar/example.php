<!DOCTYPE html>
<html>
<head>
<title>CanvasJS Chart jQuery Plugin</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>  
<script type="text/javascript" src="jquery.canvasjs.min.js"></script>
<script type="text/javascript">
$(function () {
//Better to construct options first and then pass it as a parameter
	var options = {
		title:{
			text: "AURORA Student Summery"              
		  },
		data: [//array of dataSeries              
			{ //dataSeries object
			 /*** Change type "column" to "bar", "area", "line" or "pie"***/
			 type: "column",
			 dataPoints: [
			 { label: "Toddler", y: 10,
			   click: function(e){ $('#toddlerModal').modal('show'); } },
			 { label: "Pre Kindergarten", y: 20,
			   click: function(e){ $('#elschoolModal').modal('show'); } },
			 { label: "Kindergarten", y: 5,
			   click: function(e){ $('#kinderModal').modal('show'); } },
			 { label: "Pre School", y: 35,
			   click: function(e){ $('#preschoolModal').modal('show'); } }
			 ]
		   }
		   ]
	};

	$("#chartContainer12").CanvasJSChart(options);


	$("#addDataPoint").click(function () {
		var chart = $("#chartContainer12").CanvasJSChart();
		var length = chart.options.data[0].dataPoints.length;
		chart.options.data[0].dataPoints.push({ x: (length + 1) * 10, y: Math.round((30 - Math.random() * 10)) });
		chart.render();
	});
});
</script>
</head>
<body>
<div id="chartContainer12" style="width:100%; height:280px;"></div>
<button id="addDataPoint" style="position:absolute;right:0px;margin-right:10px;">Add Data Point</button>
</body>
</html>