<?php
session_start();
if(!empty($_SESSION['user_id']) && $_SESSION['tracking_number']){
include ('function.php');
dbConnect();
if(isset($_POST['save'])){
    
    if(!empty($_POST['fn']) && !empty($_POST['sn']) && !empty($_POST['passport']) && !empty($_POST['coi']) && !empty($_POST['doi']) && !empty($_POST['exdate'])){
       $stuSql = "UPDATE student_details SET a_stu_fnationality = '".$_POST['fn']."',a_stu_snationality = '".$_POST['sn']."',a_stu_passportno = '".$_POST['passport']."',a_stu_cofissue = '".$_POST['coi']."',a_stu_dateissue = '".$_POST['doi']."',a_stu_expiredate = '".$_POST['exdate']."' WHERE a_stu_trackingNumber= '".$_SESSION['tracking_number']."'";
       $applyResult = mysql_query($stuSql) or die(mysql_error());
        if($applyResult){
            echo "<script type='text/javascript'>window.location='sec4.php';</script>";
        } 
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">SECTION 3 -- Passport Details</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Information: Nationalities (Tracking Number: <?php echo $_SESSION['tracking_number']; ?>)
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="#">
                                <div class="row">
                                    <div class="col-lg-12" id="passportdiv">
                                        <div class="col-lg-12 border">
                                            <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>*First Nationality</label>
                                                        <input name="fn" class="form-control" placeholder="Type Here..." />
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Passport Number</label>
                                                        <input name="passport" class="form-control" placeholder="Type Here..."/>
                                                    </div>
                                                </div>
        										<div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Country of Issue</label>
                                                        <input name="coi" class="form-control" placeholder="Type Here..."/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Date of Issue</label>
                                                        <input name="doi" type="text" class="form-control" id="datetimepicker2"  name="issuedate" value="<?php echo date('Y-m-d'); ?>" />
                                                        
                                                    </div>
                                                </div>
        										<div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Date of Expiry</label>
                                                        <input name="exdate" type="text" class="form-control" id="datetimepicker3"  name="exdate" value="<?php echo date('Y-m-d'); ?>" />
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 pull-right">
                                                    <button type="button" class="btn btn-danger btn-circle disabled pull-right marbtm rmvbtn" id="rmvbtn"><i class="glyphicon glyphicon-trash"></i></button>
                                                </div>
                                                
                                                
                                        </div>    
                                    </div>
                                    <div class="col-lg-12" id="addbtn">
                                        <div class="col-lg-12">
                                            <button type="button" class="btn btn-success" id="addbtnclick">Add New &nbsp;<i class="glyphicon glyphicon-plus"></i></button>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12 text-center">
                                        <div class="form-group">
                                            <button type="submit" name="save" id="save" class="btn btn-outline btn-success">CONTINUE <i class="fa fa-hdd-o fa-1x"></i></button>
                                            <button type="submit" class="btn btn-outline btn-danger">SAVE & QUIT <i class="fa fa-database fa-1x"></i></button>
                                        </div>
                                    </div>
                                    
                                    <!-- /.col-lg-6 (nested) -->
                                </div>
                                <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
              $('#datetimepicker2').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
	<script type="text/javascript">
        $(document).ready(function() {
              $('#datetimepicker3').datetimepicker({
        	yearOffset:0,
        	lang:'en',
        	timepicker:false,
        	format:'Y-m-d',
        	formatDate:'Y-m-d',// and tommorow is maximum date calendar
            //minDate:'-1970/01/021'
        });
                              
          });
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        var count=1;
        var id;
        $('#addbtnclick').click(function(){
            switch(count){
                case 1 : 
                     id = "Second";
                     break;
                case 2 :
                     id = "Third";
                     break;
                case 3 : 
                     id = "Fourth";
                     break;
                case 4 :
                     id = "Fifth";
                     break;
                case 5 :
                     id = "Sixth";
                     break;
                case 6 :
                     id = "Seventh";
                     break;
                case 7 :
                     id = "Eighth";
                     break;
                case 8 :
                     id = "Ninth";
                     break;
                case 9 :
                     id = "Tenth";
                     break;
            }
            var did = '<div class="col-lg-12 border" id="adddivrmvbtn'+count+'">'
                        +'<div class="col-lg-6">'
                        +'<div class="form-group">'
                        +'<label>*'+id+' Nationality</label>'
                        +'<input name="fn" class="form-control" placeholder="Type Here..." />'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-lg-6">'
                        +'<div class="form-group">'
                        +'<label>Passport Number</label>'
                        +'<input name="passport" class="form-control" placeholder="Type Here..."/>'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-lg-6">'
                        +'<div class="form-group">'
                        +'<label>Country of Issue</label>'
                        +'<input name="coi" class="form-control" placeholder="Type Here..."/>'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-lg-6">'
                        +'<div class="form-group">'
                        +'<label>Date of Issue</label>'
                        +'<input name="doi" type="text" class="form-control" id="datetimepicker2"  name="issuedate" value="<?php echo date('Y-m-d'); ?>" />'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-lg-6">'
                        +'<div class="form-group">'
                        +'<label>Expiry Date</label>'
                        +'<input name="exdate" type="text" class="form-control" id="datetimepicker3"  name="exdate" value="<?php echo date('Y-m-d'); ?>" />'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-lg-12 pull-right">'
                        +'<button type="button" class="btn btn-danger btn-circle pull-right marbtm rmvbtn" id="rmvbtn'+count+'"><i class="glyphicon glyphicon-trash"></i></button>'
                        +'</div>'
                        +'</div>';
            $('#passportdiv').append(did);
            count++;
        }); 
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("div").on("click",".rmvbtn",function(event) {
          event.preventDefault();
          var id = event.target.id;
          $("#adddiv"+id).fadeOut("slideup", function() {
                $(this).remove();
          });
          
          
        }); 
    });
    </script>
</body>

</html>
<?php
}else{
  require_once 'login.php';
}
?>