<?php
session_start();

// Include your function.php file that presumably contains dbConnect() function
include('function.php');

// Check if user is logged in
if (!empty($_SESSION['user_id'])) {
    // Establish database connection
    $conn = dbConnect();

    // Initialize variables
    $sucId = 0;
    $count = 0;
    $accessArry = [];

    // Fetch user's access area based on session user_id
    $getAccessArea = mysqli_query($conn, "SELECT aurora_user_access_area FROM users WHERE aurora_signup_username = '" . $_SESSION['user_id'] . "'") or die(mysqli_error($conn));
    
    if (mysqli_num_rows($getAccessArea) == 1) {
        $getrows = mysqli_fetch_array($getAccessArea);
        $accessArry = $getrows['aurora_user_access_area'];
    }

    // Explode access area into an array
    $explodeArry = explode(",", $accessArry);
    $pushVal = [];

    // Process access areas
    foreach ($explodeArry as $value) {
        $newAccArry = explode("-", $value);
        foreach ($newAccArry as $val) {
            if ($newAccArry[0] == $val) {
                $pushVal[] = $newAccArry[0];
            }
        }
    }

    // Retrieve user details for editing
    $getQuery = "SELECT * FROM user_alias as ua, users as u WHERE ua.email = '" . mysqli_real_escape_string($conn, $_GET['email']) . "' AND ua.user_id = u.aurora_sign_id AND u.aurora_sign_id = '" . mysqli_real_escape_string($conn, $_GET['id']) . "'";
    $result = mysqli_query($conn, $getQuery);

    if (mysqli_num_rows($result) == 1) {
        $getRow = mysqli_fetch_array($result);
        $username = $getRow['aurora_signup_username'];
        $firstName = $getRow['aurora_signup_firstname'];
        $lastName = $getRow['aurora_signup_lastname'];
        $phone = $getRow['aurora_signup_mobile'];
        $email = $getRow['aurora_signup_email'];
        $userAccess = $getRow['aurora_user_access'];
        $password = $getRow['aurora_signup_passwd'];
        $accessArea = $getRow['aurora_user_access_area'];
        $accessArray = explode(",", $accessArea);
        $alias_id = $getRow['id'];
    }

    // Handle form submission for updating user details
    if (isset($_POST['save'])) {
        // Ensure 'acss2' is set and is an array
        if (isset($_POST['acss2']) && is_array($_POST['acss2'])) {
            $access = $_POST['acss2'];
            if (count($access) > 0) {
                $arrAcc = [];

                // Construct array of access rights
                foreach ($access as $value) {
                    $read = isset($_POST[$value . 'read']) ? $_POST[$value . 'read'] : '';
                    $edit = isset($_POST[$value . 'edit']) ? $_POST[$value . 'edit'] : '';
                    $delete = isset($_POST[$value . 'delete']) ? $_POST[$value . 'delete'] : '';

                    $string = implode(',', array_filter([$read, $edit, $delete]));
                    $reExplode = explode(",", $string);

                    foreach ($reExplode as $echoVl) {
                        $arrAcc[] = $echoVl;
                    }
                }

                // Prepare access area string based on conditions
                if (isset($_POST['accessName'])) {
                    if ($_POST['accessName'] == 'aduser' && count($access) < 4) {
                        $string121 = implode(",", $arrAcc);
                    } elseif ($_POST['accessName'] == 'nouser') {
                        $string121 = 'AIP-R,AIP-E,AIP-D';
                    } else {
                        $string121 = implode(",", $arrAcc);
                    }
                } else {
                    // Handle the case where $_POST['accessName'] is not set
                    $string121 = implode(",", $arrAcc); // Default behavior if accessName is not set
                }

                // Perform database update query
                $upQuery = "UPDATE users SET aurora_signup_firstname='" . mysqli_real_escape_string($conn, $_POST['firstName']) . "', aurora_signup_lastname='" . mysqli_real_escape_string($conn, $_POST['lastName']) . "', aurora_signup_mobile='" . mysqli_real_escape_string($conn, $_POST['mobile']) . "', aurora_signup_email='" . mysqli_real_escape_string($conn, $_POST['email']) . "', aurora_user_access_area='" . mysqli_real_escape_string($conn, $string121) . "' WHERE aurora_sign_id = '" . mysqli_real_escape_string($conn, $_GET['id']) . "'";
                $upResult = mysqli_query($conn, $upQuery);

                // Handle query execution result and redirect
                if ($upResult) {
                    $sucId = 1;
                    echo '<script type="text/javascript">alert("Update Completed");</script>';
                    echo '<script type="text/javascript">window.location.href = "alluser.php";</script>';
                } else {
                    $sucId = 2;
                }
            } else {
                $sucId = 3;
                echo '<script type="text/javascript">
                    setTimeout(function() {
                        $(".alert").fadeOut("slow", function() {
                            $(this).remove();
                        });
                    }, 5000);
                </script>';
            }
        } else {
            echo '<script type="text/javascript">alert("Access field(s) are missing.");</script>';
        }
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- FavIcon for all devices -->
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->


        <div id="page-wrapper">
            <div class="row">
                <!-- Progress Bar 
                                        <div class="row voffset2" style="margin-left: 5%;">
    <?php //include('appMenu.php');  ?>
                                        </div>
                                         End Progress Bar -->
                <div class="col-lg-12">
                    <h1 class="page-header">User Management</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <?php
    if ($sucId == 1) {
        echo '<div class="row voffset2">
            <div class="col-lg-12">
               <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>Welcome! Information sucessfully updated. Thank You
               </div>
            </div>
          </div>';
    } elseif ($sucId == 2) {
        echo '<div class="row voffset2">
            <div class="col-lg-12">
               <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>Sorry!  Update is not completed.Contact with your system admin. Thank You
               </div>
            </div>
          </div>';
    }
    elseif ($sucId == 3) {
        echo '<div class="row voffset2">
            <div class="col-lg-12">
               <div class="alert alert-danger alert-dismissable" style="margin-left: 15px;margin-right: 15px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>Sorry! Empty access field(s). Please contact with your system admin. Thank You
               </div>
            </div>
          </div>';
    }
    ?>

                <div class="col-lg-12">
                    <div class="panel panel-arra">
                        <div class="panel-heading">
                            <?php include('loginname.php'); ?>
                        </div>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills">

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div class="tab-pane fade in active" id="home-pills">
                                    <div class="row text-center" id="nofication"></div>
                                    <h2 class="text-center">Information Update</h2>
                                    <div class="dataTable_wrapper voffset2">
                                        <form method="POST" action="#">
                                            <fieldset>
                                                <!-- <label>First Name</label>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="firstName" name="firstName"
                                                        type="text" value="<?php echo $firstName; ?>">
                                                </div> -->
                                                <?php
$firstName = $firstName ?? '';
?>
<label>First Name</label>
<div class="form-group">
    <input class="form-control" placeholder="firstName" name="firstName" type="text" value="<?php echo htmlspecialchars($firstName); ?>">
</div>

                                                <label>Last Name</label>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="lastName" name="lastName"
                                                        type="text" value="<?php echo $lastName; ?>">
                                                </div>
                                                <label>Mobile Number</label>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="mobile" name="mobile"
                                                        type="text" value="<?php echo $phone; ?>">
                                                </div>
                                                <label>Email</label>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="E-mail" name="email"
                                                        type="email" autofocus value="<?php echo $email; ?>">
                                                </div>
                                                <?php 
                                     if(getActiveUser($_GET['email'])=='ACTIVE' && $userAccess == 'User'){
                                        ?>
                                                <div class="form-group">
                                                    <label style="width: 100%; margin-top: 1%;">Acess</label>
                                                    <select class="form-control" name="accessName"
                                                        style="float:left; width: 100%;">
                                                        <option value="aduser">Admitted User</option>
                                                        <option value="nouser">User</option>
                                                    </select>
                                                </div>
                                                <?php
                                     }else{
                                        ?>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="No Student Found"
                                                        type="text" readonly="">
                                                </div>
                                                <?php
                                        
                                     }
                                    ?>
                                               <?php
// Ensure $accessArray is initialized
$accessArray = $accessArray ?? [];
?>
<div class="row pull-right" style="padding-right: 10%;">
    <a id="accessControlBtn">Change Access Area</a> || 
    <a id="passControlBtn" data-toggle="modal" data-target="#passModal">Password Reset</a>
    <?php 
    if ($_SESSION['alias_email'] == $_SESSION['user_id']) {
        echo ' || <a id="passControlBtn" data-toggle="modal" data-target="#aliasModal" onmouseover="$(this).css(\'cursor\',\'pointer\')">Check Alias</a>';
    } 
    ?>
</div>

<div class="row" id="accessArea" style="display: block;">
    <div class="col-lg-12">
        <div class="form-group">
            <label>Existing Access Area </label>
            <?php
            if (is_array($accessArray)) {
                foreach ($accessArray as $value) {
                    ?>
                    <div class="checkbox">
                        <label>
                            <input name="acss[]" type="checkbox" value="" checked="checked" disabled="disabled" />
                            <?php echo getAccessDetail($value); ?>
                        </label>
                    </div>
                    <?php
                }
            } else {
                echo "<p>No access areas available.</p>";
            }
            ?>
        </div>
    </div>
</div>


                                                <div class="row" id="accessUpdate" style="display: none;">
                                                    <div class="col-lg-12"><label>Access Area</label></div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <table class="table" id="dataTables-payment">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center"> Sl.</th>
                                                                        <th><input type="checkbox" id="selecctall" />
                                                                            Access Name</th>
                                                                        <th class="text-center">Read Only</th>
                                                                        <th class="text-center">Edit</th>
                                                                        <th class="text-center">Delete</th>
                                                                        <th class="text-center">Select All</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                    <?php
                                                        $i = 1;
                                                        if ($_SESSION['access'] == "SuperAdmin") {
                                                            $query = mysqli_query(dbConnect(), "SELECT * FROM ARRA_menu ORDER BY ARRA_menu_id") or die(mysqli_error());
                                                            while ($rows = mysqli_fetch_array($query)) {
                                                                ?>
                                                                    <tr>
                                                                        <td class="text-center"><?php echo $i; ?> </td>
                                                                        <td><input name="acss2[]" type="checkbox"
                                                                                class="chkselect"
                                                                                id="<?php echo $rows["ARRA_menu_short"]; ?>"
                                                                                value="<?php echo $rows["ARRA_menu_short"]; ?>" />
                                                                            <?php echo $rows["ARRA_menu_detail"]; ?>
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                name="<?php echo $rows["ARRA_menu_short"] . "read"; ?>"
                                                                                class="chkselect1"
                                                                                id="<?php echo $rows["ARRA_menu_short"] . "-ReadId"; ?>"
                                                                                type="checkbox"
                                                                                value="<?php echo $rows["ARRA_menu_short"] . "-R"; ?>"
                                                                                disabled="disabled"></td>
                                                                        <td class="text-center"><input
                                                                                name="<?php echo $rows["ARRA_menu_short"] . "edit"; ?>"
                                                                                class="chkselect1"
                                                                                id="<?php echo $rows["ARRA_menu_short"] . "-EditId"; ?>"
                                                                                type="checkbox"
                                                                                value="<?php echo $rows["ARRA_menu_short"] . "-E"; ?>"
                                                                                disabled="disabled"></td>
                                                                        <td class="text-center"><input
                                                                                name="<?php echo $rows["ARRA_menu_short"] . "delete"; ?>"
                                                                                class="chkselect1"
                                                                                id="<?php echo $rows["ARRA_menu_short"] . "-DeleteId"; ?>"
                                                                                type="checkbox"
                                                                                value="<?php echo $rows["ARRA_menu_short"] . "-D"; ?>"
                                                                                disabled="disabled"></td>
                                                                        <td class="text-center"><input name=""
                                                                                class="chkselect1" type="checkbox"
                                                                                class="select"
                                                                                id="<?php echo $rows["ARRA_menu_short"] . "-Id"; ?>"
                                                                                value="<?php echo $rows["ARRA_menu_short"] . "all"; ?>"
                                                                                disabled="disabled"></td>
                                                                    </tr>





                                                                    <?php
                                                                $i++;
                                                            }
                                                        } else {
                                                            foreach (array_unique($pushVal) as $valAs) {
                                                                if ($_SESSION['access'] == "SubAdmin" AND $valAs == "UM") {
                                                                    echo "";
                                                                } else {
                                                                    //echo $valAs;
                                                                    ?>
                                                                    <tr>
                                                                        <td class="text-center"><?php echo $i; ?> </td>
                                                                        <td>&nbsp;&nbsp;<input name="acss2[]"
                                                                                type="checkbox" class="chkselect"
                                                                                <?php if(getAccArray($valAs,$_GET['id']) == true){ echo "checked";} ?>
                                                                                id="<?php echo $valAs; ?>"
                                                                                value="<?php echo $valAs; ?>" />
                                                                            <?php echo getAccessDetail($valAs); ?></td>
                                                                        <td class="text-center"><input
                                                                                name="<?php echo $valAs . "read"; ?>"
                                                                                class="chkselect1"
                                                                                <?php if(getInArray($valAs."-R",$_GET['id']) == true){ echo "checked";}else{ echo "disabled"; } ?>
                                                                                id="<?php echo $valAs . "-ReadId"; ?>"
                                                                                type="checkbox"
                                                                                value="<?php echo $valAs . "-R"; ?>">
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                name="<?php echo $valAs . "edit"; ?>"
                                                                                class="chkselect1"
                                                                                <?php if(getInArray($valAs."-E",$_GET['id']) == true){ echo "checked";}else{ echo "disabled"; }  ?>
                                                                                id="<?php echo $valAs . "-EditId"; ?>"
                                                                                type="checkbox"
                                                                                value="<?php echo $valAs . "-E"; ?>">
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                name="<?php echo $valAs . "delete"; ?>"
                                                                                class="chkselect1"
                                                                                <?php if(getInArray($valAs."-D",$_GET['id']) == true){ echo "checked";}else{ echo "disabled"; } ?>
                                                                                id="<?php echo $valAs . "-DeleteId"; ?>"
                                                                                type="checkbox"
                                                                                value="<?php echo $valAs . "-D"; ?>">
                                                                        </td>
                                                                        <td class="text-center"><input name=""
                                                                                class="chkselect1" type="checkbox"
                                                                                class="select"
                                                                                id="<?php echo $valAs . "-Id"; ?>"
                                                                                value="<?php echo $valAs . "all"; ?>"
                                                                                disabled="disabled"></td>
                                                                    </tr>
                                                                    <?php
                                                                $i++;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Change this to a button or input when using this as a form -->
                                                <div class="col-lg-12 text-center">
                                                    <div class="form-group">
                                                        <button type="submit" name="save" id="save"
                                                            class="btn btn-new">UPDATE</button>
                                                        <!-- <button type="submit" class="btn btn-danger"
                                                            onclick="history.go( - 1);"><i
                                                                class="fa fa-arrow-circle-o-left fa-1x"></i>
                                                            CANCEL</button> -->
                                                            <button type="submit" class="btn btn-danger" onclick="history.go(-1);">
    <i class="fa fa-arrow-circle-o-left fa-1x"></i>
    CANCEL
</button>

                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>

                </div>

            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="passModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Password Reset</h4>
                </div>
                <div class="modal-body">
                    <form id="pass-reset-form" method="POST" action="#" onsubmit="return ResetPassValidate();">

                        <label>New Password</label>
                        <div class="form-group">
                            <input class="form-control" placeholder="New Password" name="newpass" type="password"
                                id="rePass" />
                        </div>
                        <label>Confirm Password</label>
                        <div class="form-group">
                            <input class="form-control" placeholder="Confirm Password" name="mobile" type="password"
                                id="conRePass" onkeyup="resetPass(); return false;" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="passsubmit" name="passreset" class="btn btn-default" data-dismiss="modal"
                        value="Password Reset" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="aliasModal" tabindex="-1" role="dialog" aria-labelledby="passModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#156059;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="color:#FFFFFF;">User Alias</h4>
                </div>
                <div class="modal-body">

                    <?php
    $i = 1;
    $queryUser = mysqli_query(dbConnect(), "SELECT * FROM user_alias WHERE email <> '" . $_GET['email'] . "' AND user_id = '" . $_GET['id'] . "'") or die(mysqli_errno());
    if (mysqli_num_rows($queryUser)) {
        $resRows = mysqli_fetch_array($queryUser);
        $name = explode(' ', $resRows['name']);
        ?>
                    <div class="row">
                        <form method="POST" action="#">
                            <input type="hidden" id="al_id" value="<?php echo $resRows['id']; ?>">
                            <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">First Name</label><input
                                    class="form-control" style="float:left; width: 94%; margin-right: 2%;"
                                    placeholder="First Name" id="alfname" name="fname" type="text"
                                    value="<?php echo $name[0]; ?>" required></div>
                            <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Last Name</label><input
                                    class="form-control" style="float:left; width: 94%; margin-right: 2%;"
                                    placeholder="Last Name" id="allname" name="lname" type="text"
                                    value="<?php echo $name[1]; ?>" required></div>
                            <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Email</label><input
                                    class="form-control" style="float:left; width: 90%; margin-right: 2%;"
                                    placeholder="E-mail" id="alemail" name="alias" type="email"
                                    value="<?php echo $resRows['email']; ?>" required></div>
                            <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Mobile</label><input
                                    class="form-control" style="float:left; width: 94%; margin-right: 2%;"
                                    placeholder="Mobile Number" id="almobile" name="almobile" type="text"
                                    value="<?php echo $resRows['mobilenumber']; ?>" required></div>
                            <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Password</label><input
                                    class="form-control" style="float:left; width: 94%; margin-right: 2%;"
                                    placeholder="Password" id="alpassword" name="passalias" type="password" value=""
                                    required></div>
                            <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Notification</label><input
                                    style="float:left;" name="alnotification" id="alnotification" type="checkbox"></div>
                        </form>
                    </div>
                    <?php
                            } else {
                                $i++;
                                echo "No record found";
                            }
                            ?>



                </div>


                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-new dis"  data-value = "<?php echo $total; ?>" onclick="addDiv($(this).data('value'))">Add <?php echo 3 - $total; ?> More Alias</button> -->
                    <!-- <input type="submit" id="passsubmit" name="passreset" class="btn btn-new" data-dismiss="modal" value="Password Reset"/> -->
                    <!-- <input type="button" style="display:none;"  id="aliassubmit" name="aliassubmit" class="btn btn-new" value="Save"/> -->
                    <!-- <button type="button" class="btn btn-new" id="aliassubmit" name="aliassubmit">Save</button> -->
                    <?php if ($i > 1) {
                                ?>
                    <button type="button" class="btn btn-new" data-dismiss="modal" onCLick='callmodal("#addNewAllias")'
                        id="addalias" name="addalias">Add New</button>
                    <?php
    } else {
        ?>
                    <button type="button" class="btn btn-new" id="alupdate" name="alupdate">Update</button>
                    <?php }
    ?>

                    <button type="button" class="btn btn-new" data-dismiss="modal" id="closeTrigger">Close</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="addNewAllias" tabindex="-1" role="dialog" aria-labelledby="addNewCategoryLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#156059; color:#FFFFFF;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="addNewCategoryLabel">Add New Alias</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12" id="appDiv">
                                <div class="col-lg-12">
                                    <input type="hidden" id="al_idnew" value="<?php echo $_GET['id']; ?>">
                                    <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">First
                                            Name</label><input class="form-control"
                                            style="float:left; width: 94%; margin-right: 2%;" placeholder="First Name"
                                            id="fnamenew" name="fnamenew" type="text" value="" required></div>
                                    <div class="col-lg-4"><label style="width: 100%; margin-top: 1%;">Last
                                            Name</label><input class="form-control"
                                            style="float:left; width: 94%; margin-right: 2%;" placeholder="Last Name"
                                            id="lnamenew" name="lnamenew" type="text" value="" required></div>
                                    <div class="col-lg-4"><label
                                            style="width: 100%; margin-top: 1%;">Email</label><input
                                            class="form-control" style="float:left; width: 90%; margin-right: 2%;"
                                            placeholder="E-mail" id="aliasnew" name="aliasnew" type="email" value=""
                                            required></div>
                                    <div class="col-lg-4"><label
                                            style="width: 100%; margin-top: 1%;">Moile</label><input
                                            class="form-control" style="float:left; width: 94%; margin-right: 2%;"
                                            placeholder="Mobile Number" id="almobilenew" name="almobilenew" type="text"
                                            value="" required></div>
                                    <div class="col-lg-4"><label
                                            style="width: 100%; margin-top: 1%;">Password</label><input
                                            class="form-control" style="float:left; width: 94%; margin-right: 2%;"
                                            placeholder="Password" id="passaliasnew" name="passaliasnew" type="password"
                                            value="" required></div>
                                    <div class="col-lg-4"><label
                                            style="width: 100%; margin-top: 1%;">Notification</label><input
                                            style="float:left;" id="alnotificationnew" name="notificationnew"
                                            type="checkbox" value="1"></div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-new" id="alAdd" name="alAdd">Add</button>
                    <button type="button" class="btn btn-default catcancel btn-width-cancel"
                        data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#selecctall').click(function(event) {
            if (this.checked) {
                $('.chkselect').each(function() {
                    this.checked = true;
                });
                $('.chkselect1').each(function() {
                    this.disabled = false;
                    this.checked = true;
                });
            } else {
                $('.chkselect').each(function() {
                    this.checked = false;
                });
                $('.chkselect1').each(function() {
                    this.checked = false;
                    this.disabled = true;
                });
            }
        });

        $("input").click(function(event) {
            var id = event.target.id;
            if ($(this).is(':checked')) {
                if (id.endsWith("-Id")) {
                    id = id.replace('-Id', '');
                    $('#' + id + '-ReadId').prop('checked', true);
                    $('#' + id + '-EditId').prop('checked', true);
                    $('#' + id + '-DeleteId').prop('checked', true);
                } else {
                    $('#' + id + '-ReadId').prop('disabled', false);
                    $('#' + id + '-EditId').prop('disabled', false);
                    $('#' + id + '-DeleteId').prop('disabled', false);
                    $('#' + id + '-Id').prop('disabled', false);
                }
            } else {
                if (id.endsWith("-Id")) {
                    id = id.replace('-Id', '');
                    $('#' + id + '-ReadId').prop('checked', false);
                    $('#' + id + '-EditId').prop('checked', false);
                    $('#' + id + '-DeleteId').prop('checked', false);
                } else {
                    $('#' + id + '-ReadId').prop('disabled', true);
                    $('#' + id + '-EditId').prop('disabled', true);
                    $('#' + id + '-DeleteId').prop('disabled', true);
                    $('#' + id + '-Id').prop('disabled', true);
                }
            }
        });

        $("#accessControlBtn, #passControlBtn").hover(function() {
            $(this).css('cursor', 'pointer');
        }, function() {
            $(this).css('cursor', 'auto');
        });

        $("#accessControlBtn").click(function() {
            $("#accessArea").toggle("slow");
            $("#accessUpdate").toggle("slow");
        });

        $('#passsubmit').on('click', function(e) {
            e.preventDefault();
            var alias_id = <?php echo $alias_id; ?>;
            var id = <?php echo $_GET['id']; ?>;
            var data = {
                aliasID: alias_id,
                id: id,
                newpass: $('#rePass').val()
            };

            $.ajax({
                url: "passreset.php",
                type: "POST",
                data: data,
                success: function(data) {
                    var html =
                        '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Successfully reset</div>';
                    $("#nofication").html(html);
                }
            });
        });

        $('#alupdate').on('click', function(e) {
            e.preventDefault();
            var id = $('#al_id').val();
            var fname = $('#alfname').val();
            var lname = $('#allname').val();
            var email = $('#alemail').val();
            var mobile = $('#almobile').val();
            var password = $('#alpassword').val();
            var notification = $('#alnotification').is(":checked") ? '1' : '0';

            $.ajax({
                url: "alupdate.php",
                type: "POST",
                dataType: "json",
                data: {
                    id: id,
                    fname: fname,
                    lname: lname,
                    email: email,
                    mobile: mobile,
                    password: password,
                    notification: notification
                },
                beforeSend: function() {
                    console.log(id + ',' + fname + ',' + lname + ',' + email + ',' +
                        mobile + ',' + password + ',' + notification);
                },
                success: function(edata, txtSta) {
                    window.location.reload();
                },
                error: function(jqXHR, txtSta, errorThrown) {
                    console.log(jqXHR);
                    console.log(txtSta);
                    console.log(errorThrown);
                }
            });
        });

        $('#alAdd').on('click', function(e) {
            e.preventDefault();
            var id = $('#al_idnew').val();
            var fname = $('#fnamenew').val();
            var lname = $('#lnamenew').val();
            var email = $('#aliasnew').val();
            var mobile = $('#almobilenew').val();
            var password = $('#passaliasnew').val();
            var notification = $('#alnotificationnew').is(":checked") ? '1' : '0';

            $.ajax({
                url: "aladd.php",
                type: "POST",
                dataType: "json",
                data: {
                    id: id,
                    fname: fname,
                    lname: lname,
                    email: email,
                    mobile: mobile,
                    password: password,
                    notification: notification
                },
                beforeSend: function() {
                    console.log(id + ',' + fname + ',' + lname + ',' + email + ',' +
                        mobile + ',' + password + ',' + notification);
                },
                success: function(edata, txtSta) {
                    window.location.reload();
                },
                error: function(jqXHR, txtSta, errorThrown) {
                    console.log(jqXHR);
                    console.log(txtSta);
                    console.log(errorThrown);
                }
            });
        });

        $('#addalias').on('click', function(e) {
            $('#aliasModal').modal('hide');
        });
    });

    function resetPass() {
        var pass1 = document.getElementById('rePass');
        var pass2 = document.getElementById('conRePass');
        var message = document.getElementById('confirmMessage');
        var submit = document.getElementById('send');
        var goodColor = "#66cc66";
        var badColor = "#ff4444";

        if (pass1.value == pass2.value) {
            pass2.style.backgroundColor = goodColor;
            message.style.color = goodColor;
        } else {
            pass2.style.backgroundColor = badColor;
            message.style.color = badColor;
        }
    }

    function ResetPassValidate() {
        var pass1 = $('#rePass').val();
        var pass2 = $('#conRePass').val();
        if (pass1 == pass2) {
            return true;
        } else {
            alert('Passwords did not match!');
            return false;
        }
    }

    function callmodal(modalid) {
        $(modalid).modal("show");
    }
    </script>
</body>

</html>
<?php
} else {
    require_once 'login.php';
}
?>