<?php 
session_start();
// echo $_SESSION['studentName'];
if (isset($_SESSION['studentName'])) {
    echo $_SESSION['studentName'];
}
if(!empty($_SESSION['user_id'])){
//$_SESSION['tracking_number'] = "";
include ('function.php');
dbConnect();
//$message = 0;
// if(isset($_REQUEST["clickagreed"])){
	// $query = mysqli_query(dbConnect(),"UPDATE ARRA_tracking SET ARRA_tracking_status = 'COMPLETE' WHERE ARRA_tracking_number = '".$_REQUEST["continue"]."'") ;
	// if($query){
		// $message = "Thank you for submitting your application. After verifying and short listed your form, you'll be eligible for interview.";
	// }
// }
$message = "Line 15";

if(isset($_POST['continue'])){
	$query = mysqli_query(dbConnect(),"UPDATE ARRA_tracking SET ARRA_tracking_status = 'COMPLETE',
																ARRA_tracking_appliedBy ='".$_POST['applyName']."',
																ARRA_tracking_appliedDate = '".$_POST['applyDate']."' 
																WHERE ARRA_tracking_number = '".$_POST['tId']."'") ;
	if($query){
		$message = "ok";
		$aB = $_POST['applyName'];
		$aD = $_POST['applyDate'];
		$tId = $_POST['tId'];
		if(isset($_SESSION['tracking_number']) && formSubmitMail($_SESSION['user_id'], $_SESSION['tracking_number'])){
				if($_SESSION['tracking_number'] == true){
					unset($_SESSION['tracking_number']);
					unset($_SESSION['dob']);
					unset($_SESSION['studentName']);
				}
			}
		
		
	}else{
		$message = "wrong";
	}
}

if(isset($_REQUEST["saveandexit"])){
	$query = mysqli_query(dbConnect(),"UPDATE ARRA_tracking SET ARRA_tracking_status = 'PENDING' WHERE ARRA_tracking_number = '".$_REQUEST["saveandexit"]."'") ;
	if($query){
		echo "<script type='text/javascript'>window.location='dashboard.php';</script>";
	}
}

if(isset($_REQUEST["cancel"])){
	$query = mysqli_query(dbConnect(),"UPDATE ARRA_tracking SET ARRA_tracking_status = 'INCOMPLETE' WHERE ARRA_tracking_number = '".$_REQUEST["cancel"]."'") ;
	if($query){
		echo "<script type='text/javascript'>window.location='dashboard.php';</script>";
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>..::AIS::..</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="../favico/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include('nav.php') ?>
        <!-- End Navigation -->
        <div id="page-wrapper">
            
			<div class="row">
                <div class="col-lg-12">
                    <h4><br></h4>
                </div>
                
            </div>
            
			<!--<div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Terms and Conditions</h3>
                </div>
                
            </div>
            -->
			<div class="row">
        <?php if($message == "ok"){
				?>
				<div class="col-lg-12" id="viewsucess">
					<div class="row text-center">
						<div class="col-md-6 col-md-offset-3">
							<div class="alert alert-success">
								<h3>Thank you for applying to <br/> Aurora International School.</h3>
								<p>Your Tracking Number: <?php if(isset($tId)) echo $tId;  else echo "" ;?></p>
								<p>Applied By: <?php if(isset($aB)) echo $aB; else echo "" ;?></p>
								<p>Applied Date: <?php if(isset($aD)) echo $aD; else echo "" ;?></p>
								<p> <strong>Please click on the download button for the Payment Instruction:</strong> <br/>
								<a href="payments_instruction_pdf.php?trackingid=<?php echo $tId; ?>" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-download fa-1x"></i> &nbsp; Download</button></a></p>
								<p>Please print the payment instruction and bring it to the campus to pay the Application Processing Fee.</p><br /><br />
								<button type="button" onclick="window.location.href='dashboard.php'; " class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-ok"></i> Finish</button>
							</div>
						</div>
					</div>
				</div>
				<?php				
				}else{
					?>
						<div class="col-lg-12" id="viewterms">
							<div class="panel panel-green">
								<div class="panel-body">							
									<p class="text-center"><img src="../images/auroralogo.png"><br><br></p>
									<p class="text-center"><strong>TERMS AND CONDITIONS</strong></p>
									<p class="text-justify">I certify that all the information given  in this application form is true, correct and accurate. I understand that  failure to provide complete and accurate information is grounds for a  re-evaluation of the application and review of a student&rsquo;s continued enrollment  at Aurora International School. I have read and understood Aurora International  School&rsquo;s &ldquo;Fee Policies&rdquo; and &ldquo;Admission Policies and Procedures&rdquo; documents,  which were provided as additional documents to this application form.</p>
									<p>I understand that  Aurora International School reserves the right to refuse admission to any  applicant and I agree to abide by the decisions of the Admissions Committee.</p>
									<!-- Start Input Feild for submitted by and date -->
									<hr/>
						<form action= "" method="POST" >
									<div class="col-lg-12 tooltip-demo">
										<label>Please type your full name in acknowledgement of these terms and conditions.</label>
										<div class="col-lg-6 voffset2">
											<div class="form-group">
											<label>Applied By</label>
											<input name="tId" id="tId" type="hidden" value="<?php echo isset($_SESSION['tracking_number']) ? $_SESSION['tracking_number'] : ''; ?>" />
											<input name="applyName" id="applyName" class="form-control" required data-toggle="tooltip" data-placement="top" title="Type your name" />
										</div>
										</div>
										<div class="col-lg-6 voffset2">
											<div class="form-group">
												<label>Date</label>
												<input  name="applyDate" id="datetimepicker2" class="form-control" value="<?php echo date("Y-m-d"); ?>" data-toggle="tooltip" data-placement="top" title="Current date" value="<?php echo date('Y-m-d'); ?>" />
											</div>
										</div>
									</div>
									
									<!-- End Input Feild for submitted by and date -->
								</div>
							</div>
							
						</div>
						<div class="col-lg-12 text-center" id="viewbuttons">
							<button  type="submit" id="clickagreed"  name="continue" type="button" class="btn btn-outline btn-footer"><i class="glyphicon glyphicon-ok"></i>AGREE & SUBMIT</button>
							<button type="button" onClick="window.location.href='terms.php?saveandexit=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-footer">DISAGREE & EXIT<i class="fa fa-home fa-1x"></i></button>
							<button type="button" onClick="window.location.href='terms.php?cancel=<?php echo $_REQUEST["trackingid"]; ?>'" class="btn btn-outline btn-danger"><i class="fa fa-arrow-circle-o-left fa-1x"></i>CANCEL</button>
							<br><br><br><br>
						</div>
						</form>
				<?php
				} ?>
				
				
			</div>
		</div>
	</div>
	<!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
	
	<!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../bower_components/bootstrap/dist/js/jquery.datetimepicker.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
		<script type="text/javascript">
		/*
		$(document).ready(function() {
			$("#viewsucess").hide();
				$("#clickagreed" ).click(function() {
				$("#viewsucess").show();
				$("#viewterms").hide();
				$("#viewbuttons").hide();
				$.get("terms.php",{'func':'1'},function(data){ //alert(data);
				});
				
				
				
				
				
				
				var name = $("#applyName").val();
				//console.log(name);
				var date = $("#datetimepicker2").val();
				//console.log(date);
				var tId = $("#tId").val();
				//console.log(tId);
				//alert(name +","+ date +","+ tId);
				
				//Ajax Use for bad coding style.
				$.ajax({
					url: 'ajaxupdate.php',
					type: 'POST',
					data: {name: name, date:date, tId:tId }
				});
				
			});
		});*/
    </script>
	<script type="text/javascript">
		// $(document).ready(function(){
			// $('#clickagreed').attr('disabled',true);
			// $('#applyName').keyup(function(){
				// if($(this).val().length !=0)
					// $('#clickagreed').attr('disabled', false);            
				// else
					// $('#clickagreed').attr('disabled',true);
			// })
		// });
	</script>
	
	<script type="text/javascript">
		$('#datetimepicker2').datetimepicker({
			yearOffset:0,
			lang:'en',
			timepicker:false,
			format:'Y-m-d',
            formatDate:'Y-m-d',// and tommorow is maximum date calendar
			minDate:'-1970/01/01', // yesterday is minimum date
			maxDate:'+1970/01/01' // and tommorow is maximum date calendar
		});
    </script>
	
	<script>
                // tooltip demo
                $('.tooltip-demo').tooltip({
                    selector: "[data-toggle=tooltip]",
                    container: "body"
				})
				
                // popover demo
                $("[data-toggle=popover]")
                .popover()
			</script>
</body>
</html>
<?php
}else{
  require_once 'login.php';
}
?>